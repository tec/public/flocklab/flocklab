/**
 * @file
 *
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * @author Roman Lim <lim@tik.ee.ethz.ch>
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 *
 *
 * @section LICENSE
 *
 * Copyright (c) 2010 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 *
 *
 */

/**
 * @section DESCRIPTION
 *
 * Use this module to configure and read out channels from AD7708/AD7718 devices
 * The module generates a /dev/ad77x8 file which can be configured with IOCTL's
 * and read to obtain measurement values.
 * 
 */

/* ---- Include Files ---------------------------------------------------- */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/spi/spi.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>
#include <linux/kfifo.h>
#include <linux/sched.h>
//DEBUG #include <linux/gpio.h>						/* GPIO setting and reading */
#include "ad77x8.h"

#define DEV_NAME "ad77x8"
#define DEV_NAME_LONG "AD7708/AD7718 ADC"


/* ---- Module documentation and authoring information --------------------*/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("Driver for ADC " DEV_NAME_LONG);
MODULE_SUPPORTED_DEVICE(DEV_NAME);
MODULE_VERSION("1.22");

/* ---- Function declarations ---------------------------------------------*/
static int ad77x8_probe(struct spi_device*);
static int __devexit ad77x8_remove(struct spi_device*);
static irqreturn_t ad77x8_interrupt (int, void*);
static int ad77x8_dev_open(struct inode*, struct file*);
static int ad77x8_dev_release(struct inode*, struct file*);
static ssize_t ad77x8_dev_read(struct file*, char*, size_t, loff_t*);
static int ad77x8_dev_ioctl(struct inode*, struct file*, unsigned int, unsigned long);


/* ---- Global variables --------------------------------------------------*/
/**
 * Struct defining the ADC on the SPI bus.
 */
static struct spi_driver ad77x8_driver = {
	.driver = {
		.name	= "ad77x8",
		.owner	= THIS_MODULE,
	},
	.probe		= ad77x8_probe,
	.remove		= __devexit_p(ad77x8_remove),
/*	.suspend	= ad77x8_suspend,
	.resume		= ad77x8_resume, */
};

/**
 * ADC configuration with field length. Each entry of the struct represents a register
 * on the ADC. It can be set by combining the configuration options defined in ad77x8.h
 */
struct ad77x8_adc_config {
	unsigned int nchop:		1;
	unsigned int negbuf:	1;
	unsigned int chcon:		1;
	unsigned int oscpd:		1;
	unsigned int mode:		3;
	unsigned int channel:	4;
	unsigned int polar:		1;
	unsigned int range:		3;
	unsigned int sf:		8;
	unsigned int : 			0;		// align to a word boundary
};

/**
 * Struct for AD77x8 driver
 */
struct ad77x8_drv {
	struct spi_device *spi;
	struct mutex lock;		///< used to lock this struct when updating struct entries.
	struct mutex lock_adc;	///< used to lock the adc for exclusive access. each reader locks the device while it is reading.
	int resolution; 		///< resolution in number of bytes per sample
	struct ad77x8_adc_config adc_config;
	wait_queue_head_t waitq;
	int data_ready;
	int calibrated;
	struct ad77x8_dev *dev;
	struct kfifo out_fifo;
	spinlock_t outfifo_lock;
	struct ad77x8_spi *spi_priv;
};

/**
 * Struct holding all values needed for the dev maintenance
 */
struct ad77x8_dev {
	char *devname;								///< Name of the device
	dev_t dev_nr;								///< Device number to which device is registered to
	struct cdev dev_cdev;
	struct class *dev_class;
	int device_open_count;						///< Used to prevent opening the device multiple times
	struct file_operations fops_dev;
	wait_queue_head_t devreaders;				///< Waitqueue for blocking I/O to dev readers
	struct ad77x8_drv *drv;
};
static struct ad77x8_dev ad77x8dev;

/**
 * Struct for spi_transfers
 */
struct ad77x8_spi {
	u8 tx_buf;
	struct kfifo msg_fifo;
	spinlock_t msgfifo_lock;
};
static struct ad77x8_spi ad77x8spi;

// Buffer size of the circular buffer:
#define BUF_LEN		16384	// Has to be a power of 2


/* ---- More function declarations ----------------------------------------*/
static void _spi_message_completion(struct ad77x8_drv*);


/* ---- Helper functions --------------------------------------------------*/
/**************************************************************************************//**
 * Produce bitmask for mode register
 *
 * @param adc_config Pointer to the struct holding the configuration data
 *
 * @return 	Return the bitmask (=value) of all configuration parameters shifted
 * 			and added together. This can directly be written to the ADC register
  *****************************************************************************************/
static inline unsigned int _bitmask_mode_reg(struct ad77x8_adc_config *adc_config) {
	return
			adc_config->nchop 		<< 7 |
			adc_config->negbuf 		<< 6 |
			0				 		<< 5 |		///< REFSEL is hardcoded to be on REFIN1
			adc_config->chcon 		<< 4 |
			adc_config->oscpd 		<< 3 |
			adc_config->mode;
}

/**************************************************************************************//**
 * Produce bitmask for ADC configuration register (adccon)
 *
 * @param adc_config Pointer to the struct holding the configuration data
 *
 * @return 	Return the bitmask (=value) of all configuration parameters shifted
 * 			and added together. This can directly be written to the ADC register
  *****************************************************************************************/
static inline unsigned int _bitmask_adccon_reg(struct ad77x8_adc_config *adc_config) {
	return
			adc_config->channel		<< 4 |
			adc_config->polar 		<< 3 |
			adc_config->range;
}

/**************************************************************************************//**
 * Read a register from the ADC
 *
 * @param spi Pointer to the ADC driver
 * @param addr Address to read
 *
 * @return Read value on success, negative error number otherwise.
  *****************************************************************************************/
static inline int _read_reg(struct spi_device* spi, u8 addr)
{
	int ret;
	u8 tx = COMM_REG_READ | addr;
	ret = spi_w8r8(spi, tx);
#ifdef DEBUG
	if (ret < 0) {
		dev_dbg(&spi->dev, ">< error %u\n", ret);
	} else {
		dev_dbg(&spi->dev, "> %02x\n", tx);
		dev_dbg(&spi->dev, "< %02x\n", ret & 0xff);
	}
#endif
	return ret;
}


/**************************************************************************************//**
 * Read a data register from the ADC
 *
 * @param spi Pointer to the ADC driver
 * @param data Buffer into which read data will be placed
 * @param len Size of buffer in bytes
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
static inline int _read_data_reg(struct spi_device *spi, u8 *data, int len)
{
	int ret;
	u8 tx = (COMM_REG_READ | AD77x8_ADC_DATA_REG);
	ret = spi_write_then_read(spi, &tx, 1, data, len);
#ifdef DEBUG
	if (ret != 0) {
		dev_dbg(&spi->dev, ">< error %u\n", ret);
	} else {
		dev_dbg(&spi->dev, "> %02x\n", tx);
		if (len == AD77x8_RES_16)
			dev_dbg(&spi->dev, "< %02x %02x\n", data[0], data[1]);
		else if (len == AD77x8_RES_24)
			dev_dbg(&spi->dev, "< %02x %02x %02x\n", data[0], data[1], data[2]);
		else
			dev_dbg(&spi->dev, "< unknown size\n");
	}
#endif
	return ret;
}


/**************************************************************************************//**
 * Write to register
 *
 * @param spi Pointer to the ADC driver
 * @param addr Address to write to
 * @param val Value to write
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
static inline int _write_reg(struct spi_device* spi, u8 addr, u8 val)
{
	int ret;
	u8 tx[2] = { addr, val };
	ret = spi_write(spi, tx, 2);
#ifdef DEBUG
	if (ret != 0)
		dev_dbg(&spi->dev, ">< error %u\n", ret);
	else
		dev_dbg(&spi->dev, "> %02x %02x\n", tx[0], tx[1]);
#endif
	return ret;
}


/**************************************************************************************//**
 * Swap bytes because of endianess (LE on pxa270)
 *
 * @param[in] resolution Resolution of the ADC.
 * @param[in] adc_data	Pointer to the array holding the adc data
 * @param[out] value Pointer to where the data should be copied.
 *
 ******************************************************************************************/
static inline void _swap_bytes(int resolution, u8 *adc_data, u32 *value)
{
	memset(value, 0, 4);
	if (resolution == AD77x8_RES_16) {
		// 2 byte swapping
    	*value = ((u32) adc_data[0]) << 8 | adc_data[1];
	} else {
		// 3 byte swapping
		*value = ((u32) adc_data[0]) << 16 | ((u32) adc_data[1]) << 8 | adc_data[2];
	}
}


/**************************************************************************************//**
 * Function called upon completion of a spi transfer
 *
 ******************************************************************************************/
static void _spi_message_completion(struct ad77x8_drv *drv)
{
	/* Local variables */
	struct ad77x8_spi *spi_priv = drv->spi_priv;
	struct spi_message *msg;
	struct spi_transfer *xfer;
	u32 value;
	//DEBUG int gpio;

	//DEBUG printk(KERN_ERR "spi_msg_comp entered\n");
	// Get the message from the fifo:
	kfifo_out_locked(&spi_priv->msg_fifo, (unsigned char*) &msg, sizeof(struct spi_message *), &spi_priv->msgfifo_lock);
	xfer = list_entry(msg->transfers.prev, struct spi_transfer, transfer_list);
	//DEBUG printk(KERN_ERR "spi_msg_comp: Message fifo length: %d\n", kfifo_len(spi_priv->msg_fifo));
	// Get the data from the message:
	if (msg->status == 0) {
		// Swap the bytes:
		_swap_bytes(drv->resolution, xfer->rx_buf, &value);
		//DEBUG printk(KERN_ERR "spi_msg_comp:  Value read: >%d<\n", value);
		// Free all memory associated with the spi message:
		kfree(xfer->rx_buf);
		kfree(list_entry(msg->transfers.next, struct spi_transfer, transfer_list));
		kfree(msg);
		/*DEBUG:
		if (value == 0) {
			gpio = gpio_get_value(30);
			if (gpio > 0)
				gpio = 0;
			else
				gpio = 1;
			gpio_set_value(30, gpio);
		}*/
		// Insert the data into the output buffer:
		kfifo_in_locked(&drv->out_fifo, (unsigned char *) &value, sizeof(value), &drv->outfifo_lock);
		//DEBUG printk(KERN_ERR "spi_msg_comp: Output fifo length: %d<\n", kfifo_len(drv->out_fifo));
		// Wake up sleeping readers:
		wake_up_interruptible(&(drv->dev->devreaders));
	} else {
		printk(KERN_ERR "%s: Unusual spi message status: %d\n", drv->dev->devname, msg->status);
	}

}


/**************************************************************************************//**
 * Reset the ADC and request its ID. The ID is needed to determine the x in AD77x8.
 *
 * @param spi Pointer to the ADC driver
 * @param id Buffer to place the ID into
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
static inline int _reset_and_request_id(struct spi_device *spi, u8 *id)
{
	int ret;
	u8 reset_and_req_id[5] = {0xff, 0xff, 0xff, 0xff, COMM_REG_READ | AD77x8_ID_REG};
	ret = spi_write_then_read(spi, reset_and_req_id, 5, id, 1);
#ifdef DEBUG
        if (ret != 0) {
                dev_dbg(&spi->dev, ">< error %u\n", ret);
        } else {
                dev_dbg(&spi->dev, "> %02x %02x %02x %02x %02x\n", reset_and_req_id[0], reset_and_req_id[1], reset_and_req_id[2], reset_and_req_id[3], reset_and_req_id[4]);
                dev_dbg(&spi->dev, "< %02x\n", *id);
        }
#endif
	return ret;
}



/**************************************************************************************//**
 * Probing function which is automatically called when ADC is registered on the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
static int ad77x8_probe(struct spi_device *spi)
{
	struct ad77x8_drv *drv;
	struct device *spi_dev = &spi->dev;
	struct ad77x8_dev* dev = &ad77x8dev;
	struct ad77x8_spi *spi_priv = &ad77x8spi;
	int spi_msg_buf_len = 1024;			// Has to be a power of 2!
	u8 id;
	int rs = 0;


	dev_dbg(spi_dev, "%s\n", __func__);

	if (spi->max_speed_hz > AD77x8_MAX_SPEED_HZ)
		spi->max_speed_hz = AD77x8_MAX_SPEED_HZ;
	spi->mode = SPI_MODE_3; // CPOL=1, CPHA=1
	spi_setup(spi);

	// reset and identify ADC over SPI
	_reset_and_request_id(spi, &id);
	if ((id & AD77x8_ID_BIT) != AD77x8_ID_AD7708 && (id & AD77x8_ID_BIT) != AD77x8_ID_AD7718)
		return -ENODEV;
	// set ADC to sleep mode
	_write_reg(spi, AD77x8_MODE_REG, AD77x8_MODE_ADC_SLEEP);
		
	/* get memory for driver's per-chip state */
	drv = kzalloc(sizeof(struct ad77x8_drv), GFP_KERNEL);
	if (!drv) {
		rs = -ENOMEM;
		printk( KERN_ERR "%s: Unable to allocate memory for driver. Error %d occurred.\n", DEV_NAME, rs );
		return rs;
	}

	if ((id & AD77x8_ID_BIT) == AD77x8_ID_AD7708)
		drv->resolution = AD77x8_RES_16;
	else if ((id & AD77x8_ID_BIT) == AD77x8_ID_AD7718)
		drv->resolution = AD77x8_RES_24;
	dev_info(spi_dev, "Device is %s\n", drv->resolution==AD77x8_RES_16?"AD7708":"AD7718");

	// Initialize waitqueues, mutexes, spinlocks:
	init_waitqueue_head(&drv->waitq);
	mutex_init(&drv->lock);
	mutex_init(&drv->lock_adc);

	// Initialize the FIFO/circular buffer. This has to be done before registering the interrupt handler
	// as ad77x8_interrupt calls kfifo_in_locked().
	drv->out_fifo.buffer = kzalloc(sizeof(u32)*BUF_LEN, GFP_KERNEL);		// Freed in ad77x8_remove()
	if (!(drv->out_fifo.buffer)) {
		rs = -ENOMEM;
		printk( KERN_ERR "%s: Unable to allocate memory for buffer. Error %d occurred.\n", DEV_NAME, rs );
		goto error_1;
	}
	spin_lock_init(&drv->outfifo_lock);
	kfifo_init(&drv->out_fifo, drv->out_fifo.buffer, BUF_LEN);

	drv->spi = spi_dev_get(spi);
	dev_set_drvdata(spi_dev, drv);
	drv->calibrated = 0;

	// Initialize SPI transfer queues:
	drv->spi_priv = spi_priv;
	spi_priv->tx_buf = (COMM_REG_READ | AD77x8_ADC_DATA_REG);
	spi_priv->msg_fifo.buffer = kzalloc(sizeof(struct spi_message)*spi_msg_buf_len, GFP_KERNEL);		// Freed in ad77x8_remove()
	if (!(spi_priv->msg_fifo.buffer)) {
		rs = -ENOMEM;
		printk( KERN_ERR "%s: Unable to allocate memory for SPI message buffer. Error %d occurred.\n", DEV_NAME, rs );
		goto error_2;
	}
	spin_lock_init(&spi_priv->msgfifo_lock);
	kfifo_init(&spi_priv->msg_fifo, spi_priv->msg_fifo.buffer, spi_msg_buf_len);

	// Initialize struct for device:
	dev->dev_nr 			= 0;
	dev->dev_class 			= NULL;
	dev->device_open_count	= 0;
	dev->fops_dev.owner 	= THIS_MODULE;
	dev->fops_dev.ioctl		= ad77x8_dev_ioctl;
	dev->fops_dev.open		= ad77x8_dev_open;
	dev->fops_dev.read		= ad77x8_dev_read;
	dev->fops_dev.release	= ad77x8_dev_release;
	if (drv->resolution == AD77x8_RES_16)
		dev->devname = "ad7708";
	else
		dev->devname = "ad7718";
	init_waitqueue_head(&dev->devreaders);
	// Link struct to driver and vice versa:
	drv->dev = dev;
	dev->drv = drv;

	/* Create and register device */
	// Get a major number
	if (( rs = alloc_chrdev_region( &(dev->dev_nr), 0, 1, dev->devname )) < 0 ) {
		printk( KERN_ERR "%s: Unable to allocate major for device. Error %d occurred.\n", dev->devname, rs );
		goto error_3;
	}
	printk(KERN_DEBUG "%s: Allocated device numbers for device. Major:%d minor:%d\n", dev->devname, MAJOR( dev->dev_nr ), MINOR( dev->dev_nr ));

	// Register the device. The device becomes "active" as soon as cdev_add is called.
	cdev_init( &(dev->dev_cdev), &(dev->fops_dev) );
	dev->dev_cdev.owner = THIS_MODULE;
	dev->dev_cdev.ops = &(dev->fops_dev);
   	if (( rs = cdev_add( &(dev->dev_cdev), dev->dev_nr, 1 )) != 0 ) {
   		printk( KERN_ERR "%s: cdev_add for device failed with error %d.\n", dev->devname, rs );
   		goto error_3;
   	}
	//DEBUG:
   	//printk(KERN_DEBUG "%s: Device registered.\n", dev->devname);
	// Create a class, so that udev will make the /dev entry automatically:
	dev->dev_class = class_create( THIS_MODULE, dev->devname );
    if ( IS_ERR( dev->dev_class )) {
		printk( KERN_ERR "%s: Unable to create class for device.\n", dev->devname );
		rs = -1;
		goto error_3;
    }

    device_create( dev->dev_class, NULL, dev->dev_nr, NULL, dev->devname);
	printk(KERN_DEBUG "%s: Device node created.\n", dev->devname);

	// Register interrupt handler for data ready pin of adc:
	if ((rs = request_irq(spi->irq, ad77x8_interrupt, IRQF_TRIGGER_FALLING, DEV_NAME, drv))) {
		printk( KERN_ERR "%s: Unable to register IRQ handler. Error %d occurred.\n", DEV_NAME, rs );
		goto error_1;
	}

	// Reset the adc:
	ad77x8_reset();

	// Calibrate the adc:
	ad77x8_calibrate(NULL);

	printk(KERN_DEBUG "%s: Driver started successfully.\n", dev->devname);
	return rs;

error_3:
	kfree(spi_priv->msg_fifo.buffer);
	printk( KERN_ERR "%s: error_3: Error %d occured.\n", DEV_NAME, rs);
	goto error_2;
error_2:
	kfree(drv->out_fifo.buffer);
	printk( KERN_ERR "%s: error_2: Error %d occured.\n", DEV_NAME, rs);
	goto error_1;
error_1:
	kfree(drv);
	printk( KERN_ERR "%s: error_1: Error %d occured.\n", DEV_NAME, rs);
	return rs;
}


/**************************************************************************************//**
 * Called when unregistering the ADC from the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return Always returns 0
  *****************************************************************************************/
static int __devexit ad77x8_remove(struct spi_device *spi)
{
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;

	// Unregister the device if it exists:
	if (dev) {
		// Unregister device:
		device_destroy( dev->dev_class, dev->dev_nr );
		class_destroy( dev->dev_class );
		cdev_del( &(dev->dev_cdev) );
		unregister_chrdev_region( dev->dev_nr, 1 );
		printk(KERN_DEBUG "%s: Unregistered device.\n", DEV_NAME);
	}

	free_irq(spi->irq, drv);
	printk(KERN_DEBUG "%s: Freed IRQ.\n", DEV_NAME);
	// Free fifo buffers:
	kfifo_free(&drv->out_fifo);
	kfifo_free(&drv->spi_priv->msg_fifo);
	printk(KERN_DEBUG "%s: Freed FIFO buffers.\n", DEV_NAME);
	kfree(drv);
	printk(KERN_DEBUG "%s: Freed driver memory.\n", DEV_NAME);
	printk(KERN_DEBUG "%s: Driver unloaded\n", DEV_NAME);

	return 0;
}





/**************************************************************************************//**
 * Interrupt handler for the data ready interrupt of ADC
 *
 * @param irq IRQ number
 * @param context Context of the interrupt
 *
 * @return Handling status of the IRQ handler. Is always IRQ_HANDLED
  *****************************************************************************************/
static irqreturn_t ad77x8_interrupt (int irq, void* context)
{
	struct ad77x8_drv *drv = context;
	struct spi_device *spi = drv->spi;
	struct ad77x8_spi *spi_priv = drv->spi_priv;
	struct spi_transfer *xfer;
	struct spi_message *msg;
	u8 *rx_buf;

	drv->data_ready = 1;
	//DEBUG printk(KERN_ERR "----> IRQ handler entered. calibrated: %d, adc mode: %d\n", drv->calibrated, drv->adc_config.mode);

	// If we are calibrating or in single mode, just wake up calibration process and return
	// For single mode, data will be fetched in ad77x8_read_single()
	if ( (drv->calibrated == 0) || (drv->adc_config.mode == AD77x8_MODE_ADC_SINGLE) ) {
		wake_up_interruptible(&drv->waitq);
		return IRQ_HANDLED;
	}

	// Otherwise, insert data into ringbuffer:
	if (drv->adc_config.mode == AD77x8_MODE_ADC_CONTINUOUS) {
		// Allocate memory for the spi transfer and message:
		xfer = kzalloc(2*sizeof(struct spi_transfer), GFP_ATOMIC);		// Freed in _spi_message_completion()
		msg =  kmalloc(sizeof(struct spi_message), GFP_ATOMIC);			// Freed in _spi_message_completion()
		rx_buf = kzalloc((drv->resolution)*sizeof(u8), GFP_ATOMIC);		// Freed in _spi_message_completion()
		if (!xfer || !msg || !rx_buf) {
			return -ENOMEM;
		}
		spi_message_init(msg);
		// Prepare transfer:
		xfer[0].len = 1;
		spi_message_add_tail(&xfer[0], msg);
		xfer[1].len = drv->resolution;
		spi_message_add_tail(&xfer[1], msg);
		xfer[0].tx_buf = &spi_priv->tx_buf;
		xfer[1].rx_buf = rx_buf;
		// Prepare message:
		msg->complete = (void *) _spi_message_completion;
		msg->context = drv;
		// Schedule the spi message:
		kfifo_in_locked(&spi_priv->msg_fifo, (unsigned char*) &msg, sizeof(struct spi_message *), &spi_priv->msgfifo_lock);
		//DEBUG printk(KERN_ERR "----> IRQ handler: put onto fifo. New length: %d\n", kfifo_len(spi_priv->msg_fifo));
		spi_async(spi, msg);
		return IRQ_HANDLED;
	} else {
		return IRQ_HANDLED;
	}
}

/**************************************************************************************//**
 * Function to configure and read a single ADC channel
 *
 * @param drv Pointer to the ADC driver
 * @param value Buffer to store the value to
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
static int ad77x8_read_single(struct ad77x8_drv *drv, u32 *value)
{
	struct ad77x8_adc_config *adc_config = &(drv->adc_config);
	struct spi_device *spi = drv->spi;
	int ret, state;
	
	// Lock the device for the reading process:
	mutex_lock(&drv->lock);

	// Start sampling by writing the mode register:
	drv->data_ready = 0;
	_write_reg(spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));  // --> wait for interrupt
	ret = wait_event_interruptible_timeout(drv->waitq, drv->data_ready, msecs_to_jiffies(500));
	if (ret <= 0){
		mutex_unlock(&drv->lock);
		return ret;
	}

	state = _read_reg(spi, AD77x8_STAT_REG);
	if ((state & AD77x8_STAT_RDY) != 0) {
		u8 adc_data[3];
		// read 2 or 3 bytes
		_read_data_reg(spi, adc_data, drv->resolution);
		mutex_unlock(&drv->lock);
		// Swap the bytes:
        _swap_bytes(drv->resolution, adc_data, value);
		return 0;
	} else {
		mutex_unlock(&drv->lock);
		return -ENODATA;
	}
}

/**************************************************************************************//**
 * Function which writes all data in the output buffer into the (kernelspace) memory
 * provided by the caller. This function can be used by other kernel modules to read
 * out the buffer with the ADC measurements in it.
 * This function is exported.
 *
 * @param to 	Destination address to copy data to. Must be kernel space memory.
 * @param n		Number of bytes to copy.
 *
 * @return 		Returns number of bytes copied or negative error number
  *****************************************************************************************/
int ad77x8_get_buffer(void *to, unsigned long n) {
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	int xfer_size = sizeof(u32);
	int count = 0;

	// If the queue is empty, send the reader to sleep.
	if (kfifo_is_empty(&drv->out_fifo)) {
		wait_event_interruptible(dev->devreaders, (!kfifo_is_empty(&drv->out_fifo)));
	}
	// As long as the end of the fifo is not reached, copy the data:
	while ( (kfifo_len(&drv->out_fifo) != 0) && n ) {
		// There is still data in the buffer. So get the next value:
		kfifo_out_locked(&drv->out_fifo, (unsigned char*) to, xfer_size, &drv->outfifo_lock);
		// Book-keeping:
		to += xfer_size;
		n -= xfer_size;
		count += xfer_size;
	}
	return count;
}
EXPORT_SYMBOL_GPL(ad77x8_get_buffer);

/**************************************************************************************//**
 * Calibrate the ADC. This function is exported.
 *
 * @param args_calib	Pointer to the struct holding the parameters needed for calibration.
 * 						If the argument is NULL, use the data found in the drivers adc_config
 * 						struct.
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
int ad77x8_calibrate(struct ad77x8_ioctl_configure *args_calib)
{
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_adc_config *adc_config = &drv->adc_config;
	struct spi_device *spi = drv->spi;
	u8 channel;
	u8 adc_mode_orig, channel_orig;
	int rs=0, line=0;
	
	// Reset the calibration flag:
	drv->calibrated = 0;

	// If the argument is NULL, use the data found in the drivers adc_config struct.
	// Otherwise, use the options provided:
	if (args_calib != NULL) {
		//Check arguments passed:
		if (
				(args_calib->nchop > 1) || (args_calib->nchop < 0) ||
				(args_calib->negbuf > 1) || (args_calib->negbuf < 0) ||
				(args_calib->polar > 1) || (args_calib->polar < 0) ||
				(args_calib->range > 7) || (args_calib->range < 0) ||
				(args_calib->sf > 255) || (args_calib->sf < 3) ||
				((args_calib->nchop == 0) && (args_calib->sf < 13))
				) {
			printk(KERN_ERR "%s: Error in argument range.\n", dev->devname);
			return -EINVAL;
		}
		mutex_lock(&drv->lock);

		// Argument check passed. Now configure the driver:
		adc_config->nchop	= args_calib->nchop;
		adc_config->negbuf	= args_calib->negbuf;
		adc_config->polar	= args_calib->polar;
		adc_config->range	= args_calib->range;
		adc_config->sf		= args_calib->sf;
	} else {
		mutex_lock(&drv->lock);
	}
	// Setup ADC for calibration by writing filter register:
	_write_reg(spi, AD77x8_FILTER_REG, adc_config->sf );
	// Save the original adc mode and channel to restore them in the end:
	adc_mode_orig = adc_config->mode;
	channel_orig = adc_config->channel;
	
	// Start calibration
	for (channel=0; channel<5; channel++) { // there are five calibration register pairs ...
		// First do zero scale calibration
		drv->data_ready = 0;
		// Set channel and mode:
		adc_config->channel = channel;
		adc_config->mode = AD77x8_MODE_ADC_CALIB_INT_ZERO;
		// Start calibration by writing to the registers:
		_write_reg(spi, AD77x8_ADC_CONTROL_REG, _bitmask_adccon_reg(adc_config));
		_write_reg(spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));
		printk(KERN_DEBUG "%s: started zero scale calibration for channel %d\n", dev->devname, channel);
		// Wait for interrupt which signals that the calibration has finished
		rs = wait_event_interruptible_timeout(drv->waitq, drv->data_ready, msecs_to_jiffies(500));
		if (rs <= 0) {
			line = __LINE__;
			goto calib_error;
		}
		rs = _read_reg(spi, AD77x8_STAT_REG);
		if ((rs & AD77x8_STAT_CAL)==0 || (rs & AD77x8_STAT_ERR)!=0) {
			line = __LINE__;
			goto calib_error;
		}
		// Then do full scale calibration
		drv->data_ready = 0;
		// Set adc mode:
		adc_config->mode = AD77x8_MODE_ADC_CALIB_INT_FULL;
		_write_reg(spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));
		printk(KERN_DEBUG "%s: started full scale calibration for channel %d\n", dev->devname, channel);
		// Wait for interrupt which signals that the calibration has finished
		rs = wait_event_interruptible_timeout(drv->waitq, drv->data_ready, msecs_to_jiffies(500));
		if (rs <= 0){
			line = __LINE__;
			goto calib_error;
		}
		rs = _read_reg(spi, AD77x8_STAT_REG);
		if ((rs & AD77x8_STAT_CAL)==0 || (rs & AD77x8_STAT_ERR) != 0){
			line = __LINE__;
			goto calib_error;
		}
	}

	// Put the ADC in sleep mode:
	adc_config->mode = AD77x8_MODE_ADC_SLEEP;
	_write_reg(spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));
	// Restore original channel and adc mode settings:
	adc_config->channel = channel_orig;
	adc_config->mode = adc_mode_orig;
	// Mark the adc as calibrated:
	drv->calibrated = 1;
	// Unlock and return:
	mutex_unlock(&drv->lock);
  	return 0;

calib_error:    
	// Put the ADC in sleep mode:
	adc_config->mode = AD77x8_MODE_ADC_SLEEP;
	_write_reg(spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));
	// Restore original channel and adc mode settings:
	adc_config->channel = channel_orig;
	adc_config->mode = adc_mode_orig;
	mutex_unlock(&drv->lock);
	printk(KERN_ERR "%s: Error at line %d during zero scale calibration: %d\n", dev->devname, line, rs);
   	return -ENODATA;

}
EXPORT_SYMBOL_GPL(ad77x8_calibrate);



/**************************************************************************************//**
 * Configure the ADCand calibrate if needed. This function is exported.
 *
 * @param args_conf Pointer to the struct holding the configuration data
 *
 * @return 0 on success, negative errno otherwise
  *****************************************************************************************/
int ad77x8_configure(struct ad77x8_ioctl_configure *args_conf)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_adc_config *adc_config = &(drv->adc_config);
	int rs;

	//Check arguments passed:
	rs = ad77x8_check_config(args_conf);
	if (rs != 0) {
		printk(KERN_ERR "%s: Error in argument range.\n", dev->devname);
		return rs;
	}
	// Lock the driver:
	mutex_lock(&drv->lock);
	
	// If the range is about to change, we need to calibrate (according to datasheet):
	if (adc_config->range != args_conf->range)
		drv->calibrated = 0;
	
	// Put the ADC into idle mode to allow writing the filter register:
	adc_config->mode = AD77x8_MODE_ADC_IDLE;
	_write_reg(drv->spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));

	// Configure the driver:
	adc_config->nchop	= args_conf->nchop;
	adc_config->negbuf	= args_conf->negbuf;
	adc_config->chcon	= args_conf->chcon;
	adc_config->oscpd	= args_conf->oscpd;
	adc_config->mode	= args_conf->mode;
	adc_config->channel	= args_conf->channel;
	adc_config->polar	= args_conf->polar;
	adc_config->range	= args_conf->range;
	adc_config->sf		= args_conf->sf;

	// Configure the measurement: This is done by writing the adccon and filter registers.
	// If single mode is used, the mode register is written when the /dev/ad77x8 is openend. 
	// In all other modes, the mode register is written immediately.
	_write_reg(drv->spi, AD77x8_FILTER_REG, adc_config->sf);
	_write_reg(drv->spi, AD77x8_ADC_CONTROL_REG, _bitmask_adccon_reg(adc_config));
	if (adc_config->mode != AD77x8_MODE_ADC_SINGLE) {
		drv->data_ready = 0;
		_write_reg(drv->spi, AD77x8_MODE_REG, _bitmask_mode_reg(adc_config));
	}
	
	// Unlock the driver:
	mutex_unlock(&drv->lock);
	
	// If the device has not been calibrated yet, do so:
	if (drv->calibrated == 0) {
		printk(KERN_INFO "%s: Configuration values require calibration of ADC.\n", dev->devname);
		rs = ad77x8_calibrate(NULL);
		if ( rs != 0 )
			return rs;
		printk(KERN_INFO "%s: Calibration done.\n", dev->devname);
	}

	printk(KERN_DEBUG "%s: ADC configured.\n", dev->devname);
	return 0;
}
EXPORT_SYMBOL_GPL(ad77x8_configure);


/**************************************************************************************//**
 * Check the configuration of the adc parameters. This function is exported.
 *
 * @param conf Pointer to the struct which is to be checked
 *
 * @return 0 on success, negative error number otherwise
 *
  *****************************************************************************************/
int ad77x8_check_config(struct ad77x8_ioctl_configure *conf)
{
	//Check arguments passed:
	if (
			(conf->nchop > 1) || (conf->nchop < 0) ||
			(conf->negbuf > 1) || (conf->negbuf < 0) ||
			(conf->chcon > 1) || (conf->chcon < 0) ||
			(conf->oscpd > 1) || (conf->oscpd < 0) ||
			(conf->mode > 7) || (conf->mode < 0) ||
			(conf->channel > 15) || (conf->channel < 0) ||
			(conf->polar > 1) || (conf->polar < 0) ||
			(conf->range > 7) || (conf->range < 0) ||
			(conf->sf > 255) || (conf->sf < 3) ||
			((conf->nchop == 0) && (conf->sf < 13))
			) {
		return -EINVAL;
	} else
		return 0;
}
EXPORT_SYMBOL_GPL(ad77x8_check_config);


/**************************************************************************************//**
 * Read out configuration from the ADC. This function is exported.
 *
 * @param[in] spi  Pointer to the SPI device
 * @param[out] conf Pointer to the struct which is going to hold the configuration values
 *
  *****************************************************************************************/
void ad77x8_get_config(struct spi_device *spi, struct ad77x8_ioctl_configure *conf)
{
	/* Local variables */
	int rs;

	// Get mode register and parse it into the struct:
	rs =_read_reg(spi, AD77x8_MODE_REG);
	conf->nchop		= (rs & 0x80) >> 7;
	conf->negbuf	= (rs & 0x40) >> 6;
	conf->chcon		= (rs & 0x10) >> 4;
	conf->oscpd		= (rs & 0x08) >> 3;
	conf->mode		= rs & 0x07;

	// Do the same for the adccon register:
	rs =_read_reg(spi, AD77x8_ADC_CONTROL_REG);
	conf->channel	= (rs & 0xF0) >> 4;
	conf->polar		= (rs & 0x08) >> 3;
	conf->range		= rs & 0x07;

	// The filter register can be read out as it is:
	conf->sf =_read_reg(spi, AD77x8_FILTER_REG);
}


/**************************************************************************************//**
 * Reset the ADC. This function is exported.
 *****************************************************************************************/
void ad77x8_reset(void)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_adc_config *adc_config = &(drv->adc_config);

	mutex_lock(&drv->lock);

	// Reset the drivers adc configuration to the default values
	adc_config->nchop	= AD77x8_MODE_CHOP_DISABLE;
	adc_config->negbuf	= AD77x8_MODE_NEGBUF_BUFFER;
	adc_config->chcon	= AD77x8_MODE_CHCON_10_5;
	adc_config->oscpd	= AD77x8_MODE_OSCPD_STBY;
	adc_config->mode	= AD77x8_MODE_ADC_SLEEP;
	adc_config->channel	= AD77x8_ADCCON_REF_REF;
	adc_config->polar	= AD77x8_ADCCON_POLAR_UNI;
	adc_config->range	= AD77x8_ADCCON_RANGE_2560mV;
	adc_config->sf		= 13;

	// Now write the adccon and filter registers:
	_write_reg(drv->spi, AD77x8_FILTER_REG, adc_config->sf);
	_write_reg(drv->spi, AD77x8_ADC_CONTROL_REG, _bitmask_adccon_reg(adc_config));

	// Empty the output fifo:
	kfifo_reset(&drv->out_fifo);

	mutex_unlock(&drv->lock);

	printk(KERN_DEBUG "%s: ADC reset to default values.\n", dev->devname);
}
EXPORT_SYMBOL_GPL(ad77x8_reset);


/**************************************************************************************//**
 * Flush the output buffer of the ADC. This function is exported.
 *****************************************************************************************/
void ad77x8_flush_buffer(void)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;

	// Empty the output fifo:
	kfifo_reset(&drv->out_fifo);
}
EXPORT_SYMBOL_GPL(ad77x8_flush_buffer);


/**************************************************************************************//**
 * Reserve the ADC for exclusive access. This function is exported.
 *****************************************************************************************/
void ad77x8_reserve_adc(void)
{
	/* Local variables */
	struct ad77x8_drv *drv = (&ad77x8dev)->drv;

	mutex_lock(&drv->lock_adc);
}
EXPORT_SYMBOL_GPL(ad77x8_reserve_adc);


/**************************************************************************************//**
 * Release the ADC from exclusive access and put it to sleep mode. This function is exported.
 *****************************************************************************************/
void ad77x8_release_adc(void)
{
	/* Local variables */
	struct ad77x8_drv *drv = (&ad77x8dev)->drv;


	// Put ADC to sleep mode:
	drv->adc_config.mode = AD77x8_MODE_ADC_SLEEP;
	_write_reg(drv->spi, AD77x8_MODE_REG, _bitmask_mode_reg(&drv->adc_config));

	mutex_unlock(&drv->lock_adc);
}
EXPORT_SYMBOL_GPL(ad77x8_release_adc);


/**************************************************************************************//**
 *
 * IOCTL handler for /dev/ad77x8
 *
 * @param inode Inode
 * @param filp File pointer
 * @param cmd Command number of the ioctl to perform
 * @param arg Arguments passed from user space ioctl() function
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int ad77x8_dev_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_ioctl_configure args_conf;
	int rs;

	// Extract type and number bitfield. If wrong cmd, return ENOTTY (inappropriate ioctl):
	if (_IOC_TYPE(cmd) != AD77x8_IOCTL_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > AD77x8_IOCTL_MAXNR) return -ENOTTY;
	// See which cmd it was and act accordingly:
	switch (cmd) {
		case AD77x8_IOCTL_CALIBRATE:
			// Get the arguments.
			if ( (copy_from_user(&args_conf, (unsigned int __user *)  arg, sizeof(struct ad77x8_ioctl_configure))) != 0) {
				printk(KERN_ERR "%s: Error getting the arguments.\n", dev->devname);
				return -ENOTTY;
			}
			// Calibrate the device:
			mutex_lock(&drv->lock_adc);
			rs = ad77x8_calibrate(&args_conf);
			mutex_unlock(&drv->lock_adc);
			return rs;
			break;

		case AD77x8_IOCTL_CONFIGURE:
			// Get the arguments.
			if ( (copy_from_user(&args_conf, (unsigned int __user *) arg, sizeof(struct ad77x8_ioctl_configure))) != 0) {
				printk(KERN_ERR "%s: Error getting the arguments.\n", dev->devname);
				return -ENOTTY;
			}
			mutex_lock(&drv->lock_adc);
			rs = ad77x8_configure(&args_conf);
			mutex_unlock(&drv->lock_adc);
			return rs;
			break;

		case AD77x8_IOCTL_RESET:
			ad77x8_reset();
			return 0;
			break;

		case AD77x8_IOCTL_GETCONFIG:
			// Get the configuration from the ADC and write it to args_conf:
			ad77x8_get_config(drv->spi, &args_conf);
			//Now write out the configuration on the syslog:
			printk(KERN_DEBUG "%s: ADC configuration: \n", dev->devname);
			printk(KERN_DEBUG "%s: nchop: %d\n", dev->devname, args_conf.nchop);
			printk(KERN_DEBUG "%s: negbuf: %d\n", dev->devname, args_conf.negbuf);
			printk(KERN_DEBUG "%s: chcon: %d\n", dev->devname, args_conf.chcon);
			printk(KERN_DEBUG "%s: oscpd: %d\n", dev->devname, args_conf.oscpd);
			printk(KERN_DEBUG "%s: mode: %d\n", dev->devname, args_conf.mode);
			printk(KERN_DEBUG "%s: channel: %d\n", dev->devname, args_conf.channel);
			printk(KERN_DEBUG "%s: polar: %d\n", dev->devname, args_conf.polar);
			printk(KERN_DEBUG "%s: range: %d\n", dev->devname, args_conf.range);
			printk(KERN_DEBUG "%s: sf: %d\n", dev->devname, args_conf.sf);
			// Pass the configuration back to the user:
			copy_to_user((unsigned int __user *) arg, &args_conf, sizeof(struct ad77x8_ioctl_configure));
			break;

		default:
			printk(KERN_ERR "%s: Requested IOCTL not found.\n", dev->devname);
			return -ENOTTY;
	}

	return 0;
} // ad77x8_dev_ioctl

/**************************************************************************************//**
 *
 * Function is executed when a user opens the dev file
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int ad77x8_dev_open(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_adc_config *adc_config = &drv->adc_config;

	if (dev->device_open_count)
		return -EBUSY;
	dev->device_open_count++;
	/* Check if data is in the fifo buffer if continuous mode is set.
	 * If not, send the reading process (user space) to sleep. The process will be
	 * waken up in the interrupt handler after inserting data into the fifo buffer.
	 */
	if ( (adc_config->mode == AD77x8_MODE_ADC_CONTINUOUS) && (kfifo_is_empty(&drv->out_fifo)) ) {
		wait_event_interruptible(dev->devreaders, (!kfifo_is_empty(&drv->out_fifo)));
	}

	try_module_get(THIS_MODULE);

	return 0;
} // ad77x8_dev_open



/**************************************************************************************//**
 *
 * Function is executed when a user reads the dev file
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, negative error number otherwise
 *
 *****************************************************************************************/
static ssize_t ad77x8_dev_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;
	struct ad77x8_drv *drv = dev->drv;
	struct ad77x8_adc_config *adc_config = &drv->adc_config;
	u32 value;
	int xfer_size = sizeof(value);
	int rs;
	int count = 0;

	// If single mode is set, turn it on now and get the data:
	mutex_lock(&drv->lock_adc);
	if (adc_config->mode == AD77x8_MODE_ADC_SINGLE) {
		rs = ad77x8_read_single(drv, &value);
		rs = copy_to_user(__user buffer, &value, xfer_size);
		if (rs != 0) {
			printk(KERN_ERR "%s: Not all data copied to userspace. Still missing %d bytes.\n", dev->devname, rs);
			rs = -EFAULT;
			goto releaseandreturn;
		}
		buffer += xfer_size;
		length -= xfer_size;
		count += xfer_size;
		rs = count;
		goto releaseandreturn;
	} else if (adc_config->mode == AD77x8_MODE_ADC_CONTINUOUS) {
		// We are in continuous mode and there should be data in the fifo buffer (otherwise
		// the ad77x8_dev_open function would have sent the reading process to sleep).

		// Output all data in the fifo buffer as long as there is still space in the device buffer
		//DEBUG printk(KERN_ERR "%s: Line %d, kfifo_avail: %d, length: %d\n", dev->devname, __LINE__, kfifo_avail(&drv->out_fifo), length);
		while ( (!kfifo_is_empty(&drv->out_fifo)) && length ) {
			// There is still data in the buffer. So get the next value:
			kfifo_out_locked(&drv->out_fifo, (unsigned char*) &value, xfer_size, &drv->outfifo_lock);
			//DEBUG printk(KERN_ERR "Value: >%d<\n", value);
			copy_to_user(__user buffer, &value, xfer_size);
			//DEBUG printk(KERN_ERR "Value: >%d< xfer_size: %d\n", value, xfer_size);
			// Book-keeping:
			buffer += xfer_size;
			length -= xfer_size;
			count += xfer_size;
		}
		rs = count;
		goto releaseandreturn;
	} else {
		// We are in a mode that does not require any reading. Thus return:
		rs = -EAGAIN;
		goto releaseandreturn;
	}

	rs = 0;
	goto releaseandreturn;

releaseandreturn:
	mutex_unlock(&drv->lock_adc);
	return rs;

} // ad77x8_dev_read



/**************************************************************************************//**
 *
 * Function is executed when a user closes the dev file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int ad77x8_dev_release(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct ad77x8_dev *dev = &ad77x8dev;

	dev->device_open_count--;

	module_put(THIS_MODULE);

	return 0;
} // ad77x8_dev_release



/* ---- Init and Cleanup ----------------------------------------------------------*/
/**
 * Initialization function for the kernel module
 */
static int __init ad77x8_init(void)
{
 	return spi_register_driver(&ad77x8_driver);
}
module_init(ad77x8_init);


/**
 * Destruction function for the kernel module
 */
static void __exit ad77x8_exit(void)
{
  	spi_unregister_driver(&ad77x8_driver);
}
module_exit(ad77x8_exit);
