/**
 * @file
 *
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * @author Roman Lim <lim@tik.ee.ethz.ch>
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 *
 *
 * @section LICENSE
 *
 * Copyright (c) 2010 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 *
 *
 */

#ifndef AD77X8_H_
#define AD77X8_H_

#define AD77x8_ID_BIT					0xf0
#define AD77x8_ID_AD7708				0x50
#define AD77x8_ID_AD7718				0x40
#define AD77x8_RES_16					2
#define AD77x8_RES_24					3
#define AD77x8_MAX_SPEED_HZ				5000000
#define AD77x8_CHANNELS					10
#define AD77x8_FORMAT_RAW				0
#define AD77x8_FORMAT_MV				1
#define AD77x8_SF_DEFAULT				13


/*
 * IOCTL definitions
 */
#define AD77x8_IOCTL_MAGIC  			'O'			// Magic number for ioctl commands
/**
 * Calibration command. Takes 6 parameters in a struct ad77x8_ioctl_calibrate
 */
#define AD77x8_IOCTL_CALIBRATE			_IOWR(AD77x8_IOCTL_MAGIC,	0, int[9])
/**
 * Configure command. Takes 9 parameters in a struct ad77x8_ioctl_configure
 */
#define AD77x8_IOCTL_CONFIGURE			_IOWR(AD77x8_IOCTL_MAGIC, 1, int[9])
#define AD77x8_IOCTL_RESET				_IO(AD77x8_IOCTL_MAGIC, 2)
/**
 * Get the current configuration of the ADC. Receives the configuration as a pointer to a struct ad77x8_ioctl_configure
 */
#define AD77x8_IOCTL_GETCONFIG			_IOR(AD77x8_IOCTL_MAGIC, 2, int)
#define AD77x8_IOCTL_MAXNR				4			// Denotes maximum number of cmd's above

/**
 * Struct for AD77x8_IOCTL_CONFIGURE
 */
struct ad77x8_ioctl_configure {
	unsigned int nchop;			///< Chop mode (0: enabled, 1: disabled)
	unsigned int negbuf;		///< Negbuf (0: unbuffered, 1: buffered)
	unsigned int chcon;			///< Channel converter mode (0: 8/4 channels, 1: 10/5 channels)
	unsigned int oscpd;			///< Oscillator power-down mode (0: standby, 1: stop)
	unsigned int mode;			///< ADC mode (0-7 according to data sheet)
	unsigned int channel;		///< Channel selection (0-15 according to data sheet)
	unsigned int polar;			///< Unipolar/Bipolar mode selection (0: biploar, 1: unipolar)
	unsigned int range;			///< ADC input range (0-7 according to data sheet)
	unsigned int sf;			///< SF filter selection (13-255 with chop enabled, 3-255 with chop disabled according to data sheet)
};

/**
 * Register addresses
 */
enum {
	AD77x8_COMM_REG = 					0x00,
	AD77x8_STAT_REG = 					0x00,
	AD77x8_MODE_REG = 					0x01,
	AD77x8_ADC_CONTROL_REG = 			0x02,
	AD77x8_FILTER_REG =					0x03,
	AD77x8_ADC_DATA_REG = 				0x04,
	AD77x8_ADC_OFFSET_REG = 			0x05,
	AD77x8_ADC_GAIN_REG = 				0x06,
	AD77x8_IO_CONTROL_REG = 			0x07,
	AD77x8_ID_REG = 					0x0F,
	COMM_REG_READ =						0x40,
};

/**
 * Options for mode register
 */
enum {
	AD77x8_MODE_CHOP_ENABLE =			0x00,
	AD77x8_MODE_CHOP_DISABLE =			0x01,
	AD77x8_MODE_NEGBUF_UNBUFFER =		0x00,
	AD77x8_MODE_NEGBUF_BUFFER =			0x01,
	AD77x8_MODE_REFSEL_REFIN1 =			0x00,
	AD77x8_MODE_REFSEL_REFIN2 =			0x01,
	AD77x8_MODE_CHCON_8_4 =				0x00,
	AD77x8_MODE_CHCON_10_5 =			0x01,
	AD77x8_MODE_OSCPD_STBY =			0x00,
	AD77x8_MODE_OSCPD_STOP =			0x01,
	AD77x8_MODE_ADC_SLEEP =				0x00,
	AD77x8_MODE_ADC_IDLE =				0x01,
	AD77x8_MODE_ADC_SINGLE = 			0x02,
	AD77x8_MODE_ADC_CONTINUOUS =		0x03,
	AD77x8_MODE_ADC_CALIB_INT_ZERO =	0x04,
	AD77x8_MODE_ADC_CALIB_INT_FULL =	0x05,
	AD77x8_MODE_ADC_CALIB_SYS_ZERO =	0x06,
	AD77x8_MODE_ADC_CALIB_SYS_FULL =	0x07,
};

/**
 * Status bits
 */
enum {
	AD77x8_STAT_RDY = 					0x80,
	AD77x8_STAT_CAL = 					0x20,
	AD77x8_STAT_ERR = 					0x08,
	AD77x8_STAT_LOCK = 					0x01,
};

/**
 * ADC configuration register for 8 channel mode
 */
enum {
	AD77x8_ADCCON_AIN1_AINCOM = 		0x00,
	AD77x8_ADCCON_AIN2_AINCOM = 		0x01,
	AD77x8_ADCCON_AIN3_AINCOM =			0x02,
	AD77x8_ADCCON_AIN4_AINCOM = 		0x03,
	AD77x8_ADCCON_AIN5_AINCOM = 		0x04,
	AD77x8_ADCCON_AIN6_AINCOM = 		0x05,
	AD77x8_ADCCON_AIN7_AINCOM = 		0x06,
	AD77x8_ADCCON_AIN8_AINCOM = 		0x07,
	AD77x8_ADCCON_AIN1_AIN2 = 			0x08,
	AD77x8_ADCCON_AIN3_AIN4 = 			0x09,
	AD77x8_ADCCON_AIN5_AIN6 = 			0x0a,
	AD77x8_ADCCON_AIN7_AIN8 = 			0x0b,
	AD77x8_ADCCON_AIN2_AIN2 = 			0x0c,
	AD77x8_ADCCON_AINCOM_AINCOM = 		0x0d,
	AD77x8_ADCCON_REF_REF = 			0x0e,
	AD77x8_ADCCON_OPEN = 				0x0f,
	AD77x8_ADCCON_POLAR_BI =			0x00,
	AD77x8_ADCCON_POLAR_UNI =			0x01,
	AD77x8_ADCCON_RANGE_20mV =			0x00,
	AD77x8_ADCCON_RANGE_40mV =			0x01,
	AD77x8_ADCCON_RANGE_80mV =			0x02,
	AD77x8_ADCCON_RANGE_160mV =			0x03,
	AD77x8_ADCCON_RANGE_320mV =			0x04,
	AD77x8_ADCCON_RANGE_640mV =			0x05,
	AD77x8_ADCCON_RANGE_1280mV =		0x06,
	AD77x8_ADCCON_RANGE_2560mV =		0x07,
};


/* ---- Public Prototypes --------------------------------------------------*/
/**
 * Function which writes all data in the output buffer into the (kernelspace) memory
 * provided by the caller. This function can be used by other kernel modules to read
 * out the buffer with the adc measurements in it.
 */
int ad77x8_get_buffer(void *to, unsigned long n);

/**
 * Calibrate the ADC
 */
int ad77x8_calibrate(struct ad77x8_ioctl_configure *args_calib);

/**
 * Configure the ADC
 */
int ad77x8_configure(struct ad77x8_ioctl_configure *args_conf);

/**
 * Check the configuration of the adc parameters
 */
int ad77x8_check_config(struct ad77x8_ioctl_configure *conf);

/**
 * Reset the ADC. If the argument is null, the standard driver is used.
 */
void ad77x8_reset(void);

/**
 * Flush the output buffer of the ADC.
 */
void ad77x8_flush_buffer(void);

/**
 * Reserve the ADC for exlusive access.
 */
void ad77x8_reserve_adc(void);

/**
 * Release the ADC from exlusive access.
 */
void ad77x8_release_adc(void);




#endif /*AD77X8_H_*/
