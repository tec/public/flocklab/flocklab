/**
 * @file
 *
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 * @author Roman Lim <lim@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * Copyright (c) 2011-2012 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 *
 *
 */

/**
 * @section DESCRIPTION
 *
 * Use this module to read values from the Analog-to-Digital converter TI ADS1271
 * The module generates a device file at /dev/ads1271 which can be read values from.
 *
 */

/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/completion.h>
#include <linux/spi/spi.h>
#include <linux/clocksource.h>
#include <mach/regs-ost.h>
#include <mach/ssp.h>
#include <mach/regs-ssp.h>
#include <mach/pxa2xx_spi.h>
#include <mach/pxa27x.h>
#include <mach/dma.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>

#include "ads1271.h"

#define FRAME_GPIO 14

#define MODULE_NAME "ads1271"

// major = 0 -> dynamically allocated number
#define ADS1271_MAJOR 0
#define ADS1271_N_MINORS 8
static DECLARE_BITMAP(minors, ADS1271_N_MINORS);

#define DMA_INT_MASK		(DCSR_ENDINTR | DCSR_STARTINTR | DCSR_BUSERR)
#define RESET_DMA_CHANNEL	(DCSR_NODESC | DMA_INT_MASK)
#define IS_DMA_ALIGNED(x)	((((u32)(x)) & 0x07) == 0)
#define MAX_DMA_LEN		8191
#define DMA_ALIGNMENT		8
// Size of FIFO which stores samples.
// Make sure that FIFO is large enough to hold all samples according to packet_threshold
// Should be synchronized with ADS1271_SAMPLE_FIFO_LENGTH in flocklab_powerprof_shared.h
#define SAMPLE_FIFO_LENGTH (MAX_DMA_LEN / sizeof(int))
#define SAMPLE_FIFO_SIZE (SAMPLE_FIFO_LENGTH * sizeof(int))

static int dev_major_nr;
static struct class* dev_class;

static ushort nth_sample = ADS1271_SAMPLE_DIVIDER_DEFAULT;
module_param(nth_sample, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(nth_sample, "retrieve each n-th sample (2,4,6,...,2047, default=2).");
static uint32_t adc_clock_hz = 14745600;
module_param(adc_clock_hz, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(adc_clock_hz, "Clock speed in Hz of the ADC oscillator (default=14745600).");
static ushort adc_clock_ratio = 512;
module_param(adc_clock_ratio, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(adc_clock_ratio, "Clock ratio of the currently set ADC mode (512 for High-Resolution/Low-Power mode, 256 for High-Speed mode, default=512).");
static ushort adc_filter_delay = 39;
module_param(adc_filter_delay, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(adc_filter_delay, "Filter delay of the currently set ADC mode in samples (39 for High-Resolution mode, 38 for Low-Power/High-Speed mode, default=39).");

static LIST_HEAD(dev_list);
static DEFINE_MUTEX(dev_list_lock);

// Variables for OS timer
static unsigned int oscr_mult;
#define OSCR_SHIFT 20

/**
 * All available SSP registers (c.f. PXA270 Processor Developer's Manual). (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
#define DEFINE_SSP_REG(reg, off) \
static inline u32 read_##reg(void const __iomem *p) \
{ return __raw_readl(p + (off)); } \
\
static inline void write_##reg(u32 v, void __iomem *p) \
{ __raw_writel(v, p + (off)); }
DEFINE_SSP_REG(SSCR0, 0x00)
DEFINE_SSP_REG(SSCR1, 0x04)
DEFINE_SSP_REG(SSSR, 0x08)
DEFINE_SSP_REG(SSITR, 0x0c)
DEFINE_SSP_REG(SSDR, 0x10)
DEFINE_SSP_REG(SSTO, 0x28)
DEFINE_SSP_REG(SSPSP, 0x2c)

/**
 * Struct for storing all communication properties, which are necessary for using the SPI protocol and the SSP registers. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
struct driver_data {
        /* Driver model hookup */
        struct platform_device *pdev;

        /* SSP Info */
        struct ssp_device *ssp;
        /* SPI framework hookup */
        enum pxa_ssp_type ssp_type;
        struct spi_master *master;

        /* PXA hookup */
        struct pxa2xx_spi_master *master_info;

        /* DMA setup stuff */
        int rx_channel;
        int tx_channel;
        u32 *null_dma_buf;

        /* SSP register addresses */
        void __iomem *ioaddr;
        u32 ssdr_physical;

        /* SSP masks*/
        u32 dma_cr1;
        u32 int_cr1;
        u32 clear_sr;
        u32 mask_sr;

        /* Driver message queue */
        struct workqueue_struct *workqueue;
        struct work_struct pump_messages;
        spinlock_t lock;
        struct list_head queue;
        int busy;
        int run;

        /* Message Transfer pump */
        struct tasklet_struct pump_transfers;

        /* Current message transfer state info */
        struct spi_message* cur_msg;
        struct spi_transfer* cur_transfer;
        struct chip_data *cur_chip;
        size_t len;
        void *tx;
        void *tx_end;
        void *rx;
        void *rx_end;
        int dma_mapped;
        dma_addr_t rx_dma;
        dma_addr_t tx_dma;
        size_t rx_map_len;
        size_t tx_map_len;
        u8 n_bytes;
        u32 dma_width;
        int (*write)(struct driver_data *drv_data);
        int (*read)(struct driver_data *drv_data);
        irqreturn_t (*transfer_handler)(struct driver_data *drv_data);
        void (*cs_control)(u32 command);
};

/**
 * Struct for storing all communication properties of the selected ADC chip.  (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
struct chip_data {
	u32 cr0;
	u32 cr1;
	u32 psp;
	u32 timeout;
	u8 n_bytes;
	u32 dma_width;
	u32 dma_burst_size;
	u32 threshold;
	u32 dma_threshold;
	u8 enable_dma;
	u8 bits_per_word;
	u32 speed_hz;
	int gpio_cs;
	int gpio_cs_inverted;
	int (*write)(struct driver_data *drv_data);
	int (*read)(struct driver_data *drv_data);
	void (*cs_control)(u32 command);
};

/* ---- Function declarations ---------------------------------------------*/
static int ads1271_probe(struct spi_device*);
static int __devexit ads1271_remove(struct spi_device*);
static irqreturn_t ads1271_dma_transfer(struct driver_data *drv_data);
static void tasklet_handler(unsigned long data);
static int ads1271_open(struct inode*, struct file*);
static ssize_t ads1271_read(struct file*, char*, size_t, loff_t*);
static int ads1271_release(struct inode*, struct file*);
static void ads1271_dma_handler(int channel, void *data);
void cleanup(struct spi_device *spi, int init_state);

static struct spi_driver ads1271_spi_driver = {
	.driver = {
		.name	= "ads1271",
		.owner	= THIS_MODULE,
	},
	.probe		= ads1271_probe,
	.remove		= __devexit_p(ads1271_remove),
};

static const struct file_operations ads1271_dev_fops = {
	.owner		= THIS_MODULE,
	.open		= ads1271_open,
	.read		= ads1271_read,
	.release	= ads1271_release,
};

/* ---- Enums to for init / cleanup ---------------------------------------------*/
enum {
	INIT_DRIVER_MEM,
	INIT_DMA,
	INIT_MEM_BUFFER_STRCT,
	INIT_MEM_SPARE_BUFFER_STRCT,
	INIT_MEM_BUFFER,
	INIT_MAP_BUFFERS,
	INIT_CREATE_DEVICE,
	INIT_CREATE_FILE,
	INIT_DONE,
};

/**
 * Sets the chip_select output on hardware high. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
static void cs_assert(struct driver_data *drv_data)
{
	struct chip_data *chip = drv_data->cur_chip;

	if (chip->cs_control) {
		chip->cs_control(PXA2XX_CS_ASSERT);
		return;
	}

	if (gpio_is_valid(chip->gpio_cs))
		gpio_set_value(chip->gpio_cs, chip->gpio_cs_inverted);
}


/**
 * Sets the chip_select output on hardware low. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
static void cs_deassert(struct driver_data *drv_data)
{
	struct chip_data *chip = drv_data->cur_chip;

	if (chip->cs_control) {
		chip->cs_control(PXA2XX_CS_DEASSERT);
		return;
	}

	if (gpio_is_valid(chip->gpio_cs))
		gpio_set_value(chip->gpio_cs, !chip->gpio_cs_inverted);
}


static ssize_t ads1271_sysfs_show_errors(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct spi_device* spi;
	struct ads1271_driver_data* ads1271_driver;
	int cnt = 0;

	spi = to_spi_device(dev);
	if (spi == NULL) {
		return -ENODEV;
	}

	ads1271_driver = spi_get_drvdata(spi);

	if (ads1271_driver->kfifo_full_occured) {
		cnt += sprintf(buf + cnt, "kfifo_full_occured=%u\n", ads1271_driver->kfifo_full_occured);
	}
	if (ads1271_driver->rx_fifo_overrun_occured) {
		cnt += sprintf(buf + cnt, "rx_fifo_overrun_occured=%u\n", ads1271_driver->rx_fifo_overrun_occured);
	}
	if (ads1271_driver->rx_fifo_empty_occured) {
		cnt += sprintf(buf + cnt, "rx_fifo_empty_occured=%u\n", ads1271_driver->rx_fifo_empty_occured);
	}

	return cnt;
} //ads1271_sysfs_show_errors
static DEVICE_ATTR(errors, S_IRUGO, ads1271_sysfs_show_errors, NULL);

static void __ads1271_oscr_to_timestamp(uint32_t oscr, struct timeval * t) {
	int timestamp_ovrhd, timestamp_ovrhd_secs, timestamp_ovrhd_usecs;
		
	do_gettimeofday(t);
	timestamp_ovrhd = OSCR - oscr;
	if (timestamp_ovrhd < 0) {
		printk(KERN_WARNING MODULE_NAME ": timestamp in the future.\n");
	}
	timestamp_ovrhd = ((u32)(((u64)timestamp_ovrhd * (u64)oscr_mult) >> OSCR_SHIFT)) / 1000; // overhead in microseconds
	timestamp_ovrhd_secs = timestamp_ovrhd / 1000000;
	timestamp_ovrhd_usecs = timestamp_ovrhd - (timestamp_ovrhd_secs*1000000);
	t->tv_sec -= timestamp_ovrhd_secs;
	if (t->tv_usec < timestamp_ovrhd_usecs) {
		t->tv_sec --;
		t->tv_usec = 1000000 + (t->tv_usec - timestamp_ovrhd_usecs);
	} else {
		t->tv_usec = t->tv_usec - timestamp_ovrhd_usecs;
	}
}

/******************************************************************************************
 * Function to prepare FIFO contents to be transferred to user.
 *
 * @param fifo Pointer to the kfifo
 * @param ads1271_driver Pointer to the driver struct
 *
 * @return 0 on success, -errno otherwise
 ******************************************************************************************/
static void tasklet_handler(unsigned long data) {
	struct ads1271_driver_data* ads1271_driver;

	// Get the arguments:
	ads1271_driver = (struct ads1271_driver_data*) data;
	
	// start time
	ads1271_driver->packet_start = ads1271_driver->packet_start_next;
	__ads1271_oscr_to_timestamp(ads1271_driver->tasklet_sampling_start - ads1271_driver->adc_filter_delay, &(ads1271_driver->packet_start_next));
	
	// end time
	ads1271_driver->packet_end = ads1271_driver->packet_start_next;
	if (ads1271_driver->packet_end.tv_usec < ads1271_driver->sampling_period) {
		ads1271_driver->packet_end.tv_sec --;
		ads1271_driver->packet_end.tv_usec += 1000000;
	}
	ads1271_driver->packet_end.tv_usec -= ads1271_driver->sampling_period;
		
	// Wake up readers:
	// unmap buffer
	if (ads1271_driver->spare_buffer->is_mapped) {
		dma_unmap_single(&ads1271_driver->spi->dev, ads1271_driver->spare_buffer->sg_dma, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);
		ads1271_driver->spare_buffer->is_mapped = 0;
	}
	else {
		printk(KERN_WARNING MODULE_NAME ": buffer was not mapped.\n");
	}
	//printk(KERN_INFO MODULE_NAME ": tasklet packet len %d samples\n", ads1271_driver->tasklet_packet_len);
	ads1271_driver->packet_len = ads1271_driver->transfer_size / sizeof(int);
	complete(&(ads1271_driver->reader_wait));
	
}

/******************************************************************************************
 * Probing function which is automatically called when ADC is registered on the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return 0 on success, -errno otherwise
 ******************************************************************************************/
static int ads1271_probe(struct spi_device *spi)
{
	int ret;
	struct driver_data* driver;
	struct ads1271_driver_data* ads1271_driver;
	struct chip_data* chip;
	void __iomem *reg;
	unsigned long minor;
	dev_t dev_nr;
	u32 altfn, gafr;

	ads1271_driver = kzalloc(sizeof(struct ads1271_driver_data), GFP_KERNEL);
	if (ads1271_driver == NULL) {
		printk(KERN_ERR MODULE_NAME ": unable to allocate memory.\n");
		return -ENOMEM;
	}

	ads1271_driver->spi = spi;
	INIT_LIST_HEAD(&(ads1271_driver->dev_list_entry));
	mutex_init(&(ads1271_driver->dev_mutex));
	init_completion(&(ads1271_driver->reader_wait));
	init_completion(&(ads1271_driver->stop_wait));
	spin_lock_init(&(ads1271_driver->drv_vars_lock));
	tasklet_init(&(ads1271_driver->tasklet), tasklet_handler, (unsigned long) ads1271_driver);

	driver = dev_get_drvdata(&(spi->master->dev));
	// thats a little hacky, but it works
	ads1271_driver->old_transfer_handler = driver->transfer_handler;
	driver->transfer_handler = ads1271_dma_transfer;
	ads1271_driver->old_rx = driver->rx;
	driver->rx = ads1271_driver;
	ads1271_driver->old_rx_end = driver->rx_end;
	driver->rx_end = ads1271_driver;
	ads1271_driver->old_cur_msg = driver->cur_msg;
	driver->cur_msg = &(ads1271_driver->spi_message);
	driver->cur_msg->spi = spi;
	reg = driver->ioaddr;
	ads1271_driver->driver = driver;

	// force certain SPI parameters
	spi->bits_per_word = 24;
	spi->max_speed_hz = 2000000;
	spi->mode = SPI_MODE_0; // CPOL=0, CPHA=0
	spi_setup(spi);
	dev_set_drvdata(&(spi->dev), ads1271_driver);
	chip = spi_get_ctldata(spi);
	ads1271_driver->old_cur_chip = driver->cur_chip;
	driver->cur_chip = chip;	
	// set PSP mode
	chip->cr0 = chip->cr0 | SSCR0_PSP;
	// set slave frame, slave clock / read without write (RWOT)
	chip->cr1 = chip->cr1 | SSCR1_RWOT | SSCR1_SFRMDIR | SSCR1_SCLKDIR;
	//
	//chip->psp = chip->psp & ~SSPSP_SFRMP;
	chip->psp = SSPSP_SFRMP | SSPSP_SCMODE(0);
	// printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	//
	write_SSCR0(chip->cr0 & ~SSCR0_SSE, reg);
	write_SSSR(SSSR_ROR | SSSR_TINT, reg);
	write_SSCR1(chip->cr1 | chip->threshold, reg);
	write_SSPSP(chip->psp, reg);
	ads1271_driver->spi_irq_gpio_num = irq_to_gpio(spi->irq);
	// select alt fun
	altfn = 2; // use as SSPSFRM2
	gafr = GAFR(FRAME_GPIO) & ~(0x3 << (((FRAME_GPIO) & 0xf)*2));
	GAFR(FRAME_GPIO) = gafr |  (altfn  << (((FRAME_GPIO) & 0xf)*2));
	//printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	
	/* Get DMA channel for rx */
	driver->rx_channel = -1;
	driver->rx_channel = pxa_request_dma("ads1271_spi_rx_dma",
							DMA_PRIO_HIGH,
							ads1271_dma_handler,
							driver);
	if (driver->rx_channel < 0) {
		printk(KERN_ERR MODULE_NAME ": problem (%d) requesting rx channel\n", driver->rx_channel);
		cleanup(spi, INIT_DRIVER_MEM);
		return -ENODEV;
	}
	else {
		printk(KERN_INFO MODULE_NAME ": reserved dma rx channel %d\n", driver->rx_channel);
	}
	DRCMR(driver->ssp->drcmr_rx) = DRCMR_MAPVLD | driver->rx_channel;
	// alloc dma buffer
	ads1271_driver->buffer = kzalloc(sizeof(struct ads1271_buffer), GFP_KERNEL);
	if (!ads1271_driver->buffer) {
		printk(KERN_ERR MODULE_NAME ": could not allocate memory for buffer struct\n");
		cleanup(spi, INIT_DMA);
		return -ENOMEM;
	}
	ads1271_driver->spare_buffer = kzalloc(sizeof(struct ads1271_buffer), GFP_KERNEL);
	if (!ads1271_driver->spare_buffer) {
		printk(KERN_ERR MODULE_NAME ": could not allocate memory for spare buffer struct\n");
		cleanup(spi, INIT_MEM_BUFFER_STRCT);
		return -ENOMEM;
	}
	ads1271_driver->buffer->sg_cpu = kmalloc(SAMPLE_FIFO_SIZE, GFP_KERNEL);
	if (!ads1271_driver->buffer->sg_cpu) {
		printk(KERN_ERR MODULE_NAME ": could not allocate memory for buffer\n");
		cleanup(spi, INIT_MEM_SPARE_BUFFER_STRCT);
		return -ENOMEM;
	}
	ads1271_driver->spare_buffer->sg_cpu = kmalloc(SAMPLE_FIFO_SIZE, GFP_KERNEL);
	if (!ads1271_driver->spare_buffer->sg_cpu) {
		printk(KERN_ERR MODULE_NAME ": could not allocate memory for spare buffer\n");
		cleanup(spi, INIT_MEM_BUFFER);
		return -ENOMEM;
	}

	mutex_lock(&dev_list_lock);
	minor = find_first_zero_bit(minors, ADS1271_N_MINORS);
	if (minor < ADS1271_N_MINORS) {
		dev_nr = MKDEV(dev_major_nr, minor);
		ads1271_driver->dev = device_create(dev_class, &(spi->dev), dev_nr, ads1271_driver, "ads1271-%d", spi->master->bus_num);
		ret = IS_ERR(ads1271_driver->dev) ? PTR_ERR(ads1271_driver->dev) : 0;
	} else {
		printk(KERN_ERR MODULE_NAME ": no minor number available.\n");
		mutex_unlock(&dev_list_lock);
		cleanup(spi, INIT_MAP_BUFFERS);
		return -ENODEV;
	}
	if (ret == 0) {
		set_bit(minor, minors);
		list_add(&(ads1271_driver->dev_list_entry), &dev_list);
	} else {
		mutex_unlock(&dev_list_lock);
		cleanup(spi, INIT_MAP_BUFFERS);
		return ret;
	}
	mutex_unlock(&dev_list_lock);
	printk(KERN_INFO MODULE_NAME ": device /dev/ads1271-%d created. Major:%d minor:%d.\n", spi->master->bus_num, MAJOR(ads1271_driver->dev->devt), MINOR(ads1271_driver->dev->devt));

	ret = device_create_file(ads1271_driver->dev, &dev_attr_errors);
	if (ret) {
		cleanup(spi, INIT_CREATE_DEVICE);
		return ret;
	}

	printk(KERN_INFO MODULE_NAME ": probe successful.\n");
	return 0;
} //ads1271_probe

/******************************************************************************************
 * This interrupt is triggered when an error occurs in the SPI bus.
 *
 * @param drv_data pointer to the struct struct driver_data* drv_data
 *
 * @return always IRQ_HANDLED
 ******************************************************************************************/
static irqreturn_t ads1271_dma_transfer(struct driver_data *drv_data)
{
	u32 irq_status;
	void __iomem *reg = drv_data->ioaddr;
	struct ads1271_driver_data* ads1271_driver;
	
	irq_status = read_SSSR(reg);
	ads1271_driver = (struct ads1271_driver_data *) drv_data->rx;
	
	if (unlikely(irq_status & SSSR_ROR)) {
		ads1271_driver->rx_fifo_overrun_occured++;
		write_SSSR(SSSR_ROR, reg); // reset ROR bit
	}

	return IRQ_HANDLED;
} // ads1271_dma_transfer

inline static void ads1271_next_dma(struct ads1271_driver_data* ads1271_driver) {
	int channel;
	// map	
	ads1271_driver->buffer->sg_dma = dma_map_single(&ads1271_driver->spi->dev, ads1271_driver->buffer->sg_cpu, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);
	ads1271_driver->buffer->is_mapped = 1;
//  	if (dma_mapping_error(&spi->dev, ads1271_driver->dma_sample_buf)) {
//  		dma_unmap_single(&spi->dev, ads1271_driver->dma_sample_buf, MAX_DMA_LEN, DMA_FROM_DEVICE);
//  		printk(KERN_ERR MODULE_NAME ": problem mapping dma buffer\n");
//  	}
//  	else
// 		printk(KERN_INFO MODULE_NAME ": mapped dma buffer, orig: %x, dma: %x\n", (u32)ads1271_driver->sample_buf, (u32)ads1271_driver->dma_sample_buf);

	// set DMA regs
	channel = ads1271_driver->driver->rx_channel;
	DCSR(channel) = RESET_DMA_CHANNEL;
	DSADR(channel) = ads1271_driver->driver->ssdr_physical;
	DTADR(channel) = ads1271_driver->buffer->sg_dma;
	DCMD(channel) = DCMD_INCTRGADDR
						| DCMD_FLOWSRC
						| DCMD_WIDTH4
						| DCMD_BURST8
						| ads1271_driver->transfer_size
						| DCMD_ENDIRQEN;
	// enable DMA channel
	DCSR(channel) |= DCSR_RUN;
}

/******************************************************************************************
 * This function is called from the DMA interrupt handler when a DMA transfer has finished.
 *
 * @param channel DMA channel number
 * @param data pointer to the struct struct driver_data* drv_data
 *
 * @return none
 ******************************************************************************************/
static void ads1271_dma_handler(int channel, void *data)
{
	struct driver_data *driver;
	struct ads1271_driver_data * ads1271_driver;
	//void __iomem *reg = driver->ioaddr;
	uint32_t oscr_now;
	u32 irq_status;
	struct ads1271_buffer * swp;
	
	oscr_now = OSCR;
	driver = data;
	ads1271_driver = (struct ads1271_driver_data *) driver->rx;
	
	irq_status = DCSR(channel) & DMA_INT_MASK;
	if (irq_status & DCSR_ENDINTR) {
		// finished dma transfer, setup new one
		// clear int
		DCSR(channel)= DCSR_ENDINTR;

		// swap buffers
		swp = ads1271_driver->spare_buffer;
		ads1271_driver->spare_buffer = ads1271_driver->buffer;
		ads1271_driver->buffer = swp;

		switch (ads1271_driver->irqstate) {
		case ADS1271_IRQ_STOP:
			if (oscr_now - ads1271_driver->stop_osc_value >= ads1271_driver->adc_filter_delay) {
				write_SSCR0(driver->cur_chip->cr0 & ~SSCR0_SSE, driver->ioaddr);
				write_SSSR(SSSR_ROR | SSSR_TINT, driver->ioaddr);
				write_SSPSP(driver->cur_chip->psp, driver->ioaddr);
				ads1271_driver->irqstate = ADS1271_IRQ_NONE;
				complete(&(ads1271_driver->stop_wait));
				break;
			}
		default:
			ads1271_next_dma(ads1271_driver);
			if (ads1271_driver->irqstate==ADS1271_IRQ_STARTSAMPLING) {
				ads1271_driver->irqstate=ADS1271_IRQ_SAMPLE;
			}
		}
		ads1271_driver->tasklet_sampling_start = oscr_now;
		// schedule tasklet
		tasklet_schedule(&(ads1271_driver->tasklet));
	}
}

/******************************************************************************************
 *
 * Function is executed when a user opens the dev file
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, -errno otherwise
 *
 ******************************************************************************************/
static int ads1271_prepare_internal_ret;
static int ads1271_open(struct inode *inode, struct file *filp)
{
	struct ads1271_driver_data* ads1271_driver = ads1271_prepare_internal(inode->i_rdev);
	if (ads1271_driver != NULL) {
		// Start measurement:
		// printk(KERN_INFO MODULE_NAME ": start measurement...\n");

		ads1271_start_sampling(ads1271_driver);
		// mark as non-seekable
		nonseekable_open(inode, filp);
		// mark as read-only -> necessary??
		filp->f_mode &= ~(FMODE_WRITE);

		filp->private_data = ads1271_driver;
		return 0;
	} else {
		return ads1271_prepare_internal_ret;
	}
} //ads1271_open

/*******************************************************************************************
 *
 * Prepares the device and ADC for measuring.
 *
 * @param dev_nr Device number of output device.
 *
 * @return nothing
 *
 *******************************************************************************************/
struct ads1271_driver_data* ads1271_prepare_internal(dev_t dev_nr)
{
	struct ads1271_driver_data* ads1271_driver;
	struct chip_data* chip;
	void __iomem *reg;

	ads1271_prepare_internal_ret = ENXIO;

	printk(KERN_INFO MODULE_NAME ": preparing device...\n");

	mutex_lock(&dev_list_lock);
	list_for_each_entry(ads1271_driver, &dev_list, dev_list_entry) {
		if (ads1271_driver->dev->devt == dev_nr) {
			ads1271_prepare_internal_ret = 0;
			break;
		}
	}
	mutex_unlock(&dev_list_lock);
	if (ads1271_prepare_internal_ret) {
		printk(KERN_WARNING MODULE_NAME ": could not find minor.\n");
		return NULL;
	}

	mutex_lock(&(ads1271_driver->dev_mutex));
	if (ads1271_driver->dev_open_count != 0) {
		mutex_unlock(&(ads1271_driver->dev_mutex));
		printk(KERN_WARNING MODULE_NAME ": device already reserved.\n");
		ads1271_prepare_internal_ret = -EBUSY;
		return NULL;
	}

	// Reset variables:
	ads1271_driver->kfifo_full_occured			= 0;
	ads1271_driver->rx_fifo_overrun_occured		= 0;
	ads1271_driver->rx_fifo_empty_occured		= 0;
	ads1271_driver->irqstate					= ADS1271_IRQ_NONE;
	ads1271_driver->is_sampling					= 0;

	// Calculate the sampling period:
	ads1271_prepare_internal_ret = ads1271_change_samplingrate(ads1271_driver, nth_sample);
	if (ads1271_prepare_internal_ret != 0) {
		mutex_unlock(&(ads1271_driver->dev_mutex));
		return NULL;
	}

	ads1271_driver->dev_open_count = 1;

	chip = ads1271_driver->driver->cur_chip;
	reg = ads1271_driver->driver->ioaddr;

	// enable SPI, enable receive timeout, assert chip select
	printk(KERN_ERR MODULE_NAME ": ads1271_prepare_internal: SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	write_SSCR0(chip->cr0 & ~SSCR0_SSE, reg);
	write_SSSR(SSSR_ROR | SSSR_TINT, reg);
	write_SSCR1((chip->cr1 | chip->threshold) & ~SSCR1_RIE, reg);
	printk(KERN_ERR MODULE_NAME ": ads1271_prepare_internal: SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0 | SSCR0_SSE,chip->cr1 | chip->threshold | SSCR1_RIE, chip->psp);
	cs_assert(ads1271_driver->driver);

	try_module_get(THIS_MODULE);

	mutex_unlock(&(ads1271_driver->dev_mutex));

	printk(KERN_INFO MODULE_NAME ": device prepared and opened.\n");

	return ads1271_driver;
} // ads1271_prepare_internal
EXPORT_SYMBOL_GPL(ads1271_prepare_internal);


/*******************************************************************************************
 *
 * Starts a measurement by enabling the chip select pin of the ADC. Before calling this
 * function, the ADC needs to be reserved by calling ads1271_prepare_internal()
 * Function is called from __ossr_interrupt_handler() in flocklab_ostimer.c
 *
 * @param ads1271_driver Pointer to the driver struct of the ADC
 *
 * @return nothing
 *
 *******************************************************************************************/
void ads1271_start_sampling(struct ads1271_driver_data *ads1271_driver)
{
	unsigned long drvvarslock_flags;
	void __iomem *reg;
	struct chip_data* chip;
	
	reg = ads1271_driver->driver->ioaddr;

	// Only start sampling if it is not sampling yet:
	if (ads1271_driver->is_sampling != 1) {
		// printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d, SSSR: %d.\n",read_SSCR0(reg),read_SSCR1(reg),read_SSPSP(reg),read_SSSR(reg));
		// printk(KERN_INFO MODULE_NAME ": start sampling.\n");
		// Reset variables:
		ads1271_driver->is_sampling = 1;
		spin_lock_irqsave(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
		ads1271_driver->irqstate = ADS1271_IRQ_STARTSAMPLING;
		// Enable the interrupt to handle the SPI:
		chip = ads1271_driver->driver->cur_chip;
		write_SSCR1((chip->cr1 | chip->threshold | SSCR1_RSRE) & ~SSCR1_RIE, reg);
		write_SSCR0(chip->cr0 | SSCR0_SSE, reg);
		ads1271_driver->start_osc_value = OSCR;
		spin_unlock_irqrestore(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
		ads1271_next_dma(ads1271_driver);
		__ads1271_oscr_to_timestamp(ads1271_driver->start_osc_value - ads1271_driver->adc_filter_delay, &(ads1271_driver->packet_start_next));
	}
	else {
		printk(KERN_WARNING MODULE_NAME ": start sampling, already sampling.\n");
	}

} // ads1271_start_sampling
EXPORT_SYMBOL_GPL(ads1271_start_sampling);


/*******************************************************************************************
 *
 * Function is executed when a user reads the dev file
 * If ther ringbuffer is empty, the function will block until at least one block is
 * available. If the ringbuffer is not empty, it will read values of the ringbuffer and copy
 * them to user space until either the ringbuffer is empty of the requested number of values
 * are copied to userspace. After any values are read, the function sets overflow_occured to 0.
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, 0 or -EFAULT otherwise.
 *
 *******************************************************************************************/
static ssize_t ads1271_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	struct ads1271_driver_data* ads1271_driver = (struct ads1271_driver_data*) filp->private_data;
	return ads1271_read_internal(ads1271_driver, buffer, length);
} // ads1271_read

ssize_t ads1271_read_internal(struct ads1271_driver_data *ads1271_driver, char *buffer, size_t length)
{
	int ret = 0;
	int len; // number of samples to provide
	unsigned long drvvarslock_flags;
	int skip;
	int * srcbuf, * dstbuf;
	
	dstbuf = (int*)buffer;
	wait_for_completion_interruptible_timeout(&(ads1271_driver->reader_wait), HZ*3);
	spin_lock_irqsave(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
	len = ads1271_driver->packet_len;
	spin_unlock_irqrestore(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
	skip = ads1271_driver->nth_sample;
	len = len / ads1271_driver->nth_sample;
	if (len > 0) {
		if (length < len * sizeof(int)) {
			len = length / sizeof(int);
			printk(KERN_WARNING MODULE_NAME ": read buffer (%d bytes) too short.\n", length);
		}
		ret = len * sizeof(int);
		for (srcbuf = ads1271_driver->spare_buffer->sg_cpu;len > 0;len--) {
			*(dstbuf++)=*srcbuf;
			srcbuf+=skip;
		}
		spin_lock_irqsave(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
		ads1271_driver->packet_len = 0;
		spin_unlock_irqrestore(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
	}
	//printk(KERN_INFO MODULE_NAME ": read %d bytes, time: %d.%06d %d.%06d\n", ret, 
// 			(u32)ads1271_driver->packet_start.tv_sec, (u32)ads1271_driver->packet_start.tv_usec, 
// 			(u32)ads1271_driver->packet_end.tv_sec, (u32)ads1271_driver->packet_end.tv_usec);
	return ret;
} // ads1271_read_internal
EXPORT_SYMBOL_GPL(ads1271_read_internal);


/*******************************************************************************************
 *
 * Stops a measurement by disabling the chip select pin of the ADC.
 *
 * @param ads1271_driver Pointer to the driver struct of the ADC
 *
 * @return nothing
 *
 *******************************************************************************************/
void ads1271_stop_sampling(struct ads1271_driver_data *ads1271_driver)
{
	unsigned long drvvarslock_flags;
	if (!ads1271_driver->is_sampling) {
		printk(KERN_WARNING MODULE_NAME ": already stopped\n");
		return;
	}
	ads1271_driver->stop_osc_value = OSCR;
	// printk(KERN_INFO MODULE_NAME ": stop sampling\n");
	spin_lock_irqsave(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
	ads1271_driver->irqstate = ADS1271_IRQ_STOP;
	spin_unlock_irqrestore(&(ads1271_driver->drv_vars_lock), drvvarslock_flags);
	ads1271_driver->is_sampling = 0;
} // ads1271_stop_sampling
EXPORT_SYMBOL_GPL(ads1271_stop_sampling);

/******************************************************************************************
 *
 * Function is executed when a user closes the dev file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 ******************************************************************************************/
static int ads1271_release(struct inode *inode, struct file *filp)
{
	struct ads1271_driver_data* ads1271_driver = (struct ads1271_driver_data*) filp->private_data;
	return ads1271_release_internal(ads1271_driver);
} // ads1271_release

int ads1271_release_internal(struct ads1271_driver_data* ads1271_driver)
{
	struct spi_device* spi;
	void __iomem *reg;
	struct chip_data* chip;

	printk(KERN_INFO MODULE_NAME ": closing device...\n");

	mutex_lock(&(ads1271_driver->dev_mutex));
	ads1271_driver->dev_open_count = 0;

	// Stop sampling of the adc:
	if (ads1271_driver->is_sampling) {
		ads1271_stop_sampling(ads1271_driver);
		wait_for_completion_interruptible(&(ads1271_driver->stop_wait));
		cs_deassert(ads1271_driver->driver);
	}
	printk(KERN_INFO MODULE_NAME ": stopped sampling\n");

	chip = ads1271_driver->driver->cur_chip;
	reg = ads1271_driver->driver->ioaddr;
	write_SSCR0(chip->cr0 & ~SSCR0_SSE, reg);
	write_SSCR1(chip->cr1 | chip->threshold, reg);
	write_SSSR(SSSR_ROR | SSSR_TINT, reg);
	printk(KERN_INFO MODULE_NAME ": ads1271_release_internal: SSCR0: %d, SSCR1: %d.\n",chip->cr0,chip->cr1);
	spi = ads1271_driver->spi;

	if (ads1271_driver->kfifo_full_occured) {
		printk(KERN_WARNING MODULE_NAME ": kfifo_full_occurred=%u\n", ads1271_driver->kfifo_full_occured);
	}
	if (ads1271_driver->rx_fifo_overrun_occured) {
		printk(KERN_WARNING MODULE_NAME ": rx_fifo_overrun_occurred=%u\n", ads1271_driver->rx_fifo_overrun_occured);
	}
	if (ads1271_driver->rx_fifo_empty_occured) {
		printk(KERN_WARNING MODULE_NAME ": rx_fifo_empty_occurred=%u\n", ads1271_driver->rx_fifo_empty_occured);
	}
	module_put(THIS_MODULE);

	mutex_unlock(&(ads1271_driver->dev_mutex));

	printk(KERN_INFO MODULE_NAME ": device closed.\n");

	return 0;
} // ads1271_release_internal
EXPORT_SYMBOL_GPL(ads1271_release_internal);



/*******************************************************************************************
 *
 * Function to flush the output buffer (kfifo) of the driver and reset the buffer statistics.
 *
 * @param inode Inode
 * @param filp File pointer
 *
 *
 * @return Always returns 0
 *
 *******************************************************************************************/
int ads1271_flush(struct ads1271_driver_data* ads1271_driver)
{
//	unsigned long drvvarslock_flags;

	ads1271_driver->kfifo_full_occured = 0;
	complete(&(ads1271_driver->reader_wait));
	printk(KERN_INFO MODULE_NAME ": sample fifo flushed.\n");

	return 0;
} // ads1271_flush
EXPORT_SYMBOL_GPL(ads1271_flush);



/*******************************************************************************************
 *
 * Function to change the sampling rate of the driver.
 *
 * @param inode Inode
 * @param filp File pointer
 *
 *
 * @return Always returns 0
 *
 *******************************************************************************************/
int ads1271_change_samplingrate(struct ads1271_driver_data* ads1271_driver, unsigned int nth_sample)
{
	int was_sampling;
	struct chip_data* chip;
	u64 sp;
	
	// Check if the driver is currently sampling. If yes, stop sampling before changing the samplingrate:
	was_sampling = ads1271_driver->is_sampling;
	if (unlikely(was_sampling)) {
		ads1271_stop_sampling(ads1271_driver);
		wait_for_completion_interruptible(&(ads1271_driver->stop_wait));
	}
	printk(KERN_INFO MODULE_NAME ": change sampling rate: %u -> %u\n", ads1271_driver->nth_sample, nth_sample);
	ads1271_driver->nth_sample = nth_sample;
	//ads1271_driver->sampling_period = (((adc_clock_ratio * get_clock_tick_rate())/ (adc_clock_hz / 16)) * nth_sample) / 16;
	sp = (u64)ads1271_driver->nth_sample * adc_clock_ratio * 1000000;
	do_div(sp, adc_clock_hz);
	ads1271_driver->sampling_period = sp;
	ads1271_driver->adc_filter_delay = (((adc_clock_ratio * get_clock_tick_rate())/ (adc_clock_hz / 16)) * adc_filter_delay) / 16;
	// set thr
	chip = ads1271_driver->driver->cur_chip;
	chip->threshold = (SSCR1_RxTresh(1) & SSCR1_RFT) | (chip->threshold & ~SSCR1_RFT);
	// set transfer length a multiple of nth_sample
	ads1271_driver->transfer_size = (SAMPLE_FIFO_LENGTH / ads1271_driver->nth_sample) * sizeof(int);
	ads1271_driver->transfer_size *= ads1271_driver->nth_sample;
	if (!ads1271_driver->transfer_size) {
		printk(KERN_ERR MODULE_NAME ": nth_sample too big\n");
		return -EINVAL;
	}
		

	if (unlikely(was_sampling)) {
		ads1271_start_sampling(ads1271_driver);
	}

	printk(KERN_INFO MODULE_NAME ": nth_sample=%u\n", nth_sample);
	printk(KERN_INFO MODULE_NAME ": adc_clock_hz=%uHz\n", adc_clock_hz);
	printk(KERN_INFO MODULE_NAME ": adc_clock_ratio=%u\n", adc_clock_ratio);
	printk(KERN_INFO MODULE_NAME ": adc_filter_delay=%u ticks\n", ads1271_driver->adc_filter_delay);
	printk(KERN_INFO MODULE_NAME ": adc_filter_delay=%uus\n", ads1271_get_filter_delay_us(ads1271_driver));
	printk(KERN_INFO MODULE_NAME ": sampling_period=%uus\n", ads1271_driver->sampling_period);
	printk(KERN_INFO MODULE_NAME ": transfer_size=%u bytes\n", ads1271_driver->transfer_size);

	return 0;
} // ads1271_change_samplingrate
EXPORT_SYMBOL_GPL(ads1271_change_samplingrate);

unsigned int ads1271_get_filter_delay_us(struct ads1271_driver_data* ads1271_driver) {
	return ((u32)(((u64)ads1271_driver->adc_filter_delay * (u64)oscr_mult) >> OSCR_SHIFT)) / 1000;
}
EXPORT_SYMBOL_GPL(ads1271_get_filter_delay_us);

/******************************************************************************************
 * Called when unregistering the ADC from the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return Always returns 0
 ******************************************************************************************/
static int __devexit ads1271_remove(struct spi_device *spi)
{
	struct ads1271_driver_data* ads1271_driver;

	ads1271_driver = spi_get_drvdata(spi);

	mutex_lock(&(ads1271_driver->dev_mutex));
	if (ads1271_driver->dev_open_count != 0) {
		mutex_unlock(&(ads1271_driver->dev_mutex));
		return -EBUSY;
	}
	mutex_unlock(&(ads1271_driver->dev_mutex));
	cleanup(spi, INIT_DONE);

	return 0;
} // ads1271_remove

void cleanup(struct spi_device *spi, int init_state) {
   	struct ads1271_driver_data* ads1271_driver;
	struct driver_data* driver;

	ads1271_driver = spi_get_drvdata(spi);
	driver = ads1271_driver->driver;
	
    switch(init_state) {
		case INIT_DONE:
		case INIT_CREATE_FILE:
			// remove dev file
			device_remove_file(ads1271_driver->dev, &dev_attr_errors);
		case INIT_CREATE_DEVICE:
			// destroy device
			mutex_lock(&dev_list_lock);
			list_del(&(ads1271_driver->dev_list_entry));
			device_destroy(dev_class, ads1271_driver->dev->devt);
			clear_bit(MINOR(ads1271_driver->dev->devt), minors);
			mutex_unlock(&dev_list_lock);
			printk(KERN_INFO MODULE_NAME ": device /dev/ads1271-%d destroyed.\n", spi->master->bus_num);
		case INIT_MAP_BUFFERS:
			// unmap buffers
			if (ads1271_driver->buffer->is_mapped)
				dma_unmap_single(&spi->dev, ads1271_driver->buffer->sg_dma, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);
			if (ads1271_driver->spare_buffer->is_mapped)
				dma_unmap_single(&spi->dev, ads1271_driver->spare_buffer->sg_dma, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);	
			// free dma buffers
			kfree(ads1271_driver->spare_buffer->sg_cpu);
		case INIT_MEM_BUFFER:
			kfree(ads1271_driver->buffer->sg_cpu);
		case INIT_MEM_SPARE_BUFFER_STRCT:
			kfree(ads1271_driver->spare_buffer);
		case INIT_MEM_BUFFER_STRCT:
			kfree(ads1271_driver->buffer);
		case INIT_DMA:
			// release dma channel
			if (driver->rx_channel >= 0) {
				DRCMR(driver->ssp->drcmr_rx) = 0;
				pxa_free_dma(driver->rx_channel);
				printk(KERN_INFO MODULE_NAME ": released dma channel %d\n", driver->rx_channel);
			}
		case INIT_DRIVER_MEM:
			// revert changes on driver_data
			driver->cur_chip = ads1271_driver->old_cur_chip;
			driver->cur_msg = ads1271_driver->old_cur_msg;
			driver->rx = ads1271_driver->old_rx;
			driver->rx_end = ads1271_driver->old_rx_end;
			driver->transfer_handler = ads1271_driver->old_transfer_handler;
			
			//free driver memory
			kfree(ads1271_driver);
            break;
    }
} // cleanup_module

/* ---- Init and Cleanup ----------------------------------------------------------*/
/**
 * Initialization function for the kernel module
 */
static int __init ads1271_init(void)
{
	int rs;
	dev_major_nr = register_chrdev(ADS1271_MAJOR, MODULE_NAME, &ads1271_dev_fops);
	if (dev_major_nr < 0) {
		return dev_major_nr;
	}
	dev_class = class_create(THIS_MODULE, MODULE_NAME);
	if (IS_ERR(dev_class)) {
		unregister_chrdev(dev_major_nr, MODULE_NAME);
		return PTR_ERR(dev_class);
	}

	oscr_mult = clocksource_hz2mult(get_clock_tick_rate(), OSCR_SHIFT);
	
	rs = spi_register_driver(&ads1271_spi_driver);
	if (rs < 0) {
		class_destroy(dev_class);
		unregister_chrdev(dev_major_nr, MODULE_NAME);
	}
	
	return rs;
} // ads1271_init
module_init(ads1271_init);


/**
 * Destruction function for the kernel module
 */
static void __exit ads1271_exit(void)
{
	spi_unregister_driver(&ads1271_spi_driver);
	class_destroy(dev_class);
	unregister_chrdev(dev_major_nr, MODULE_NAME);
} // ads1271_exit
module_exit(ads1271_exit);


/* ---- Module documentation and authoring information --------------------*/
MODULE_AUTHOR("ETH Zurich 2011-2013, Christoph Walser <walserc@tik.ee.ethz.ch>, Mustafa Yuecel <yuecel@tik.ee.ethz.ch>, Roman Lim <lim@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("24-Bit, Wide Bandwith ADC driver for TI ADS1271 on PXA270 using DMA");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.8-"COMPILETIME);
MODULE_SUPPORTED_DEVICE(MODULE_NAME);
