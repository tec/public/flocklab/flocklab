/**
 * @file
 *
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * *
 * @section LICENSE
 *
 * Copyright (c) 2011-2012 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 *
 *
 */

#ifndef ADS1271_H
#define ADS1271_H

#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/spi/spi.h>

#define ADS1271_SAMPLE_DIVIDER_DEFAULT 2

struct ads1271_driver_data* ads1271_prepare_internal(dev_t);
void ads1271_start_sampling(struct ads1271_driver_data*);
void ads1271_stop_sampling(struct ads1271_driver_data*);
ssize_t ads1271_read_internal(struct ads1271_driver_data*, char*, size_t);
int ads1271_release_internal(struct ads1271_driver_data*);
int ads1271_flush(struct ads1271_driver_data*);
int ads1271_change_samplingrate(struct ads1271_driver_data*, unsigned int);
unsigned int ads1271_get_filter_delay_us(struct ads1271_driver_data* ads1271_driver);

enum ads1271_irq_state {
	ADS1271_IRQ_NONE,
	ADS1271_IRQ_STARTSAMPLING,
	ADS1271_IRQ_SAMPLE,
	ADS1271_IRQ_STOP,
};

struct ads1271_buffer {
	unsigned short is_mapped;
	void * sg_cpu;
	dma_addr_t sg_dma;
};

struct ads1271_driver_data {
	unsigned int nth_sample;
	unsigned int spi_irq_gpio_num;
	unsigned int is_sampling;
	struct spi_device* spi;
	struct driver_data* driver;
	struct spi_message spi_message;
	struct list_head dev_list_entry;
	struct device* dev;
	unsigned int dev_open_count;
	struct mutex dev_mutex;
	struct completion reader_wait;
	spinlock_t drv_vars_lock;
	struct timeval packet_start;
	struct timeval packet_start_next;
	struct timeval packet_end;
	unsigned int sampling_period;
	unsigned int packet_len;
	unsigned int kfifo_full_occured;
	unsigned int rx_fifo_overrun_occured;
	unsigned int rx_fifo_empty_occured;
	irqreturn_t (*old_transfer_handler)(struct driver_data *drv_data);
	void *old_rx;
	void *old_rx_end;
	struct spi_message* old_cur_msg;
	struct chip_data *old_cur_chip;
	u32 tasklet_sampling_start;
	u32 tasklet_sampling_end;
	struct tasklet_struct tasklet;
	struct completion stop_wait;
	enum ads1271_irq_state irqstate;
	unsigned int tasklet_packet_len;
	unsigned int adc_filter_delay;	// filter delay in OSC ticks (see datasheet page 24 or http://www.google.com/url?sa=t&rct=j&q=ads1271%20delay&source=web&cd=1&ved=0CEYQFjAA&url=http%3A%2F%2Fwww.ti.com%2Fgeneral%2Fdocs%2Flit%2Fgetliterature.tsp%3FliteratureNumber%3Dsbas355%26fileType%3Dpdf&ei=JzopULXMJKrP4QS_w4DYAw&usg=AFQjCNHL8LCeB1RlSs9QiNhQrimFdP1Oug&cad=rja)
	unsigned int start_osc_value;
	unsigned int stop_osc_value;
	
	struct ads1271_buffer * buffer;
	struct ads1271_buffer * spare_buffer;
	unsigned int transfer_size;
};

#endif 
