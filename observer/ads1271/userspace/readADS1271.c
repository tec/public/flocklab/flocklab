#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/fcntl.h>
#include <unistd.h>

#define DEVICE "/dev/ads1271-2"

#define BUFFER_ENTRIES (8192)
#define BUFFER_SIZE (BUFFER_ENTRIES * sizeof(int))

static struct {
	int n;
	int numread;
	double sum_mA;
	int minlsb;
	int mins;
	double max_mA;
	double min_mA;
} stats;

/* ---- Function declarations ---------------------------------------------*/
int convert_to_mamp(int32_t* buf, int length);
int main(int argc, char *argv[]);

int convert_to_mamp(int32_t* buf, int length) {
	uint i,j;
	char bin[33];
	bin[32]=0;
	int lsb;
	double conv;

	for (i = 0; i < length; i++) {
		stats.n++;
		// Convert ADC value (two's compliment) to integer:
		if (buf[i] & 0x800000) {
			*((char*)&buf[i] + 3) = 0xff; // sign extend 24 -> 32 bit
		}
		conv = ((buf[i] * 19.868217294) / 1e6);
		stats.sum_mA += conv;
		lsb = -1;
		for (j=0;j<32;j++) {			
			bin[31-j]=((buf[i] & (1<<j)) > 0)?'1':'0';
			if ((buf[i] & (1<<j)) > 0 && lsb==-1)
				lsb = j;
		}
		if ((lsb > -1) && (lsb < stats.minlsb)) {
			stats.minlsb = lsb;
			stats.mins = i;
		}
		if (conv > stats.max_mA)
			stats.max_mA = conv;
		if (conv < stats.min_mA)
			stats.min_mA = conv;
		printf("%s, %u, %d, %.3f, %.3f\n", bin, stats.numread+i, buf[i], conv, stats.sum_mA/stats.n);
	}
	
	stats.numread +=i;

	return 0;
}

int main(int argc, char *argv[])
{
	int file;
	int32_t *buf;
	unsigned int n;
	time_t start, end;
	int num, totalnum;
	int numbuf;
	
	if (argc == 2)
		num = strtol(argv[1], (char **) NULL, 10);
	else
		num = BUFFER_ENTRIES;
	totalnum=num;
	buf = malloc(BUFFER_SIZE);
	if (buf == NULL) {
		return -1;
	}

	stats.sum_mA=0;
	stats.n=0;
	stats.numread=0;
	stats.minlsb=32;
	stats.max_mA=0;
	stats.min_mA=9999;
	
	fprintf(stderr, "samples = [\n");
	fprintf(stderr, "binary, n, buf[n], mA[n], avg_mA\n");

	
	file = open(DEVICE, O_RDONLY);
	if (file < 0) {
		fprintf(stderr, "can't open device file %s.\n", DEVICE);
		free(buf);
		return -3;
	}
	start = time(NULL);
	
	while (num>0)
	{
		numbuf = num > BUFFER_ENTRIES?BUFFER_ENTRIES:num;
		n = read(file, (void*)buf, numbuf * sizeof(int));
		numbuf = n / sizeof(int);
		num -= numbuf;
		convert_to_mamp(buf, numbuf);
	}
	end = time(NULL);
	fprintf(stderr, "open_time_s = %u;\n", (int)(end - start));
	close(file);
	
	fprintf(stderr, "Statistics: Samples read: %u, average current[mA]: %.3f, minlsb: %d (lsb=1 first in sample %d), current (min, max): %.3f, %.3f\n",stats.numread, stats.sum_mA/totalnum, stats.minlsb, stats.mins, stats.min_mA, stats.max_mA);

	free(buf);

	return 0;
}
