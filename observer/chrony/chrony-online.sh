#!/bin/sh
# /usr/bin/chrony-online.sh
PASSWORD=`awk '$1 ~ /^1$/ {print $2; exit}' /etc/chrony.keys`
/usr/bin/chronyc << EOF | sed '/^200 OK$/d'
password $PASSWORD
burst 10/100
EOF
exit 0

