#!/bin/sh 

# Author: Christoph Walser, <walserc@tik.ee.ethz.ch>
# Version: $Revision$
# Date: $Date$
# ID: $Id$
# Source: $URL$

THISSCRIPT=chrony_watchdog.sh
DAEMON=chronyd
INITSCRIPT=/etc/init.d/chrony

# Check if daemon is still running and restart it if not:
if ! pgrep $DAEMON &> /dev/null
then
	if [ `netstat -nt | sed '/:22\s\+[0-9]\+.*ESTABLISHED/!d' | wc -l` -eq 0 ]
	then 
		if [ `mount | sed '/\/media\/card/!d' | wc -l` -eq 1 ]
		then
			/usr/bin/logger "$THISSCRIPT: $DAEMON is not running anymore. Trying to restart it..."
			$INITSCRIPT start
		else
			/usr/bin/logger "$THISSCRIPT: $DAEMON is not running anymore. SD card not mounted, skip service restart."
		fi
	else
		/usr/bin/logger "$THISSCRIPT: $DAEMON is not running anymore. There are logged in users, skip service restart."
	fi
fi
