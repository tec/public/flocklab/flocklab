/**
 * @file
 * @author Balz Maag <bmaag@ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * 
 * Configures FPGA with flocklab-service commands according to test configuration
 *
 * $Date: 2014-10-03 11:20:00 +0200 (Fri, 3 October 2014) $
 * $Revision: 1977 $
 * $Id: flocklab_config_daemon.c 1977 2014-10-03 11:20:00Z bmaag$
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <termios.h> // used for UART communication
#include <fcntl.h> 
#include <unistd.h>
#include <time.h>   // used for callback timer
#include <syslog.h>
#include <signal.h>

#include "flocklab_daq_config.h"

#define ENTER_ATOMIC sigprocmask (SIG_BLOCK, &signal_mask, NULL)
#define EXIT_ATOMIC sigprocmask (SIG_UNBLOCK, &signal_mask, NULL)
#define DAQ_CONFIG_DEBUG 0

FILE *fp, *ft;
int fd;
unsigned char startHeader; 	// 
unsigned char *tmpChar;
unsigned char *cmd;
unsigned char *ack;
static int quiet = 1;
static sigset_t signal_mask;
static volatile sig_atomic_t isTerminating = 0;

static void signal_handler(int);

/*
 * 
 * UART communcation settings stuff
 * 
 */

int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                syslog(LOG_ERR, "Error %d from getting serial attributes", errno);
                return 0;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag |= IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                syslog(LOG_ERR, "Error %d from setting serial attributes", errno);
                return 0;
        }
        return 1;
}

void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                syslog(LOG_ERR, "Error %d from getting serial attributes", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                syslog(LOG_ERR, "Error %d from setting serial attributes", errno);
}


/*
 * 
 * FPGA UART packets handling
 * 
 */
int create_command_packet(unsigned char *header, int cmd_size){
  int i; 
  cmd = (unsigned char *)malloc(cmd_size * sizeof(unsigned char));
  cmd[0] = *header;

  for(i = 1; i < cmd_size; i++){
	 fread(tmpChar, sizeof(unsigned char) , 1, fp);
	 // TODO: TEST for header 0b1XXXX, else its a header packet and we have an error
	 cmd[i] = *tmpChar;
  }
  return 1;
}

int send_command_packet(int cmd_size){
  int i, n;
  // TODO error handling
#if DAQ_CONFIG_DEBUG
  char debugstring[cmd_size*2+1];
#endif
  for(i = 0; i < cmd_size; i ++){
	 n = write(fd, &cmd[i], sizeof(unsigned char));
#if DAQ_CONFIG_DEBUG
	 sprintf(&debugstring[i*2], "%02x", cmd[i]);
#endif
	 if(n < 1){
		syslog(LOG_ERR, "Couldn't send command to fpga\n");
	 }	 
  }
#if DAQ_CONFIG_DEBUG
  syslog(LOG_DEBUG, "Command sent to fpga: %s\n", debugstring);
#endif
  return 1;
}

int get_ack(){
  int r;
  r = read(fd, ack, 2*sizeof(unsigned char));

  switch(*ack){
	 case FIFO_FULL:
		return -1;
	 case FIFO_HALF_FULL:
		return 0;
	 default:
		return 1;	 
  }
}
  

struct timespec time_to_start(struct timeval current_time, long start_epoch_sec){
  struct timespec res_startTime; 		// time interval to start of test
  struct timeval target_time;
  
  target_time.tv_sec = start_epoch_sec - 1;
  target_time.tv_usec = 900000L;
  
  if (current_time.tv_sec >= target_time.tv_sec && current_time.tv_usec >= target_time.tv_usec) {
    syslog(LOG_ERR, "Missed test start time!\n");
    res_startTime.tv_sec = 0;
    res_startTime.tv_nsec = 0;
  }
  else {
    res_startTime.tv_sec = target_time.tv_sec - current_time.tv_sec;
    if (target_time.tv_usec >= current_time.tv_usec) {
      res_startTime.tv_nsec = (target_time.tv_usec - current_time.tv_usec) * 1000L;
    }
    else {
      res_startTime.tv_nsec = (target_time.tv_usec - current_time.tv_usec + 1000000L) * 1000L;
      res_startTime.tv_sec--;
    }
  }  
  return res_startTime;  
}

void usage(){
  printf("/---------------------------------------------------------------------------------------\\\n");
	printf("|Usage: flocklab_config_daemon command\t\t\t\t\t\t|\n");
	printf("|Commands:\t\t\t\t\t\t\t\t\t\t|\n");
	printf("|  --file=<path-to-file>\t\t\tLocation of binary command file\t\t|\n");
	printf("|  --quiet\t\t\tDon't verbose debug and error information\t\t|\n");
	printf("\\---------------------------------------------------------------------------------------/\n\n");
}

/*
 * 
 * main routine
 * 
 */
int main(int argc, char *argv[]){  
  unsigned char *tmpStartTime;   // 4 bytes start time information
  long tmpStartSec;					// epoch timestamp of stat time
  struct timeval time_cur;			// current time
  struct timeval ts_act;
  struct timeval ts_wait;
  struct timespec waitActTime;
  struct timespec startTime;
  long actCount;
  int fifo_state;
  int i, r;
  char receivedStart;
  char sentStart;
  char fifoWait;
  char fifo_was_full;
	unsigned int barrier;
	char is_barrier;
 
  
  if ( argc < 2 ) {
		usage();
		syslog(LOG_ERR, "Wrong API use.");
		closelog();
		return -1;
	}
	
	 quiet = 0;
	for (i = 1; i < argc; i++) {
		if (strcmp( argv[i], "--quiet") == 0) {
				quiet = 1;
			break;
		}
	}
	if (quiet) {
		openlog("flocklab_config_daemon", LOG_CONS | LOG_PID, LOG_USER);
	} else {
		openlog("flocklab_config_daemon", LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER);
	}
	
  char *config_file;  
  if(strncmp(argv[1], "--file=", strlen("--file=")) == 0){
	 config_file = strtok(argv[1],"=");
	 config_file = strtok(NULL,"=");
  }
  
  if(config_file == NULL){
	 syslog(LOG_ERR, "Couldn't specify binary config-file.");
  } else {
    // open config file (binary)
	 fp = fopen(config_file, "rb");  
	 if(!fp){
		syslog(LOG_ERR, "Couldn't open binary config-file.");
		return 0;
	 }
  }
  
  //open UART device descriptor
  fd = open(FPGA_UART, O_RDWR | O_NOCTTY | O_SYNC);
  
  if(!fd){
	 syslog(LOG_ERR, "Couldn't open FPGA UART device descriptor.");
  }
  set_interface_attribs(fd, B1000000, 0);  // set speed to 1000000 bps, 8n1 (no parity)
  set_blocking(fd,1);
	
	/* Catch signals */
	signal(SIGINT, signal_handler);
	sigemptyset(&signal_mask);
	sigaddset(&signal_mask, SIGINT);
	ENTER_ATOMIC;
   
  // allocate memory and init sequence-control variables
  tmpChar = malloc(sizeof(unsigned char));
  tmpStartTime = (unsigned char *)malloc(4*sizeof(unsigned char));
  ack = (unsigned char *)malloc(2*sizeof(unsigned char));
  receivedStart = 0;
  sentStart = 0;
  actCount = 0;
  fifoWait = 0;
	gettimeofday(&time_cur, NULL);
	barrier = time_cur.tv_sec - 1;
	is_barrier = 0;
   // read config file byte by byte, because UART packets may differ in length (1,2 or 7 bytes)
  while(fread(tmpChar, sizeof(unsigned char), 1, fp) == 1){
    // check header
    switch((*tmpChar & 0xF0)){
		case H_ROUTE:
//  		  printf("route\n");
		  create_command_packet(tmpChar, N_CMD_SIMPLE);
		  send_command_packet(N_CMD_SIMPLE);
		  get_ack();
		  free(cmd);
		  break;
		case H_TRACE:
// 		  printf("trace\n");
		  create_command_packet(tmpChar, N_CMD_DATA);
		  send_command_packet(N_CMD_DATA);
		  free(cmd);
		  break;
		case H_ACT:
 		  // syslog(LOG_INFO, "act");
		  create_command_packet(tmpChar, N_CMD_TIME);
		  send_command_packet(N_CMD_TIME);
		  gettimeofday(&time_cur, NULL);
		  ts_act.tv_sec = ((cmd[1] & 0x07) << 14) | ((cmd[2] & 0x7F) << 7) | (cmd[3] & 0x7F);
		  ts_act.tv_usec =  ((cmd[4] & 0x7F) << 14) | ((cmd[5] & 0x7F) << 7) | (cmd[6] & 0x7F);
		  ts_act.tv_usec = ts_act.tv_usec / 2;
		  if(ts_act.tv_sec > time_cur.tv_sec || (ts_act.tv_sec == time_cur.tv_sec && ts_act.tv_usec > time_cur.tv_usec)){
			syslog(LOG_ERR,"Actuation command too late: dropped packet at test-time: %ld.%06ld" , ts_act.tv_sec, ts_act.tv_usec);
		  }
		  switch(fifo_state = get_ack()){
			 case -1: // fifo-full, next package will be dropped!
				// if ack returns at more than two times in a row fifo=full, then the current packet was not placed into the fifo 
				if(fifo_was_full == 1){
				  syslog(LOG_ERR, "Actuation FIFO full, actuation command not in FIFO\n");
				}
				fifo_was_full = 1;
				if(ts_act.tv_sec != 0 && ts_act.tv_usec != 0){ 
				  gettimeofday(&time_cur, NULL);
				  waitActTime.tv_sec = ts_wait.tv_sec - (time_cur.tv_sec - tmpStartSec);
				  waitActTime.tv_nsec = ts_wait.tv_usec * 1000;
// 				  printf("%ld sec and %ld nanosec\n", waitActTime.tv_sec, waitActTime.tv_nsec);
				  fifoWait = 0;
				  if(nanosleep(&waitActTime, NULL) == -1){
					 syslog(LOG_ERR, "Invalid sleeping time or interrupt occured");
				  }
				}
				break;
			 case 0:
				// half-full: save timestamp of act packet
				fifo_was_full = 0;
				if(fifoWait == 0){
				  ts_wait.tv_sec = ts_act.tv_sec;
				  ts_wait.tv_usec = ts_act.tv_usec;
				  fifoWait = 1;
				}
				break;
		  }
		  actCount += 1;
		  free(cmd);
		  break;
		case H_NTH:
// 		  printf("nth\n");
		  create_command_packet(tmpChar, N_CMD_DATA);
		  send_command_packet(N_CMD_DATA);
		  get_ack();
		  free(cmd);
		  break;
		case H_START:
		  // save the package header, need 4 more bytes holding the start time
		  startHeader = *tmpChar;
		  // 0x01001111 = start, 0x01000000 = stop
		  if ((*tmpChar & 0x0F) == 0x0F){

			 for(i = 0; i < 4; i++){
				  r = fread(tmpChar, sizeof(unsigned char) , 1, fp);
				  if(r < 1){
					syslog(LOG_ERR, "Couldn't read test start-time\n");
					return 0;
				  }
				  tmpStartTime[i] = *tmpChar;
				}

				tmpStartSec = (tmpStartTime[0] << 24) | (tmpStartTime[1] << 16) | (tmpStartTime[2] << 8) | tmpStartTime[3];
				// better to fill the fifo first as long as there is more than 1sec time left
				receivedStart = 1;
		  } else if ((*tmpChar & 0x0F) == 0x00) {
// 			 printf("stop\n");
			 // TODO: time needed? probably not, stop_test.py is called after the reset pin is pulled high for the last time and we can immediately stop the test as there is no more test-data to be sent
				create_command_packet(&startHeader, N_CMD_SIMPLE);
				send_command_packet(N_CMD_SIMPLE);
				get_ack();
				free(cmd);
		  }
		  
		  break;
		case H_CB:
		  create_command_packet(tmpChar, N_CMD_DATA);
		  send_command_packet(N_CMD_DATA);
		  free(cmd);
		  break;
		case H_BARRIER:
			for(i = 0; i < 4; i++){
				r = fread(tmpChar, sizeof(unsigned char) , 1, fp);
				if(r < 1){
					syslog(LOG_ERR, "Couldn't read barrier time\n");
					return 0;
				}
				tmpStartTime[i] = *tmpChar;
			}
			barrier = (tmpStartTime[0] << 24) | (tmpStartTime[1] << 16) | (tmpStartTime[2] << 8) | tmpStartTime[3];
			is_barrier = 1;
			break;
		case H_ERR:
		  create_command_packet(tmpChar, N_CMD_SIMPLE); 
		  send_command_packet(N_CMD_SIMPLE);
		  free(cmd);
		  break;
		default:
		  break;
    } 
    if(receivedStart == 1 && sentStart == 0){
      gettimeofday(&time_cur, NULL);
      // if the test starts on the next second, we need to trigger the start command 
      // if the fifo is filled up to its limit and the test needs to be started before adding additional packets, we need to trigger the start command
      if ((time_cur.tv_sec >= tmpStartSec - 2) || (actCount >= FIFO_SIZE) || is_barrier){
        startTime = time_to_start(time_cur, tmpStartSec);
        EXIT_ATOMIC;        
        if(isTerminating || nanosleep(&startTime, NULL) == 1){
          if (isTerminating) {
              syslog(LOG_INFO, "Interrupted, shutting down.");
              break;
          }
          syslog(LOG_ERR, "Invalid sleeping time or interrupt occured");
          return 0;
        }
        ENTER_ATOMIC;
        create_command_packet(&startHeader, N_CMD_SIMPLE);
        send_command_packet(N_CMD_SIMPLE);
        get_ack();
        free(cmd);
        sentStart = 1;
        gettimeofday(&time_cur, NULL);
        syslog(LOG_INFO, "Issued start command during setup at %lu.%06lu", (u_long)time_cur.tv_sec, (u_long)time_cur.tv_usec);
      }
    }
    if (is_barrier) {
			gettimeofday(&time_cur, NULL);
			if (time_cur.tv_sec < barrier) {
				// wait until we passed barrier in time..
				syslog(LOG_INFO, "Waiting for barrier time %u at %lu.%06lu for %lus", barrier, (u_long)time_cur.tv_sec, (u_long)time_cur.tv_usec, (u_long)barrier - (u_long)time_cur.tv_sec);
				EXIT_ATOMIC;
				if (!isTerminating)
                    sleep(barrier - time_cur.tv_sec);
				if (isTerminating) {
					syslog(LOG_INFO, "Interrupted, shutting down.");
					break;
				}
				ENTER_ATOMIC;
				syslog(LOG_INFO, "Resume after barrier");
			}
			is_barrier = 0;
		}
  }
  EXIT_ATOMIC;
  
  // we are done transmitting all the necessary commands to the fifo, check if the test is already running, otherwise start it 0.5sec before the starting-time
  if(isTerminating==0 && sentStart == 0){
		gettimeofday(&time_cur, NULL);
		startTime = time_to_start(time_cur, tmpStartSec);
// 		printf("start - fifo: not full, cmds: empty\n");
// 		printf("%ld.%06ld\n", startTime.tv_sec, startTime.tv_nsec);
		if(nanosleep(&startTime, NULL) == 1){
			syslog(LOG_ERR, "Invalid sleeping time or interrupt occured");
			return 0;
		}
		create_command_packet(&startHeader, N_CMD_SIMPLE);
		send_command_packet(N_CMD_SIMPLE);
		get_ack();	
		free(cmd);
    sentStart = 1;
    gettimeofday(&time_cur, NULL);
    syslog(LOG_INFO, "Issued start command after completed setup at %lu.%06lu", (u_long)time_cur.tv_sec, (u_long)time_cur.tv_usec);
  }
  
  closelog();
  free(tmpChar);
  fclose(fp);
  close(fd);
	syslog(LOG_INFO, "configuration done.");
  return 1;
} //main

static void signal_handler(int sig)
{
	switch(sig) {
		case SIGINT:
			// Set the termination flag for main()
			isTerminating = 1;
			break;
		default:
			break;
	}
	return;
} // signal_handler
