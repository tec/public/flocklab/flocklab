#define CONFIG_FILE "/media/card/FlockDAQ/test_config/daq_config"

// FPGA UART device
#define FPGA_UART "/dev/flocklab/usb/daq3"
#define BAUD = 1000000

// FPGA UART headers
#define H_ROUTE 		0x00
#define H_TRACE 		0x10
#define H_ACT 			0x20
#define H_NTH 			0x30
#define H_START 		0x40
#define H_CB 			0x50
#define H_ERR 			0x70
// internal header for barrier
#define H_BARRIER		0x60

// command size (bytes)
#define N_CMD_SIMPLE 1
#define N_CMD_DATA	2
#define N_CMD_TIME   7

// commands
#define CMD_START 0x4F
#define CMD_STOP 0x40

// act-fifo control
#define FIFO_FULL 0x5F
#define FIFO_HALF_FULL 0x41 // NOTE: filling level of half-full flag is user-defined and currently at 1500 out of 2048
#define ACT_WAIT_OFFSET 2500000L // nsec
#define FIFO_SIZE 2047

// Free UART Header, might be used for later purposes
#define H_UNUSED 0x60

