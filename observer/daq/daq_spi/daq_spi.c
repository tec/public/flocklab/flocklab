/**
 * @file
 *
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 * @author Roman Lim <lim@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * Copyright (c) 2011-2012 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date: 2013-09-25 12:08:26 +0200 (Wed, 25 Sep 2013) $
 * $Revision: 2428 $
 * $Id: daq_spi.c 2428 2013-09-25 10:08:26Z walserc $
 * $URL: https://svn.ee.ethz.ch/flocklab/trunk/observer/daq_spi/daq_spi.c $
 *
 *
 */

/**
 * @section DESCRIPTION
 *
 * Use this module to read values from the Analog-to-Digital converter TI DAQ_SPI
 * The module generates a device file at /dev/daq_spi which can be read values from.
 *
 */

/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/completion.h>
#include <linux/spi/spi.h>
#include <linux/clocksource.h>
#include <mach/regs-ost.h>
#include <mach/ssp.h>
#include <mach/regs-ssp.h>
#include <mach/pxa2xx_spi.h>
#include <mach/pxa27x.h>
#include <mach/dma.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <asm/uaccess.h>
#include "daq_spi.h"

#define MY_MACIG 'G'
#define READ_TRAILING _IOW(MY_MACIG, 1, int)
#define FRAME_GPIO 14

#define MODULE_NAME "daq_spi"

// major = 0 -> dynamically allocated number
#define DAQ_SPI_MAJOR 0
#define DAQ_SPI_N_MINORS 8
static DECLARE_BITMAP(minors, DAQ_SPI_N_MINORS);

#define DMA_INT_MASK		(DCSR_ENDINTR | DCSR_EORINTR | DCSR_BUSERR)
#define RESET_DMA_CHANNEL	DMA_INT_MASK // new DMA_INT_MASK old: (DCSR_NODESC | DMA_INT_MASK)
#define IS_DMA_ALIGNED(x)	((((u32)(x)) & 0x07) == 0)
#define MAX_DMA_LEN		8188 // old: 8191, new:4096
#define DMA_ALIGNMENT		8
// Size of FIFO which stores samples.
// Make sure that FIFO is large enough to hold all samples according to packet_threshold
// Should be synchronized with DAQ_SPI_SAMPLE_FIFO_LENGTH in flocklab_powerprof_shared.h
#define SAMPLE_FIFO_LENGTH (MAX_DMA_LEN / sizeof(int))
#define SAMPLE_FIFO_SIZE (SAMPLE_FIFO_LENGTH * sizeof(int))

#define NUM_DMA_BUFFERS 1024 // should be at least 2

static int dev_major_nr;
static struct class* dev_class;

// static ushort nth_sample = DAQ_SPI_SAMPLE_DIVIDER_DEFAULT;
// module_param(nth_sample, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
// MODULE_PARM_DESC(nth_sample, "retrieve each n-th sample (2,4,6,...,2047, default=2).");
static uint32_t adc_clock_hz = 14745600;
// module_param(adc_clock_hz, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
// MODULE_PARM_DESC(adc_clock_hz, "Clock speed in Hz of the ADC oscillator (default=14745600).");
static ushort adc_clock_ratio = 512;
// module_param(adc_clock_ratio, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
// MODULE_PARM_DESC(adc_clock_ratio, "Clock ratio of the currently set ADC mode (512 for High-Resolution/Low-Power mode, 256 for High-Speed mode, default=512).");
static ushort adc_filter_delay = 39;
// module_param(adc_filter_delay, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
// MODULE_PARM_DESC(adc_filter_delay, "Filter delay of the currently set ADC mode in samples (39 for High-Resolution mode, 38 for Low-Power/High-Speed mode, default=39).");

static LIST_HEAD(dev_list);
static DEFINE_MUTEX(dev_list_lock);

// Variables for OS timer
static unsigned int oscr_mult;
#define OSCR_SHIFT 20

/**
 * All available SSP registers (c.f. PXA270 Processor Developer's Manual). (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
#define DEFINE_SSP_REG(reg, off) \
static inline u32 read_##reg(void const __iomem *p) \
{ return __raw_readl(p + (off)); } \
\
static inline void write_##reg(u32 v, void __iomem *p) \
{ __raw_writel(v, p + (off)); }
DEFINE_SSP_REG(SSCR0, 0x00)
DEFINE_SSP_REG(SSCR1, 0x04)
DEFINE_SSP_REG(SSSR, 0x08)
DEFINE_SSP_REG(SSITR, 0x0c)
DEFINE_SSP_REG(SSDR, 0x10)
DEFINE_SSP_REG(SSTO, 0x28)
DEFINE_SSP_REG(SSPSP, 0x2c)

/**
 * Struct for storing all communication properties, which are necessary for using the SPI protocol and the SSP registers. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
struct driver_data {
        /* Driver model hookup */
        struct platform_device *pdev;

        /* SSP Info */
        struct ssp_device *ssp;
        /* SPI framework hookup */
        enum pxa_ssp_type ssp_type;
        struct spi_master *master;

        /* PXA hookup */
        struct pxa2xx_spi_master *master_info;

        /* DMA setup stuff */
        int rx_channel;
        int tx_channel;
        u32 *null_dma_buf;

        /* SSP register addresses */
        void __iomem *ioaddr;
        u32 ssdr_physical;

        /* SSP masks*/
        u32 dma_cr1;
        u32 int_cr1;
        u32 clear_sr;
        u32 mask_sr;

        /* Driver message queue */
        struct workqueue_struct *workqueue;
        struct work_struct pump_messages;
        spinlock_t lock;
        struct list_head queue;
        int busy;
        int run;

        /* Message Transfer pump */
        struct tasklet_struct pump_transfers;

        /* Current message transfer state info */
        struct spi_message* cur_msg;
        struct spi_transfer* cur_transfer;
        struct chip_data *cur_chip;
        size_t len;
        void *tx;
        void *tx_end;
        void *rx;
        void *rx_end;
        int dma_mapped;
        dma_addr_t rx_dma;
        dma_addr_t tx_dma;
        size_t rx_map_len;
        size_t tx_map_len;
        u8 n_bytes;
        u32 dma_width;
        int (*write)(struct driver_data *drv_data);
        int (*read)(struct driver_data *drv_data);
        irqreturn_t (*transfer_handler)(struct driver_data *drv_data);
        void (*cs_control)(u32 command);
};

/**
 * Struct for storing all communication properties of the selected ADC chip.  (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
struct chip_data {
	u32 cr0;
	u32 cr1;
	u32 psp;
	u32 timeout;
	u8 n_bytes;
	u32 dma_width;
	u32 dma_burst_size;
	u32 threshold;
	u32 dma_threshold;
	u8 enable_dma;
	u8 bits_per_word;
	u32 speed_hz;
	int gpio_cs;
	int gpio_cs_inverted;
	int (*write)(struct driver_data *drv_data);
	int (*read)(struct driver_data *drv_data);
	void (*cs_control)(u32 command);
};


/* ---- Function declarations ---------------------------------------------*/
static int daq_spi_probe(struct spi_device*);
static int __devexit daq_spi_remove(struct spi_device*);
static irqreturn_t daq_spi_dma_transfer(struct driver_data *drv_data);
//static void tasklet_handler(unsigned long data);
static int daq_spi_open(struct inode*, struct file*);
static ssize_t daq_spi_read(struct file*, char*, size_t, loff_t*);
static int daq_spi_release(struct inode*, struct file*);
static void daq_spi_dma_handler(int channel, void *data);
void cleanup(struct spi_device *spi, int init_state);
static void* aligned_malloc(size_t size, size_t alignment);
static void aligned_free(void* p);
static int daq_spi_ioctl(struct inode *inode, struct file *filep, unsigned int cmd, unsigned long arg);


static struct spi_driver daq_spi_driver = {
	.driver = {
		.name	= "ads1271",
		.owner	= THIS_MODULE,
	},
	.probe		= daq_spi_probe,
	.remove		= __devexit_p(daq_spi_remove),
};

static const struct file_operations daq_spi_dev_fops = {
	.owner		= THIS_MODULE,
	.open		= daq_spi_open,
	.read		= daq_spi_read,
	.release	= daq_spi_release,
	.ioctl 	= daq_spi_ioctl,
};

/* ---- Enums to for init / cleanup ---------------------------------------------*/
enum {
	INIT_DRIVER_MEM,
	INIT_MEM_BUFFER_STRCT,
	INIT_DMA,
	INIT_CREATE_DEVICE,
	INIT_CREATE_FILE,
	INIT_DONE,
};

/**
 * Configures the EOR timeout based on the ioctl command by the flocklab_dbd
 */

int daq_spi_ioctl(struct inode *inode, struct file *filep, unsigned int cmd, unsigned long arg) {
//  void __iomem *reg;
 int channel;
 unsigned long drvvarslock_flags;
 struct daq_spi_driver_data* daq_spi_driver = (struct daq_spi_driver_data*) filep->private_data;
 
 if (cmd == READ_TRAILING){
	printk(KERN_ERR MODULE_NAME ": Request for trailing bytes\n");
	daq_spi_driver->stop_requested = 1;
	channel = daq_spi_driver->driver->rx_channel;
	// tell channel to stop after EOR
	spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	DCSR(channel) |= DCSR_EORSTOPEN;
	spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	printk(KERN_ERR MODULE_NAME ": stop DMA channel on EOR\n");
 }
 return 1;
}


/**
 * Sets the chip_select output on hardware high. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
static void cs_assert(struct driver_data *drv_data)
{
	struct chip_data *chip = drv_data->cur_chip;

	if (chip->cs_control) {
		chip->cs_control(PXA2XX_CS_ASSERT);
		return;
	}

	if (gpio_is_valid(chip->gpio_cs))
		gpio_set_value(chip->gpio_cs, chip->gpio_cs_inverted);
}


/**
 * Sets the chip_select output on hardware low. (copied from driver/spi/pxa27x_spi.c due to limited scope)
 */
static void cs_deassert(struct driver_data *drv_data)
{
	struct chip_data *chip = drv_data->cur_chip;

	if (chip->cs_control) {
		chip->cs_control(PXA2XX_CS_DEASSERT);
		return;
	}

	if (gpio_is_valid(chip->gpio_cs))
		gpio_set_value(chip->gpio_cs, !chip->gpio_cs_inverted);
}


static ssize_t daq_spi_sysfs_show_errors(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct spi_device* spi;
	struct daq_spi_driver_data* daq_spi_driver;
	int cnt = 0;

	spi = to_spi_device(dev);
	if (spi == NULL) {
		return -ENODEV;
	}

	daq_spi_driver = spi_get_drvdata(spi);

	if (daq_spi_driver->kfifo_full_occured) {
		cnt += sprintf(buf + cnt, "kfifo_full_occured=%u\n", daq_spi_driver->kfifo_full_occured);
	}
	if (daq_spi_driver->rx_fifo_overrun_occured) {
		cnt += sprintf(buf + cnt, "rx_fifo_overrun_occured=%u\n", daq_spi_driver->rx_fifo_overrun_occured);
	}
	//new:
	if (daq_spi_driver->dma_buffer_not_read_occured) {
		cnt += sprintf(buf + cnt, "dma_buffer_not_read_occured=%u\n", daq_spi_driver->dma_buffer_not_read_occured);
	}
	if (daq_spi_driver->dma_buffer_empty_occured) {
		cnt += sprintf(buf + cnt, "dma_buffer_empty_occured=%u\n", daq_spi_driver->dma_buffer_empty_occured);
	}
	if (daq_spi_driver->rx_fifo_empty_occured) {
		cnt += sprintf(buf + cnt, "rx_fifo_empty_occured=%u\n", daq_spi_driver->rx_fifo_empty_occured);
	}

	return cnt;
} //daq_spi_sysfs_show_errors
static DEVICE_ATTR(errors, S_IRUGO, daq_spi_sysfs_show_errors, NULL);

/******************************************************************************************
 * Probing function which is automatically called when ADC is registered on the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return 0 on success, -errno otherwise
 ******************************************************************************************/
static int daq_spi_probe(struct spi_device *spi)
{
	int ret, i;
	struct driver_data* driver;
	struct daq_spi_driver_data* daq_spi_driver;
	struct chip_data* chip;
	void __iomem *reg;
	unsigned long minor;
	dev_t dev_nr;
	u32 altfn, gafr;

	daq_spi_driver = kzalloc(sizeof(struct daq_spi_driver_data), GFP_KERNEL);
	if (daq_spi_driver == NULL) {
		printk(KERN_ERR MODULE_NAME ": unable to allocate memory.\n");
		return -ENOMEM;
	}
	
	daq_spi_driver->spi = spi;
	INIT_LIST_HEAD(&(daq_spi_driver->dev_list_entry));
	mutex_init(&(daq_spi_driver->dev_mutex));
	init_completion(&(daq_spi_driver->reader_wait));
	init_completion(&(daq_spi_driver->stop_wait));
	spin_lock_init(&(daq_spi_driver->drv_vars_lock));
	//tasklet_init(&(daq_spi_driver->tasklet), tasklet_handler, (unsigned long) daq_spi_driver);

	driver = dev_get_drvdata(&(spi->master->dev));
	// thats a little hacky, but it works
	daq_spi_driver->old_transfer_handler = driver->transfer_handler;
	driver->transfer_handler = daq_spi_dma_transfer;
	daq_spi_driver->old_rx = driver->rx;
	driver->rx = daq_spi_driver;
	daq_spi_driver->old_rx_end = driver->rx_end;
	driver->rx_end = daq_spi_driver;
	daq_spi_driver->old_cur_msg = driver->cur_msg;
	driver->cur_msg = &(daq_spi_driver->spi_message);
	driver->cur_msg->spi = spi;
	reg = driver->ioaddr;
	daq_spi_driver->driver = driver;

	// force certain SPI parameters
	spi->bits_per_word = 32;
	spi->max_speed_hz = 12000000;
	spi->mode = SPI_MODE_0; // CPOL=0, CPHA=0
	spi_setup(spi);
	dev_set_drvdata(&(spi->dev), daq_spi_driver);
	chip = spi_get_ctldata(spi);
	daq_spi_driver->old_cur_chip = driver->cur_chip;
	driver->cur_chip = chip;	
	// set PSP mode
	chip->cr0 = chip->cr0 | SSCR0_PSP;
	// set slave frame, slave clock / read without write (RWOT)
	chip->cr1 = chip->cr1 | SSCR1_RWOT | SSCR1_SFRMDIR | SSCR1_SCLKDIR;
	chip->cr1 = chip->cr1 & ~SSCR1_TRAIL;
	chip->cr1 = chip->cr1 & ~SSCR1_TINTE;
	
	//chip->psp = chip->psp & ~SSPSP_SFRMP;
	chip->psp = SSPSP_SFRMP | SSPSP_SCMODE(0);
	printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	//
	write_SSCR0(chip->cr0 & ~SSCR0_SSE, reg);
	write_SSSR(SSSR_ROR | SSSR_TINT, reg);
	write_SSCR1(chip->cr1 | chip->threshold, reg);
	write_SSPSP(chip->psp, reg);
	daq_spi_driver->spi_irq_gpio_num = irq_to_gpio(spi->irq);

	// select alt fun
	altfn = 2; // use as SSPSFRM2
	gafr = GAFR(FRAME_GPIO) & ~(0x3 << (((FRAME_GPIO) & 0xf)*2));
	GAFR(FRAME_GPIO) = gafr |  (altfn  << (((FRAME_GPIO) & 0xf)*2));
	//printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	
	/* Get DMA channel for rx */
	driver->rx_channel = -1;
	driver->rx_channel = pxa_request_dma("daq_spi_rx_dma",
							DMA_PRIO_HIGH,
							daq_spi_dma_handler,
							driver);
	if (driver->rx_channel < 0) {
		printk(KERN_ERR MODULE_NAME ": problem (%d) requesting rx channel\n", driver->rx_channel);
		cleanup(spi, INIT_DRIVER_MEM);
		return -ENODEV;
	}
	else {
		printk(KERN_INFO MODULE_NAME ": reserved dma rx channel %d\n", driver->rx_channel);
	}
	DRCMR(driver->ssp->drcmr_rx) = DRCMR_MAPVLD | driver->rx_channel;
	
	// allocate DMA buffers
	daq_spi_driver->buffer = kzalloc(NUM_DMA_BUFFERS * sizeof(struct daq_spi_buffer), GFP_KERNEL);
	if (!daq_spi_driver->buffer) {
		printk(KERN_ERR MODULE_NAME ": could not allocate memory for buffer struct\n");
		cleanup(spi, INIT_DMA);
		return -ENOMEM;
	}
	
	// for each buffer, allocate descriptors and data memory
	for (i=0;i<NUM_DMA_BUFFERS;i++) {
		(daq_spi_driver->buffer + i)->dma_desc_cpu = (struct dma_desc*) aligned_malloc(sizeof(struct dma_desc), 16);
		(daq_spi_driver->buffer + i)->sg_cpu = kmalloc(SAMPLE_FIFO_SIZE, GFP_KERNEL);
		if ((daq_spi_driver->buffer + i)->dma_desc_cpu == NULL || (daq_spi_driver->buffer + i)->sg_cpu == NULL) {
			printk(KERN_ERR MODULE_NAME ": could not allocate memory for DMA buffers and/or DMA channel descriptors\n");
			cleanup(spi, INIT_MEM_BUFFER_STRCT);
			return -ENOMEM;
		}
	}

	mutex_lock(&dev_list_lock);
	minor = find_first_zero_bit(minors, DAQ_SPI_N_MINORS);
	if (minor < DAQ_SPI_N_MINORS) {
		dev_nr = MKDEV(dev_major_nr, minor);
		daq_spi_driver->dev = device_create(dev_class, &(spi->dev), dev_nr, daq_spi_driver, "daq_spi-%d", spi->master->bus_num);
		ret = IS_ERR(daq_spi_driver->dev) ? PTR_ERR(daq_spi_driver->dev) : 0;
	} else {
		printk(KERN_ERR MODULE_NAME ": no minor number available.\n");
		mutex_unlock(&dev_list_lock);
		cleanup(spi, INIT_MEM_BUFFER_STRCT);
		return -ENODEV;
	}
	if (ret == 0) {
		set_bit(minor, minors);
		list_add(&(daq_spi_driver->dev_list_entry), &dev_list);
	} else {
		mutex_unlock(&dev_list_lock);
		cleanup(spi, INIT_MEM_BUFFER_STRCT);
		return ret;
	}
	mutex_unlock(&dev_list_lock);
	printk(KERN_INFO MODULE_NAME ": device /dev/daq_spi-%d created. Major:%d minor:%d.\n", spi->master->bus_num, MAJOR(daq_spi_driver->dev->devt), MINOR(daq_spi_driver->dev->devt));

	ret = device_create_file(daq_spi_driver->dev, &dev_attr_errors);
	if (ret) {
		cleanup(spi, INIT_CREATE_DEVICE);
		return ret;
	}

	printk(KERN_INFO MODULE_NAME ": probe successful.\n");
	return 0;
} //daq_spi_probe

/******************************************************************************************
 * This interrupt is triggered when an error occurs in the SPI bus.
 *
 * @param drv_data pointer to the struct struct driver_data* drv_data
 *
 * @return always IRQ_HANDLED
 ******************************************************************************************/
static irqreturn_t daq_spi_dma_transfer(struct driver_data *drv_data)
{
	u32 irq_status;
	void __iomem *reg = drv_data->ioaddr;
	struct daq_spi_driver_data* daq_spi_driver;
	
	irq_status = read_SSSR(reg);
	daq_spi_driver = (struct daq_spi_driver_data *) drv_data->rx;
	
	if (unlikely(irq_status & SSSR_ROR)) {
		daq_spi_driver->rx_fifo_overrun_occured++;
		write_SSSR(SSSR_ROR, reg); // reset ROR bit
// 		printk(KERN_INFO MODULE_NAME ": fifo overrun occured, SSSR = %d\n", read_SSSR(reg));
		
	}

	return IRQ_HANDLED;
} // daq_spi_dma_transfer

/******************************************************************************************
 * This funktion initially sets up the DMA and its descriptor chain.
 *
 * @param 
 *
 * @return 
 ******************************************************************************************/
inline static void daq_spi_setup_dma(struct daq_spi_driver_data* daq_spi_driver) {
	int channel;
	int i;
	struct daq_spi_buffer *daqbuf;
	
	for (i=0;i<NUM_DMA_BUFFERS;i++) {
		daqbuf = daq_spi_driver->buffer + i;
		daqbuf->sg_dma = dma_map_single(&daq_spi_driver->spi->dev, daqbuf->sg_cpu, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);
		daqbuf->dma_desc_dma = dma_map_single(&daq_spi_driver->spi->dev, daqbuf->dma_desc_cpu, sizeof(struct dma_desc), DMA_FROM_DEVICE);
		daqbuf->is_mapped = 1;
// 		printk(KERN_INFO MODULE_NAME ": mapped DMA memory %d, descriptor: 0x%x, 0x%x.\n", i, (u32)daqbuf->dma_desc_cpu,(u32)daqbuf->dma_desc_dma);
	}
	for (i=0;i<NUM_DMA_BUFFERS;i++) {
		daqbuf = daq_spi_driver->buffer + i;
		//set Descriptor
		daqbuf->dma_desc_cpu->ddadr = (daq_spi_driver->buffer + (i + 1) % NUM_DMA_BUFFERS)->dma_desc_dma; //(u32)daq_spi_driver->dma_desc_cpu2 | 0b1; // last 4 bits need to be 0
		//daqbuf->dma_desc_cpu->ddadr |= 0b1;
		daqbuf->dma_desc_cpu->dsadr = daq_spi_driver->driver->ssdr_physical;
		daqbuf->dma_desc_cpu->dtadr = daqbuf->sg_dma;
		daqbuf->dma_desc_cpu->dcmd = DCMD_INCTRGADDR
							| DCMD_FLOWSRC
							| DCMD_WIDTH4
							| DCMD_BURST8
							| daq_spi_driver->transfer_size
							| DCMD_ENDIRQEN;
	}
	
	//set initial DMA regs
	channel = daq_spi_driver->driver->rx_channel;
	DCSR(channel) = RESET_DMA_CHANNEL;
	DDADR(channel) = daq_spi_driver->buffer->dma_desc_dma; //(u32)daq_spi_driver->dma_desc1;
	
	// enable DMA channel
	DCSR(channel) |= DCSR_RUN | DCSR_EORINTR | DCSR_EORIRQEN;
}

// Alignment must be power of 2 (1,2,4,8,16...)
static void* aligned_malloc(size_t size, size_t alignment) {
  uintptr_t r = (uintptr_t)kmalloc(size + --alignment + sizeof(uintptr_t), GFP_KERNEL);
  uintptr_t t = r + sizeof(uintptr_t);
  uintptr_t o =(t + alignment) & ~(uintptr_t)alignment;
  if (!r) return NULL;
  ((uintptr_t*)o)[-1] = r;
  return (void*)o;
}

static void aligned_free(void* p) {
   if (!p) return;
   kfree((void*)(((uintptr_t*)p)[-1]));
}


/******************************************************************************************
 * This function is called from the DMA interrupt handler when a DMA transfer has finished.
 *
 * @param channel DMA channel number
 * @param data pointer to the struct struct driver_data* drv_data
 *
 * @return none
 ******************************************************************************************/
static void daq_spi_dma_handler(int channel, void *data)
{
	struct driver_data *driver;
	struct daq_spi_driver_data * daq_spi_driver;
 	void __iomem *reg;
	u32 irq_status;
	unsigned int len_read_dma;
 	driver = data;

	daq_spi_driver = (struct daq_spi_driver_data *) driver->rx;
	
 	reg = driver->ioaddr;
	
	irq_status = DCSR(channel) & DMA_INT_MASK;
	if (irq_status & (DCSR_ENDINTR | DCSR_EORINTR)){
		// finished dma transfer, setup new one
		if (irq_status & DCSR_ENDINTR) {
			// clear int
			DCSR(channel) |= DCSR_ENDINTR;
			len_read_dma = daq_spi_driver->transfer_size;
			//printk(KERN_WARNING MODULE_NAME ": full buffer, len_read_dma = %d\n", len_read_dma);
		}
		else {
			// EOR flag is set, reset it
			DCSR(channel) |= DCSR_EORINTR;
			len_read_dma = DCMD(channel) & 0x01FFF;
			len_read_dma = daq_spi_driver->transfer_size - len_read_dma;
			printk(KERN_DEBUG MODULE_NAME ": trailing bytes, len_read_dma = %d\n", len_read_dma);
			if (!(DCSR(channel) & DCSR_EORSTOPEN))
				return; // wait for buffer beeing filled
		}

		if (daq_spi_driver->irqstate==DAQ_SPI_IRQ_STARTSAMPLING) {
			daq_spi_driver->irqstate=DAQ_SPI_IRQ_SAMPLE;
		}
		
		//new:
		if((daq_spi_driver->buffer + daq_spi_driver->dma_write_buffer_idx)->packet_len != 0) {
		  daq_spi_driver->dma_buffer_not_read_occured++;
		  printk(KERN_WARNING MODULE_NAME ": Ooops, buffer wasnt fully read. remaining words: len=%d, DCSR=%x\n", (daq_spi_driver->buffer + daq_spi_driver->dma_write_buffer_idx)->packet_len, DCSR(channel));
		}
		(daq_spi_driver->buffer + daq_spi_driver->dma_write_buffer_idx)->packet_len = len_read_dma / sizeof(int);
		daq_spi_driver->dma_write_buffer_idx++;
		if (daq_spi_driver->dma_write_buffer_idx == NUM_DMA_BUFFERS)
			daq_spi_driver->dma_write_buffer_idx = 0;
// 		printk(KERN_WARNING MODULE_NAME ": DMA done w: %d, r: %d\n", daq_spi_driver->dma_write_buffer_idx, daq_spi_driver->next_buffer_idx_to_read);
		complete(&(daq_spi_driver->reader_wait));
	}
	else {
		printk(KERN_WARNING MODULE_NAME ": some DCSR error: irq_status: %d\n", DCSR(channel));
	}
}

/******************************************************************************************
 *
 * Function is executed when a user opens the dev file
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, -errno otherwise
 *
 ******************************************************************************************/
static int daq_spi_prepare_internal_ret;
static int daq_spi_open(struct inode *inode, struct file *filp)
{
	struct daq_spi_driver_data* daq_spi_driver = daq_spi_prepare_internal(inode->i_rdev);
	if (daq_spi_driver != NULL) {
		// Start measurement:
		// printk(KERN_INFO MODULE_NAME ": start measurement...\n");

		daq_spi_start_sampling(daq_spi_driver);
		// mark as non-seekable
		nonseekable_open(inode, filp);
		// mark as read-only -> necessary??
		filp->f_mode &= ~(FMODE_WRITE);

		filp->private_data = daq_spi_driver;
		return 0;
	} else {
		return daq_spi_prepare_internal_ret;
	}
} //daq_spi_open

/*******************************************************************************************
 *
 * Prepares the device and ADC for measuring.
 *
 * @param dev_nr Device number of output device.
 *
 * @return nothing
 *
 *******************************************************************************************/
struct daq_spi_driver_data* daq_spi_prepare_internal(dev_t dev_nr)
{
	struct daq_spi_driver_data* daq_spi_driver;
	struct chip_data* chip;
	void __iomem *reg;

	daq_spi_prepare_internal_ret = ENXIO;

	printk(KERN_INFO MODULE_NAME ": preparing device...\n");

	mutex_lock(&dev_list_lock);
	list_for_each_entry(daq_spi_driver, &dev_list, dev_list_entry) {
		if (daq_spi_driver->dev->devt == dev_nr) {
			daq_spi_prepare_internal_ret = 0;
			break;
		}
	}
	mutex_unlock(&dev_list_lock);
	if (daq_spi_prepare_internal_ret) {
		printk(KERN_WARNING MODULE_NAME ": could not find minor.\n");
		return NULL;
	}

	mutex_lock(&(daq_spi_driver->dev_mutex));
	if (daq_spi_driver->dev_open_count != 0) {
		mutex_unlock(&(daq_spi_driver->dev_mutex));
		printk(KERN_WARNING MODULE_NAME ": device already reserved.\n");
		daq_spi_prepare_internal_ret = -EBUSY;
		return NULL;
	}

	// Reset variables:
	daq_spi_driver->kfifo_full_occured			= 0;
	daq_spi_driver->rx_fifo_overrun_occured		= 0;
	daq_spi_driver->rx_fifo_empty_occured		= 0;
	daq_spi_driver->irqstate					= DAQ_SPI_IRQ_NONE;
	daq_spi_driver->is_sampling					= 0;
	//new:
	daq_spi_driver->dma_buffer_empty_occured = 0;
	daq_spi_driver->dma_buffer_not_read_occured = 0;
	
	// Calculate the sampling period:
	daq_spi_prepare_internal_ret = daq_spi_change_samplingrate(daq_spi_driver, 1);
	if (daq_spi_prepare_internal_ret != 0) {
		mutex_unlock(&(daq_spi_driver->dev_mutex));
		return NULL;
	}

	daq_spi_driver->dev_open_count = 1;

	chip = daq_spi_driver->driver->cur_chip;
	reg = daq_spi_driver->driver->ioaddr;

	// enable SPI, enable receive timeout, assert chip select
	printk(KERN_ERR MODULE_NAME ": daq_spi_prepare_internal: SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0,chip->cr1,chip->psp);
	write_SSCR0(chip->cr0 & ~SSCR0_SSE, reg);
	write_SSSR(SSSR_ROR | SSSR_TINT, reg);
	write_SSCR1((chip->cr1 | chip->threshold) & ~SSCR1_RIE, reg);
	printk(KERN_ERR MODULE_NAME ": daq_spi_prepare_internal: SSCR0: %d, SSCR1: %d, SSPSP: %d.\n",chip->cr0 | SSCR0_SSE,chip->cr1 | chip->threshold | SSCR1_RIE, chip->psp);
	cs_assert(daq_spi_driver->driver);

	try_module_get(THIS_MODULE);

	mutex_unlock(&(daq_spi_driver->dev_mutex));

	printk(KERN_INFO MODULE_NAME ": device prepared and opened.\n");

	return daq_spi_driver;
} // daq_spi_prepare_internal
// EXPORT_SYMBOL_GPL(daq_spi_prepare_internal);


/*******************************************************************************************
 *
 * Starts a measurement by enabling the chip select pin of the ADC. Before calling this
 * function, the ADC needs to be reserved by calling daq_spi_prepare_internal()
 * Function is called from __ossr_interrupt_handler() in flocklab_ostimer.c
 *
 * @param daq_spi_driver Pointer to the driver struct of the ADC
 *
 * @return nothing
 *
 *******************************************************************************************/
void daq_spi_start_sampling(struct daq_spi_driver_data *daq_spi_driver)
{
	unsigned long drvvarslock_flags;
	void __iomem *reg;
	struct chip_data* chip;
	
	reg = daq_spi_driver->driver->ioaddr;

	// Only start sampling if it is not sampling yet:
	if (daq_spi_driver->is_sampling != 1) {
		// printk(KERN_ERR MODULE_NAME ": SSCR0: %d, SSCR1: %d, SSPSP: %d, SSSR: %d.\n",read_SSCR0(reg),read_SSCR1(reg),read_SSPSP(reg),read_SSSR(reg));
		printk(KERN_INFO MODULE_NAME ": start sampling.\n");
		// Reset variables:
		daq_spi_driver->is_sampling = 1;
		daq_spi_driver->stop_requested = 0;
		spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
		daq_spi_driver->irqstate = DAQ_SPI_IRQ_STARTSAMPLING;
		// Enable the interrupt to handle the SPI:
		chip = daq_spi_driver->driver->cur_chip;
		write_SSTO(0xffffff, reg); // set timeout to max value
		write_SSCR1((chip->cr1 | chip->threshold | SSCR1_RSRE | SSCR1_TRAIL ) & ~SSCR1_RIE, reg);
		write_SSCR0(chip->cr0 | SSCR0_SSE, reg);
// 		daq_spi_driver->start_osc_value = OSCR;
		spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
		
		daq_spi_setup_dma(daq_spi_driver);// new: 
	}
	else {
		printk(KERN_WARNING MODULE_NAME ": start sampling, already sampling.\n");
	}

} // daq_spi_start_sampling
// EXPORT_SYMBOL_GPL(daq_spi_start_sampling);


/*******************************************************************************************
 *
 * Function is executed when a user reads the dev file
 * If ther ringbuffer is empty, the function will block until at least one block is
 * available. If the ringbuffer is not empty, it will read values of the ringbuffer and copy
 * them to user space until either the ringbuffer is empty of the requested number of values
 * are copied to userspace. After any values are read, the function sets overflow_occured to 0.
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, 0 or -EFAULT otherwise.
 *
 *******************************************************************************************/
static ssize_t daq_spi_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	struct daq_spi_driver_data* daq_spi_driver = (struct daq_spi_driver_data*) filp->private_data;
	return daq_spi_read_internal(daq_spi_driver, buffer, length);
} // daq_spi_read

ssize_t daq_spi_read_internal(struct daq_spi_driver_data *daq_spi_driver, char *buffer, size_t length)
{
	int ret = 0;
	int rs;
	int len; // number of samples to provide
	int new_len = 0;
	unsigned long drvvarslock_flags;
	int * srcbuf, * dstbuf;
//	u32 trail_status;
	void __iomem *reg;
	
	reg = daq_spi_driver->driver->ioaddr;	
	dstbuf = (int*)buffer;
	spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	if ((daq_spi_driver->buffer + daq_spi_driver->next_buffer_idx_to_read)->packet_len == 0) {
		spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
		do {
			rs = wait_for_completion_interruptible_timeout(&(daq_spi_driver->reader_wait), HZ*3);
			if (rs < 0) {
				printk(KERN_DEBUG MODULE_NAME ": read interrupted, num to go %d\n", DCMD(daq_spi_driver->driver->rx_channel) & 0x01FFF);
				// return if interrupted or timed out
				return ret;
			}
			else if (rs == 0) {
				printk(KERN_DEBUG MODULE_NAME ": read timeout (%d), num to go %d\n", rs, DCMD(daq_spi_driver->driver->rx_channel) & 0x01FFF);
				if (daq_spi_driver->stop_requested) {
					printk(KERN_DEBUG MODULE_NAME ": stop requested\n");
					daq_spi_stop_sampling(daq_spi_driver);
					// check buffer state
					spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
					(daq_spi_driver->buffer + daq_spi_driver->dma_write_buffer_idx)->packet_len = (daq_spi_driver->transfer_size - (DCMD(daq_spi_driver->driver->rx_channel) & 0x01FFF)) / sizeof(int);
					// return if timed out after stop request
					if ((daq_spi_driver->buffer + daq_spi_driver->next_buffer_idx_to_read)->packet_len > 0) {
						spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
						printk(KERN_WARNING MODULE_NAME ": buffer not empty\n");
						rs = 1;
					}
					else {
						spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
						printk(KERN_WARNING MODULE_NAME ": buffer empty\n");
						return ret;
					}
				}
			}
		} while (rs <= 0);
		spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	}
	INIT_COMPLETION(daq_spi_driver->reader_wait);
	len = (daq_spi_driver->buffer + daq_spi_driver->next_buffer_idx_to_read)->packet_len;
	spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
//	 trail_status = read_SSSR(reg);
// 	 printk(KERN_INFO MODULE_NAME ": user is reading file, SSSR= %d, SSCR1= %d\n", trail_status, read_SSCR1(reg));

	if (len > 0) {
		//printk(KERN_WARNING MODULE_NAME ": read buffer (%d bytes) ,len: %d\n", length, len);
		if (length < len * sizeof(int)) {
			printk(KERN_WARNING MODULE_NAME ": read buffer (%d bytes) too short. len: %d\n", length, len);
			new_len = len - length / sizeof(int);
			len = length / sizeof(int);
		}
		ret = len * sizeof(int);
		srcbuf = (daq_spi_driver->buffer + daq_spi_driver->next_buffer_idx_to_read)->sg_cpu;
		memcpy(dstbuf, srcbuf, len * sizeof(int));
		spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
		(daq_spi_driver->buffer + daq_spi_driver->next_buffer_idx_to_read)->packet_len = new_len;
		spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
		if (new_len == 0) {
			daq_spi_driver->next_buffer_idx_to_read++;
			if (daq_spi_driver->next_buffer_idx_to_read == NUM_DMA_BUFFERS)
				daq_spi_driver->next_buffer_idx_to_read = 0;
		}
	} else {
	  daq_spi_driver->dma_buffer_empty_occured++;
	  //printk(KERN_WARNING MODULE_NAME ": read buffer empty. len==0\n");
// 	  *(dstbuf++)= 0xffffffff; //better to wait for len != 0
	  ret = 0;
	}
	//printk(KERN_INFO MODULE_NAME ": read %d bytes, time: %d.%06d %d.%06d\n", ret, 
// 			(u32)daq_spi_driver->packet_start.tv_sec, (u32)daq_spi_driver->packet_start.tv_usec, 
// 			(u32)daq_spi_driver->packet_end.tv_sec, (u32)daq_spi_driver->packet_end.tv_usec);
	return ret;
} // daq_spi_read_internal
// EXPORT_SYMBOL_GPL(daq_spi_read_internal);


/*******************************************************************************************
 *
 * Stops a measurement by disabling the chip select pin of the ADC.
 *
 * @param daq_spi_driver Pointer to the driver struct of the ADC
 *
 * @return nothing
 *
 *******************************************************************************************/
void daq_spi_stop_sampling(struct daq_spi_driver_data *daq_spi_driver)
{
	unsigned long drvvarslock_flags;
	struct driver_data* driver;
	if (!daq_spi_driver->is_sampling) {
		printk(KERN_WARNING MODULE_NAME ": already stopped\n");
		return;
	}
	driver = daq_spi_driver->driver;
	spin_lock_irqsave(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	write_SSCR0(driver->cur_chip->cr0 & ~SSCR0_SSE, driver->ioaddr);
	write_SSSR(SSSR_ROR | SSSR_TINT, driver->ioaddr);
	write_SSPSP(driver->cur_chip->psp, driver->ioaddr);
	daq_spi_driver->irqstate = DAQ_SPI_IRQ_NONE;
	spin_unlock_irqrestore(&(daq_spi_driver->drv_vars_lock), drvvarslock_flags);
	daq_spi_driver->is_sampling = 0;
} // daq_spi_stop_sampling
// EXPORT_SYMBOL_GPL(daq_spi_stop_sampling);

/******************************************************************************************
 *
 * Function is executed when a user closes the dev file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 ******************************************************************************************/
static int daq_spi_release(struct inode *inode, struct file *filp)
{
	struct daq_spi_driver_data* daq_spi_driver = (struct daq_spi_driver_data*) filp->private_data;
	return daq_spi_release_internal(daq_spi_driver);
} // daq_spi_release

int daq_spi_release_internal(struct daq_spi_driver_data* daq_spi_driver)
{
	struct spi_device* spi;
	void __iomem *reg;
	struct chip_data* chip;

	printk(KERN_INFO MODULE_NAME ": closing device...\n");

	mutex_lock(&(daq_spi_driver->dev_mutex));
	daq_spi_driver->dev_open_count = 0;
	
	// Stop sampling of the adc:
	daq_spi_stop_sampling(daq_spi_driver);
	cs_deassert(daq_spi_driver->driver);
	printk(KERN_INFO MODULE_NAME ": stopped sampling\n");

	chip = daq_spi_driver->driver->cur_chip;
	reg = daq_spi_driver->driver->ioaddr;
	write_SSCR1(chip->cr1 | chip->threshold, reg);
	printk(KERN_INFO MODULE_NAME ": daq_spi_release_internal: SSCR0: %d, SSCR1: %d.\n",chip->cr0,chip->cr1);
	spi = daq_spi_driver->spi;
	
// 	DCSR(daq_spi_driver->driver->rx_channel) = RESET_DMA_CHANNEL; //TODO. correct?
// 	printk(KERN_INFO MODULE_NAME ": stopped DMA\n");
	
	if (daq_spi_driver->kfifo_full_occured) {
		printk(KERN_WARNING MODULE_NAME ": kfifo_full_occurred=%u\n", daq_spi_driver->kfifo_full_occured);
	}
	if (daq_spi_driver->rx_fifo_overrun_occured) {
		printk(KERN_WARNING MODULE_NAME ": rx_fifo_overrun_occurred=%u\n", daq_spi_driver->rx_fifo_overrun_occured);
	}
	//new:
	if (daq_spi_driver->dma_buffer_not_read_occured) {
		printk(KERN_WARNING MODULE_NAME ": dma_buffer_not_read_occured=%u\n", daq_spi_driver->dma_buffer_not_read_occured);
	}
	if (daq_spi_driver->dma_buffer_empty_occured) {
		printk(KERN_WARNING MODULE_NAME ": dma_buffer_empty_occured=%u\n", daq_spi_driver->dma_buffer_empty_occured);
	}
	if (daq_spi_driver->rx_fifo_empty_occured) {
		printk(KERN_WARNING MODULE_NAME ": rx_fifo_empty_occurred=%u\n", daq_spi_driver->rx_fifo_empty_occured);
	}
	module_put(THIS_MODULE);

	mutex_unlock(&(daq_spi_driver->dev_mutex));

	printk(KERN_INFO MODULE_NAME ": device closed.\n");

	return 0;
} // daq_spi_release_internal
// EXPORT_SYMBOL_GPL(daq_spi_release_internal);



/*******************************************************************************************
 *
 * Function to flush the output buffer (kfifo) of the driver and reset the buffer statistics.
 *
 * @param inode Inode
 * @param filp File pointer
 *
 *
 * @return Always returns 0
 *
 *******************************************************************************************/
int daq_spi_flush(struct daq_spi_driver_data* daq_spi_driver)
{
	daq_spi_driver->kfifo_full_occured = 0;
	complete(&(daq_spi_driver->reader_wait));
	printk(KERN_INFO MODULE_NAME ": sample fifo flushed.\n");
	
	return 0;
} // daq_spi_flush
// EXPORT_SYMBOL_GPL(daq_spi_flush);



/*******************************************************************************************
 *
 * Function to change the sampling rate of the driver.
 *
 * @param inode Inode
 * @param filp File pointer
 *
 *
 * @return Always returns 0
 *
 *******************************************************************************************/
int daq_spi_change_samplingrate(struct daq_spi_driver_data* daq_spi_driver, unsigned int nth_sample)
{
	int was_sampling;
	struct chip_data* chip;
	u64 sp;
	
	// Check if the driver is currently sampling. If yes, stop sampling before changing the samplingrate:
	was_sampling = daq_spi_driver->is_sampling;
	if (unlikely(was_sampling)) {
		daq_spi_stop_sampling(daq_spi_driver);
		wait_for_completion_interruptible(&(daq_spi_driver->stop_wait));
	}
	printk(KERN_INFO MODULE_NAME ": change sampling rate: %u -> %u\n", daq_spi_driver->nth_sample, nth_sample);
	daq_spi_driver->nth_sample = nth_sample;
	//daq_spi_driver->sampling_period = (((adc_clock_ratio * get_clock_tick_rate())/ (adc_clock_hz / 16)) * nth_sample) / 16;
	sp = (u64)daq_spi_driver->nth_sample * adc_clock_ratio * 1000000;
	do_div(sp, adc_clock_hz);
	daq_spi_driver->sampling_period = sp;
	daq_spi_driver->adc_filter_delay = (((adc_clock_ratio * get_clock_tick_rate())/ (adc_clock_hz / 16)) * adc_filter_delay) / 16;
	// set thr
	chip = daq_spi_driver->driver->cur_chip;
	chip->threshold = (SSCR1_RxTresh(8) & SSCR1_RFT) | (chip->threshold & ~SSCR1_RFT);
	// set transfer length a multiple of nth_sample
	daq_spi_driver->transfer_size = (SAMPLE_FIFO_LENGTH / daq_spi_driver->nth_sample) * sizeof(int);
	daq_spi_driver->transfer_size *= daq_spi_driver->nth_sample;
	if (!daq_spi_driver->transfer_size) {
		printk(KERN_ERR MODULE_NAME ": nth_sample too big\n");
		return -EINVAL;
	}
		

	if (unlikely(was_sampling)) {
		daq_spi_start_sampling(daq_spi_driver);
	}

	printk(KERN_INFO MODULE_NAME ": nth_sample=%u\n", nth_sample);
	printk(KERN_INFO MODULE_NAME ": adc_clock_hz=%uHz\n", adc_clock_hz);
	printk(KERN_INFO MODULE_NAME ": adc_clock_ratio=%u\n", adc_clock_ratio);
	printk(KERN_INFO MODULE_NAME ": adc_filter_delay=%u ticks\n", daq_spi_driver->adc_filter_delay);
	printk(KERN_INFO MODULE_NAME ": adc_filter_delay=%uus\n", daq_spi_get_filter_delay_us(daq_spi_driver));
	printk(KERN_INFO MODULE_NAME ": sampling_period=%uus\n", daq_spi_driver->sampling_period);
	printk(KERN_INFO MODULE_NAME ": transfer_size=%u bytes\n", daq_spi_driver->transfer_size);

	return 0;
} // daq_spi_change_samplingrate
// EXPORT_SYMBOL_GPL(daq_spi_change_samplingrate);

unsigned int daq_spi_get_filter_delay_us(struct daq_spi_driver_data* daq_spi_driver) {
	return ((u32)(((u64)daq_spi_driver->adc_filter_delay * (u64)oscr_mult) >> OSCR_SHIFT)) / 1000;
}
// EXPORT_SYMBOL_GPL(daq_spi_get_filter_delay_us);

/******************************************************************************************
 * Called when unregistering the ADC from the SPI
 *
 * @param spi Pointer to the ADC driver
 *
 * @return Always returns 0
 ******************************************************************************************/
static int __devexit daq_spi_remove(struct spi_device *spi)
{
	struct daq_spi_driver_data* daq_spi_driver;

	daq_spi_driver = spi_get_drvdata(spi);

	mutex_lock(&(daq_spi_driver->dev_mutex));
	if (daq_spi_driver->dev_open_count != 0) {
		mutex_unlock(&(daq_spi_driver->dev_mutex));
		return -EBUSY;
	}
	mutex_unlock(&(daq_spi_driver->dev_mutex));
	cleanup(spi, INIT_DONE);

	return 0;
} // daq_spi_remove

void cleanup(struct spi_device *spi, int init_state) {
	struct daq_spi_driver_data* daq_spi_driver;
	struct driver_data* driver;
	int i;

	daq_spi_driver = spi_get_drvdata(spi);
	driver = daq_spi_driver->driver;
	
    switch(init_state) {
		case INIT_DONE:
		case INIT_CREATE_FILE:
			// remove dev file
			device_remove_file(daq_spi_driver->dev, &dev_attr_errors);
		case INIT_CREATE_DEVICE:
			// destroy device
			mutex_lock(&dev_list_lock);
			list_del(&(daq_spi_driver->dev_list_entry));
			device_destroy(dev_class, daq_spi_driver->dev->devt);
			clear_bit(MINOR(daq_spi_driver->dev->devt), minors);
			mutex_unlock(&dev_list_lock);
			printk(KERN_INFO MODULE_NAME ": device /dev/daq_spi-%d destroyed.\n", spi->master->bus_num);
		case INIT_MEM_BUFFER_STRCT:
			// check for allocated and mapped buffers, free allocated buffers...
			for (i=0;i<NUM_DMA_BUFFERS;i++) {
				if ((daq_spi_driver->buffer + i)->dma_desc_cpu != NULL) {
					if ((daq_spi_driver->buffer + i)->is_mapped)
						dma_unmap_single(&spi->dev, (daq_spi_driver->buffer + i)->dma_desc_dma, sizeof(struct dma_desc), DMA_FROM_DEVICE);
					aligned_free((daq_spi_driver->buffer + i)->dma_desc_cpu);
				}
				if ((daq_spi_driver->buffer + i)->sg_cpu != NULL) {
					if ((daq_spi_driver->buffer + i)->is_mapped)
						dma_unmap_single(&spi->dev, (daq_spi_driver->buffer + i)->sg_dma, SAMPLE_FIFO_SIZE, DMA_FROM_DEVICE);
					kfree((daq_spi_driver->buffer + i)->sg_cpu);
				}
			}
			kfree(daq_spi_driver->buffer);
		case INIT_DMA:
			// release dma channel
			if (driver->rx_channel >= 0) {
				DRCMR(driver->ssp->drcmr_rx) = 0;
				pxa_free_dma(driver->rx_channel);
				printk(KERN_INFO MODULE_NAME ": released dma channel %d\n", driver->rx_channel);
			}
		case INIT_DRIVER_MEM:
			// revert changes on driver_data
			driver->cur_chip = daq_spi_driver->old_cur_chip;
			driver->cur_msg = daq_spi_driver->old_cur_msg;
			driver->rx = daq_spi_driver->old_rx;
			driver->rx_end = daq_spi_driver->old_rx_end;
			driver->transfer_handler = daq_spi_driver->old_transfer_handler;
			
			//free driver memory
			kfree(daq_spi_driver);
            break;
    }
} // cleanup_module

/* ---- Init and Cleanup ----------------------------------------------------------*/
/**
 * Initialization function for the kernel module
 */
static int __init daq_spi_init(void)
{
	int rs;
	dev_major_nr = register_chrdev(DAQ_SPI_MAJOR, MODULE_NAME, &daq_spi_dev_fops);
	if (dev_major_nr < 0) {
		return dev_major_nr;
	}

	dev_class = class_create(THIS_MODULE, MODULE_NAME);
	if (IS_ERR(dev_class)) {
		unregister_chrdev(dev_major_nr, MODULE_NAME);
		return PTR_ERR(dev_class);
	}

	oscr_mult = clocksource_hz2mult(get_clock_tick_rate(), OSCR_SHIFT);
	
	rs = spi_register_driver(&daq_spi_driver);
	if (rs < 0) {
		class_destroy(dev_class);
		unregister_chrdev(dev_major_nr, MODULE_NAME);
	}
	
	return rs;
} // daq_spi_init
module_init(daq_spi_init);


/**
 * Destruction function for the kernel module
 */
static void __exit daq_spi_exit(void)
{
	spi_unregister_driver(&daq_spi_driver);
	class_destroy(dev_class);
	unregister_chrdev(dev_major_nr, MODULE_NAME);
} // daq_spi_exit
module_exit(daq_spi_exit);


/* ---- Module documentation and authoring information --------------------*/
MODULE_AUTHOR("ETH Zurich 2014, Christoph Walser <walserc@tik.ee.ethz.ch>, Mustafa Yuecel <yuecel@tik.ee.ethz.ch>, Roman Lim <lim@tik.ee.ethz.ch>, Balz Maag <balz.maag@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("32-Bit, Wide Bandwith SPI driver for DAQ(FPGA) on PXA270 using DMA");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.9-"COMPILETIME);
MODULE_SUPPORTED_DEVICE(MODULE_NAME);
