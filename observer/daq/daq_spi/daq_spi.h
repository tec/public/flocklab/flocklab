/**
 * @file
 *
 * @author Mustafa Yuecel <yuecel@tik.ee.ethz.ch>
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * *
 * @section LICENSE
 *
 * Copyright (c) 2011-2012 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date: 2013-01-18 20:32:54 +0100 (Fri, 18 Jan 2013) $
 * $Revision: 2220 $
 * $Id: daq_spi.h 2220 2013-01-18 19:32:54Z rlim $
 * $URL: https://svn.ee.ethz.ch/flocklab/trunk/observer/daq_spi/daq_spi.h $
 *
 *
 */

#ifndef DAQ_SPI_H
#define DAQ_SPI_H

#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/spi/spi.h>

#define DAQ_SPI_SAMPLE_DIVIDER_DEFAULT 2

struct daq_spi_driver_data* daq_spi_prepare_internal(dev_t);
void daq_spi_start_sampling(struct daq_spi_driver_data*);
void daq_spi_stop_sampling(struct daq_spi_driver_data*);
ssize_t daq_spi_read_internal(struct daq_spi_driver_data*, char*, size_t);
int daq_spi_release_internal(struct daq_spi_driver_data*);
int daq_spi_flush(struct daq_spi_driver_data*);
int daq_spi_change_samplingrate(struct daq_spi_driver_data*, unsigned int);
unsigned int daq_spi_get_filter_delay_us(struct daq_spi_driver_data* daq_spi_driver);

enum daq_spi_irq_state {
	DAQ_SPI_IRQ_NONE,
	DAQ_SPI_IRQ_STARTSAMPLING,
	DAQ_SPI_IRQ_SAMPLE,
	DAQ_SPI_IRQ_STOP,
};

/**
 * Struct for storing a DMA descriptor
 */
struct dma_desc {
	u32 ddadr;
	u32 dsadr;
	u32 dtadr;
	u32 dcmd;
};

struct daq_spi_buffer {
	void * sg_cpu;
	dma_addr_t sg_dma;
	struct dma_desc *dma_desc_cpu;
	dma_addr_t dma_desc_dma;
	unsigned short is_mapped;
	unsigned int packet_len;
};

struct daq_spi_driver_data {
	unsigned int nth_sample;
	unsigned int spi_irq_gpio_num;
	unsigned int is_sampling;
	struct spi_device* spi;
	struct driver_data* driver;
	struct spi_message spi_message;
	struct list_head dev_list_entry;
	struct device* dev;
	unsigned int dev_open_count;
	struct mutex dev_mutex;
	struct completion reader_wait;
	spinlock_t drv_vars_lock;
	struct timeval packet_start;
	struct timeval packet_start_next;
	struct timeval packet_end;
	unsigned int sampling_period;
	unsigned int kfifo_full_occured;
	unsigned int rx_fifo_overrun_occured;
	unsigned int rx_fifo_empty_occured;
	unsigned int dma_buffer_empty_occured;
	unsigned int dma_buffer_not_read_occured;
	irqreturn_t (*old_transfer_handler)(struct driver_data *drv_data);
	void *old_rx;
	void *old_rx_end;
	struct spi_message* old_cur_msg;
	struct chip_data *old_cur_chip;
	u32 tasklet_sampling_start;
	u32 tasklet_sampling_end;
	struct tasklet_struct tasklet;
	struct completion stop_wait;
	enum daq_spi_irq_state irqstate;
	unsigned int adc_filter_delay;	// filter delay in OSC ticks (see datasheet page 24 or http://www.google.com/url?sa=t&rct=j&q=daq_spi%20delay&source=web&cd=1&ved=0CEYQFjAA&url=http%3A%2F%2Fwww.ti.com%2Fgeneral%2Fdocs%2Flit%2Fgetliterature.tsp%3FliteratureNumber%3Dsbas355%26fileType%3Dpdf&ei=JzopULXMJKrP4QS_w4DYAw&usg=AFQjCNHL8LCeB1RlSs9QiNhQrimFdP1Oug&cad=rja)
	unsigned int start_osc_value;
	unsigned int stop_osc_value;	
	struct daq_spi_buffer * buffer;
	unsigned int next_buffer_idx_to_read;
	unsigned int dma_write_buffer_idx;
	unsigned int transfer_size;
  unsigned int stop_requested;
};

#endif 
