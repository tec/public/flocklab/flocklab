#!/bin/bash -l
#
# Author: Balz Maag, (bmaag@tik.ee.ethz.ch)
#
# $Revision: 1 $
# $Id: flockdaq_observer_patch.sh 2849 2014-11-11 09:20:00Z bmaag $
# $URL: https://svn.ee.ethz.ch/flocklab/trunk/observer/daq/flockdaq_observer_patch.sh $
#
# Purpose: Script to patch an observer for the FlockDAQ extension
# Usage: e.g. observer 29 --> ./flockdaq_observer_patch.sh flocklab-observer029 29


########################################################
#
# Constants (set according to your needs)
#
########################################################

# Constants on local computer:
TESTMNGMTPATH="testmanagement"
CONFIGDAEMONPATH="configdaemon"
ADS1271TPATH="ads1271"
DAQSPITPATH="daq_spi"
USERSPACEPATH="userspace"
SCRIPTPATH=`dirname $0`

# Constants on remote Gumstix:
USERSPACEMODULEPATH="/usr/bin";
FLOCKDAQPATH="/media/card/FlockDAQ"

echo "********************************************";
echo "Patching observer for FlockDAQ.";
echo "********************************************";

# # Check for options:
# if [ "$2" ]; then
# 	echo "found option $2"
# fi

GUMSTIX=$1;
if [ -z $GUMSTIX ]; then 
	echo "Enter full DNS name of Gumstix which you want to patch: ";
	read GUMSTIX;
fi
echo "Gumstix to patch: $GUMSTIX";

OBSNUM=`echo $GUMSTIX | sed 's/.*observer[0]*\([0-9]*\).*/\1/'`
echo "Observer number to patch: $OBSNUM";

echo -e "\n/----------------------------------------------------------------------------------------------------------------\\"
echo -e "| WARNING: Make sure that the target is accessible using SSH and that an SD card is inserted and mounted as EXT3.|";
echo -e "| WARNING: Make sure the FlockDAQ Board is mounted and the observer is set up according to setup_new_observer.sh |";
echo -e "| WARNING: If the CC430 has already been flashed with the sync-prot image before, make sure the loggger          |"; 
echo -e "| (synclog.sh) is not running if you want to reflash it (Reset line is controlled by the serial port)            |";
echo -e "\\----------------------------------------------------------------------------------------------------------------/\n"

########################
#
# PATCH
#
#######################

#create FlockDAQ directory
echo -e "\nCreate directories and symbolic links on mmc...\n";
ssh root@$GUMSTIX "mkdir -p $FLOCKDAQPATH/build";
ssh root@$GUMSTIX "mkdir -p $FLOCKDAQPATH/test_config";
ssh root@$GUMSTIX "mkdir -p $FLOCKDAQPATH/various"

# upload files
echo -e "\nUpload adapted scripts, modules and daemon programs...\n"
# start and stop test
scp $TESTMNGMTPATH/flocklab_starttest.py root@$GUMSTIX:$USERSPACEMODULEPATH/.
scp $TESTMNGMTPATH/flocklab_stoptest.py root@$GUMSTIX:$USERSPACEMODULEPATH/.
scp $TESTMNGMTPATH/flocklab_daq_prepare.sh root@$GUMSTIX:$FLOCKDAQPATH/various/.
# config daemon
scp $CONFIGDAEMONPATH/flocklab_config_daemon root@$GUMSTIX:$USERSPACEMODULEPATH/.
scp $CONFIGDAEMONPATH/flocklab_daq_config.py root@$GUMSTIX:$USERSPACEMODULEPATH/.
# adapted ads1271 module
scp $DAQSPITPATH/daq_spi.ko root@$GUMSTIX:/lib/modules/2.6.33.14/kernel/drivers/mfd/.
# adapted flocklab_dbd
scp $USERSPACEPATH/flocklab_dbd root@$GUMSTIX:$USERSPACEMODULEPATH/.
scp fpga/flocklab_daq_route.py root@$GUMSTIX:$USERSPACEMODULEPATH/.

ssh root@$GUMSTIX "chmod +x $FLOCKDAQPATH/various/*"

# udev rules for FTDI ports
echo -e "\nApply new udev rules...\n";
scp 52-flocklab.rules root@$GUMSTIX:/etc/udev/rules.d/;
ssh root@$GUMSTIX "udevadm control --reload-rules"
ssh root@$GUMSTIX "udevadm trigger"

# copy FPGA and CC430 images
echo -e "\nUpload FPGA and CC430 images...\n"
scp fpga/DAQ_Board_rev12.bit root@$GUMSTIX:/lib/firmware/
# compile glossy with node number
cd glossy_sync/apps/glossy-test
./make_syncprot_observer.sh $OBSNUM
scp ./glossy-test.hex root@$GUMSTIX:$FLOCKDAQPATH/build/glossy-obs$OBSNUM.hex
cd ../../../

# libftdi and xc3sprog for FPGA JTAG
echo -e "\nInstall libraries..\n"
ssh root@$GUMSTIX "opkg update; opkg install xc3sprog";

# DAQ_Board_rev12
echo -e "\nConfigure FPGA...\n"
ssh root@$GUMSTIX "xc3sprog -c ft4232h -v -p 0 /lib/firmware/DAQ_Board_rev12.bit;";

# set FPGA to route through
ssh root@$GUMSTIX "python /media/card/FlockDAQ/various/flocklab_daq_route.py"

# add startup script which repgroms the FPGA after every power cycle
echo -e "\nUpload init script\n"
scp ../services/userspace/flocklab.sh root@$GUMSTIX:/etc/init.d/.
ssh root@$GUMSTIX "update-rc.d -f flocklab.sh remove"
ssh root@$GUMSTIX "update-rc.d flocklab.sh defaults 99 01"

#upload new kernel and userspace modulues (smaller event-buffers)
# echo -e "\nUpload flocklab-kernel modules\n"
# scp -r ./kernelspace/lib/modules root@$GUMSTIX:/lib/.

# echo -e "\nUpload flocklab-userspace modules\n"
# scp ./userspace/flocklab_gpiosetting root@$GUMSTIX:/usr/bin/.;
# scp ./userspace/flocklab_gpiomonitor root@$GUMSTIX:/usr/bin/.;
# scp ./userspace/flocklab_powerprofiling root@$GUMSTIX:/usr/bin/.;

ssh root@$GUMSTIX "depmod"

# flash sync-protocol
echo -e "\nFlash CC430...\n"
# NOTE: PYTHONPATH variable is not set if called through ssh
ssh root@$GUMSTIX "export PYTHONPATH=/media/card/opkg/usr/lib/python2.6/; /usr/bin/python -m msp430.bsl5.uart -p /dev/flocklab/usb/daq2 -e -S -V -P $FLOCKDAQPATH/build/glossy-obs$OBSNUM.hex;";

echo -e "\nUpload synchronization-protocol output logger...\n"
scp synclog.sh root@$GUMSTIX:/etc/init.d/. 


echo -e "\n********************************************";
echo -e "PATCH finished.";
echo -e "********************************************";
exit;

