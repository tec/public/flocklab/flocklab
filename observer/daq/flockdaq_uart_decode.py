#!/usr/bin/env python
#
# reads the Flocklab FPGA (FlockDAQ) UART output (command interface)
#
# 2019 rdaforno

import sys
import time
import serial
from serial.serialutil import SerialException

# config
serialPort = '/dev/flocklab/usb/daq3'
baudRate   = 1000000
parameter  = 0x03       # parameter to print (set to 0 to print all of them)


# read from serial port and convert and print the output
def printFPGASerialOutput():
  global serialPort
  global baudRate
  global parameter

  try:
    ser = serial.Serial(port=serialPort, baudrate=baudRate)
    if ser.isOpen():
      print "connected to " + ser.portstr + " (" + str(ser.baudrate) + ")"
      param = 0
      val = 0
      cnt = 0
      lastSec = 0
      output = ""
      # clear the buffers
      ser.flushInput()
      ser.flushOutput()
      # keep reading byte after byte
      while True:
        if ser.inWaiting() > 0:
          c = ser.read()
          if param is 0:
            if ord(c) <= 0x07:
              param = ord(c)
              if param is 0x01:
                output = "hccnt: "
              elif param is 0x02:
                output = "offset: "
              elif param is 0x03:
                output = "diff: "
              elif param is 0x04:
                output = "avg diff: "
              elif param is 0x05:
                output = "diff off cnt: "
              elif param is 0x06:
                output = "full second: "     # unsigned
              elif param is 0x07:
                output = "spread factor: "
          else:
            val = (val << 7) + (ord(c) & 0x7f)  # clear MSB (only 7 data bits)
            cnt = cnt + 1
            if cnt == 4:                # we have all 4 data frames
              if param is not 0x06 and (val & 0x08000000):    # MSB set?
                val = (val & 0x07ffffff) - 0x08000000
              output += "%d\n" % val
              # only print one argument
              if parameter is 0 or param is parameter:
                sys.stdout.write(output)
              param = 0
              cnt = 0
              val = 0
          sys.stdout.flush()
        time.sleep(0.01)
  except SerialException:
    print "device %s unavailable" % serialPort
  except ValueError:
    print "invalid arguments"
  except OSError:
    print "device %s not found" % serialPort
  except KeyboardInterrupt:
    print "\naborted by user"
  except:
    type, value, tb = sys.exc_info()
    print "error: %s (%s)" % (value.message, type)
  if ser.isOpen():
    ser.close()
  print "port closed"


if __name__== "__main__":
  printFPGASerialOutput()
