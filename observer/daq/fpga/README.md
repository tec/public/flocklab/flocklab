## Versions

The FlockDAQ FPGA has two 1 PPS inputs: 
* PPS_IN: Signal from FlockDAQ CC430 which receives the Glossy floods
* GPS_IN: Signal from a GPS receiver

### Glossy Sync
* **Signal path**: GPS -> Glossy flood -> FlockDAQ CC430 -> FlockDAQ FPGA
* Latest bitstream: `DAQ_Board_rev12.bit`

### Direct GPS Sync
* **Signal path**: GPS -> FlockDAQ FPGA
* Latest bitstream: `fpga_bitstream_gps.bit`

## Changelog (default bitstream (Glossy Sync))
### Around 2013
Roman Lim updated the counter granularity by 5x. Width of divider has been adapted accordingly and new bitstream has been generated (but update was not pushed to svn sources, this has been fixed in 2018)

## Old Description text
DAQ_Board_rev12.bit is an image of the DAQ for the FlockDAQ Board revision 1.2.
Major difference to Beni's intial Design are:
* actuation FIFO size is 2048 instead of 128
* the serial lines of the Gumstix are also routed to the FPGA and by default connected directly through, may be used for future purposes, e.g. tracing CTS-pin
* the on-board LED has no special use so far, its turned on if the DAQ booted correctly
