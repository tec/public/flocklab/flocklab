--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:23:43 03/26/2014
-- Design Name:   
-- Module Name:   C:/Users/disslerb/Documents/MaT_VHDL/DAQ_v1.3/act_tester.vhd
-- Project Name:  DAQ_v1.3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: actuator
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY act_tester IS
END act_tester;
 
ARCHITECTURE behavior OF act_tester IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT actuator
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         start_I : IN  std_logic;
         css_I : IN  std_logic_vector(20 downto 0);
         sss_I : IN  std_logic_vector(16 downto 0);
         fifo_data_I : IN  std_logic_vector(45 downto 0);
         fifo_empty_I : IN  std_logic;
         fifo_rd_en_O : OUT  std_logic;
         call_config_I : IN  std_logic_vector(10 downto 0);
         call_tr_lvl_I : IN  std_logic_vector(4 downto 0);
         call_nfrm_I : IN  std_logic;
         def_sig1_act_I : IN  std_logic;
         def_sig2_act_I : IN  std_logic;
         def_rst_out_I : IN  std_logic;
         def_adc_off_act_I : IN  std_logic;
         sig1_act_O : OUT  std_logic;
         sig2_act_O : OUT  std_logic;
         rst_out_O : OUT  std_logic;
         adc_off_O : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal start_I : std_logic := '0';
   signal css_I : std_logic_vector(20 downto 0) := (others => '0');
   signal sss_I : std_logic_vector(16 downto 0) := (others => '0');
   signal fifo_data_I : std_logic_vector(45 downto 0) := (others => '0');
   signal fifo_empty_I : std_logic := '0';
   signal call_config_I : std_logic_vector(10 downto 0) := (others => '0');
   signal call_tr_lvl_I : std_logic_vector(4 downto 0) := (others => '0');
   signal call_nfrm_I : std_logic := '0';
   signal def_sig1_act_I : std_logic := '0';
   signal def_sig2_act_I : std_logic := '0';
   signal def_rst_out_I : std_logic := '0';
   signal def_adc_off_act_I : std_logic := '0';

 	--Outputs
   signal fifo_rd_en_O : std_logic;
   signal sig1_act_O : std_logic;
   signal sig2_act_O : std_logic;
   signal rst_out_O : std_logic;
   signal adc_off_O : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	
	signal css_int : INTEGER;
	signal sss_int : INTEGER;
	constant CSS_MAX : INTEGER := 10;
	
	signal act_cnt : INTEGER;
	
	type 	testdata1  is array (15 downto 0) of std_logic_vector(3 downto 0) ;
	constant lvl : testdata1 := ("1111", "0000", "1111", "0000", "1111", "0000", "1111", "0000", 
											"1111", "0000", "1111", "0000", "1111", "0000", "1111", "0000");
											
	
	type 	testdata2  is array (15 downto 0) of INTEGER ;
	constant sss : testdata2 := (1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
	
	
	type 	testdata3  is array (15 downto 0) of INTEGER ;
	constant css : testdata3 := (5, 10, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: actuator PORT MAP (
          clk => clk,
          rst => rst,
          start_I => start_I,
          css_I => css_I,
          sss_I => sss_I,
          fifo_data_I => fifo_data_I,
          fifo_empty_I => '0',
          fifo_rd_en_O => fifo_rd_en_O,
          call_config_I => "00000000000",
          call_tr_lvl_I => "00000",
          call_nfrm_I => '1',
          def_sig1_act_I => '1',
          def_sig2_act_I => '1',
          def_rst_out_I => '0',
          def_adc_off_act_I => '0',
          sig1_act_O => sig1_act_O,
          sig2_act_O => sig2_act_O,
          rst_out_O => rst_out_O,
          adc_off_O => adc_off_O
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
	css_I <= std_logic_vector(to_unsigned(css_int,21));
	sss_I <= std_logic_vector(to_unsigned(sss_int,17));
	
	
	
	process (clk, rst)
	begin
		if rst = '1' then
			css_int <= 0;
			sss_int <= 0;
			act_cnt <= 15;
		elsif rising_edge(clk) then	
			
			if (start_I = '1') then
				if css_int = CSS_MAX then
					css_int <= 0;
					sss_int <= sss_int + 1;
				else
					css_int <= css_int + 1;
				end if;
			end if;
			
			if fifo_rd_en_O = '1' then
				act_cnt <= act_cnt - 1;
			end if;			
			
			fifo_data_I <= "1111" & lvl(act_cnt) & std_logic_vector(to_unsigned(sss(act_cnt),17)) & std_logic_vector(to_unsigned(css(act_cnt),21));
			
		end if;
	end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rst <= '1';
      wait for 100 ns;	
		rst <= '0';
      wait for clk_period*10;

      -- insert stimulus here 
		start_I <= '1';
		
		

      wait;
   end process;

END;
