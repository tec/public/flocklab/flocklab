----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:44:59 02/15/2014 
-- Design Name: 
-- Module Name:    actuator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity actuator is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           start_I : in  STD_LOGIC;
           
			  --time signals
			  css_I : in  STD_LOGIC_VECTOR (20 downto 0);
           sss_I : in  STD_LOGIC_VECTOR (16 downto 0);
           
			  --fifo signals			  
           fifo_data_I : in  STD_LOGIC_VECTOR (45 downto 0);
           fifo_empty_I : in  STD_LOGIC;
			  fifo_rd_en_O : out  STD_LOGIC;
			  
			  call_config_I : in  STD_LOGIC_VECTOR (10 downto 0); -- 4bit pin & 7bit pkg delay
			  call_tr_lvl_I : in  STD_LOGIC_VECTOR (4 downto 0);
			  call_nfrm_I : in  STD_LOGIC; -- adc_spi nfrm to count the adc-samples
			  
			  --default output
			  def_sig1_act_I : in  STD_LOGIC;
           def_sig2_act_I : in  STD_LOGIC;
           def_rst_out_I : in  STD_LOGIC;
           def_adc_off_act_I : in  STD_LOGIC;
			  
			  --output signals
			  sig1_act_O : out  STD_LOGIC;
           sig2_act_O : out  STD_LOGIC;
           rst_out_O : out  STD_LOGIC;
           adc_off_O : out  STD_LOGIC);
end actuator;

architecture Behavioral of actuator is
	
	signal fifo_rd_en_int : STD_LOGIC;
	signal adc_off_act : STD_LOGIC;
	
	signal ADCr_off_call : STD_LOGIC_VECTOR (2 downto 0);
	signal NFRMr : STD_LOGIC_VECTOR (2 downto 0);
	
	signal adc_off_call : STD_LOGIC;
	signal adc_off_call_delayed : STD_LOGIC;
	signal cb_delay_cnt : INTEGER range 0 to 127;
	
	constant VALID_FRM : INTEGER := 3*60*60;
	constant MAX_SSS : INTEGER := 24*60*60;
	
	signal num_sss_fifo : INTEGER;
	signal num_sss_int : INTEGER;
	
begin
	
	fifo_rd_en_O <= fifo_rd_en_int;
	
	adc_off_O <= '0' when adc_off_act = '0' or adc_off_call_delayed = '0'
						else '1'; -- the ADC is turned on if either actuation or callback turned it on
						
	num_sss_fifo <= to_integer(unsigned(fifo_data_I(37 downto 21)));
	num_sss_int	<= to_integer(unsigned(sss_I));
	
						
	
	actuation : process (clk, rst, def_sig1_act_I, def_sig2_act_I, def_rst_out_I, def_adc_off_act_I)
	begin
		if rst = '1' then
			
			--sig1_act_O 		<= def_sig1_act_I;
			--sig2_act_O 		<= def_sig2_act_I;
			sig1_act_O 		<= '0';
			sig2_act_O 		<= '0';
			rst_out_O 		<= def_rst_out_I;
			adc_off_act 	<= def_adc_off_act_I;
			
			fifo_rd_en_int	<= '0';
			
		elsif rising_edge(clk) then
			fifo_rd_en_int	<= '0';
			
			if fifo_empty_I = '0' and fifo_rd_en_int = '0' and start_I = '1' then
				
				if fifo_data_I(37 downto 0) = sss_I & css_I then
					fifo_rd_en_int	<= '1';
					
					if fifo_data_I(45) = '1' then
						sig1_act_O 		<= fifo_data_I(41);
					end if;
					if fifo_data_I(44) = '1' then
						sig2_act_O 		<= fifo_data_I(40);
					end if;
					if fifo_data_I(43) = '1' then
						rst_out_O 		<= fifo_data_I(39);
					end if;
					if fifo_data_I(42) = '1' then
						adc_off_act 		<= fifo_data_I(38);
					end if;				
				
				elsif num_sss_int <= MAX_SSS - VALID_FRM AND (num_sss_fifo < num_sss_int OR num_sss_fifo > num_sss_int + VALID_FRM)  then -- fifo_data_I(sss) > than or < then
					-- descard element, without using it
					fifo_rd_en_int	<= '1';
				elsif num_sss_int > MAX_SSS - VALID_FRM AND (num_sss_fifo > num_sss_int + VALID_FRM - MAX_SSS AND num_sss_fifo < num_sss_int) then
					-- descard element, without using it
					fifo_rd_en_int	<= '1';
				end if;
				
			end if;		
			
		end if;
	end process;
	
			  
	callback : process (clk, rst)
	begin
		if rst = '1' then
			
			--TODO what is init value
			adc_off_call <= '1';
			
		elsif rising_edge(clk) then
			
			if call_config_I(10) = '0' OR start_I = '0' then		
				adc_off_call <= '1';
			else
				case call_config_I(9 downto 7) is
					
					when "000"		=> adc_off_call <= call_tr_lvl_I(4); --LED1
											
											
					when "001"		=> adc_off_call <= call_tr_lvl_I(3); --LED2
											
											
					when "010"		=> adc_off_call <= call_tr_lvl_I(2); --LED3
											
											
					when "011"		=> adc_off_call <= call_tr_lvl_I(1); --INT1
											
											
					when "100"		=> adc_off_call <= call_tr_lvl_I(0); --INT2
											
											
					when others		=> adc_off_call <= '1';
											
				end case;
				
			end if;		
			
		end if;
	end process;
	
	cb_delay : process (clk, rst)
	begin
		if rst = '1' then
			
			--TODO what is init value
			adc_off_call_delayed 	<= '1';
			cb_delay_cnt <= 0;
			ADCr_off_call <= "111";
			NFRMr <= "111";
			
		elsif rising_edge(clk) then
			ADCr_off_call 	<= ADCr_off_call(1 downto 0) & adc_off_call;
			NFRMr <= NFRMr(1 downto 0) & call_nfrm_I;
			
			if ADCr_off_call(2 downto 1) = "00" then		
				adc_off_call_delayed <= '0'; --on
			elsif ADCr_off_call(2 downto 1) = "01" then	
				cb_delay_cnt <= to_integer(unsigned(call_config_I(6 downto 0)));
			elsif ADCr_off_call(2 downto 1) = "11" AND NFRMr(2 downto 1) = "10" then
				if cb_delay_cnt = 0 then
					adc_off_call_delayed <= '1'; --off
				else
					cb_delay_cnt <= cb_delay_cnt - 1;
				end if;
			end if;		
			
		end if;
	end process;

end Behavioral;

