----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:15:20 01/29/2014 
-- Design Name: 
-- Module Name:    adc_packaging - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adc_packaging is
    Generic (PKG_BATCH_SIZE : positive);
	 Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           time_clk : in  STD_LOGIC;
			  
			  adc_off_I : in  STD_LOGIC;
			  adc_off_delay_O : out  STD_LOGIC;
			  adc_sample_div_I : in STD_LOGIC_VECTOR(10 downto 0);
			  --debug
			  --debug_led_O : out  STD_LOGIC;
			  
			  --time signals; time_clk domain!!
           css_I : in  STD_LOGIC_VECTOR (20 downto 0);
           sss_I : in  STD_LOGIC_VECTOR (16 downto 0);
			  full_second_I: in STD_LOGIC;
			  
			  -- SPI input signals from ADC
           spi_clk_I : in  STD_LOGIC;
           spi_nfrm_I : in  STD_LOGIC;
           spi_rx_I : in  STD_LOGIC;
			  
			  -- Fifo output signals (to sram, to gum)
           fifo_data_O : out  STD_LOGIC_VECTOR (31 downto 0);
           fifo_wr_en_O : out  STD_LOGIC;
           fifo_full_I : in  STD_LOGIC);
end adc_packaging;

architecture Behavioral of adc_packaging is

	--constant PKG_BATCH_SIZE : positive := 200; -- TODO
	
	constant HD_ADC_FIRST 	: STD_LOGIC_VECTOR (5 downto 0) := "111000";
	constant HD_ADC_LAST 	: STD_LOGIC_VECTOR (5 downto 0) := "111001";
	constant HD_ADC_INTER 	: STD_LOGIC_VECTOR (5 downto 0) := "111010";
	constant HD_ADC_SEC 		: STD_LOGIC_VECTOR (5 downto 0) := "111011";
	constant HD_ADC_SPL		: STD_LOGIC_VECTOR (5 downto 0) := "111100";
	--constant HD_ADC_ON 		: STD_LOGIC_VECTOR (5 downto 0) := "111101";
	--constant HD_ADC_OFF 	: STD_LOGIC_VECTOR (5 downto 0) := "111110";
	constant SSS_READ_OK_WINDOW : Integer := 15;
	constant ADC_OFF_SSS_UPDATE_INTERVAL : Integer := 50;
	
	COMPONENT spi_slave_v2
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		spi_clk_I : IN std_logic;
		spi_nfrm_I : IN std_logic;
		spi_rx_I : IN std_logic;
		data_ack_I : IN std_logic;          
		data_O : OUT std_logic_vector(23 downto 0);
		data_new_O : OUT std_logic
		);
	END COMPONENT;
	
	type states is (adcoff, adcoffsec, idle, wait_for_last, last, lastsec, sec, first, inter, sample);
	signal state : states := adcoff;
	
	--SPI signals
	--signal spi_rst : std_logic;
	
	signal spi_data : std_logic_vector(23 downto 0);
	signal spi_data_new : std_logic;
	signal spi_data_ack : std_logic;
	

	signal SFRMr : std_logic_vector(2 downto 0);
	signal sfrm_risingedge : STD_LOGIC := '0';
	
	signal css_slow_int : STD_LOGIC_VECTOR (20 downto 0);
	signal sss_slow_int : STD_LOGIC_VECTOR (16 downto 0);
	signal sss_slow_old_int : STD_LOGIC_VECTOR (16 downto 0);
	signal sss_slow_old_int_valid : STD_LOGIC;
	signal read_sss_ok_int: STD_LOGIC;
	signal read_sss_countdown_int: Integer range 0 to 15;
	signal adcoff_sss_update_count : Integer range 0 to 50;
	
	signal first_spl : STD_LOGIC := '0';	
	signal pkg_cnt : Integer range 0 to PKG_BATCH_SIZE;
	
	signal inter_cnt : Integer range 0 to 30; --debug signal
	
	signal nth_cnt : Integer range 0 to (2**11)-1;
	
	
begin
	
	--spi_rst <= rst OR adc_off_I;
	
	SPI_IN: spi_slave_v2 PORT MAP(
		clk => clk,
		rst => rst,
		spi_clk_I => spi_clk_I,
		spi_nfrm_I => spi_nfrm_I,
		spi_rx_I => spi_rx_I,
		data_O => spi_data,
		data_new_O => spi_data_new,
		data_ack_I => spi_data_ack 
	);

	
	-- !!! time_clk domain
	process (time_clk, rst)
	begin
		if rst = '1' then
			SFRMr <= "111";
		elsif rising_edge(time_clk) then
			SFRMr <= SFRMr(1 downto 0) & spi_nfrm_I;
		end if;
	end process;
	
	sfrm_risingedge <= '1' when (SFRMr(2 downto 1) = "01") else '0';
	
	-- !!! cross clock domain: 
	process (time_clk, rst)
	begin
		if rst = '1' then
			css_slow_int <= (others => '0'); -- clk domain
			sss_slow_int <= (others => '0'); -- clk domain
		elsif rising_edge(time_clk) then
			if sfrm_risingedge = '1' then -- time_clk domain; TODO rising or falling edge?
				css_slow_int <= css_I;
				sss_slow_int <= sss_I;
			end if;
		end if;
	end process;

	-- !!! cross clock domain: 
	process (time_clk, rst)
	begin
		if rst = '1' then
			read_sss_ok_int <= '0';
			read_sss_countdown_int <= 0;
		elsif rising_edge(time_clk) then
			if full_second_I = '1' then -- time_clk domain;
				read_sss_countdown_int <= SSS_READ_OK_WINDOW;
			elsif read_sss_countdown_int > 0 then
				read_sss_ok_int <= '1'; -- ok one cycle after update
				read_sss_countdown_int <= read_sss_countdown_int - 1;
			else
				read_sss_ok_int <= '0';
			end if;
		end if;
	end process;

	
	process (clk, rst)
	begin
		if rst = '1' then
			first_spl <= '0';
			spi_data_ack <= '0';
			pkg_cnt <= 0;
			fifo_data_O <= (others => '0');
			fifo_wr_en_O <= '0';
			
			inter_cnt <= 0;
			nth_cnt <= 0; --TODO or just '<= 1'
			
			adc_off_delay_O <= '1';
			state <= adcoff;
			sss_slow_old_int <= (others => '0');
			sss_slow_old_int_valid <= '0';
			adcoff_sss_update_count <= 0;
			
		elsif rising_edge(clk) then
			
			case state is --TODO check if when wr_en, the next cycle is really read, not the over next
			
				when adcoff => fifo_wr_en_O <= '0';
									spi_data_ack <= '0';
									
									if adc_off_I = '0' then --turn ADC on, go idle
										adc_off_delay_O <= '0';
										first_spl <= '1';
										state <= idle;
									else --if adc_off_I = '1'
										-- check SSS for update
										if read_sss_ok_int = '1' AND (sss_slow_old_int_valid = '0' OR sss_slow_old_int /= sss_I) then
											sss_slow_old_int_valid <= '1';
											sss_slow_old_int <= sss_I;
											if adcoff_sss_update_count = 0 then
												adcoff_sss_update_count <= ADC_OFF_SSS_UPDATE_INTERVAL; -- in off state, only send a few full second packets, merely to ensure second overflows are captured
												state <= adcoffsec;
											else
												adcoff_sss_update_count <= adcoff_sss_update_count - 1;
											end if;
										else
											state <= adcoff;
										end if;
									end if;
				
				when adcoffsec=>fifo_data_O <= HD_ADC_SEC	& sss_slow_old_int & "000000000";
									fifo_wr_en_O <= '1';

									state <= adcoff;				
				
				when idle	=>	fifo_wr_en_O <= '0';
									spi_data_ack <= '0';
									
									if adc_off_I = '1' then --turn ADC off, after last sample received							
										state <= wait_for_last; -- send a last-pkg before the sample pkg
										
									elsif spi_data_new = '1' AND spi_data_ack = '0' AND fifo_full_I = '0' then
										if sss_slow_old_int_valid = '0' OR sss_slow_old_int /= sss_slow_int then
											state <= sec;
											sss_slow_old_int_valid <= '1';
											sss_slow_old_int <= sss_slow_int;
											
										elsif first_spl = '1' then -- send a first- and sec-pkg before the sample pkg
											first_spl <= '0';
											nth_cnt <= to_integer(unsigned(adc_sample_div_I))-1;
											pkg_cnt <= PKG_BATCH_SIZE;
											state <= first;
											
										elsif pkg_cnt = 0 then	-- send a inter-pkg before the sample pkg
											if nth_cnt = 0 then
												nth_cnt <= to_integer(unsigned(adc_sample_div_I))-1;
												pkg_cnt <= PKG_BATCH_SIZE;
												state <= inter;
											else
												spi_data_ack <= '1';
												nth_cnt <= nth_cnt -1;
											end if;
											
										else	-- just send a sample pkg
											if nth_cnt = 0 then
												nth_cnt <= to_integer(unsigned(adc_sample_div_I))-1;
												pkg_cnt <= pkg_cnt - 1;
												state <= sample;
											else
												spi_data_ack <= '1';
												nth_cnt <= nth_cnt -1;
											end if;
										end if;
										
									else
										state <= idle;
									end if;
									
				when wait_for_last	=>	spi_data_ack <= '0';
												if adc_off_I = '0' then --dont turn ADC off, go idle
													adc_off_delay_O <= '0';
													--first_spl <= '1';
													state <= idle;
												elsif spi_data_new = '1' AND spi_data_ack = '0' AND fifo_full_I = '0' then -- turn ADC off, save last sample										
													
													if nth_cnt = 0 then
														nth_cnt <= to_integer(unsigned(adc_sample_div_I))-1;
														fifo_data_O <= HD_ADC_SPL 	& spi_data 		& "00";
														fifo_wr_en_O <= '1';
														
														spi_data_ack <= '1';
														
														adc_off_delay_O <= '1';
														
														if sss_slow_old_int_valid = '0' OR sss_slow_old_int /= sss_slow_int then
															state <= lastsec;
															sss_slow_old_int_valid <= '1';
															sss_slow_old_int <= sss_slow_int;
														else
															state <= last;
														end if;
													else
														spi_data_ack <= '1';
														nth_cnt <= nth_cnt -1;
														state <= wait_for_last;
													end if;
													
												else -- wait for last sample
													state <= wait_for_last;
												end if;				

				when lastsec=>	fifo_data_O <= HD_ADC_SEC 	& sss_slow_int	& "000000000";
									fifo_wr_en_O <= '1';

									state <= last;	
				
				when last	=>	spi_data_ack <= '0';		
									fifo_data_O <= HD_ADC_LAST		& css_slow_int	& "00000";
									fifo_wr_en_O <= '1';
									state <= adcoff;					
									
									
				when first	=>	fifo_data_O <= HD_ADC_FIRST 	& css_slow_int	& "00000";
									fifo_wr_en_O <= '1';
													
									state <= sample;									
									
									
				when sec	=>		fifo_data_O <= HD_ADC_SEC 	& sss_slow_int	& "000000000";
									fifo_wr_en_O <= '1';

									state <= idle;									
									
									
				when inter	=>	fifo_data_O <= HD_ADC_INTER 	& css_slow_int & std_logic_vector(to_unsigned(inter_cnt, 5));
									fifo_wr_en_O <= '1';
									
									if inter_cnt >= 30 then
										inter_cnt <= 0;
									else
										inter_cnt <= inter_cnt + 1;
									end if;
									
									state <= sample;
									
				
				when sample	=>	fifo_data_O <= HD_ADC_SPL 	& spi_data 		& "00";
									fifo_wr_en_O <= '1';
													
									spi_data_ack <= '1';
									state <= idle;
				
			end case;
					
		end if;
	end process;		
										



end Behavioral;

