----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:56:25 03/12/2014 
-- Design Name: 
-- Module Name:    burst_generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity burst_generator is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           sw1_I : in  STD_LOGIC;
           sw2_I : in  STD_LOGIC;
           sw3_I : in  STD_LOGIC;
           pins_O : out  STD_LOGIC_Vector(2 downto 0));
end burst_generator;

architecture Behavioral of burst_generator is
	
	
	signal SW1r : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
	signal SW2r : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
	signal SW3r : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
	
	signal sw1_int : STD_LOGIC := '0';
	signal sw2_int : STD_LOGIC := '0';
	signal sw3_int : STD_LOGIC := '0';
	
	signal PINr : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
	
	constant DELAY : INTEGER := 350; -- 0 for tr fifo; 15 for SRAM; 350 for SPI
	constant BURST_SIZE1 : INTEGER := 360;
	constant BURST_SIZE2 : INTEGER := 350;
	constant BURST_SIZE3 : INTEGER := 340;
	constant BREAK_SIZE : INTEGER := 0; -- 16'000 for tr; 2M for SRAM; 0 for SPI (continious)
	
	
	type states is (idle, burst, break); --(no cycles; pos no. of cycles; negative no. of cycles to compensate)
	signal state : states := idle;
	
	signal cycle_cnt : Integer := 0;
	signal delay_cnt : Integer range 0 to 1000 := 0;
	
begin
	
	pins_O <= PINr;
	-- sw debouners:
	
	debouncer : process (clk, rst)
	begin	
		if rst = '1' then	
			
			SW1r <= (others => '0');
			sw1_int <= '0';
			SW2r <= (others => '0');
			sw2_int <= '0';
			SW3r <= (others => '0');
			sw3_int <= '0';
						
		elsif rising_edge(clk) then
			SW1r <= SW1r(1 downto 0) & sw1_I;
			SW2r <= SW2r(1 downto 0) & sw2_I;
			SW3r <= SW3r(1 downto 0) & sw3_I;
					
			if SW1r = "111" then
				sw1_int <= '1';
			elsif SW1r = "000" then
				sw1_int <= '0';
			end if;
			
			if SW2r = "111" then
				sw2_int <= '1';
			elsif SW2r = "000" then
				sw2_int <= '0';
			end if;	
			
			if SW3r = "111" then
				sw3_int <= '1';
			elsif SW3r = "000" then
				sw3_int <= '0';
			end if;
			
		end if;
	end process;
	
	
	burst_gen : process (clk, rst)
	begin	
		if rst = '1' then	
		
			state <= idle;
			cycle_cnt <= 0;
			delay_cnt <= 0;
			PINr <= (others => '0');			
									
		elsif rising_edge(clk) then
			
			case state is
				
				when idle => 	delay_cnt <= DELAY;
									if sw1_int = '1' then
										
										delay_cnt <= BURST_SIZE1;
										state <= burst;
									elsif sw2_int = '1' then
										delay_cnt <= BURST_SIZE2;
										state <= burst;
									elsif sw3_int = '1' then
										delay_cnt <= BURST_SIZE3;
										state <= burst;
									else
										state <= idle;
									end if;
									
				when burst =>	if delay_cnt > 0 then	
										-- short break
										delay_cnt <= delay_cnt - 1;
										state <= burst;
									else										
										delay_cnt <= DELAY;
										state <= idle;
										PINr <= PINr(1 downto 0) & not(PINr(2));
--										if cycle_cnt > 0 then
--											--toggle --TODO
--											PINr <= PINr(1 downto 0) & not(PINr(2));
--											cycle_cnt <= cycle_cnt - 1;
--											state <= burst;												
--										else
--											-- make long break/abort
--											cycle_cnt <= BREAK_SIZE;
--											state <= break;
--										end if;								
									end if;
											
											
				when break =>	if cycle_cnt > 0 then
										cycle_cnt <= cycle_cnt - 1;
										state <= break;
									else
										-- end break
										state <= idle;
									end if;
			end case;
		end if;
	end process;
	
	
	
	
	

end Behavioral;

