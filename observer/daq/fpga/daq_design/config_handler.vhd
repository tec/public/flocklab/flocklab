----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:27:15 02/04/2014 
-- Design Name: 
-- Module Name:    config_handler - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity config_handler is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  -- UART rx-fifo
           rxfifo_din_I 	: in  STD_LOGIC_VECTOR (7 downto 0);
           rxfifo_empty_I 	: in  STD_LOGIC;
           rxfifo_valid_I 	: in  STD_LOGIC;
           rxfifo_rd_en_O 	: out  STD_LOGIC;
			  
			  -- UART tx-fifo
			  txfifo_wr_en_O : OUT std_logic;
			  txfifo_full_I : IN std_logic;
			  txfifo_data_O : OUT std_logic_vector(7 downto 0);	
			  
			  -- act-fifo
           actfifo_dout_O 	: out  STD_LOGIC_VECTOR (45 downto 0);
           actfifo_full_I 	: in  STD_LOGIC;
		   actfifo_half_full_I : in STD_LOGIC;
           actfifo_wr_en_O : out  STD_LOGIC;
			  
			  -- config direct out
           tr_config_O 			: out  STD_LOGIC_VECTOR (7 downto 0);
           call_config_O 		: out  STD_LOGIC_VECTOR (10 downto 0);
           sample_div_conf_O 	: out  STD_LOGIC_VECTOR (10 downto 0);
           start_tr_O 			: out  STD_LOGIC;
			  route_through_O 		: out  STD_LOGIC);
end config_handler;

architecture Behavioral of config_handler is
	
	type states is (idle, trace, act1, act2, act3, act4, act5, act6, callback, nth);
	signal state : states := idle;
	
	constant HD_ROUTE	 	: STD_LOGIC_VECTOR (2 downto 0) := "000";
	constant HD_TR		 	: STD_LOGIC_VECTOR (2 downto 0) := "001";
	constant HD_ACT	 	: STD_LOGIC_VECTOR (2 downto 0) := "010";
	constant HD_NTH 		: STD_LOGIC_VECTOR (2 downto 0) := "011";
	constant HD_START 	: STD_LOGIC_VECTOR (2 downto 0) := "100";
	constant HD_CALL	 	: STD_LOGIC_VECTOR (2 downto 0) := "101";
	
	signal rxfifo_rd_en_int : std_logic;
	signal fifo_ready : boolean;
	signal tr_config4_int : std_logic_vector (3 downto 0);
		
	signal act_data_int : std_logic_vector (45 downto 7);
	signal sample_div4_int : std_logic_vector (3 downto 0);
	signal call_config_int : std_logic_vector (3 downto 0);
	
	signal lvl_cnt : Integer range 0 to 8;
	
begin

	--TODO ack for packages --> new fifo out	

	rxfifo_rd_en_O <= rxfifo_rd_en_int;
	fifo_ready <= rxfifo_empty_I = '0' AND rxfifo_valid_I = '1' AND rxfifo_rd_en_int = '0';

--	txfifo_wr_en_O
--	txfifo_full_I
--	txfifo_data_O

	process (clk, rst)
	begin
	
		if rst = '1' then
			
			tr_config4_int		<= (others => '0');
			rxfifo_rd_en_int 	<= '0';
			act_data_int		<= (others => '0');
			sample_div4_int	<= (others => '0');
			call_config_int	<= (others => '0');
			
			txfifo_data_O 		<= (others => '0');
			txfifo_wr_en_O 	<= '0';
			
			tr_config_O			<= (others => '0');
			call_config_O		<= (others => '0');
			sample_div_conf_O	<= "00000000001";
			start_tr_O			<= '0';
			route_through_O		<= '1';
			
			actfifo_dout_O		<= (others => '0');
			actfifo_wr_en_O	<= '0';
			
			state 				<= idle;

		elsif rising_edge(clk) then			-- change data on falling, read on rising
			
			actfifo_wr_en_O <= '0';
			rxfifo_rd_en_int <= '0';
			txfifo_wr_en_O 	<= '0';
			
			if fifo_ready then
				rxfifo_rd_en_int <= '1';
				if rxfifo_din_I(7) = '0' then --header package
					if rxfifo_din_I(6 downto 4) = HD_TR then -- tracing header
						tr_config4_int <= rxfifo_din_I(3 downto 0);
						state 	<= trace;
						
					elsif rxfifo_din_I(6 downto 4) = HD_ACT then -- actuation header
						act_data_int(45 downto 42) <= rxfifo_din_I(3 downto 0);
						state 	<= act1;
						
					elsif rxfifo_din_I(6 downto 4) = HD_START then -- start header
						start_tr_O <= rxfifo_din_I(3); -- [3-0] should all be the same value
						state 	<= idle;
						
						txfifo_data_O		<= HD_START & "00000";
						txfifo_wr_en_O 	<= '1';
						
					elsif rxfifo_din_I(6 downto 4) = HD_NTH then -- adc nth header
						sample_div4_int <= rxfifo_din_I(3 downto 0);
						state 	<= nth;
						
					elsif rxfifo_din_I(6 downto 4) = HD_CALL then -- callback header
						call_config_int <= rxfifo_din_I(3 downto 0);
						state 	<= callback;
						
					elsif rxfifo_din_I(6 downto 4) = HD_ROUTE then -- direct through header
						route_through_O <= rxfifo_din_I(3); -- [3-0] should all be the same value
						state 	<= idle;
						
						txfifo_data_O		<= HD_ROUTE & "00000";
						txfifo_wr_en_O 	<= '1';
						
					else --no true header
						--TODO ack error (invalid header)
						state 	<= idle ;
						
						txfifo_data_O		<= (others => '1'); --error
						txfifo_wr_en_O 	<= '1';
					end if;
					
					
					
				else -- rxfifo_din_I(7) = '1' then --data package
						
					case state is
						
						when idle 			=> --error --TODO ack error (expected header first)
													state 	<= idle ;
															
						when trace			=> -- set tr_config out
													tr_config_O <= tr_config4_int & rxfifo_din_I(6 downto 3);
													state 	<= idle;
						
													txfifo_data_O		<= HD_TR & "00000";
													txfifo_wr_en_O 	<= '1';
													
						when act1			=>  -- 35-41
													act_data_int(41 downto 35) <= rxfifo_din_I(6 downto 0);
													state 	<= act2;
													
						when act2			=>  -- 28-34
													act_data_int(34 downto 28) <= rxfifo_din_I(6 downto 0);
													state 	<= act3;
													
						when act3			=>  -- 21-27
													act_data_int(27 downto 21) <= rxfifo_din_I(6 downto 0);
													state 	<= act4;
													
						when act4			=>  -- 14-20
													act_data_int(20 downto 14) <= rxfifo_din_I(6 downto 0);
													state 	<= act5;
													
						when act5			=>  -- 7-13
													act_data_int(13 downto 7) <= rxfifo_din_I(6 downto 0);
													state 	<= act6;
													
						when act6			=>  -- 0-6 -- set adc_pkg out
													actfifo_dout_O <= act_data_int & rxfifo_din_I(6 downto 0);
													actfifo_wr_en_O <= '1';
													state 	<= idle; --TODO what if fifo full??
													if actfifo_full_I = '1' then --act error --> act_ack_error
														txfifo_data_O		<= HD_ACT & "11111";
													--elsif actfifo_almost_full_I = '1' then													
														--txfifo_data_O		<= HD_ACT & "00010";
													elsif actfifo_half_full_I = '1' then													
														txfifo_data_O		<= HD_ACT & "00001";
													else
														txfifo_data_O		<= HD_ACT & "00000";
													end if;
													txfifo_wr_en_O 	<= '1';
													
						when callback			=>  -- second pkg: set call
													call_config_O <= call_config_int & rxfifo_din_I(6 downto 0);
													state 	<= idle;
						
													txfifo_data_O		<= HD_CALL & "00000";
													txfifo_wr_en_O 	<= '1';
													
						when nth				=>	--set sample div output
													sample_div_conf_O <= sample_div4_int & rxfifo_din_I(6 downto 0);
													state 	<= idle;
						
													txfifo_data_O		<= HD_NTH & "00000";
													txfifo_wr_en_O 	<= '1';
														
															
					end case;
						
				end if;
				
--			else
--				rxfifo_rd_en_int <= '0';
			end if;
		
		end if;
		
	end process;


end Behavioral;

