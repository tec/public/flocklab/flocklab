----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:38:08 01/28/2014 
-- Design Name: 
-- Module Name:    uart_dev - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity controlling is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  --debug
           debugUart_1_O : out STD_LOGIC;
           debugUart_2_O : out STD_LOGIC;
           debugUart_3_O : out STD_LOGIC;
           debugUart_4_O : out STD_LOGIC;
			  
			  -- config signals
			  tr_config_O : out STD_LOGIC_VECTOR(7 downto 0);
			  call_config_O : out STD_LOGIC_VECTOR(10 downto 0);
           start_config_O : out STD_LOGIC;
			  sample_div_conf_O : out STD_LOGIC_VECTOR(10 downto 0);
			  route_through_O : out STD_LOGIC;
			  
			  -- actuation config fifo signals
				actfifo_dout_O : out STD_LOGIC_VECTOR(45 downto 0);
				actfifo_full_I : in  STD_LOGIC;
				actfifo_half_full_I: in STD_LOGIC;
				actfifo_wr_en_O : out STD_LOGIC;
				
				--debug
				txfifo_select_I : in STD_LOGIC;
				txfifo_din_I : in STD_LOGIC_VECTOR(7 downto 0);
				txfifo_full_O : out STD_LOGIC;
				txfifo_wr_en_I : in STD_LOGIC;
			  
			  
			  -- UART
           uart_txd_O : out STD_LOGIC;
           uart_rxd_I : in  STD_LOGIC);
end controlling;

architecture Behavioral of controlling is
	
	 ----------------------------------------------------------------------------
    -- Component declarations
    ----------------------------------------------------------------------------
    
	COMPONENT LOOPBACKPLUS
	PORT(
		CLOCK : IN std_logic;
		RESET : IN std_logic;
		RX : IN std_logic;
		read_fifo_data_I : IN std_logic_vector(7 downto 0);
		read_fifo_empty_I : IN std_logic;
		read_fifo_valid_I : IN std_logic;
		write_fifo_full_I : IN std_logic;          
		TX : OUT std_logic;
		read_fifo_rd_en_O : OUT std_logic;
		write_fifo_data_O : OUT std_logic_vector(7 downto 0);
		write_fifo_wr_en_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT fifo_8bit
	  PORT (
		 clk : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC;
		 valid : OUT STD_LOGIC
	  );
	END COMPONENT;
	
	COMPONENT config_handler
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		rxfifo_din_I : IN std_logic_vector(7 downto 0);
		rxfifo_empty_I : IN std_logic;
		rxfifo_valid_I : IN std_logic;
		actfifo_full_I : IN std_logic;     
		actfifo_half_full_I : IN std_logic; 		
		rxfifo_rd_en_O : OUT std_logic;
		
		txfifo_wr_en_O : OUT std_logic;
		txfifo_full_I : IN std_logic;      
		txfifo_data_O : OUT std_logic_vector(7 downto 0);		
		
		actfifo_dout_O : OUT std_logic_vector(45 downto 0);
		actfifo_wr_en_O : OUT std_logic;
		tr_config_O : OUT std_logic_vector(7 downto 0);
		call_config_O : OUT std_logic_vector(10 downto 0);
		sample_div_conf_O : OUT std_logic_vector(10 downto 0);
		start_tr_O : OUT std_logic;
		route_through_O : OUT std_logic
		);
	END COMPONENT;
    
    ----------------------------------------------------------------------------
    -- Signals
    ----------------------------------------------------------------------------

    signal tx, rx, rx_sync, reset, reset_sync : std_logic;
	
	signal uart_txd_int : STD_LOGIC;
	
	
	--fifo signals
	signal rx_fifo_din, rx_fifo_dout : STD_LOGIC_VECTOR (7 DOWNTO 0);		
	signal rx_fifo_wr_en, rx_fifo_rd_en, rx_fifo_full, rx_fifo_empty, rx_fifo_valid : STD_LOGIC;
	
	signal tx_fifo_din, tx_fifo_dout : STD_LOGIC_VECTOR (7 DOWNTO 0);		
	signal tx_fifo_wr_en, tx_fifo_rd_en, tx_fifo_full, tx_fifo_empty, tx_fifo_valid : STD_LOGIC;
	
	signal tx_fifo_din_conf : STD_LOGIC_VECTOR (7 DOWNTO 0);		
	signal tx_fifo_wr_en_conf, tx_fifo_full_conf : STD_LOGIC;
	
begin
	
	uart_txd_O <= uart_txd_int;
	
	debugUart_1_O <= uart_txd_int;
	debugUart_2_O <= uart_rxd_I;
	debugUart_3_O <= '0';
	debugUart_4_O <= '0';
	
--	process (clk,rst) -- ADC on/off by switch
--	begin
--		if rst = '1' then
--			uart_txd_int <= '1';
--		elsif rising_edge(clk) then
--			uart_txd_int <= '1';
--		end if;
--	end process;	

    ----------------------------------------------------------------------------
    -- Loopback instantiation
    ----------------------------------------------------------------------------

	LOOPBACKPLUS_Inst: LOOPBACKPLUS PORT MAP(
		CLOCK => clk,
		RESET => reset,
		RX => rx,
		TX => tx,
		read_fifo_data_I => tx_fifo_dout,
		read_fifo_rd_en_O => tx_fifo_rd_en,
		read_fifo_empty_I => tx_fifo_empty,
		read_fifo_valid_I => tx_fifo_valid,
		write_fifo_data_O => rx_fifo_din,
		write_fifo_wr_en_O => rx_fifo_wr_en,
		write_fifo_full_I => rx_fifo_full
	);	
	

	CONF_HANDLER: config_handler PORT MAP(
		clk => clk,
		rst => rst,
		rxfifo_din_I => rx_fifo_dout,
		rxfifo_empty_I => rx_fifo_empty,
		rxfifo_valid_I => rx_fifo_valid,
		rxfifo_rd_en_O => rx_fifo_rd_en,
		
		txfifo_wr_en_O => tx_fifo_wr_en_conf,
		txfifo_full_I => tx_fifo_full_conf,
		txfifo_data_O => tx_fifo_din_conf,
		
		actfifo_dout_O => actfifo_dout_O,
		actfifo_full_I => actfifo_full_I,
		actfifo_half_full_I => actfifo_half_full_I,
		actfifo_wr_en_O => actfifo_wr_en_O,
		tr_config_O => tr_config_O,
		call_config_O => call_config_O,
		sample_div_conf_O => sample_div_conf_O,
		route_through_O => route_through_O,
		start_tr_O => start_config_O
	);
	
    ----------------------------------------------------------------------------
    -- Fifo instantiation
    ----------------------------------------------------------------------------    
	 
	RX_FIFO : fifo_8bit
	  PORT MAP (
		 clk => clk,
		 rst => rst,
		 din => rx_fifo_din,
		 wr_en => rx_fifo_wr_en,
		 rd_en => rx_fifo_rd_en,
		 dout => rx_fifo_dout,
		 full => rx_fifo_full,
		 empty => rx_fifo_empty,
		 valid => rx_fifo_valid
	  );   
	 
	TX_FIFO : fifo_8bit
	  PORT MAP (
		 clk => clk,
		 rst => rst,
		 din => tx_fifo_din, 		--txfifo_din_I,     --din => tx_fifo_din,
		 wr_en => tx_fifo_wr_en,	--txfifo_wr_en_I, --wr_en => tx_fifo_wr_en,
		 rd_en => tx_fifo_rd_en,
		 dout => tx_fifo_dout,
		 full => tx_fifo_full, 		--txfifo_full_O,   --full => tx_fifo_full,
		 empty => tx_fifo_empty,
		 valid => tx_fifo_valid
	  );
	  
	-- TX FIFO input selector 
	tx_fifo_din			<= tx_fifo_din_conf 		when txfifo_select_I = '0' else txfifo_din_I;
	tx_fifo_wr_en  	<= tx_fifo_wr_en_conf	when txfifo_select_I = '0' else txfifo_wr_en_I;	
	tx_fifo_full_conf	<= tx_fifo_full  	when txfifo_select_I = '0' else '1';	
	txfifo_full_O 		<= '1'  				when txfifo_select_I = '0' else tx_fifo_full;
	
    ----------------------------------------------------------------------------
    -- Deglitch inputs
    ----------------------------------------------------------------------------
    
    DEGLITCH : process (clk)
    begin
        if rising_edge(clk) then
            rx_sync         <= uart_rxd_I;
            rx              <= rx_sync;
            reset_sync      <= rst;
            reset           <= reset_sync;
            uart_txd_int    <= tx;
        end if;
    end process;
	
	

end Behavioral;

