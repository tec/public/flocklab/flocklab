--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:09:39 01/30/2014
-- Design Name:   
-- Module Name:   C:/Users/disslerb/Documents/MaT_VHDL/DAQ_v1.0/daq_tester.vhd
-- Project Name:  DAQ_v1.0
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: daq_wrapper
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY daq_tester IS
END daq_tester;
 
ARCHITECTURE behavior OF daq_tester IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT daq_wrapper
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         debug_1_O : OUT  std_logic;
         debug_2_O : OUT  std_logic;
         debug_3_O : OUT  std_logic;
         debug_4_O : OUT  std_logic;
         pps_I : IN  std_logic;
         trace_signal_I : IN  std_logic_vector(7 downto 0);
         adc_spi_clk_I : IN  std_logic;
         adc_spi_nfrm_I : IN  std_logic;
         adc_spi_rx_I : IN  std_logic;
         adc_off_O : OUT  std_logic;
         adc_off_sw_I : IN  std_logic;
         gum_spi_clk_O : OUT  std_logic;
         gum_spi_nfrm_O : OUT  std_logic;
         gum_spi_tx_O : OUT  std_logic;
         gum_nready_I : IN  std_logic;
         start_sw_I : IN  std_logic;
         sram_address_O : OUT  std_logic_vector(18 downto 0);
         sram_data_IO : INOUT  std_logic_vector(15 downto 0);
         sram_cs_O : OUT  std_logic_vector(1 downto 0);
         sram_lb_O : OUT  std_logic;
         sram_ub_O : OUT  std_logic;
         sram_we_O : OUT  std_logic;
         sram_oe_O : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal pps_I : std_logic := '0';
   signal trace_signal_I : std_logic_vector(7 downto 0) := (others => '0');
   signal adc_spi_clk_I : std_logic := '0';
   signal adc_spi_nfrm_I : std_logic := '0';
   signal adc_spi_rx_I : std_logic := '0';
   signal adc_off_sw_I : std_logic := '0';
   signal gum_nready_I : std_logic := '1';
   signal start_sw_I : std_logic := '0';

	--BiDirs
   signal sram_data_IO : std_logic_vector(15 downto 0);

 	--Outputs
   signal debug_1_O : std_logic;
   signal debug_2_O : std_logic;
   signal debug_3_O : std_logic;
   signal debug_4_O : std_logic;
   signal adc_off_O : std_logic;
   signal gum_spi_clk_O : std_logic;
   signal gum_spi_nfrm_O : std_logic;
   signal gum_spi_tx_O : std_logic;
   signal sram_address_O : std_logic_vector(18 downto 0);
   signal sram_cs_O : std_logic_vector(1 downto 0);
   signal sram_lb_O : std_logic;
   signal sram_ub_O : std_logic;
   signal sram_we_O : std_logic;
   signal sram_oe_O : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: daq_wrapper PORT MAP (
          clk => clk,
          rst => rst,
          debug_1_O => debug_1_O,
          debug_2_O => debug_2_O,
          debug_3_O => debug_3_O,
          debug_4_O => debug_4_O,
          pps_I => pps_I,
          trace_signal_I => trace_signal_I,
          adc_spi_clk_I => adc_spi_clk_I,
          adc_spi_nfrm_I => adc_spi_nfrm_I,
          adc_spi_rx_I => adc_spi_rx_I,
          adc_off_O => adc_off_O,
          adc_off_sw_I => adc_off_sw_I,
          gum_spi_clk_O => gum_spi_clk_O,
          gum_spi_nfrm_O => gum_spi_nfrm_O,
          gum_spi_tx_O => gum_spi_tx_O,
          gum_nready_I => gum_nready_I,
          start_sw_I => start_sw_I,
          sram_address_O => sram_address_O,
          sram_data_IO => sram_data_IO,
          sram_cs_O => sram_cs_O,
          sram_lb_O => sram_lb_O,
          sram_ub_O => sram_ub_O,
          sram_we_O => sram_we_O,
          sram_oe_O => sram_oe_O
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
	
   pps_process :process
   begin
		pps_I <= '0';
		wait for 990*clk_period;
		pps_I <= '1';
		wait for clk_period * 10;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst <= '1';
		
		adc_off_sw_I 	<= '1';
		wait for 100 ns;	
		rst <= '0';
      wait for clk_period*10;

      -- insert stimulus here	
		start_sw_I		<= '1';
		adc_off_sw_I 	<= '0'; -- TODO check timing for this signal
		
				
				


wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';
		
		

      wait;
   end process;

END;
