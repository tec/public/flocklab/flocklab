----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:22:02 01/24/2014 
-- Design Name: 
-- Module Name:    daq_wrapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity daq_wrapper is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  -- debug
           debug_1_O : out  STD_LOGIC;
           debug_2_O : out  STD_LOGIC;
           debug_3_O : out  STD_LOGIC;
           debug_4_O : out  STD_LOGIC;
			  debug_gumstix_0_O : OUT std_logic;
			  debug_gumstix_1_O : OUT std_logic;
			  leds		: out  STD_LOGIC;
			  -- BALZ changes: set these debug signals to a default value, used to be switches on the anvyl board
			  pps_on_debug_I : in  STD_LOGIC := '0'; -- debug swicht to enable/disable the pps pulse for the time calibration, but not for the UART debug out
			  sw1_I : in  STD_LOGIC := '0'; -- debug switch
			  sw2_I : in  STD_LOGIC := '0'; -- debug switch
			  sw3_I : in  STD_LOGIC := '0'; -- debug switch
			  txfifo_select_I : in  STD_LOGIC := '1'; -- debug switch to select UART TX FIFO inputs (debug timing info (=='1') or UART acks (=='0'))
			  
			  --timing
			  pps_I : in  STD_LOGIC;
			  
			  --tracing
			  trace_signal_I : in  STD_LOGIC_VECTOR (4 downto 0); -- [LED1, LED2, LED3, INT1, INT2]
			  
			  --actuation
			  actuation_signals_O : out  STD_LOGIC_VECTOR (2 downto 0); -- [SIG1, SIG2, RST]
			    
			  
			  -- ADC SPI
           adc_spi_clk_I : in  STD_LOGIC;
           adc_spi_nfrm_I : in  STD_LOGIC;
           adc_spi_rx_I : in  STD_LOGIC;
			  
           adc_off_O : out  STD_LOGIC; -- new signal to turn on/off the ADC (delayed)
			  
			  
			  --Gum SPI
           gum_spi_clk_O : out  STD_LOGIC;
           gum_spi_nfrm_O : out  STD_LOGIC;
           gum_spi_tx_O : out  STD_LOGIC;
			  
			  --Control
			  gum_nready_I : in  STD_LOGIC; -- GUM SPI ready to receive			 
			  route_through_O : out STD_LOGIC; -- tell routing whether or not to route
			  
			  --UART			  
			  uart_txd_O : out  STD_LOGIC;
			  uart_rxd_I : in  STD_LOGIC;
			  
			  -- SRAM
			  sram_address_O : out  STD_LOGIC_VECTOR (18 downto 0);
           sram_data_IO : inout  STD_LOGIC_VECTOR (15 downto 0);
           sram_cs_O : out  STD_LOGIC;
           sram_lb_O : out  STD_LOGIC;
           sram_ub_O : out  STD_LOGIC;
           sram_we_O : out  STD_LOGIC;
           sram_oe_O : out  STD_LOGIC);
			  
end daq_wrapper;

architecture Behavioral of daq_wrapper is

	component clock_gen
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_main         : out    std_logic; -- 100 MHz
	  CLK_spi_sl          : out    std_logic; -- 20  old:16 MHz
	  CLK_spi_ma          : out    std_logic; -- 24 MHz --> 12 MHz spi_clk -- TODO: Max freq on gum right now 2MHz!!! at 4MHz it stopps eventually (but works first)
	  -- Status and control signals
	  RESET             : in     std_logic;
	  LOCKED            : out    std_logic
	 );
	end component; 
	
	COMPONENT stretcher is
	generic (STRETCH_FACTOR : positive);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		sig_I : IN std_logic;          
		strech_O : OUT std_logic
		);
	END COMPONENT;

	COMPONENT adc_packaging
	Generic (PKG_BATCH_SIZE : positive);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		time_clk : IN std_logic;
		adc_off_I : IN std_logic;
		adc_off_delay_O : OUT std_logic;
		adc_sample_div_I : IN std_logic_vector(10 downto 0);
		css_I : IN std_logic_vector(20 downto 0);
		sss_I : IN std_logic_vector(16 downto 0);
		full_second_I : IN std_logic;
		spi_clk_I : IN std_logic;
		spi_nfrm_I : IN std_logic;
		spi_rx_I : IN std_logic;
		fifo_full_I : IN std_logic;       
		fifo_data_O : OUT std_logic_vector(31 downto 0);
		fifo_wr_en_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT spi_master_from_fifo
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		fifo_data_I : IN std_logic_vector(31 downto 0);
		fifo_empty_I : IN std_logic;
		fifo_valid_I : IN std_logic;
		gum_ready_I : IN std_logic;          
		spi_clk_O : OUT std_logic;
		spi_nfrm_O : OUT std_logic;
		spi_tx_O : OUT std_logic;
		fifo_rd_en_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT sram_btw_fifo
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		sram_full_O : out  STD_LOGIC;
		trfifo_data_I : IN std_logic_vector(15 downto 0);
		trfifo_empty_I : IN std_logic;
		trfifo_valid_I : IN std_logic;
		adcfifo_data_I : IN std_logic_vector(15 downto 0);
		adcfifo_empty_I : IN std_logic;
		adcfifo_valid_I : IN std_logic;
		outfifo_full_I : IN std_logic;
		sram_data_IO : INOUT std_logic_vector(15 downto 0);      
		trfifo_rd_en_O : OUT std_logic;
		adcfifo_rd_en_O : OUT std_logic;
		outfifo_data_O : OUT std_logic_vector(15 downto 0);
		outfifo_wr_en_O : OUT std_logic;
		sram_address_O : OUT std_logic_vector(18 downto 0);
		sram_cs_O : OUT std_logic;
		sram_lb_O : OUT std_logic;
		sram_ub_O : OUT std_logic;
		sram_we_O : OUT std_logic;
		sram_oe_O : OUT std_logic
		);
	END COMPONENT;
	
	-- 32 bit in, 1024 slots; 16bit out 2048 slots; First-Word Fall-Through
	COMPONENT fifo_to_sram_3216
	  PORT (
		 rst : IN STD_LOGIC;
		 wr_clk : IN STD_LOGIC;
		 rd_clk : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC;
		 valid : OUT STD_LOGIC
	  );
	END COMPONENT;
	
	-- 16 bit in, 2051 slots; 32bit out 1025 slots; First-Word Fall-Through
	COMPONENT fifo_from_sram_1632
	  PORT (
		 rst : IN STD_LOGIC;
		 wr_clk : IN STD_LOGIC;
		 rd_clk : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC;
		 valid : OUT STD_LOGIC
	  );
	END COMPONENT;
	
	COMPONENT time_calibration
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		start_I : IN std_logic;
		pps_I : IN std_logic;
		
		--debug
		tx_fifo_data : out  STD_LOGIC_VECTOR (7 downto 0);
		tx_fifo_wr_en : out  STD_LOGIC;
		tx_fifo_full : in  STD_LOGIC;
		pps_on_debug_I : in  STD_LOGIC;
		leds_pps_O: out STD_LOGIC;
		
		CSS_O : OUT std_logic_vector(20 downto 0);
		SSS_O : OUT std_logic_vector(16 downto 0);
		hard_reset_O : OUT std_logic;
		full_second_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT tracing_v2
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		start_I : IN std_logic;
		css_I : IN std_logic_vector(20 downto 0);
		sss_I : IN std_logic_vector(16 downto 0);
		full_second_I : IN std_logic;
		hard_rst_I : IN std_logic;
		on_off_conf_I : IN std_logic_vector(7 downto 0);
		trace_signal_I : IN std_logic_vector(7 downto 0);
		fifo_full_I : IN std_logic;          
		fifo_package_O : OUT std_logic_vector(31 downto 0);
		fifo_wr_en_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT controlling
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		actfifo_full_I : IN std_logic;
		actfifo_half_full_I : IN std_logic;
		uart_rxd_I : IN std_logic;          
		debugUart_1_O : OUT std_logic;
		debugUart_2_O : OUT std_logic;
		debugUart_3_O : OUT std_logic;
		debugUart_4_O : OUT std_logic;
		
		--more debug
		txfifo_select_I : in STD_LOGIC;
		txfifo_din_I : in STD_LOGIC_VECTOR(7 downto 0);
		txfifo_full_O : out STD_LOGIC;
		txfifo_wr_en_I : in STD_LOGIC;
		
		tr_config_O : OUT std_logic_vector(7 downto 0);
		call_config_O : OUT std_logic_vector(10 downto 0);
		start_config_O : OUT std_logic;
		route_through_O : OUT std_logic;
		sample_div_conf_O : OUT std_logic_vector(10 downto 0);
		actfifo_dout_O : OUT std_logic_vector(45 downto 0);
		actfifo_wr_en_O : OUT std_logic;
		uart_txd_O : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT actuator
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		start_I : IN std_logic;
		css_I : IN std_logic_vector(20 downto 0);
		sss_I : IN std_logic_vector(16 downto 0);
		fifo_data_I : IN std_logic_vector(45 downto 0);
		fifo_empty_I : IN std_logic;
		call_config_I : IN std_logic_vector(10 downto 0);
		call_tr_lvl_I : IN std_logic_vector(4 downto 0);
		call_nfrm_I : IN std_logic;
		def_sig1_act_I : IN std_logic;
		def_sig2_act_I : IN std_logic;
		def_rst_out_I : IN std_logic;
		def_adc_off_act_I : IN std_logic;          
		fifo_rd_en_O : OUT std_logic;
		sig1_act_O : OUT std_logic;
		sig2_act_O : OUT std_logic;
		rst_out_O : OUT std_logic;
		adc_off_O : OUT std_logic
		);
	END COMPONENT;
	
	-- 46 bit in, 128(130) slots; 46bit out 128(130) slots; First-Word Fall-Through
	COMPONENT fifo_act
	  PORT (
		 clk : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(45 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(45 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC;
		 valid : OUT STD_LOGIC;
		 prog_full : OUT STD_LOGIC
	  );
	END COMPONENT;
	
	COMPONENT burst_generator
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		sw1_I : IN std_logic;
		sw2_I : IN std_logic;
		sw3_I : IN std_logic;          
		pins_O : OUT std_logic_vector(2 downto 0)
		);
	END COMPONENT;
	------internal signals---------------------------------
	
	-- timing signals
	signal css_int : STD_LOGIC_VECTOR(20 DOWNTO 0);
	signal sss_int : STD_LOGIC_VECTOR(16 DOWNTO 0);
	signal hard_reset_int : STD_LOGIC;
	signal full_second_int : STD_LOGIC;
	
	--adc fifo signals
		-- input side
	signal adc_fifo_data : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal adc_fifo_wr_en : STD_LOGIC;
	signal adc_fifo_full : STD_LOGIC;
		-- output side
	signal fifo_sram_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal fifo_sram_rd_en : STD_LOGIC;
	signal fifo_sram_empty : STD_LOGIC;
	signal fifo_sram_valid : STD_LOGIC;
	
	--tracing fifo signals
		-- input side
	signal trfifo_data : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal trfifo_wr_en : STD_LOGIC;
	signal trfifo_full : STD_LOGIC;	
		-- output side
	signal trfifo_sram_rd_en : STD_LOGIC;
	signal trfifo_sram_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal trfifo_sram_empty : STD_LOGIC;
	signal trfifo_sram_valid : STD_LOGIC;
	
	--output fifo signals
		-- input side
	signal sram_fifo_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal sram_fifo_wr_en : STD_LOGIC;
	signal sram_fifo_full : STD_LOGIC;
		-- output side
	signal fifo_gum_data : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal fifo_gum_rd_en : STD_LOGIC;
	signal fifo_gum_empty : STD_LOGIC;
	signal fifo_gum_valid : STD_LOGIC;
	
	--actuation fifo signals
		-- input side
	signal actfifo_din : STD_LOGIC_VECTOR(45 DOWNTO 0);
	signal actfifo_wr_en : STD_LOGIC;
	signal actfifo_full : STD_LOGIC;
		-- output side
	signal actfifo_rd_en : STD_LOGIC;
	signal actfifo_dout : STD_LOGIC_VECTOR(45 DOWNTO 0);
	signal actfifo_empty : STD_LOGIC;
	signal actfifo_valid : STD_LOGIC;
	signal actfifo_half_full : STD_LOGIC;
	
	-- clock signals
	signal main_clk : STD_LOGIC; -- 100 MHz
	signal spi_sl_clk : STD_LOGIC; -- 20MHz
	signal spi_ma_clk : STD_LOGIC; -- 24MHz (aiming for 24MHz); gum_spi_clk_O = 1/2*spi_ma_clk
	signal sram_clk : STD_LOGIC; -- 100 MHz
	
	--set and rst signals
	signal start_int : STD_LOGIC;
	signal start_conf_int : STD_LOGIC;
	signal route_through_int : STD_LOGIC;
	signal route_through_rst : STD_LOGIC;
	signal gum_ready : STD_LOGIC;
	signal adc_off_int : STD_LOGIC; -- Input control signal, first delayed then ADC Hardware
	signal adc_off_delayed : STD_LOGIC; --Output to the ADC Hardware
	
	signal adc_sample_div : STD_LOGIC_VECTOR(10 downto 0);
	
	
	-- tracing signals
	signal tr_config : STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal trace_signals_int : STD_LOGIC_VECTOR(7 DOWNTO 0);
	
	--callback signal
	signal call_config_int : STD_LOGIC_VECTOR(10 DOWNTO 0);
	signal actuation_signals_int : STD_LOGIC_VECTOR(2 DOWNTO 0);
	signal CALLr : STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal callb : STD_LOGIC;
	
	-- debug signals
	signal gum_spi_nfrm_int : STD_LOGIC;
	signal gum_spi_clk_int : STD_LOGIC;
	signal gum_spi_tx_int : STD_LOGIC;
	
	signal debug_nfrm : STD_LOGIC;
	signal debug_3_int : STD_LOGIC;
	--signal debug_cnt : Integer;
	signal sram_full_int : STD_LOGIC;
	signal led_trfifo_full : STD_LOGIC;
	signal led_sram_full : STD_LOGIC;
	signal led_outfifo_full : STD_LOGIC;
	signal leds_int: STD_LOGIC;
	
	signal burst_pins : STD_LOGIC_VECTOR(2 downto 0);
	
	signal uart_txd_int  : STD_LOGIC;
	
	--time uart debug
	signal txfifo_din : STD_LOGIC_VECTOR(7 downto 0);
	signal txfifo_full : STD_LOGIC;
	signal txfifo_wr_en : STD_LOGIC;

begin

	gum_spi_nfrm_O <= gum_spi_nfrm_int;
	gum_spi_tx_O <= gum_spi_tx_int;
	gum_spi_clk_O <= gum_spi_clk_int;
	sram_clk <= main_clk;
	uart_txd_O <= uart_txd_int;
	actuation_signals_O <= actuation_signals_int;
	
	route_through_O <= route_through_int;
	--start_int <= not(gum_nready_I) OR sw1_I; --new by UART
	route_through_rst <= route_through_int OR rst;
	
	gum_ready <= not(gum_nready_I);
	
	adc_off_O <= adc_off_delayed; --Output to the ADC Hardware
	
	--debug_nfrm <= adc_spi_nfrm_I AND sw1_I;
	
	trace_signals_int <= trace_signal_I & actuation_signals_int; -- [LED1, LED2, LED3, INT1, INT2, SIG1, SIG2, RST]
		
	
		-- TODO: official 'trace_signal_I & actuation_signals_int' now for debug
		-- BALZ changes!
	--leds <= sss_int(0) & debug_3_int & led_trfifo_full & led_sram_full & led_outfifo_full & not(adc_off_int) & not(adc_off_delayed);
	leds <= '1';
--	process (main_clk,rst, sw1_I) -- debug signal generator
--	begin
--		if rst = '1' or sw1_I = '1' then
--			debug_nfrm <= '0';
--			--debug_cnt <= 0;
--		elsif rising_edge(main_clk) then
--			debug_nfrm <= css_int(3);			
--		end if;
--	end process;
--	
--	process (main_clk,rst, sw2_I) -- debug signal generator
--	begin
--		if rst = '1' or sw2_I = '1' then
--			callb <= '0';
--			--debug_cnt <= 0;
--		elsif rising_edge(main_clk) then
--			callb <= css_int(2);			
--		end if;
--	end process;
	
	led1_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 100000)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => trfifo_full,
		strech_O => led_trfifo_full
	);
	
	led2_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 100000)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => sram_full_int,
		strech_O => led_sram_full
	);
	
	led3_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 100000)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => sram_fifo_full,
		strech_O => led_outfifo_full
	);
	
	debug1_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 100)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => trace_signals_int(3)
		-- strech_O => debug_1_O
	);

	debug2_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 100)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => trace_signals_int(4)
		-- strech_O => debug_2_O
	);

	
	debug3_stretcher: stretcher 
	generic map (STRETCH_FACTOR => 30000000)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => full_second_int,
		strech_O => debug_3_int
	);
	debug_4_O <= debug_3_int;
	
	debug4_stretcher: stretcher  
	generic map (STRETCH_FACTOR => 100)
	PORT MAP(
		clk => main_clk,
		rst => rst,
		sig_I => sram_fifo_wr_en
		-- strech_O => debug_4_O
	);
	
	-- mirror Gumstix SPI bus to debug port
	debug_1_O <= gum_spi_clk_int;
	debug_2_O <= gum_spi_nfrm_int;
	debug_3_O <= gum_spi_tx_int;
	
	--###################################--
	--      start on full second         --
	--###################################--
	
	process (main_clk,rst) -- callback switch debounce
	begin
		if rst = '1' then
			start_int <= '0';
		elsif rising_edge(main_clk) then
			if start_conf_int = '1' AND full_second_int = '1' then
				start_int <= '1';
			elsif start_conf_int = '0' then
				start_int <= '0';
			end if;
		end if;
	end process;
	
--	process (main_clk,rst) -- callback switch debounce
--	begin
--		if rst = '1' then
--			callb <= '1';
--			CALLr <= "1111";
--		elsif rising_edge(main_clk) then
--			CALLr <= CALLr(2 downto 0) & sw2_I;
--			if CALLr(3 downto 1) = "111" then
--				callb <= '1';
--			elsif CALLr(3 downto 1) = "000" then
--				callb <= '0';
--			end if;
--		end if;
--	end process;

	clock_generator : clock_gen
	  port map
		(-- Clock in ports
		 CLK_IN1 => clk,
		 -- Clock out ports
		 CLK_main => main_clk,
		 CLK_spi_sl => spi_sl_clk,
		 CLK_spi_ma => spi_ma_clk,
		 -- Status and control signals
		 RESET  => rst);


	ADC_PKG: adc_packaging 
	Generic Map (PKG_BATCH_SIZE => 100) --TODO: what size?
	PORT MAP(
		clk => spi_sl_clk,
		rst => route_through_rst,
		
		time_clk => main_clk,
		adc_off_I => adc_off_int,
		adc_off_delay_O => adc_off_delayed,
		adc_sample_div_I => adc_sample_div,
		
		css_I => css_int,
		sss_I => sss_int,
		full_second_I => full_second_int,
		
		spi_clk_I => adc_spi_clk_I,
		spi_nfrm_I => adc_spi_nfrm_I,
		spi_rx_I => adc_spi_rx_I,
		
		fifo_data_O => adc_fifo_data,
		fifo_wr_en_O => adc_fifo_wr_en,
		fifo_full_I => adc_fifo_full
	);
	
	SPI_OUT: spi_master_from_fifo PORT MAP(
		clk => spi_ma_clk,
		rst => route_through_rst,
		spi_clk_O => gum_spi_clk_int,
		spi_nfrm_O => gum_spi_nfrm_int,
		spi_tx_O => gum_spi_tx_int,
		fifo_data_I => fifo_gum_data,
		fifo_rd_en_O => fifo_gum_rd_en,
		fifo_empty_I => fifo_gum_empty,
		fifo_valid_I => fifo_gum_valid,
		gum_ready_I => gum_ready
	);
	
	SRAM: sram_btw_fifo PORT MAP(
		clk => sram_clk,
		rst => route_through_rst,
		sram_full_O => sram_full_int,
		trfifo_rd_en_O => trfifo_sram_rd_en,
		trfifo_data_I => trfifo_sram_data,
		trfifo_empty_I => trfifo_sram_empty,
		trfifo_valid_I => trfifo_sram_valid,
		adcfifo_rd_en_O => fifo_sram_rd_en,
		adcfifo_data_I => fifo_sram_data,
		adcfifo_empty_I => fifo_sram_empty,
		adcfifo_valid_I => fifo_sram_valid,
		outfifo_data_O => sram_fifo_data,
		outfifo_full_I => sram_fifo_full,
		outfifo_wr_en_O => sram_fifo_wr_en,
		sram_address_O => sram_address_O,
		sram_data_IO => sram_data_IO,
		sram_cs_O => sram_cs_O,
		sram_lb_O => sram_lb_O,
		sram_ub_O => sram_ub_O,
		sram_we_O => sram_we_O,
		sram_oe_O => sram_oe_O
	);
	
	fifo_out : fifo_from_sram_1632
	  PORT MAP (
		 rst => route_through_rst,
		 wr_clk => sram_clk,
		 rd_clk => spi_ma_clk,
		 din => sram_fifo_data,
		 wr_en => sram_fifo_wr_en,
		 rd_en => fifo_gum_rd_en,
		 dout => fifo_gum_data,
		 full => sram_fifo_full,
		 empty => fifo_gum_empty,
		 valid => fifo_gum_valid
	  );

	fifo_adc : fifo_to_sram_3216
	  PORT MAP (
		 rst => route_through_rst,
		 wr_clk => spi_sl_clk,
		 rd_clk => sram_clk,
		 din => adc_fifo_data,
		 wr_en => adc_fifo_wr_en,
		 rd_en => fifo_sram_rd_en,
		 dout => fifo_sram_data,
		 full => adc_fifo_full,
		 empty => fifo_sram_empty,
		 valid => fifo_sram_valid
		 );

	fifo_tracing : fifo_to_sram_3216
	  PORT MAP (
		 rst => route_through_rst,
		 wr_clk => main_clk,
		 rd_clk => sram_clk,
		 din => trfifo_data,
		 wr_en => trfifo_wr_en,
		 rd_en => trfifo_sram_rd_en,
		 dout => trfifo_sram_data,
		 full => trfifo_full,
		 empty => trfifo_sram_empty,
		 valid => trfifo_sram_valid
		 );

	TIME_CALIB: time_calibration PORT MAP(
		clk => main_clk,
		rst => rst,
		start_I => start_int,
		pps_I => pps_I,
		
		--debug
		tx_fifo_data => txfifo_din,
		tx_fifo_wr_en => txfifo_wr_en,
		tx_fifo_full => txfifo_full,
		pps_on_debug_I => pps_on_debug_I,
		leds_pps_O => leds_int,
		CSS_O => css_int,
		SSS_O => sss_int,
		hard_reset_O => hard_reset_int,
		full_second_O => full_second_int
	);

	TRACING: tracing_v2 PORT MAP(
		clk => main_clk,
		rst => route_through_rst,
		start_I => start_int,
		css_I => css_int,
		sss_I => sss_int,
		full_second_I => full_second_int,
		hard_rst_I => hard_reset_int,
		on_off_conf_I => tr_config,
		trace_signal_I => trace_signals_int,
		fifo_package_O => trfifo_data,
		fifo_wr_en_O => trfifo_wr_en,
		fifo_full_I => trfifo_full
	);

	CONTROL: controlling PORT MAP(
		clk => main_clk,
		rst => rst,
		--debugUart_1_O => ,
		--debugUart_2_O => ,
		--debugUart_3_O => ,
		--debugUart_4_O => ,
		
		txfifo_select_I => txfifo_select_I,
		txfifo_din_I => txfifo_din,
		txfifo_full_O => txfifo_full,
		txfifo_wr_en_I => txfifo_wr_en,
		
		
		tr_config_O => tr_config,
		call_config_O => call_config_int,
		start_config_O => start_conf_int,
		route_through_O => route_through_int,
		sample_div_conf_O => adc_sample_div,
		actfifo_dout_O => actfifo_din,
		actfifo_full_I => actfifo_full,
		actfifo_half_full_I => actfifo_half_full,
		actfifo_wr_en_O => actfifo_wr_en,
		uart_txd_O => uart_txd_int,
		uart_rxd_I => uart_rxd_I
	);

	ACTUATION: actuator PORT MAP(
		clk => main_clk,
		rst => route_through_rst,
		start_I => start_int,
		css_I => css_int,
		sss_I => sss_int,
		fifo_data_I => actfifo_dout,
		fifo_empty_I => actfifo_empty,
		fifo_rd_en_O => actfifo_rd_en,
		call_config_I => call_config_int,
		call_nfrm_I => adc_spi_nfrm_I,
		call_tr_lvl_I => trace_signals_int(7 downto 3),
		def_sig1_act_I => '0', --TODO def assignements
		def_sig2_act_I => '0',
		def_rst_out_I => '1',
		def_adc_off_act_I => '1',
		sig1_act_O => actuation_signals_int(2),
		sig2_act_O => actuation_signals_int(1),
		rst_out_O => actuation_signals_int(0),
		adc_off_O => adc_off_int
	);
	
	fifo_actuation : fifo_act
	  PORT MAP (
		 clk => main_clk,
		 rst => route_through_rst,
		 din => actfifo_din,
		 wr_en => actfifo_wr_en,
		 rd_en => actfifo_rd_en,
		 dout => actfifo_dout,
		 full => actfifo_full,
		 prog_full => actfifo_half_full,
		 empty => actfifo_empty --,
		 --valid => actfifo_valid
	  );
	  
	  

	BURST_GEN: burst_generator PORT MAP(
		clk => main_clk,
		rst => rst,
		sw1_I => sw1_I,
		sw2_I => sw2_I,
		sw3_I => sw3_I,
		pins_O => burst_pins
	);
	

end Behavioral;

