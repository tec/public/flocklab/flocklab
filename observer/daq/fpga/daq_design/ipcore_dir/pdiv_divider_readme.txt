The following files were generated for 'pdiv_divider' in directory
/home/bmaag/code/daq_vhdl/rev1.2/DAQ_Board_rev1.2/ipcore_dir/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * pdiv_divider.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * pdiv_divider.ngc
   * pdiv_divider.vhd
   * pdiv_divider.vho

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * pdiv_divider.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * pdiv_divider.asy

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * pdiv_divider_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * pdiv_divider.gise
   * pdiv_divider.xise

Deliver Readme:
   Readme file for the IP.

   * pdiv_divider_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * pdiv_divider_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

