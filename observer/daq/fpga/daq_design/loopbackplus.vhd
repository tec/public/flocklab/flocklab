--------------------------------------------------------------------------------
-- UART
-- Simple loopback
--           
-- @author         Peter A Bennett
-- @copyright      (c) 2012 Peter A Bennett
-- @license        LGPL      
-- @email          pab850@googlemail.com
-- @contact        www.bytebash.com
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LOOPBACKPLUS is
    port 
    (  
        -- General
        CLOCK                   :   in      std_logic;
        RESET                   :   in      std_logic;    
        RX                      :   in      std_logic;
        TX                      :   out     std_logic;
		  
		  --fifo read
        read_fifo_data_I          :   in     std_logic_vector(7 downto 0);
        read_fifo_rd_en_O         :   out    std_logic;    
        read_fifo_empty_I         :   in     std_logic;
        read_fifo_valid_I         :   in     std_logic;
		  
		  
		  --fifo write
        write_fifo_data_O         :   out    std_logic_vector(7 downto 0);
        write_fifo_wr_en_O        :   out    std_logic;    
        write_fifo_full_I         :   in     std_logic
        --write_fifo_wr_ack_I       :   in     std_logic
    );
end LOOPBACKPLUS;

architecture RTL of LOOPBACKPLUS is
    ----------------------------------------------------------------------------
    -- UART constants
    ----------------------------------------------------------------------------
    
    constant BAUD_RATE              : positive := 1000000;  -- init: 115200; MAX_value by experiment: 1000000
    constant CLOCK_FREQUENCY        : positive := 100000000;
	 
	 --TODO impl: acknolegement, now loopback for debug
	 --constant ACK_DATA : std_logic_vector(7 downto 0) := "00110011";
	 --constant ACK_HEADER : std_logic_vector(7 downto 0) := "00001111";
    
    ----------------------------------------------------------------------------
    -- Component declarations
    ----------------------------------------------------------------------------
    component UART is
        generic (
                BAUD_RATE           : positive;
                CLOCK_FREQUENCY     : positive
            );
        port (  -- General
                CLOCK               :   in      std_logic;
                RESET               :   in      std_logic;    
                DATA_STREAM_IN      :   in      std_logic_vector(7 downto 0);
                DATA_STREAM_IN_STB  :   in      std_logic;
                DATA_STREAM_IN_ACK  :   out     std_logic;
                DATA_STREAM_OUT     :   out     std_logic_vector(7 downto 0);
                DATA_STREAM_OUT_STB :   out     std_logic;
                DATA_STREAM_OUT_ACK :   in      std_logic;
                TX                  :   out     std_logic;
                RX                  :   in      std_logic
             );
    end component UART;
    
    ----------------------------------------------------------------------------
    -- UART signals
    ----------------------------------------------------------------------------
    
    signal uart_data_in             : std_logic_vector(7 downto 0);
    signal uart_data_out            : std_logic_vector(7 downto 0);
    signal uart_data_in_stb         : std_logic;
    signal uart_data_in_ack         : std_logic;
    signal uart_data_out_stb        : std_logic;
    signal uart_data_out_ack        : std_logic;
	 
	 signal write_fifo_wr_en_int : std_logic;
  
begin
	
	write_fifo_wr_en_O <= write_fifo_wr_en_int;
	
    ----------------------------------------------------------------------------
    -- UART instantiation
    ----------------------------------------------------------------------------

    UART_inst1 : UART
    generic map (
            BAUD_RATE           => BAUD_RATE,
            CLOCK_FREQUENCY     => CLOCK_FREQUENCY
    )
    port map    (  
            -- General
            CLOCK               => CLOCK,
            RESET               => RESET,
            DATA_STREAM_IN      => uart_data_in,
            DATA_STREAM_IN_STB  => uart_data_in_stb,
            DATA_STREAM_IN_ACK  => uart_data_in_ack,
            DATA_STREAM_OUT     => uart_data_out,
            DATA_STREAM_OUT_STB => uart_data_out_stb,
            DATA_STREAM_OUT_ACK => uart_data_out_ack,
            TX                  => TX,
            RX                  => RX
    );
    
    ----------------------------------------------------------------------------
    -- Simple loopback, retransmit any received data
    ----------------------------------------------------------------------------
    
    UART_LOOPBACK : process (CLOCK)
    begin
        if rising_edge(CLOCK) then
            if RESET = '1' then
                uart_data_in_stb        <= '0';
                uart_data_out_ack       <= '0';
                uart_data_in            <= (others => '0');
            else
                -- Acknowledge data receive strobes and set up a transmission
                -- request
					 read_fifo_rd_en_O 	 <= '0';
					 write_fifo_wr_en_int	 <= '0';
                uart_data_out_ack       <= '0';
					 
					 -- on receiving new UART package, write data to RX-FIFO
                if uart_data_out_stb = '1' AND write_fifo_full_I = '0'AND  write_fifo_wr_en_int = '0' then 
                    uart_data_out_ack   	<= '1';
						  write_fifo_data_O     <= uart_data_out;
						  write_fifo_wr_en_int	<= '1';
					 end if;
					 
					 -- read data from TX-FIFO and send it over UART
					 if read_fifo_empty_I = '0' AND read_fifo_valid_I = '1' AND uart_data_in_stb = '0' then 
						  read_fifo_rd_en_O 	 <= '1';
                    uart_data_in_stb    <= '1';
                    uart_data_in        <= read_fifo_data_I;
					 end if;
                
                -- Clear transmission request strobe upon acknowledge.
                if uart_data_in_ack = '1' then
                    uart_data_in_stb    <= '0';
                end if;
            end if;
        end if;
    end process;
            
end RTL;