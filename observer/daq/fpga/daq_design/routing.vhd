----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    10:35:17 02/25/2014
-- Design Name:
-- Module Name:    routing - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity routing is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;

				--debug direct through
				gps_in: IN STD_LOGIC;
				debug_1_O : OUT std_logic;
				debug_2_O : OUT std_logic;
				debug_3_O : OUT std_logic;
				debug_4_O : OUT std_logic;
				debug_gumstix_0_O : OUT std_logic;
				debug_gumstix_1_O : OUT std_logic;
				leds : OUT std_logic;

				-- PPS direct through
				pps_I : IN std_logic;

				--routing
				trace_signal_I : IN std_logic_vector(4 downto 0);
				actuation_signals_O : OUT std_logic_vector(2 downto 0);

				adc_spi_clk_I : IN std_logic;
				adc_spi_nfrm_I : IN std_logic;
				adc_spi_rx_I : IN std_logic;
				gum_nready_I : IN std_logic;

				adc_off_O : OUT std_logic;
				gum_spi_clk_O : OUT std_logic;
				gum_spi_nfrm_O : OUT std_logic;
				gum_spi_tx_O : OUT std_logic;

				--original gumstix signals
				trace_signal_O : OUT std_logic_vector(4 downto 0);
				actuation_signals_I : IN std_logic_vector(2 downto 0);


				-- UART direct through
				uart_txd_O : OUT std_logic;
				uart_rxd_I : IN std_logic;

				-- SRAM direct through
				sram_data_IO : INOUT std_logic_vector(15 downto 0);
				sram_address_O : OUT std_logic_vector(18 downto 0);
				sram_cs_O : OUT std_logic;
				sram_lb_O : OUT std_logic;
				sram_ub_O : OUT std_logic;
				sram_we_O : OUT std_logic;
				sram_oe_O : OUT std_logic;

				-- BALZ: added backup tracing signals
				bt_cts_g : IN std_logic;
				bt_cts_f : OUT std_logic;
				bt_rxd_g : IN std_logic;
				bt_rxd_f : OUT std_logic;
				bt_rts_g : OUT std_logic;
				bt_rts_f : IN std_logic;
				bt_txd_g : OUT std_logic;
				bt_txd_f : IN std_logic);
				-- eoBALZ!

end routing;

architecture Behavioral of routing is

	COMPONENT daq_wrapper
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;

		pps_I : IN std_logic;
		trace_signal_I : IN std_logic_vector(4 downto 0);
		adc_spi_clk_I : IN std_logic;
		adc_spi_nfrm_I : IN std_logic;
		adc_spi_rx_I : IN std_logic;
		gum_nready_I : IN std_logic;
		uart_rxd_I : IN std_logic;
		sram_data_IO : INOUT std_logic_vector(15 downto 0);
		debug_1_O : OUT std_logic;
		debug_2_O : OUT std_logic;
		debug_3_O : OUT std_logic;
		debug_4_O : OUT std_logic;
		debug_gumstix_0_O : OUT std_logic;
		debug_gumstix_1_O : OUT std_logic;
		leds : OUT std_logic;
		actuation_signals_O : OUT std_logic_vector(2 downto 0);
		adc_off_O : OUT std_logic;
		gum_spi_clk_O : OUT std_logic;
		gum_spi_nfrm_O : OUT std_logic;
		gum_spi_tx_O : OUT std_logic;
		route_through_O : OUT std_logic;
		uart_txd_O : OUT std_logic;
		sram_address_O : OUT std_logic_vector(18 downto 0);
		sram_cs_O : OUT std_logic;
		sram_lb_O : OUT std_logic;
		sram_ub_O : OUT std_logic;
		sram_we_O : OUT std_logic;
		sram_oe_O : OUT std_logic
		);
	END COMPONENT;

	signal leds_int : STD_LOGIC;
	signal tracing_int : STD_LOGIC_VECTOR(4 downto 0);
	signal actuation_int : STD_LOGIC_VECTOR(2 downto 0);
	signal route_through_int :  STD_LOGIC;
	-- BALZ:
	signal bt_cts_int: STD_LOGIC;
	signal bt_rxd_int: STD_LOGIC;
	signal bt_rts_int: STD_LOGIC;
	signal bt_txd_int: STD_LOGIC;
	-- eoBALZ!
	signal adc_spi_clk_int 	:  STD_LOGIC;
	signal adc_spi_nfrm_int :  STD_LOGIC;
	signal adc_spi_rx_int 	:  STD_LOGIC;
	signal adc_off_int 		:  STD_LOGIC;
	signal gum_spi_clk_int 	:  STD_LOGIC;
	signal gum_spi_nfrm_int :  STD_LOGIC;
	signal gum_spi_tx_int 	:  STD_LOGIC;
	signal gum_nready_int 	:  STD_LOGIC;

begin


	--SPI inputs:
	adc_spi_clk_int	<= adc_spi_clk_I 		when route_through_int = '0' else '0';
	adc_spi_nfrm_int	<= adc_spi_nfrm_I 	when route_through_int = '0' else '1';
	adc_spi_rx_int		<= adc_spi_rx_I 		when route_through_int = '0' else '0';
	gum_nready_int		<= gum_nready_I 		when route_through_int = '0' else '1';

	--tracing inputs:
	tracing_int 	<= trace_signal_I 	when route_through_int = '0' else (others => '0');

	--output to gumstix:
	trace_signal_O <= (others => '0') 	when route_through_int = '0' else trace_signal_I;

	--SPI output:
	adc_off_O		<= adc_off_int 		when route_through_int = '0' else gum_nready_I;
	gum_spi_clk_O	<= gum_spi_clk_int	when route_through_int = '0' else adc_spi_clk_I;
	gum_spi_nfrm_O	<= gum_spi_nfrm_int	when route_through_int = '0' else adc_spi_nfrm_I;
	gum_spi_tx_O	<= gum_spi_tx_int		when route_through_int = '0' else adc_spi_rx_I;

	--actuation output
	actuation_signals_O <= actuation_int when route_through_int = '0' else actuation_signals_I;

	-- BALZ: use this part to capture the following four pins and use them as internal signals bt_xxx_int:

	-- bt_cts_int 	<= bt_cts_g 	when route_through_int = '0' else '0';
	-- bt_cts_f <= '0' when route_through_int = '0' else bt_cts_g;

	-- bt_rxd_int 	<= bt_rxd_g 	when route_through_int = '0' else '0';
	-- bt_rxd_f <= '0' when route_through_int = '0' else bt_rxd_g;

	-- bt_rts_int 	<= bt_rts_f 	when route_through_int = '0' else '0';
	-- bt_rts_g <= '0' when route_through_int = '0' else bt_rts_f;

	-- bt_txd_int 	<= bt_txd_f 	when route_through_int = '0' else '0';
	-- bt_txd_g <= '0' when route_through_int = '0' else bt_txd_f;

	-- BALZ: use this part to route the bt_xxx_f/g directly through:
	bt_cts_f <= bt_cts_g;
	bt_rxd_f <= bt_rxd_g;
	bt_rts_g <= bt_rts_f;
	bt_txd_g <= bt_txd_f;
	-- eoBALZ!

	--TODO default act values!!

	DAQ: daq_wrapper PORT MAP(
		clk => clk,
		rst => rst,
		debug_1_O => debug_1_O,
		debug_2_O => debug_2_O,
		debug_3_O => debug_3_O,
		debug_4_O => debug_4_O,
		debug_gumstix_0_O => debug_gumstix_0_O,
		debug_gumstix_1_O => debug_gumstix_1_O,
		leds => leds,
		pps_I => pps_I,
		-- Rtrueb: use gps (gps input) directly instead of gps via CC430 (pps input) input
		--pps_I => gps_in,
		-- eoRtrueb
		trace_signal_I => tracing_int,
		actuation_signals_O => actuation_int,
		adc_spi_clk_I => adc_spi_clk_int,
		adc_spi_nfrm_I => adc_spi_nfrm_int,
		adc_spi_rx_I => adc_spi_rx_int,
		adc_off_O => adc_off_int,
		gum_spi_clk_O => gum_spi_clk_int,
		gum_spi_nfrm_O => gum_spi_nfrm_int,
		gum_spi_tx_O => gum_spi_tx_int,
		gum_nready_I => gum_nready_int,
		route_through_O => route_through_int,
		uart_txd_O => uart_txd_O,
		uart_rxd_I => uart_rxd_I,
		sram_address_O => sram_address_O,
		sram_data_IO => sram_data_IO,
		sram_cs_O => sram_cs_O,
		sram_lb_O => sram_lb_O,
		sram_ub_O => sram_ub_O,
		sram_we_O => sram_we_O,
		sram_oe_O => sram_oe_O
	);

end Behavioral;
