----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:45:21 12/23/2013 
-- Design Name: 
-- Module Name:    spi_master_from_fifo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spi_master_from_fifo is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  --SPI
           spi_clk_O : out  STD_LOGIC;
           spi_nfrm_O : out  STD_LOGIC;
           spi_tx_O : out  STD_LOGIC;
			  
			  --Fifo signals
           fifo_data_I : in  STD_LOGIC_VECTOR (31 downto 0);
           fifo_rd_en_O : out  STD_LOGIC;
           fifo_empty_I : in  STD_LOGIC;
           fifo_valid_I : in  STD_LOGIC;
			  
			  --Gum ready
           gum_ready_I : in  STD_LOGIC); --former adc_off_I
end spi_master_from_fifo;

architecture Behavioral of spi_master_from_fifo is

	type states is (start, idle, frm_init, spi_clk0, spi_clk1);	
	--type sclk_states is (low, high);
	
	constant MAX_BIT : INTEGER := 32; --TODO change on 32bit Impl
	constant DELAY : INTEGER := 15; -- 0 = no delay; delay after each sent package, in half-spi cycles
	
	signal state : states := start;	
	signal delay_cnt : INTEGER range 0 to DELAY := 0;
	signal spi_bit : INTEGER range 0 to MAX_BIT := 0;	
	signal spi_data : STD_LOGIC_VECTOR (MAX_BIT-1 downto 0) := (others => '0');
	
	signal sclk_int : STD_LOGIC := '0';
	signal frm_int : STD_LOGIC := '0';

begin

	spi_nfrm_O <= not(frm_int);
	spi_clk_O <= sclk_int;
	
	--rst_spi <= rst OR not(gum_ready_I); --TODO gum_nready not as reset --> data lost, but as stay idle.
	
	process (clk, rst)
	begin
		if rst = '1' then
		
			sclk_int <= '0';
			
		elsif rising_edge(clk) then
			
			sclk_int <= not(sclk_int);
		
		end if;
	end process;
	
	
	process (clk, rst)
	begin
	
		if rst = '1' then
			
			fifo_rd_en_O	<= '0';
			spi_data 		<= (others => '0');
			frm_int			<= '0';
			spi_tx_O 		<= '0';
			spi_bit			<= 0;
			delay_cnt		<= 0;

			state 			<= start;

		elsif rising_edge(clk) then			-- change data on falling, read on rising
														-- nfrm one period before first valid databit
			case state is
				
				when start				=> fifo_rd_en_O		<= '0';
												spi_bit				<= 0;
												frm_int 				<= '0';
												spi_tx_O 			<= '0';
												
												if(gum_ready_I = '1') then -- after DMA_BUFFER_SIZE packages sent, wait for DELAY time slots
													if delay_cnt >= DELAY then
														delay_cnt	<= 0;
														state 		<= idle;
													else
														delay_cnt	<= delay_cnt + 1;
														state 		<= start;
													end if;			
													
												else
													state 			<= start;
												end if;
												
				
				when idle 				=> if fifo_empty_I = '0' AND fifo_valid_I = '1' AND sclk_int = '1' AND gum_ready_I = '1' then
													
													fifo_rd_en_O 	<= '1'; 
													spi_data 		<= fifo_data_I(MAX_BIT-1 downto 0); --TODO change on 32bit Impl
													spi_bit			<= MAX_BIT;
													frm_int 			<= '1';
													state 			<= frm_init;
													
												else
												
													state 		<= idle;
													
												end if;
													
				when frm_init			=> fifo_rd_en_O 	<= '0';
												frm_int 			<= '1';
												state 			<= spi_clk0;
											
				
				when spi_clk0			=> if spi_bit = 0 then
													frm_int 			<= '0';
													spi_tx_O			<= '0';
													state 			<= start;
												else
													frm_int 			<= '0';
													spi_tx_O			<= spi_data(spi_bit-1);
													state 			<= spi_clk1;												
											end if;
												
				
				when spi_clk1			=> spi_bit 		<= spi_bit -1;
												frm_int 		<= '0';
												state 		<= spi_clk0;
												
													
			end case;
		
		end if;
		
	end process;	
	
end Behavioral;

