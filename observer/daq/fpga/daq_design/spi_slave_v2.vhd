----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:03:50 01/20/2014 
-- Design Name: 
-- Module Name:    spi_slave_v2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spi_slave_v2 is
	Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  --debug
			  --debug_led_O : out  STD_LOGIC;
			  
			  -- SPI input signals from ADC
           spi_clk_I : in  STD_LOGIC;
           spi_nfrm_I : in  STD_LOGIC;
           spi_rx_I : in  STD_LOGIC;
			  
			  -- Fifo output signals (to sram, to gum)
           data_O : out  STD_LOGIC_VECTOR (23 downto 0);
           data_new_O : out  STD_LOGIC;
           data_ack_I : in  STD_LOGIC);
end spi_slave_v2;

architecture Behavioral of spi_slave_v2 is	

	type states is (idle, wait_for_data); --TODO:
	signal state : states := idle;
	
	constant MAX_ADC_BIT : INTEGER := 24;
	
	signal SCLKr : STD_LOGIC_VECTOR (2 downto 0);
	signal sclk_risingedge : STD_LOGIC := '0';	
	signal sclk_fallingedge : STD_LOGIC := '0';	
	
	signal SFRMr : STD_LOGIC_VECTOR (1 downto 0);
	signal sfrm_active : STD_LOGIC := '0';
	
	signal MOSIr : STD_LOGIC_VECTOR (1 downto 0);
	signal MOSI_data : STD_LOGIC := '0';	
	
	signal sbit_cnt : INTEGER range 0 to MAX_ADC_BIT := 0;	
	signal sdata_pkg_received : STD_LOGIC := '0';	
	signal sdata_pkg : STD_LOGIC_VECTOR (MAX_ADC_BIT-1 downto 0) := (others => '0');
	
begin

	--// sync SCK to the FPGA clock using a 3-bits shift register
	Shift_sclk : process (clk,rst)
	begin
		if rst = '1' then
			SCLKr <= "000";
		elsif rising_edge(clk) then
			SCLKr <= SCLKr(1 downto 0) & spi_clk_I;
		end if;
	end process;
	
	sclk_risingedge <= '1' when (SCLKr(2 downto 1) = "01") else '0';
	sclk_fallingedge <= '1' when (SCLKr(2 downto 1) = "10") else '0';

	--// same thing for SSEL
	Shift_frm : process (clk,rst)
	begin
		if rst = '1' then
			SFRMr <= "11";
		elsif rising_edge(clk) then
			SFRMr <= SFRMr(0) & spi_nfrm_I;
		end if;
	end process;
	
	
	Set_frame_window : process (clk,rst)
	begin
		if rst = '1' then
			sfrm_active <= '0';
			sdata_pkg_received <= '0';
		elsif rising_edge(clk) then			
			if sclk_risingedge = '1' AND SFRMr(1) = '0' then
				sfrm_active <= '1';				
				sdata_pkg_received <= '0';
			elsif sbit_cnt = MAX_ADC_BIT then
				sfrm_active <= '0';
				sdata_pkg_received <= '1';
			else
				sdata_pkg_received <= '0';
			end if;
		end if;
	end process;
	
	
	Count_received_bits : process (clk,rst)
	begin
		if rst = '1' then
			sbit_cnt <= 0;
		elsif rising_edge(clk) then			
			if sfrm_active = '0' then
				sbit_cnt <= 0;
			elsif sclk_fallingedge = '1' then
				sbit_cnt <= sbit_cnt + 1;
			elsif sbit_cnt = MAX_ADC_BIT then
				sbit_cnt <= 0;
			end if;
		end if;
	end process;
	
	
	--// and for MOSI
	Shift_rx_data : process (clk,rst)
	begin
		if rst = '1' then
			MOSIr <= "00";
		elsif rising_edge(clk) then
			MOSIr <= MOSIr(0) & spi_rx_I;
		end if;
	end process;
	
	MOSI_data <= MOSIr(1);
	
	
	
	--	// we handle SPI in 8-bits format, so we need a 8bit shift register	
	Fill_data_register : process (clk,rst)
	begin
		if rst = '1' then
			sdata_pkg <= (others => '0');
		elsif rising_edge(clk) then
			if sfrm_active = '1' AND sclk_fallingedge = '1' then -- TODO				
				sdata_pkg <= sdata_pkg(MAX_ADC_BIT-2 downto 0) & MOSI_data;
			end if;
		end if;
	end process;
	
	
	Output_handshake : process (clk, rst)
	begin
	
		if rst = '1' then			
				data_O <= (others => '0');
				data_new_O <= '0';
				state <= idle;

		elsif rising_edge(clk) then			
			
			case state is
				
				when idle				=> if sfrm_active = '1' then
													data_new_O <= '0';
													state <= wait_for_data;
												elsif data_ack_I = '1' then
													data_new_O <= '0';
													state <= idle;
												else
													state <= idle;													
												end if;
												

				when wait_for_data		=>	if sdata_pkg_received = '1' then
														data_O <= sdata_pkg;
														data_new_O <= '1';
														state <= idle;
													else
														state <= wait_for_data;
													end if;
													
			end case;
			
		end if;
	end process;

end Behavioral;
