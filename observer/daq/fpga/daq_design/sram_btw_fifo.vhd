----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:40:02 12/23/2013 
-- Design Name: 
-- Module Name:    sram_btw_fifo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sram_btw_fifo is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
			  -- debug
			  --debug_addr_diff_O : out  STD_LOGIC_VECTOR (18 downto 0);
			  sram_full_O : out  STD_LOGIC;
			  
			  -- Input Tracing FIFO signals
           trfifo_rd_en_O : out  STD_LOGIC;
           trfifo_data_I : in  STD_LOGIC_VECTOR (15 downto 0);
           trfifo_empty_I : in  STD_LOGIC;
			  trfifo_valid_I : in  STD_LOGIC;
			  
			  -- Input ADC FIFO signals
           adcfifo_rd_en_O : out  STD_LOGIC;
           adcfifo_data_I : in  STD_LOGIC_VECTOR (15 downto 0);
           adcfifo_empty_I : in  STD_LOGIC;
           adcfifo_valid_I : in  STD_LOGIC;
			  
			  -- Output FIFO signals
           outfifo_data_O : out  STD_LOGIC_VECTOR (15 downto 0);
           outfifo_full_I : in  STD_LOGIC;
           outfifo_wr_en_O : out  STD_LOGIC;
			  
			  -- SRAM
			  sram_address_O : out  STD_LOGIC_VECTOR (18 downto 0);
           sram_data_IO : inout  STD_LOGIC_VECTOR (15 downto 0);
           sram_cs_O : out  STD_LOGIC;
           sram_lb_O : out  STD_LOGIC;
           sram_ub_O : out  STD_LOGIC;
           sram_we_O : out  STD_LOGIC;
           sram_oe_O : out  STD_LOGIC);
end sram_btw_fifo;

architecture Behavioral of sram_btw_fifo is

	type select_states is (sel_idle, sel_tr, sel_adc, sel_read);
	
	type ram_states is (ram_idle, ram_address, 
							ram_read_1, ram_read_2, ram_read_3, ram_read_4, ram_read_5,
							ram_write_1, ram_write_2, ram_write_3, ram_write_4, ram_write_5);						
	

	signal ram_state	: ram_states;
	signal sel_state	: select_states;
	
	signal sram_lbub	: STD_LOGIC;
	
	-- addresses are unsigned, 19bit values
	signal write_address : INTEGER range  (2**sram_address_O'length)-1 downto 0 := 0; --TODO: descripe wr/rd address pointers. wr=rd => empty; wr=rd-1 => full (no more writing)
	signal read_address : INTEGER range  (2**sram_address_O'length)-1 downto 0 := 0; 
	--signal read_address : STD_LOGIC_VECTOR (18 downto 0) := (others => '0'); 
	
	signal write_data : STD_LOGIC_VECTOR (15 downto 0) := (others => '0'); 
	signal read_data : STD_LOGIC_VECTOR (15 downto 0) := (others => '0'); 
	
	-- new handled with sel_state -- signal do_read : STD_LOGIC := '1'; -- = not do_write
	
	function inc_addr (address : INTEGER) return INTEGER is
					-- subprogram_declarative_items (constant declarations, variable declarations, etc.)
				begin
					-- function body
					if address = (2**20) - 1 then
						return 0;
					else
						return address + 1;
					end if;
				end inc_addr;

begin

	sram_lb_O <= sram_lbub;
	sram_ub_O <= sram_lbub;
	
	outfifo_data_O <= read_data;
	
	--debug_addr_diff_O <= std_logic_vector(to_unsigned(abs(write_address - read_address),debug_addr_diff_O'length));
	
	process (clk, rst)
	begin
	
		if rst = '1' then

			ram_state 			<= ram_idle;
			sel_state			<= sel_idle;
			
			sram_data_IO 		<= (others => 'Z');
			sram_address_O 	<= (others => '0');
			sram_cs_O			<= '1';
			sram_lbub	 		<= '1';
			sram_we_O 	 		<= '1';
			sram_oe_O 			<= '1';

			trfifo_rd_en_O		<= '0';
			adcfifo_rd_en_O	<= '0';			
			
			outfifo_wr_en_O 	<= '0';
			
			sram_full_O 		<= '0';
			
			write_data			<= (others => '0');
			read_data		 	<= (others => '0');
			write_address		<= 0;
			read_address		<= 0;

		elsif rising_edge(clk) then --TODO: check/test ALL!!!
			
			case ram_state is
				
				when ram_idle 		=> case sel_state is
			
												when sel_idle =>
													
													sram_full_O <= '0';
													-- TODO: test address overflow
													-- sram full (less then 2 spaces left)
													if inc_addr(write_address) = read_address OR inc_addr(inc_addr(write_address)) = read_address then 
														
														sram_full_O <= '1';
														
														if outfifo_full_I = '0' then
													
															sram_address_O 	<= std_logic_vector(to_unsigned(read_address, sram_address_O'length));
															read_address		<= inc_addr(read_address);
															
															sel_state 	<= sel_read; 	-- start read cycle in ram_address															
															ram_state	<= ram_address;
															
														else
															sel_state 	<= sel_idle;
															ram_state	<= ram_idle;
														end if;
														
													else -- sram has at least 2 spaces left
														
														if adcfifo_empty_I = '0' AND adcfifo_valid_I = '1' then -- when data in adc fifo available
															
															write_data 			<= adcfifo_data_I;
															adcfifo_rd_en_O	<= '1';
															
															sram_address_O 	<= std_logic_vector(to_unsigned(write_address, sram_address_O'length));
															write_address		<= inc_addr(write_address); -- TODO: check if w_add == r_add => SRAM FULL!!!!!!!!!!!!!!
															
															sel_state 	<= sel_adc; 		-- start write cycle in ram_address															
															ram_state	<= ram_address;
															
														elsif trfifo_empty_I = '0' AND trfifo_valid_I = '1' then -- when data in tracing fifo available
															
															write_data 			<= trfifo_data_I;
															trfifo_rd_en_O		<= '1';
															
															sram_address_O 	<= std_logic_vector(to_unsigned(write_address, sram_address_O'length));
															write_address		<= inc_addr(write_address); -- TODO: check if w_add == r_add => SRAM FULL!!!!!!!!!!!!!!
															
															sel_state 	<= sel_tr; 			-- start write cycle in ram_address															
															ram_state	<= ram_address;
															
														elsif outfifo_full_I = '0' AND write_address /= read_address then -- when spi fifo not empty AND sram not empty													
															sram_address_O 	<= std_logic_vector(to_unsigned(read_address, sram_address_O'length));
															read_address		<= inc_addr(read_address);
															
															sel_state 	<= sel_read; 	-- start read cycle in ram_address															
															ram_state	<= ram_address;															
														else
															sel_state 	<= sel_idle;
															ram_state	<= ram_idle;															
														end if;
													end if;																	
												
												when sel_adc => if inc_addr(write_address) = read_address OR adcfifo_empty_I = '1' OR adcfifo_valid_I = '0' then -- ERROR: sram full (no space left) OR adc fifo empty
																		--TODO: ERROR
																		sel_state 	<= sel_idle;
																		ram_state	<= ram_idle;
																	else -- VALID: sram not full AND tracing fifo not empty															
																		write_data 			<= adcfifo_data_I;
																		adcfifo_rd_en_O		<= '1';
																		
																		sram_address_O 	<= std_logic_vector(to_unsigned(write_address, sram_address_O'length));
																		write_address		<= inc_addr(write_address); -- TODO: overflow + check if w_add == r_add => SRAM FULL!!!!!!!!!!!!!!
																		
																		sel_state 	<= sel_idle; 			-- start write cycle in ram_address, nothing selected on next ram_idle														
																		ram_state	<= ram_address;																	
																	end if;
																	
																	
												when sel_tr => if inc_addr(write_address) = read_address OR trfifo_empty_I = '1' OR trfifo_valid_I = '0' then -- ERROR: sram full (no space left) OR tracing fifo empty
																		--TODO: ERROR
																		sel_state 	<= sel_idle;
																		ram_state	<= ram_idle;
																	else -- VALID: sram not full AND tracing fifo not empty															
																		write_data 			<= trfifo_data_I;
																		trfifo_rd_en_O		<= '1';
																		
																		sram_address_O 	<= std_logic_vector(to_unsigned(write_address, sram_address_O'length));
																		write_address		<= inc_addr(write_address); -- TODO: overflow + check if w_add == r_add => SRAM FULL!!!!!!!!!!!!!!
																		
																		sel_state 	<= sel_idle; 			-- start write cycle in ram_address, nothing selected on next ram_idle														
																		ram_state	<= ram_address;																	
																	end if;
																	
																	
												when sel_read => 	sel_state 	<= sel_idle;
																		ram_state	<= ram_idle;
												
											end case;
												
				when ram_address 			=> if sel_state = sel_read then
													
														sram_cs_O		<= '0'; -- TODO: just one memblock in use
														sram_lbub		<= '0';
														sram_oe_O  		<= '1';
														sram_we_O  		<= '1';
														
														ram_state		<= ram_read_1;
														
													else -- TODO: is it always a write if sel_state != sel_read ??!
													
														sram_cs_O		<= '0'; -- TODO: just one memblock in use
														sram_lbub		<= '0';
														sram_oe_O	  	<= '1';
														sram_we_O	  	<= '0';
														
														ram_state		<= ram_write_1;
														
													end if;
													adcfifo_rd_en_O	<= '0';
													trfifo_rd_en_O		<= '0';	
													
												
				when ram_read_1 			=> sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '0';
													sram_we_O  	<= '1';
													ram_state	<= ram_read_2;
												
				when ram_read_2 			=> sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '0';
													sram_we_O  	<= '1';
													ram_state	<= ram_read_3;
												
				when ram_read_3 			=> sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '0';
													sram_we_O  	<= '1';
													ram_state	<= ram_read_4;
												
				when ram_read_4 			=> read_data 	<= sram_data_IO;
													outfifo_wr_en_O	<= '1'; --TODO should we wait for fifo_not_full??! (its implicit given)
													
													sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '1';
													ram_state	<= ram_read_5;
				
				when ram_read_5 			=> sram_cs_O	<= '1'; 
													sram_lbub	<= '1';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '1';
													outfifo_wr_en_O	<= '0';
													--TODO should we change sel_state to from read to idle? or read 2 blocks
													ram_state			<= ram_idle;
													
													
												
--				when ram_read_4 			=> read_data 	<= sram_data_IO;
--													outfifo_wr_en_O	<= '1'; --TODO should we wait for fifo_not_full??! (its implicit given)
--													
--													sram_cs_O	<= "01";
--													sram_lbub	<= '0';
--													sram_oe_O  	<= '1';
--													sram_we_O  	<= '1';
--													ram_state	<= ram_read_5;
--				
--				when ram_read_5 			=> sram_cs_O	<= "11"; 
--													sram_lbub	<= '1';
--													sram_oe_O  	<= '1';
--													sram_we_O  	<= '1';
--													outfifo_wr_en_O	<= '0';
--													ram_state			<= ram_read_6;	
--				
--				when ram_read_6 			=> sram_cs_O	<= "11"; 
--													sram_lbub	<= '1';
--													sram_oe_O  	<= '1';
--													sram_we_O  	<= '1';
--													
--													if outfifo_wr_ack_I = '1' then -- check if write was acknoledged, otherwise try again (TODO infinit loop??)
--														outfifo_wr_en_O	<= '0';
--														--TODO should we change sel_state to from read to idle? or read 2 blocks
--														ram_state			<= ram_idle;													
--													else
--														outfifo_wr_en_O	<= '1';	
--														ram_state			<= ram_read_5;												
--													end if;
													
												
				when ram_write_1 			=> sram_data_IO		<= write_data;												
													sram_cs_O		<= '0';
													sram_lbub		<= '0';
													sram_oe_O 	 	<= '1';
													sram_we_O	  	<= '0';
													ram_state		<= ram_write_2;
												
				when ram_write_2 			=> sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '0';
													ram_state	<= ram_write_3;
												
				when ram_write_3 			=> sram_cs_O	<= '0';
													sram_lbub	<= '0';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '0';
													ram_state	<= ram_write_4;
												
				when ram_write_4 			=> sram_cs_O	<= '1';
													sram_lbub	<= '1';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '0';
													ram_state	<= ram_write_5;
												
				when ram_write_5 			=> sram_data_IO	<= (others => 'Z');													
													sram_cs_O	<= '1';
													sram_lbub	<= '1';
													sram_oe_O  	<= '1';
													sram_we_O  	<= '1';														
													ram_state	<= ram_idle;
												
			end case;					
			
		end if;
		
	end process;


end Behavioral;

