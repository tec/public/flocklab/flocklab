----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:43:26 02/05/2014 
-- Design Name: 
-- Module Name:    strecher - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity stretcher is
	 Generic(STRETCH_FACTOR : positive);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           sig_I : in  STD_LOGIC;
           strech_O : out  STD_LOGIC);
end stretcher;

architecture Behavioral of stretcher is
	
	signal srt_cnt : INTEGER range 0 to STRETCH_FACTOR;
	
begin

	process (clk,rst) -- strech signal full_second_int
	begin
		if rst = '1' then
			srt_cnt <= 0;
			strech_O <= '0';
		elsif rising_edge(clk) then
			if sig_I = '1' then
				srt_cnt <= STRETCH_FACTOR;
				strech_O <= '1';
			elsif srt_cnt = 0 then
				strech_O <= '0';
			else
				srt_cnt <= srt_cnt - 1;
				strech_O <= '1';				
			end if;
		end if;
	end process;


end Behavioral;

