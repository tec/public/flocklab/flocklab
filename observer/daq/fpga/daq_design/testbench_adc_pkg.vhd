--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:21:52 08/01/2015
-- Design Name:   
-- Module Name:   /home/rlim/snpk/flocklab-svn/observer/daq/fpga/daq_design/testbench_adc_pkg.vhd
-- Project Name:  DAQ_v1.3_resize
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: adc_packaging
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY testbench_adc_pkg IS
END testbench_adc_pkg;
 
ARCHITECTURE behavior OF testbench_adc_pkg IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT adc_packaging
	 Generic (PKG_BATCH_SIZE : positive);
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         time_clk : IN  std_logic;
         adc_off_I : IN  std_logic;
         adc_off_delay_O : OUT  std_logic;
         adc_sample_div_I : IN  std_logic_vector(10 downto 0);
         css_I : IN  std_logic_vector(20 downto 0);
         sss_I : IN  std_logic_vector(16 downto 0);
			full_second_I : IN  std_logic;
         spi_clk_I : IN  std_logic;
         spi_nfrm_I : IN  std_logic;
         spi_rx_I : IN  std_logic;
         fifo_data_O : OUT  std_logic_vector(31 downto 0);
         fifo_wr_en_O : OUT  std_logic;
         fifo_full_I : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal time_clk : std_logic := '0';
   signal adc_off_I : std_logic := '1';
   signal adc_sample_div_I : std_logic_vector(10 downto 0) := "00000000010";
   signal css_I : std_logic_vector(20 downto 0) := (others => '0');
   signal sss_I : std_logic_vector(16 downto 0) := (others => '0');
	signal full_second_I : std_logic :='0';
   signal adc_spi_clk_I : std_logic := '0';
   signal adc_spi_nfrm_I : std_logic := '0';
   signal adc_spi_rx_I : std_logic := '0';
   signal fifo_full_I : std_logic := '0';

 	--Outputs
   signal adc_off_delay_O : std_logic;
   signal fifo_data_O : std_logic_vector(31 downto 0);
   signal fifo_wr_en_O : std_logic;

   -- Clock period definitions
   constant clk_period : time := 51 ns;
   constant time_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: adc_packaging 
	Generic Map (PKG_BATCH_SIZE => 100)
	PORT MAP (
          clk => clk,
          rst => rst,
          time_clk => time_clk,
          adc_off_I => adc_off_I,
          adc_off_delay_O => adc_off_delay_O,
          adc_sample_div_I => adc_sample_div_I,
          css_I => css_I,
          sss_I => sss_I,
			 full_second_I => full_second_I,
          spi_clk_I => adc_spi_clk_I,
          spi_nfrm_I => adc_spi_nfrm_I,
          spi_rx_I => adc_spi_rx_I,
          fifo_data_O => fifo_data_O,
          fifo_wr_en_O => fifo_wr_en_O,
          fifo_full_I => fifo_full_I
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   time_clk_process :process
   begin
		time_clk <= '0';
		wait for time_clk_period/2;
		time_clk <= '1';
		wait for time_clk_period/2;
   end process;
 
   css_clk_process :process
   begin
		css_I <= std_logic_vector(unsigned(css_I) + 1);
		wait for time_clk_period * 20;
   end process;
	
	sss_clk_process :process
   begin
		sss_I <= std_logic_vector(unsigned(sss_I) + 1);
		full_second_I<='1';
		wait for time_clk_period;
		full_second_I<='0';
		wait for time_clk_period * 200;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
     -- hold reset state for 100 ns.
      rst <= '1';
		
		-- adc_off_sw_I 	<= '1';
		wait for 100 ns;	
		rst <= '0';
      wait for clk_period*10;

      -- insert stimulus here	
		-- start_sw_I		<= '1';
		-- adc_off_sw_I 	<= '0'; -- TODO check timing for this signal
		
				
				


wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 83 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '0';
adc_spi_rx_I 	<= '0';

wait for 41 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 41 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '1';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 125 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 166 ns;
adc_spi_clk_I 	<= '1';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';

wait for 124 ns;
adc_spi_clk_I 	<= '0';
adc_spi_nfrm_I <= '1';
adc_spi_rx_I 	<= '0';
		

      wait;
   end process;

END;
