--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   08:30:46 07/31/2015
-- Design Name:   
-- Module Name:   /home/rlim/snpk/flocklab-svn/observer/daq/fpga/daq_design/testbench_time_calibration.vhd
-- Project Name:  DAQ_v1.3_resize
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: time_calibration
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY testbench_time_calibration IS
END testbench_time_calibration;
 
ARCHITECTURE behavior OF testbench_time_calibration IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT time_calibration
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         start_I : IN  std_logic;
         pps_I : IN  std_logic;
         tx_fifo_data : OUT  std_logic_vector(7 downto 0);
         tx_fifo_wr_en : OUT  std_logic;
         tx_fifo_full : IN  std_logic;
         pps_on_debug_I : IN  std_logic;
         leds_pps_O : OUT  std_logic;
         CSS_O : OUT  std_logic_vector(20 downto 0);
         SSS_O : OUT  std_logic_vector(16 downto 0);
         hard_reset_O : OUT  std_logic;
         full_second_O : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal start_I : std_logic := '0';
   signal pps_I : std_logic := '0';
   signal tx_fifo_full : std_logic := '0';
   signal pps_on_debug_I : std_logic := '0';

 	--Outputs
   signal tx_fifo_data : std_logic_vector(7 downto 0);
   signal tx_fifo_wr_en : std_logic;
   signal leds_pps_O : std_logic;
   signal CSS_O : std_logic_vector(20 downto 0);
   signal SSS_O : std_logic_vector(16 downto 0);
   signal hard_reset_O : std_logic;
   signal full_second_O : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: time_calibration PORT MAP (
          clk => clk,
          rst => rst,
          start_I => start_I,
          pps_I => pps_I,
          tx_fifo_data => tx_fifo_data,
          tx_fifo_wr_en => tx_fifo_wr_en,
          tx_fifo_full => tx_fifo_full,
          pps_on_debug_I => pps_on_debug_I,
          leds_pps_O => leds_pps_O,
          CSS_O => CSS_O,
          SSS_O => SSS_O,
          hard_reset_O => hard_reset_O,
          full_second_O => full_second_O
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rst <= '1';
      wait for 100 ns;	
		rst <= '0';
		
      wait for clk_period*10;
		pps_I <= '1';
		wait for clk_period*10;
		pps_I <= '0';
		wait for clk_period*(100000000-10+2);
		pps_I <= '1';
		wait for clk_period*10;
		pps_I <= '0';
		wait for clk_period*(100000000-10+1);

      wait;
   end process;

END;
