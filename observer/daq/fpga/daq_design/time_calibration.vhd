----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:00:32 12/27/2013 
-- Design Name: 
-- Module Name:    time_calibration - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;

use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity time_calibration is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
           start_I : in  STD_LOGIC; -- start of the test
           pps_I : in  STD_LOGIC; -- Pulse per second signal	

			  --debug
			  tx_fifo_data : out  STD_LOGIC_VECTOR (7 downto 0);
			  tx_fifo_wr_en : out  STD_LOGIC;
			  tx_fifo_full : in  STD_LOGIC;
			  pps_on_debug_I : in  STD_LOGIC;
			  leds_pps_O		: out  STD_LOGIC;
			  
			  --Output
           CSS_O : out  STD_LOGIC_VECTOR (20 downto 0); -- cycles since second - counter
           SSS_O : out  STD_LOGIC_VECTOR (16 downto 0); -- seconds since start - counter
           hard_reset_O : out  STD_LOGIC; -- '1' peak if the CSS counter is set to zero, due to to large pps offset
           full_second_O : out  STD_LOGIC); --  '1' peak
end time_calibration;

architecture Behavioral of time_calibration is
	
	-- internalized signals (not really needed for inputs, but for outputs)
	signal full_second : STD_LOGIC := '0';
	signal hard_reset : STD_LOGIC := '0';
	signal pps_pulse : STD_LOGIC := '0';
	signal leds_pps : STD_LOGIC := '0';
	
	constant CPS : INTEGER := 10000000-1; -- test: 20-1; official: -- 2000000-1; -- 2e6 -- CPS: timeslots per second
	constant HC_DIV : INTEGER := 10; -- cycles per timeslot
	constant HC_MAX : INTEGER := 100000000;-- test: 1000; official: -- 100000000; -- 100e6  --cycles per second with an optimal Oscillator
	constant SPD : INTEGER := 3600*24; -- seconds per day	
	
	constant PPS_SDIFF_MAX : INTEGER := 20; -- Max difference between two pps signals in [s], which still compute an new average cycle difference (MAXMAX 20, otherwise pps_cnt too big > 31bit; maxmax 31, otherwise divisor too big)
	constant PPS_CYCLE_MAX : INTEGER := (PPS_SDIFF_MAX+1)*HC_MAX; -- 
	
	constant CPS_f : INTEGER := 2000000-1;
	
	-- Output signals for higher time resolution
	signal css_cnt_f : INTEGER range CPS_f downto 0 := 0;
	signal sss_cnt_f : INTEGER range SPD downto 0 := 0;
	signal full_second_f : STD_LOGIC := '0';
	
	-- CSS signals
	signal css_cnt : INTEGER range CPS downto 0 := 0; -- counter for divided cycles since the last full second, adjusted to CPS cycles per second
	signal css_div_cnt : INTEGER range HC_DIV downto 0 := 0; -- no -1!! actual 51 cnt. variable counter to devide high cycles into css(cycles since second)
	signal css_hc_cnt : INTEGER range (HC_MAX + (HC_MAX/10)) downto 0 := 0; -- counter for high cycles since the last full second, cycles per second not static
	
	type compensate_states is (none, pos, neg); --(no cycles; pos no. of cycles; negative no. of cycles to compensate)
	
	
	--signal compensate_cycle_cnt : INTEGER range 2000000-1 downto 0 := 0;
	signal compensate_state : compensate_states := none;
	signal diff_off_state : compensate_states := none; --TODO implement douvle pos/neg
	signal compensated : STD_LOGIC := '0';
	
	-- SSS signals
	signal sss_cnt : INTEGER range SPD downto 0 := 0;
	signal rst_sss : STD_LOGIC;
	
	-- diff/off signals
	signal diff_off_cnt : INTEGER range CPS downto -CPS := 0; -- TODO range?; 
	type sdiv_states is (idle, stage2, stage3, stage4, stage5);
	signal sdiv_state : sdiv_states := idle;
	signal spread_factor : INTEGER range CPS downto 0 := 0; -- TODO range?; 
	signal spread_cnt : INTEGER range CPS downto 0 := 0; -- TODO range?; 
	
	--signal diff_add : INTEGER range CPS downto -CPS := 0;
	signal off_set : INTEGER range CPS downto -CPS := 0;
	signal off_set_en : STD_LOGIC := '0';
	
	-- diff average
	signal diff_new : INTEGER range CPS downto -CPS := 0;
	signal diff_new_en : STD_LOGIC := '0';
	signal diff_avg : INTEGER range CPS downto -CPS := 0;
	
	type 	val_array  is array (7 downto 0) of INTEGER range CPS downto -CPS;
	signal diff_values : val_array := (others => 0) ;
	
	--pps handler signals
	signal pps_cycle_cnt  : INTEGER range PPS_CYCLE_MAX downto 0 := 0;
	signal pps_cycle_diff : INTEGER range PPS_CYCLE_MAX downto 0 := 0;
	signal pps_cycle_reference: INTEGER range PPS_CYCLE_MAX downto 0 := 0;
	
	signal pps_fs_ref : INTEGER range SPD downto 0 := 0;
	signal pps_fs_diff : INTEGER range SPD downto -SPD := 0;
	
	type pps_states is (init, idle, stage1, stage2, stage3, stage4, stage5);
	signal pps_state : pps_states := init;
	signal pps_reg : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
	
	function range_comparator (main_value : INTEGER;
									 compare_value : INTEGER;
									 compare_range : INTEGER) return BOOLEAN is
					-- subprogram_declarative_items (constant declarations, variable declarations, etc.)
				begin
					-- function body
					if main_value - compare_range < compare_value AND compare_value < main_value + compare_range then
						return true;
					else
						return false;
					end if;
				end range_comparator;
				
	function add_with_range(summand1 : Integer; summand2: Integer; highBound : INTEGER; lowBound : INTEGER)
									return INTEGER is
					-- subprogram_declarative_items (constant declarations, variable declarations, etc.)
				begin
					-- function body
					if summand2 < 0 then
						if summand2 < lowBound - summand1 then
							return lowBound;
						else
							return summand1 + summand2;
						end if;
					else
						if summand2 > highBound - summand1 then
							return highBound;
						else
							return summand1 + summand2;
						end if;
					end if;
				end add_with_range;
	
	-- divider: 32bit / 6bit; signed, remainder output; Latency: 37
	component pdiv_divider
		port (
		clk: in std_logic;
		rfd: out std_logic;
		ce: in std_logic;
		dividend: in std_logic_vector(31 downto 0);
		divisor: in std_logic_vector(5 downto 0);
		quotient: out std_logic_vector(31 downto 0);
		fractional: out std_logic_vector(5 downto 0));
	end component;	

	signal divider_rfd: std_logic;
	signal divider_ce: std_logic;
	signal dividend: std_logic_vector(31 downto 0);
	signal divisor: std_logic_vector(5 downto 0);
	signal quotient: std_logic_vector(31 downto 0);
	signal remainder: std_logic_vector(5 downto 0);
	constant P_DEVIDER_LATENCY : INTEGER := 40; -- 
	
	signal pdiv_latency_cnt : INTEGER range P_DEVIDER_LATENCY downto 0 := 0;
	
	-- divider: 24bit / 25bit; unsigned, remainder output; Latency: 27
	component sdiv_divider
		port (
		clk: in std_logic;
		ce: in std_logic;
		rfd: out std_logic;
		dividend: in std_logic_vector(23 downto 0);
		divisor: in std_logic_vector(24 downto 0);
		quotient: out std_logic_vector(23 downto 0);
		fractional: out std_logic_vector(24 downto 0));
	end component;
	
	signal sdiv_rfd: std_logic;
	signal sdiv_divider_ce: std_logic;	
	signal sdiv_divisor: std_logic_vector(24 downto 0);
	signal sdiv_quotient: std_logic_vector(23 downto 0);
	--signal sdiv_remainder: std_logic_vector(21 downto 0);
	constant sdiv_dividend: std_logic_vector(23 downto 0) := std_logic_vector(to_unsigned(CPS+1,24)); --TODO really constant?
	constant S_DEVIDER_LATENCY : INTEGER := 30; --
	
	signal sdiv_latency_cnt : INTEGER range S_DEVIDER_LATENCY downto 0 := 0;
	
	
	--debug
	type uartstates is (idle, header, pkg1, pkg2, pkg3, pkg4);
	signal uart_state : uartstates := idle;
	
	type pkgstates is (idle, hccnt, offset, newdiff, avgdiff, diffoff_cnt, sdiv_factor, f_sec);
	signal pkg_state : pkgstates := idle;
	
	signal pps_diff_vector : std_logic_vector(30 downto 0);
	signal pkg_data : std_logic_vector(27 downto 0);
	
	signal fsec_event: std_logic;
		
		-- pps debug
		signal pps_on_debug_int : std_logic;
		constant PPS_DEB_MAX : Integer := 60*5; 
		constant PPS_DEB_MID : Integer := 60*2;
		signal pps_debug_cnt : Integer range PPS_DEB_MAX downto 0;
	
begin

	pps_divider : pdiv_divider
		port map (
			clk => clk,
			rfd => divider_rfd,
			ce => divider_ce,
			dividend => dividend,
			divisor => divisor,
			quotient => quotient,
			fractional => remainder);
	
	spread_divider : sdiv_divider
		port map (
			clk => clk,
			rfd => sdiv_rfd,
			ce => sdiv_divider_ce,
			dividend => sdiv_dividend,
			divisor => sdiv_divisor,
			quotient => sdiv_quotient);
	
	CSS_O <= std_logic_vector(to_unsigned(css_cnt_f, CSS_O'length));
	SSS_O <= std_logic_vector(to_unsigned(sss_cnt_f, SSS_O'length));
	
	hard_reset_O <= hard_reset;
	full_second_O <= full_second_f;
	leds_pps_O <= leds_pps;
	
	pps_one_pulse_gen : process (clk, rst)
	begin
	
		if rst = '1' then	
			
			pps_reg <= (others => '0');
			pps_pulse <= '0';
						
		elsif rising_edge(clk) then
			pps_reg <= pps_reg(1 downto 0) & pps_I;
			
			--pps_pulse <= '1' when pps_reg = "001" else '0';			
			if pps_reg = "001" then -- TODO or = "011"??
				pps_pulse <= '1';
				leds_pps <= '1';
			else
				pps_pulse <= '0';
				leds_pps <= '0';
			end if;
			
		end if;
	end process;
	

	css_counter : process (clk, rst)
	begin
	
		if rst = '1' then	-- TODO hard_reset and rst: same handling?
			css_cnt <= 0;
			css_cnt_f <= 0;
			css_div_cnt <= 0;
			css_hc_cnt <= 0;
			full_second <= '0';
			full_second_f <= '0';
			compensated <= '0';
			
		elsif rising_edge(clk) then
			
			if hard_reset = '1' then
				css_cnt <= 0;
				css_cnt_f <= 0;
				css_div_cnt <= 0;
				css_hc_cnt <= 0;
				if css_cnt < CPS/2 then
					full_second <= '0'; 
				else
					full_second <= '1'; -- TODO
				end if;	
				if css_cnt_f < CPS_f/2 then
					full_second_f <= '0'; 
				else
					full_second_f <= '1'; -- TODO
				end if;					
				compensated <= '0';
			else
				-- actual CSS counter at 2MHz
				if(css_div_cnt = HC_DIV) then
					if(css_cnt = CPS) then
						css_cnt <= 0;
						css_hc_cnt <= 0;
						full_second <= '1';
					else
						css_cnt <= css_cnt + 1; 
						css_hc_cnt <= css_hc_cnt + 1;
						full_second <= '0';
					end if;
					if(css_cnt_f = CPS_f) then
						css_cnt_f <= 0;
						full_second_f <= '1';
					else
						css_cnt_f <= css_cnt_f + 1; 
						full_second_f <= '0';
					end if;
				else				
						css_hc_cnt <= css_hc_cnt + 1;
						full_second <= '0';
						full_second_f <= '0';
				end if;
				
				-- HighCycleDiv counter (count to 50: 100MHz -> 2MHz = time resolution)
				if(css_div_cnt = HC_DIV) then 
					css_div_cnt <= 0;
					compensated <= '0' ;
				elsif (css_div_cnt = HC_DIV-3) then 
					if compensate_state = neg then		-- skip 2 cycles
						css_div_cnt <= css_div_cnt + 3;
						compensated <= '1' ;
					elsif compensate_state = none then 	-- skip 1 cycles
						css_div_cnt <= css_div_cnt + 2; 
						compensated <= '0' ;
					else		-- if compensate_state = pos: skip 0 cycles
						css_div_cnt <= css_div_cnt + 1;
						compensated <= '1' ;
					end if;
				else
					css_div_cnt <= css_div_cnt + 1;
					compensated <= '0' ;
				end if;
			end if;
		
		end if;
	
	end process;
	
	rst_sss <= '1' when (rst = '1' OR start_I = '0') else '0';
	
	sss_counter : process (clk, rst_sss)
	begin
	
		if rst_sss = '1' then	--TODO start on sensitivity list?	
			sss_cnt <= 0;
			sss_cnt_f <= 0;
			
		elsif rising_edge(clk) then			
			if full_second = '1' then
				if sss_cnt = SPD then
					sss_cnt <= 0;
				else
					sss_cnt <= sss_cnt + 1;
				end if;				
			end if;	
			if full_second_f = '1' then
				if sss_cnt_f = SPD then
					sss_cnt_f <= 0;
				else
					sss_cnt_f <= sss_cnt_f + 1;
				end if;				
			end if;
		end if;
	
	end process;
	


	diff_off_handler : process (clk, rst)
	begin
	
		if rst = '1' then
			diff_off_cnt <= 0;
			diff_off_state <= none;
			
		elsif rising_edge(clk) then
			
			-- handle diff_off_cnt changes
			-- if there is a new offset add it. If there is a full_second add avg_difference.
			-- if a cycle was compensated, add/sub one.
			 -- TODO what if diff + sth > diff_MAX??
			if off_set_en = '1' AND full_second = '1' then
				diff_off_cnt <= add_with_range(diff_off_cnt, off_set + diff_avg, CPS, -CPS);			
				
			elsif off_set_en = '1' then
				diff_off_cnt <= add_with_range(diff_off_cnt, off_set, CPS, -CPS);		
				
			elsif full_second = '1' then
				diff_off_cnt <= add_with_range(diff_off_cnt, diff_avg, CPS, -CPS);			-- just applies 'old' avg. newly calculated avg is available in the next second			
				
			elsif compensated ='1' then
				if  diff_off_state = pos then
					diff_off_cnt <= diff_off_cnt - 1;
				elsif diff_off_state = neg then					
					diff_off_cnt <= diff_off_cnt + 1;
--				else -- diff_off_state = none
--					diff_off_cnt <= diff_off_cnt; -- do nothing					
				end if;			
			end if;
			
			-- set compensate state
			if  diff_off_cnt > 0 then
				diff_off_state <= pos;
			elsif diff_off_cnt < 0 then
				diff_off_state <= neg;
			else
				diff_off_state <= none;					
			end if;
		
		end if;
	
	end process;
	
	
	compensate_spreader : process (clk, rst)
	begin
	
		if rst = '1' then
			spread_cnt <= 0;
			compensate_state <= none;
			
		elsif rising_edge(clk) then
			-- set compensate state
			if  css_div_cnt = HC_DIV  then
				if spread_cnt >= spread_factor then
					spread_cnt <= 0;
					compensate_state <= diff_off_state;
				else
					spread_cnt <= spread_cnt + 1;
					compensate_state <= none;
				end if;
			end if;		
		end if;	
	end process;
	
	-- unsigned division
	spread_division : process (clk, rst)
	begin
	
		if rst = '1' then
			spread_factor <= 0;
			sdiv_divider_ce <= '0';
			--sdiv_dividend <= (others => '0');
			sdiv_divisor <= std_logic_vector(to_unsigned(1,sdiv_divisor'length));
			
			sdiv_state <= idle;
			
		elsif rising_edge(clk) then
			
			case sdiv_state is
				
				
				when idle => 	-- wait for new inputs
									if off_set_en = '1' OR full_second = '1' then
										sdiv_state <= stage2;									
									else
										sdiv_state <= idle;
									end if;
				
				when stage2 => -- set divider inputs and enable
									if off_set_en = '1' OR full_second = '1' then
										sdiv_state <= stage2;									
									elsif (diff_off_cnt = 0) then -- no division needed
										spread_factor <= 0;
										sdiv_state <= idle;
									else
										sdiv_divider_ce <= '1';
										--sdiv_dividend <= std_logic_vector(to_unsigned(CPS+1,sdiv_dividend'length)); 
										sdiv_divisor <= std_logic_vector(to_unsigned(abs(diff_off_cnt),sdiv_divisor'length)); --TODO: Inputs ready/updated ??
										sdiv_state <= stage3;
									end if;
									
				when stage3 => -- wait for rfd
									if off_set_en = '1' OR full_second = '1' then
										sdiv_state <= stage2;									
									elsif sdiv_rfd = '1' then
										sdiv_latency_cnt <= 0;
										sdiv_state <= stage4;
									else
										sdiv_state <= stage3;
									end if;
				
				when stage4 => -- wait for devision latency (count)
									if off_set_en = '1' OR full_second = '1' then
										sdiv_state <= stage2;									
									elsif sdiv_latency_cnt = S_DEVIDER_LATENCY then -- TODO check validity of latency
										sdiv_latency_cnt <= 0;
										sdiv_divider_ce <= '0';
										sdiv_state <= stage5;
									else
										sdiv_latency_cnt <= sdiv_latency_cnt + 1;										
										sdiv_state <= stage4;
									end if;
				
				when stage5 => -- read devider output, set new value
									if to_integer(unsigned(sdiv_quotient)) < 2 then -- |quotient| > |CPS| --> discard. 
										spread_factor <= 0;
									else -- 
										spread_factor <= to_integer(unsigned(sdiv_quotient)) -1; -- - 1;
									end if;									
									
									if off_set_en = '1' OR full_second = '1' then
										sdiv_state <= stage2;									
									else
										sdiv_state <= idle;
									end if;
			
			
			end case;			
		end if;	
	end process;
	
	
	pps_diff_handler : process (clk, rst)
	begin
	
		if rst = '1' then
			 pps_state <= init;
			 pps_cycle_cnt  <= 0;
			 pps_cycle_diff <= 0;
			 pps_cycle_reference <= 0;
			 
			 pps_fs_ref 	<= 0;
			 pps_fs_diff 	<= 0;
			 
			 divider_ce <= '0';
			 dividend <= (others => '0');
			 divisor <= "000001";
			 diff_new_en <= '0';
			
		elsif rising_edge(clk) then
			
			if  pps_pulse = '1' then			
				pps_cycle_cnt  <= 0;
				if css_cnt > CPS/2 or css_cnt = 0 then
					pps_fs_ref		<= sss_cnt+1;
				else
					pps_fs_ref		<= sss_cnt;
				end if;
			elsif  pps_cycle_cnt = PPS_CYCLE_MAX then
				pps_cycle_cnt <= PPS_CYCLE_MAX;
			else
				pps_cycle_cnt <= pps_cycle_cnt +1;
			end if;
			
			
			case pps_state is
			
				when init => 
								divider_ce <= '0';
								diff_new_en <= '0';
								if  pps_pulse = '1' then	
									pps_cycle_diff <= 0;
									pps_state <= idle;
								end if;
				
				when idle =>
								divider_ce <= '0';
								diff_new_en <= '0';
								if  pps_pulse = '1' then			
									if  pps_cycle_cnt = PPS_CYCLE_MAX then
										pps_state <= idle;
									else									
										pps_cycle_diff <= pps_cycle_cnt;
										pps_cycle_reference <= 0;
										
										if css_cnt > CPS/2 or css_cnt = 0 then
											pps_fs_diff <= sss_cnt - pps_fs_ref + 1;
										else
											pps_fs_diff <= sss_cnt - pps_fs_ref;
										end if;
										
										pps_state <= stage1;
									end if;
								end if;
				
				when stage1 => -- compute seconds_diff, and reference main-value
									if hard_reset = '1' or pps_fs_diff < 1 or pps_fs_diff > PPS_SDIFF_MAX then
										pps_state <= idle;
									else
										pps_cycle_reference <= pps_fs_diff * HC_MAX;
										--TODO rundungsfehler beheben??: pps_cycle_diff <= pps_cycle_diff + pps_fs_diff/2;
										pps_state <= stage2;
									end if;
				
				when stage2 => -- set divider inputs and enable
									divider_ce <= '1';
									dividend <= std_logic_vector(to_signed(pps_cycle_diff - pps_cycle_reference, dividend'length));
									divisor <= std_logic_vector(to_signed(pps_fs_diff, divisor'length)); --Watch out!!: chack that pps_fs_diff < 32, otherwise 1st bit is 1 -> neg/2's-complement
									pps_state <= stage3;
									
				when stage3 => -- wait for rfd
									if divider_rfd = '1' then
										pdiv_latency_cnt <= 0;
										pps_state <= stage4;
									else
										pps_state <= stage3;
									end if;
				
				when stage4 => -- wait for devision latency (count)
									if pdiv_latency_cnt = P_DEVIDER_LATENCY then -- TODO check validity of latency
										pdiv_latency_cnt <= 0;
										divider_ce <= '0';
										pps_state <= stage5;
									else
										pdiv_latency_cnt <= pdiv_latency_cnt + 1;										
										pps_state <= stage4;
									end if;
				
				when stage5 => -- read devider output, set new value
									if to_integer(signed(quotient)) < -CPS OR to_integer(signed(quotient)) > CPS then -- |quotient| > |CPS| --> discard. 
										--No new diff value, start over at idle
									elsif (to_integer(signed(remainder)) > 0 AND to_integer(signed(remainder)) > to_integer(signed(divisor))/2) then -- large remainder and pos --> +1
										diff_new <= to_integer(signed(quotient)) + 1;
										diff_new_en <= '1';									
									elsif (to_integer(signed(remainder)) < 0 AND to_integer(signed(remainder)) < to_integer(signed(divisor))/2) then -- large remainder and neg --> -1
										diff_new <= to_integer(signed(quotient)) - 1;
										diff_new_en <= '1';
									else -- small remainder --> no addition
										diff_new <= to_integer(signed(quotient));
										diff_new_en <= '1';
									end if;									
									pps_state <= idle;
			
			
			end case;		
		
		end if;
	
	end process;			
	
	
	--  set hard reset here
	pps_off_handler : process (clk, rst)
	begin
	
		if rst = '1' then	
			 hard_reset <= '0';
			 off_set <= 0;
			 off_set_en <= '0';
			
		elsif rising_edge(clk) then
			
			if (pps_pulse = '1') then
				if css_hc_cnt < CPS then --full second was to early (CPS = 2% HC_MAX)
					off_set <= css_hc_cnt+1; -- TODO check new impl after :
					off_set_en <= '1'; 
					hard_reset <= '0';
				elsif css_hc_cnt > (HC_MAX + diff_avg - CPS) then --full second will be too late		TODO HC_MAX or actual estimated value (HC_MAX+-pps_diff)		
					off_set <= css_hc_cnt - (HC_MAX + diff_avg) + 1;
					off_set_en <= '1';  
					hard_reset <= '0';
				else -- hard reset, offest too large to compensate
					hard_reset <= '1';
					off_set_en <= '0';
				end if;	
			else				
				off_set_en <= '0';
				hard_reset <= '0';
			end if;
			
		end if;
	
	end process;
	
	
	diff_average : process (clk, rst)
	begin
	
		if rst = '1' then	
			 diff_avg <= 0;
			 diff_values <= (others => 0) ;		
			
		elsif rising_edge(clk) then -- TODO multiple steps, for timing optimazion
		
			-- if  diff_new_en = '1' and pps_on_debug_int = '1' then
			if  diff_new_en = '1' then				
				diff_values <= diff_values(6 downto 0) & diff_new;
				diff_avg <= (((diff_new
								-- (diff_values(15) + diff_values(14) + diff_values(13) + diff_values(12) + 
								-- diff_values(11) + diff_values(10) + diff_values(9) + diff_values(8) 
								-- + diff_values(7)
								 + diff_values(6)) + (diff_values(5) + diff_values(4))) + ((diff_values(3)
								 + diff_values(2)) + (diff_values(1) + diff_values(0) + 4 ))) /  8;
			end if;			
		
		end if;
	
	end process;
	
	pps_debuger : process (clk, rst, pps_on_debug_I)
	begin
	
		if rst = '1' or pps_on_debug_I = '1' then	
			pps_on_debug_int <= '1';
			pps_debug_cnt <= 0;
			
		elsif rising_edge(clk) then -- TODO multiple steps, for timing optimazion
		
			if full_second = '1' then
				if  pps_debug_cnt < 1 then --init pps signal lost
					pps_debug_cnt <= PPS_DEB_MAX; 
					pps_on_debug_int <= '0';
				elsif pps_debug_cnt < PPS_DEB_MID then -- get good signal again
					pps_debug_cnt <= pps_debug_cnt - 1; 
					pps_on_debug_int <= '1';
				else --simulate pps signal lost
					pps_debug_cnt <= pps_debug_cnt - 1; 
					pps_on_debug_int <= '0';
				end if;	
			end if;
		
		end if;
	
	end process;
	
	
	
	
	pps_diff_vector <= std_logic_vector(to_unsigned(pps_cycle_diff,31));

	time_debuger : process (clk, rst)
	begin
	
		if rst = '1' then			
			pkg_data <= (others => '0');	
			tx_fifo_wr_en <= '0';
			uart_state <= idle;
			pkg_state <= idle;
			fsec_event <= '0';
			
		elsif rising_edge(clk) then
			
			tx_fifo_wr_en <= '0';
			
			if full_second = '1' then
				fsec_event <= '1';
			end if;
			
			case uart_state is
			
			
				when idle => 	if  pps_pulse = '1' then
										uart_state <= header;
										pkg_state <= hccnt;
										fsec_event <= '0';
									else
										uart_state <= idle;
										pkg_state <= idle;
									end if;
				
				
				when header => 
									case pkg_state is			
			
										when idle => 	uart_state <= idle;
															pkg_state <= idle;															
															
										when hccnt => 	uart_state <= pkg1;
															pkg_state <= offset;
															-- write header to fifo
															tx_fifo_data  <= '0' & pps_diff_vector(30 downto 28) & "0001";
															tx_fifo_wr_en <= '1';
															-- data following header
															pkg_data <= pps_diff_vector(27 downto 0);
															
										when offset => uart_state <= pkg1;
															pkg_state <= newdiff;
															-- write header to fifo
															tx_fifo_data  <= '0' &  pps_on_debug_int & "000010";
															tx_fifo_wr_en <= '1';
															-- data following header
															pkg_data <= std_logic_vector(to_signed(off_set,28));
															
															
										when newdiff => 	uart_state <= pkg1;
																pkg_state <= avgdiff;
																-- write header to fifo
																tx_fifo_data  <= "00000011";
																tx_fifo_wr_en <= '1';
																-- data following header
																pkg_data <= std_logic_vector(to_signed(diff_new,28));
																
																
										when avgdiff => 	uart_state <= pkg1;
																pkg_state <= diffoff_cnt;
																-- write header to fifo
																tx_fifo_data  <= "00000100";
																tx_fifo_wr_en <= '1';
																-- data following header
																pkg_data <= std_logic_vector(to_signed(diff_avg,28));
																
																
										when diffoff_cnt =>	uart_state <= pkg1;
																	pkg_state <= sdiv_factor;
																	-- write header to fifo
																	tx_fifo_data  <= "00000101";
																	tx_fifo_wr_en <= '1';
																	-- data following header
																	pkg_data <= std_logic_vector(to_signed(diff_off_cnt,28));
																
																
										when sdiv_factor =>	uart_state <= pkg1;
																	pkg_state <= f_sec;
																	-- write header to fifo
																	tx_fifo_data  <= "00000111";
																	tx_fifo_wr_en <= '1';
																	-- data following header
																	pkg_data <= std_logic_vector(to_signed(spread_factor,28));
																
																
										when f_sec =>			uart_state <= pkg1;
																	pkg_state <= idle;
																	-- write header to fifo
																	tx_fifo_data  <= "00000110";
																	tx_fifo_wr_en <= '1';
																	-- data following header
																	pkg_data <= fsec_event & std_logic_vector(to_unsigned(sss_cnt, 27));
																	
									end case;
				
				
				when pkg1 => -- pkgstates unchanged
									uart_state <= pkg2;
									-- write header to fifo
									tx_fifo_data  <= '1' & pkg_data(27 downto 21);
									tx_fifo_wr_en <= '1';				
				
				when pkg2 => -- pkgstates unchanged
									uart_state <= pkg3;
									-- write header to fifo
									tx_fifo_data  <= '1' & pkg_data(20 downto 14);
									tx_fifo_wr_en <= '1';				
				
				when pkg3 => -- pkgstates unchanged
									uart_state <= pkg4;
									-- write header to fifo
									tx_fifo_data  <= '1' & pkg_data(13 downto 7);
									tx_fifo_wr_en <= '1';				
				
				when pkg4 => -- pkgstates unchanged
									uart_state <= header;
									-- write header to fifo
									tx_fifo_data  <= '1' & pkg_data(6 downto 0);
									tx_fifo_wr_en <= '1';			
			
			end case;
			
		end if;
	end process;

end Behavioral;


--	  tx_fifo_data : out  STD_LOGIC_VECTOR (7 downto 0);
--	  tx_fifo_wr_en : out  STD_LOGIC;
--	  tx_fifo_full : in  STD_LOGIC;

--	type uartstates is (idle, header, pkg1, pkg2, pkg3, pkg4);
--	signal uart_state : uartstates := idle;
--	
--	type pkgstates is (idle, hccnt, offset, newdiff, avgdiff, diffoff_cnt);
--	signal pkg_state : uartstates := idle;

