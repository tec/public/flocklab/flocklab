----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:52:42 01/07/2014 
-- Design Name: 
-- Module Name:    trace_packaging - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trace_packaging is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  
           start_I : in  STD_LOGIC; --TODO use it!!
			  
           event_detected_I : in  STD_LOGIC;
           --pps_I : in  STD_LOGIC;
           full_second_I : in  STD_LOGIC;
           hard_rst_I : in  STD_LOGIC;
           --css_tick_I : in  STD_LOGIC;
           states_detected_I : in  STD_LOGIC_VECTOR (7 downto 0);
           css_I : in  STD_LOGIC_VECTOR (20 downto 0);
           sss_I : in  STD_LOGIC_VECTOR (16 downto 0);			  
           fifo_package_O : out  STD_LOGIC_VECTOR (31 downto 0);
           fifo_wr_en_O : out  STD_LOGIC;
           fifo_full_I : in  STD_LOGIC);
end trace_packaging;

architecture Behavioral of trace_packaging is

	constant HD_ERR 	: STD_LOGIC_VECTOR (2 downto 0) := "011";
	-- header description: starting with '0': fifo ok; starting with 1: fifo was full, e.g. information was lost
	constant HD_TR  	: STD_LOGIC_VECTOR (1 downto 0) := "00";
	constant HD_FS 	: STD_LOGIC_VECTOR (1 downto 0) := "01";
	constant HD_HR 	: STD_LOGIC_VECTOR (1 downto 0) := "10";
	
	--constant HD_FF 	: STD_LOGIC_VECTOR (2 downto 0) := "100";
	--constant HD_FF_FS : STD_LOGIC_VECTOR (2 downto 0) := "101";
	--constant HD_FF_HR : STD_LOGIC_VECTOR (2 downto 0) := "110";
	
	signal header : STD_LOGIC_VECTOR (2 downto 0);
	signal pin_levels : STD_LOGIC_VECTOR (7 downto 0);
	signal timestamp : STD_LOGIC_VECTOR (20 downto 0);
	--signal tracing_packet : STD_LOGIC_VECTOR (31 downto 0);
	
	signal all_event_detec : STD_LOGIC;
	signal fifo_was_full : STD_LOGIC;
	
	signal rst_pkg : STD_LOGIC;
	
begin	
		

	fifo_package_O <= header & timestamp & pin_levels;
	all_event_detec <= '0' when (event_detected_I = '0' 
										AND full_second_I = '0' 
										AND hard_rst_I = '0' 
										AND fifo_was_full = '0') 
									else '1';	

	header_pkg : process (clk, rst)
	begin
		
		if rst = '1'  then	
			
			header <= HD_ERR;
			
		elsif rising_edge(clk) then
			-- header
			header(2) <= fifo_was_full;
			if hard_rst_I = '1' AND full_second_I = '1' then
				header(1 downto 0) <= HD_HR;
				header(2) <= '0';
			elsif hard_rst_I = '1' then
				header(1 downto 0) <= HD_HR;
			elsif full_second_I = '1' then
				header(1 downto 0) <= HD_FS;
			else
				header(1 downto 0) <= HD_TR;
			end if;		
			
		end if;
	end process;
	
	
	rst_pkg <= '1' when (rst = '1' OR start_I = '0') else '0';
	
	process (clk, rst_pkg)
	begin
		
		if rst_pkg = '1'  then	
			
			pin_levels <= (others => '0');
			timestamp <= (others => '0');
			
			fifo_wr_en_O <= '0';
			fifo_was_full <= '0';
			
		elsif rising_edge(clk) then
			
			-- write if event (including fifo_was_full before) -- TODO: check if right data is written if two happen 1 cycle after each other
			if(all_event_detec = '1') then
				if fifo_full_I = '0' then
					pin_levels <= states_detected_I;
					if full_second_I = '1' then
						timestamp <= "0000" & sss_I;
					else
						timestamp <= css_I;
					end if;
					fifo_was_full <= '0';
					fifo_wr_en_O <= '1';
				else
					-- report fifo full for next cycle
					fifo_was_full <= '1';
					fifo_wr_en_O <= '0';
				end if;
			else
				fifo_wr_en_O <= '0';
			end if;			
			
		end if;
		
		
	end process;

end Behavioral;

