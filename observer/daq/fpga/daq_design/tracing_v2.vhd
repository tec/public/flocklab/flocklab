----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:48:36 01/21/2014 
-- Design Name: 
-- Module Name:    tracing_v2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tracing_v2 is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           start_I : in  STD_LOGIC; --TODO use it!
			  
			  --time signals
           css_I : in  STD_LOGIC_VECTOR (20 downto 0);
           sss_I : in  STD_LOGIC_VECTOR (16 downto 0);
           full_second_I : in  STD_LOGIC;
			  hard_rst_I : in  STD_LOGIC;
			  
			  --detection signals
           on_off_conf_I : in  STD_LOGIC_VECTOR (7 downto 0);
           trace_signal_I : in  STD_LOGIC_VECTOR (7 downto 0);
			  			  
			  --packaging/fifo signals
           fifo_package_O : out  STD_LOGIC_VECTOR (31 downto 0);
           fifo_wr_en_O : out  STD_LOGIC;
           fifo_full_I : in  STD_LOGIC);
end tracing_v2;

architecture Behavioral of tracing_v2 is

	COMPONENT trace_packaging
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		start_I : IN std_logic;
		event_detected_I : IN std_logic;
		full_second_I : IN std_logic;
		hard_rst_I : IN std_logic;
		states_detected_I : IN std_logic_vector(7 downto 0);
		css_I : IN std_logic_vector(20 downto 0);
		sss_I : IN std_logic_vector(16 downto 0);
		fifo_full_I : IN std_logic;          
		fifo_package_O : OUT std_logic_vector(31 downto 0);
		fifo_wr_en_O : OUT std_logic
		);
	END COMPONENT;
	  
	  -- detection -> packaging	  
	  signal events_detected :  STD_LOGIC_VECTOR (7 downto 0);
	  signal events_detected_conf :  STD_LOGIC_VECTOR (7 downto 0); --excluding pins which are configured off
	  signal event_detected : STD_LOGIC; -- 1 if event on any configured pin 
	  
	  signal states_detected : STD_LOGIC_VECTOR (7 downto 0);
	  signal PIN0r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN1r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN2r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN3r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN4r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN5r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN6r : STD_LOGIC_VECTOR (2 downto 0);
	  signal PIN7r : STD_LOGIC_VECTOR (2 downto 0);	 
	  
	  signal FSr : STD_LOGIC_VECTOR (1 downto 0);	  
	  signal css_0 : STD_LOGIC_VECTOR (20 downto 0);	  
	  signal css_1 : STD_LOGIC_VECTOR (20 downto 0);	

     signal do_init : STD_LOGIC;

begin
	
	
	TRACE_PKG: trace_packaging PORT MAP(
		clk => clk,
		rst => rst,
		start_I => start_I,
		event_detected_I => event_detected,
		full_second_I => FSr(1),
		hard_rst_I => hard_rst_I,
		states_detected_I => states_detected,
		css_I => css_1,
		sss_I => sss_I,
		fifo_package_O => fifo_package_O,
		fifo_wr_en_O => fifo_wr_en_O,
		fifo_full_I => fifo_full_I
	);
	
	
	process (clk,rst,trace_signal_I) -- put pin levels into shift register at each cycle
	begin
		if rst = '1' then
			PIN0r <= "000";
			PIN1r <= "000";
			PIN2r <= "000";
			PIN3r <= "000";
			PIN4r <= "000";
			PIN5r <= "000";
			PIN6r <= "000";
			PIN7r <= "000";
			
			FSr	<= "00";
			
			do_init <='1';
			
		elsif rising_edge(clk) then
			if do_init = '1' then
				do_init <='0';
				PIN0r <= (2 downto 0 => trace_signal_I(0));
				PIN1r <= (2 downto 0 => trace_signal_I(1));
				PIN2r <= (2 downto 0 => trace_signal_I(2));
				PIN3r <= (2 downto 0 => trace_signal_I(3));
				PIN4r <= (2 downto 0 => trace_signal_I(4));
				PIN5r <= (2 downto 0 => trace_signal_I(5));
				PIN6r <= (2 downto 0 => trace_signal_I(6));
				PIN7r <= (2 downto 0 => trace_signal_I(7));
			else
				PIN0r <= PIN0r(1 downto 0) & trace_signal_I(0);
				PIN1r <= PIN1r(1 downto 0) & trace_signal_I(1);
				PIN2r <= PIN2r(1 downto 0) & trace_signal_I(2);
				PIN3r <= PIN3r(1 downto 0) & trace_signal_I(3);
				PIN4r <= PIN4r(1 downto 0) & trace_signal_I(4);
				PIN5r <= PIN5r(1 downto 0) & trace_signal_I(5);
				PIN6r <= PIN6r(1 downto 0) & trace_signal_I(6);
				PIN7r <= PIN7r(1 downto 0) & trace_signal_I(7);
			end if;
			
			FSr <= FSr(0) & full_second_I;
			css_0 <= css_I;
			css_1 <= css_0;
		end if;
	end process;
	
	-- read from shift register current state
	events_detected(0) <= '1' when (PIN0r(2 downto 1) = "01" OR PIN0r(2 downto 1) = "10") else '0';
	states_detected(0) <= PIN0r(1);
	events_detected(1) <= '1' when (PIN1r(2 downto 1) = "01" OR PIN1r(2 downto 1) = "10") else '0';
	states_detected(1) <= PIN1r(1);
	events_detected(2) <= '1' when (PIN2r(2 downto 1) = "01" OR PIN2r(2 downto 1) = "10") else '0';
	states_detected(2) <= PIN2r(1);
	events_detected(3) <= '1' when (PIN3r(2 downto 1) = "01" OR PIN3r(2 downto 1) = "10") else '0';
	states_detected(3) <= PIN3r(1);
	events_detected(4) <= '1' when (PIN4r(2 downto 1) = "01" OR PIN4r(2 downto 1) = "10") else '0';
	states_detected(4) <= PIN4r(1);
	events_detected(5) <= '1' when (PIN5r(2 downto 1) = "01" OR PIN5r(2 downto 1) = "10") else '0';
	states_detected(5) <= PIN5r(1);
	events_detected(6) <= '1' when (PIN6r(2 downto 1) = "01" OR PIN6r(2 downto 1) = "10") else '0';
	states_detected(6) <= PIN6r(1);
	events_detected(7) <= '1' when (PIN7r(2 downto 1) = "01" OR PIN7r(2 downto 1) = "10") else '0';
	states_detected(7) <= PIN7r(1);	
	
	events_detected_conf <= events_detected AND on_off_conf_I;
	
	event_detected <= '0' when events_detected_conf = ("00000000") else '1';	
	
	
end Behavioral;