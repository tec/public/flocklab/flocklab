import serial
import time

fpga_uart_port = '/dev/flocklab/usb/daq3'

try:
	ser = serial.Serial(fpga_uart_port, 1000000, timeout=1)
except:
	print("Error opening serial port at %s" %fpga_uart_port)

try:
	ser.write(chr(int('00001111',2)))
except:
	print("Error writing ROUTE command to FPGA")

ack = ser.read(1)

for char in ack:
	if bin(ord(char)) == '0b0':
		print("Route through ON")
	else: 
		print("Route through OFF")
