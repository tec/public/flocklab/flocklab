#include "contiki.h"
#include "glossy-sync.h"


#define DEBUG_GLOSSY_TEST 1
#define PACKET_LEN_MAX 127
#define TIME_BUFFER 1000

#define DEBUG(l, t, ...)    if (DEBUG_GLOSSY_TEST >= l) { DEBUG_PRINT_MSG(t, "GT", __VA_ARGS__); }
#define LEDS_ON(l, ...)     if (DEBUG_GLOSSY_TEST >= l) { leds_on(__VA_ARGS__); }
#define LEDS_OFF(l, ...)    if (DEBUG_GLOSSY_TEST >= l) { leds_off(__VA_ARGS__); }
#define LEDS_TOGGLE(l, ...) if (DEBUG_GLOSSY_TEST >= l) { leds_toggle(__VA_ARGS__); }


static uint8_t payload[PACKET_LEN_MAX];
static uint8_t payload_len = 0;
uint8_t payload_ok;


volatile rtimer_clock_t actVal = 0;
volatile rtimer_clock_t prevVal = 0;
volatile rtimer_clock_t intervalGPS = 0;
volatile uint16_t gpsFlag = 0;  
uint16_t T_irq;

int pps_clkDrift_correction = 0;

rtimer_clock_t *debug_time;

static struct pt pt;


char set_pps(rtimer_t *rt) {
	// clear compare reset mode
	TA0CCTL2 &= ~OUTMOD_5;
	// config compare set mode
	TA0CCTL2 |= OUTMOD_1;
	// write time to compare register
	*(&TA0CCR2) = (uint16_t)(glossy_get_t_ref() + PPS_OFFSET - pps_clkDrift_correction);
	return 1;
}

char glossy_init_scheduler(rtimer_t *rt)
{
  if (actVal != prevVal){
	 rtimer_schedule(0, actVal + SYNC_OFFSET, 0, glossy_scheduler);
  } else {
	 rtimer_schedule(0, rtimer_now() + 200, 0, glossy_init_scheduler);
  }
  return 1;
}


/*---------------------------------------------------------------------------*/
PROCESS(glossy_debug_process, "GD (Glossy Debug)");
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(glossy_debug_process, ev, data) {
	PROCESS_BEGIN();

	while (1) {
		PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
		uint8_t i;
		
		DEBUG(1, debug_time, "%u,%u,%u,%u,%llu,%llu,%i,%i", glossy_get_n_rx(), glossy_get_n_tx(), payload_ok, glossy_flood_success, t_ref_delta_ws, clkDrift,glossy_get_packet_rssi(), glossy_get_noise_rssi());
// 		DEBUG(1, debug_time, "Tslot, %llu, tau1, %llu, tau1Cor, %d", glossy_get_T_slot(), tau1, (int)(tau1 - NS_TO_RTIMER_TICKS(TAU1)));
// 
// 		DEBUG(1, debug_time, "n_rx,%u,n_tx,%u,p_ok,%u", glossy_get_n_rx(), glossy_get_n_tx(), payload_ok);
//		DEBUG(1, debug_time, "clkDrift, %llu", clkDrift);
// 		DEBUG(1, debug_time, "trefDiff, %d", t_ref_diff);
// 		DEBUG(1, debug_time, "relay_cnt_first_rx, %u", glossy_get_relay_cnt_first_rx()); 
		
//  		DEBUG(1, debug_time, "n_T_slot, %u" , glossy_get_n_T_slot());
// 		for(i = 0; i<N_TX_MAX; i++){
// 		 DEBUG(1, debug_time, "rx_start, %u, %llu", i, t_rx_start[i]);
// 		 DEBUG(1, debug_time, "rx_end, %u, %llu", i, t_rx_end[i]);
//  		 DEBUG(1, debug_time, "tx_start, %u, %llu", i, t_tx_start[i]);
//  		 DEBUG(1, debug_time, "tx_end, %u, %llu", i, t_tx_end[i]);
// 		}
		// set timestamp array values to zero
		 for(i=0; i< N_TX_MAX; i++){
		 t_rx_start[i] = 0;
		 t_tx_start[i] = 0;
// 		 if(DEBUG_VARS == 1){
// 		  t_rx_end[i] = 0;
// 		  t_tx_end[i] = 0;
// 		 }
		}
		tau1_Sum = 0;
		tau1_Cnt = 0;
	}

	PROCESS_END();

}


void glossy_debug_init(void){
 process_start(&glossy_debug_process, NULL); 
}

char glossy_scheduler(rtimer_t *rt) {
	PT_BEGIN(&pt);

	if (INITIATOR_ID == node_id) {
		while (1) {
#if 0
			TA0CCTL2 |= OUTMOD_5;
#else
			// time buffer for interrupt interference
			uint16_t gotime = (uint16_t)(rt->time + TIME_BUFFER);
			TA0CCR2 = gotime;
			TA0CCTL2 |= OUTMOD_5;
			TA0CCTL2 &= ~CCIFG;
			// RTIMER_DISABLE_OVERFLOW_INTERRUPTS();
			// PIN_SET(1, 0);
			while (!(TA0CCTL2 & CCIFG)) {};
			// PIN_CLEAR(1, 0);
			
			T_irq = (TA0R - gotime) << 1;
			if (T_irq <= 34) {
			// NOPs (variable number) to compensate
			asm volatile("add %[d], r0" : : [d] "m" (T_irq));
			asm volatile("nop");						// irq_delay = 0
			asm volatile("nop");						// irq_delay = 2
			asm volatile("nop");						// irq_delay = 4
			asm volatile("nop");						// irq_delay = 6
			asm volatile("nop");						// irq_delay = 8
			asm volatile("nop");						// irq_delay = 10
			asm volatile("nop");						// irq_delay = 12
			asm volatile("nop");						// irq_delay = 14
			asm volatile("nop");						// irq_delay = 16
			asm volatile("nop");						// irq_delay = 18
			asm volatile("nop");						// irq_delay = 20
			asm volatile("nop");						// irq_delay = 22
			asm volatile("nop");						// irq_delay = 24
			asm volatile("nop");						// irq_delay = 26
			asm volatile("nop");						// irq_delay = 28
			asm volatile("nop");						// irq_delay = 30
			asm volatile("nop");						// irq_delay = 32
			asm volatile("nop");						// irq_delay = 34
			}
#endif
			// NOTE: T_ref is approximately GPS time (actVal) + SYNC_OFFSET + TIME_BUFFER + time to start
			//       The generated PPS pulse is at T_ref + PPS_OFFSET
			//       At the time of this note (1.6.2015), total offset is O = (RTIMER_SECOND / 1000) + 1000 + (RTIMER_SECOND / 4) ~ 3264000 ticks = 251.1 ms
			//       GPS configuration should be: PPS falling edge, length: 1 s - O
			glossy_start(INITIATOR_ID, payload, payload_len, N_TX_MAX, GLOSSY_WITH_SYNC, GLOSSY_WITH_RF_CAL);
			
			// schedule to return to this point after DURATION = approx. duration of a glossy flood
			rtimer_schedule(0, rt->time + DURATION, 0, glossy_scheduler);
			
			PT_YIELD(&pt);

			// stop glossy: flushing buffers, setting glossy inactive, etc...
			glossy_stop();
			
			if (clkDrift != 0){
			   pps_clkDrift_correction = (RTIMER_SECOND - clkDrift)/PPS_FRAC;
			}
			// calculate the interval (#ticks) between recent and previous GPS pulse
			intervalGPS = actVal - prevVal;
			prevVal = actVal;
			gpsFlag = 0;
			
			// schedule a new glossy round, which happens one second after the last gps pulse plus a small offset
		
			rtimer_schedule(0, actVal + PERIOD, 0, glossy_init_scheduler);
			rtimer_schedule(3, glossy_get_t_ref() + PPS_OFFSET - pps_clkDrift_correction - PPS_GUARD, 0 , set_pps);
			// poll the debuging process in order to print some data without violating the timing behaviour (e.g. missing overflows)
			// important if ACLK > 6.5MHz
			debug_time = &rt->time;
			process_poll(&glossy_debug_process);
			
			PT_YIELD(&pt);
		}
	} else {
		while (1) {
			// schedule to return to this point after DURATION = approx. duration of a glossy flood
			if (glossy_is_t_ref_updated()) {
				rtimer_schedule(0, rt->time + DURATION, 0, glossy_scheduler);
			} else {
				rtimer_schedule(0, rt->time + INIT_DURATION, 0, glossy_scheduler);
			}
			// change compare-output mode to reset in order to set the PPS-pin to low
			TA0CCTL2 |= OUTMOD_5;
			// start glossy as receiver
			glossy_start(INITIATOR_ID, payload, GLOSSY_UNKNOWN_PAYLOAD_LEN,
					N_TX_MAX, GLOSSY_WITH_SYNC, GLOSSY_WITH_RF_CAL);
			PT_YIELD(&pt);

			// stop glossy
			glossy_stop();
// 			PIN_TOGGLE(1,2);
			
			
			// external oscillator won't exactly run at 26MHZ, therefore we need to compensate for the missing/added ticks between the reference time and the actual pulse
			if (clkDrift != 0){
			  pps_clkDrift_correction = (RTIMER_SECOND - clkDrift)/PPS_FRAC;
			}
			
			if (glossy_get_n_rx() > 0) {
				// check the content of the payload
				payload_ok = 1;
				uint8_t i;
				for (i = 0; i < glossy_get_payload_len(); i++) {
					if (payload[i] != i) {
						// wrong payload!
						payload_ok = 0;
						break;
					}
				}
			} else {
				payload_ok = 0;
			}
			
			if (gpsFlag == 1){
				   // if a GPS pulse occured between the last glossy stop and the previous one, update the interval between the two GPS pulses 
				   intervalGPS = actVal - prevVal;			   
				   prevVal = actVal;
				   gpsFlag = 0; 
				}			
			if (glossy_is_t_ref_updated()) {
				// schedule a new glossy round, which will occur after 1 second based on the previous refrence time, considering a small guard time
				rtimer_schedule(0, glossy_get_t_ref() + PERIOD - (pps_clkDrift_correction * PPS_FRAC) - GUARD_TIME, 0, glossy_scheduler);
				rtimer_schedule(3, (glossy_get_t_ref() + PPS_OFFSET - pps_clkDrift_correction - PPS_GUARD), 0 , set_pps);
				// rtimer_schedule(2, glossy_get_t_ref() + RTIMER_SECOND / 20, 0, (rtimer_callback_t)toggle_pin);
				// poll the debug process to print some data 				
				debug_time = &rt->time;
				process_poll(&glossy_debug_process);
				PT_YIELD(&pt);
			} else {
				rtimer_schedule(0, rtimer_now() + RTIMER_SECOND / 50, 0, glossy_scheduler);
// 				not_synced_cnt++;
//  				process_poll(&glossy_debug_process);
				PT_YIELD(&pt);
				
			}
			
		}
	}

	PT_END(&pt);
}

void txlvl() {
	// 0xc0: max, 0xc2: 10dBm, 0x81: 5dBm, 0x88: 3dBm, 0x8d: 0dBm
	static uint8_t pa_values[6] = {0xc2, 0xc2, 0xc2, 0xc2, 0xc2, 0xc2}; // for values see http://www.ti.com/lit/an/swra151a/swra151a.pdf
	write_data_to_register(PATABLE, (uint8_t *)pa_values, 6);
	rf1a_set_tx_power(0); // select index 0
}

/*---------------------------------------------------------------------------*/
PROCESS(glossy_test_process, "GT (Glossy test)");
AUTOSTART_PROCESSES(&glossy_test_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(glossy_test_process, ev, data) {

	PROCESS_BEGIN();

	PT_INIT(&pt);
	
	uint8_t i;
	
	//set the TX power level to +10 dBm
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_30_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_12_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_6_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_0_dBm);
 	//rf1a_set_tx_power(RF1A_TX_POWER_PLUS_10_dBm);
	rf1a_go_to_idle();
	txlvl();
	rf1a_go_to_sleep();
	
	// define passive listeners
// 	const uint8_t listeners_arr[NUM_LISTENERS] = {1, 7, 8, 10, 13, 16, 18, 20, 200};
// 	for (i = 0; i < NUM_LISTENERS; i++){
// 	 if(listeners_arr[i] == node_id){
// 	   rf1a_set_tx_power(RF1A_TX_POWER_MINUS_30_dBm);
// 	 }
// 	}
	
	// set the maximum allowed packet length to 127 bytes
	rf1a_set_maximum_packet_length(PACKET_LEN_MAX);
	rf1a_set_channel(0);

	
	PIN_SET_AS_OUTPUT(1, 0); // LED1
	PIN_SET_AS_OUTPUT(1, 1); // LED2
	PIN_SET_AS_OUTPUT(1, 2); // LED3
	PIN_SET_AS_OUTPUT(3, 6); // INT1
	PIN_SET_AS_OUTPUT(3, 7); // INT2

	// used for debugging
	//PIN_MAP_AS_OUTPUT(3, 0, PM_RFGDO2);
	//PIN_MAP_AS_OUTPUT(2, 6, PM_ACLK);
	
	// PIN 3.3 for PPS output to FPGA, enable compare
	P3DIR |= BIT3;
	P3SEL |= BIT3;

	if (INITIATOR_ID == node_id) {
		// PIN 3.2 for GPS pulse capture (TA0CCR1A)
		P3SEL |= BIT2;
		P3DIR &= ~BIT2;
		// set the content of the payload
		for (i = 0; i < PAYLOAD_LEN; i++) {
			payload[i] = i;
		}
	}

	// execute transmit_broadcast() after PERIOD, using rtimer 0
	rtimer_schedule(0, rtimer_now() + PERIOD, 0, glossy_scheduler);

	PROCESS_END();

}




