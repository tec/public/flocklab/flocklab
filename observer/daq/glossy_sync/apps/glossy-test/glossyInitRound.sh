for NEW_ID in 1 2 4 6 7 8 10 11 13 14 15 16 17 18 19 20 22 23 24 25 26 27 28 31 32 33 200 201 202 204
do
	echo $NEW_ID
	sed -i "11s/.*/#define INITIATOR_ID  $NEW_ID/" glossy-test.c
	make
	cat ./xml/upperXML.txt > ./xml/glossy_Initiator$NEW_ID.xml
	base64 glossy-test.exe >> ./xml/glossy_Initiator$NEW_ID.xml
	cat ./xml/endXML.txt >> ./xml/glossy_Initiator$NEW_ID.xml
done
