#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "usage:   install.sh [observer ID]"
    exit 1
fi

re='^[0-9]{3}$'
if ! [[ $1 =~ $re ]] ; then
   echo "observer ID must be a 3 digit integer number (e.g. 029)"
   exit 2
fi

NODEID=$1

tos-set-symbols --objcopy msp430-objcopy --objdump msp430-objdump --target ihex glossy-test.exe glossy-test.hex-$NODEID TOS_NODE_ID=$NODEID

scp  glossy-test.hex-$NODEID root@flocklab-observer$NODEID.flocklab-dyn.ethz.ch:/tmp/
ssh root@flocklab-observer$NODEID.flocklab-dyn.ethz.ch "/etc/init.d/synclog.sh stop"
ssh root@flocklab-observer$NODEID.flocklab-dyn.ethz.ch "export PYTHONPATH=/media/card/opkg/usr/lib/python2.6/; /usr/bin/python -m msp430.bsl5.uart -p /dev/flocklab/usb/daq2 -i ihex -s 115200 -e -S -V -P /tmp/glossy-test.hex-$NODEID";
ssh root@flocklab-observer$NODEID.flocklab-dyn.ethz.ch "/etc/init.d/synclog.sh start"
