#include "contiki.h"
#include "flocklab-linktest.h"
#include "rf1a.h"
#include "rf1a_core.h"
#include "debug-print.h"
#include "tosmsg.h"

#define DEBUG_FLOCKLAB_LINKTEST 0

#define DEBUG(l, t, ...)    if (DEBUG_FLOCKLAB_LINKTEST >= l) { DEBUG_PRINT_MSG(t, "", __VA_ARGS__); }
#define LEDS_ON(l, ...)     if (DEBUG_FLOCKLAB_LINKTEST >= l) { leds_on(__VA_ARGS__); }
#define LEDS_OFF(l, ...)    if (DEBUG_FLOCKLAB_LINKTEST >= l) { leds_off(__VA_ARGS__); }
#define LEDS_TOGGLE(l, ...) if (DEBUG_FLOCKLAB_LINKTEST >= l) { leds_toggle(__VA_ARGS__); }

static uint8_t payload[FL_PACKET_LEN_MAX];
static uint8_t payload_len = 0;
static radio_msg_t * pck;

static rtimer_clock_t debug_time;

static struct pt pt;
volatile rtimer_clock_t actVal;
volatile uint16_t gpsFlag; 

char set_pps(rtimer_t *rt) {
	// clear compare reset mode
	TA0CCTL2 &= ~OUTMOD_5;
	// config compare set mode
	TA0CCTL2 |= OUTMOD_1;
	// write time to compare register
	*(&TA0CCR2) = (uint16_t)(glossy_get_t_ref() + PPS_OFFSET - pps_clkDrift_correction);
	return 1;
}

static int sender = 0;
static int repetition;
static rtimer_clock_t roundstart;
static unsigned int tx;
static stats_msg_t stats;
static stats_msg_t print_stats;

void glossy_debug_init() {}

/*---------------------------------------------------------------------------*/
PROCESS(print_process, "Print Process");
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(print_process, ev, data) {
	PROCESS_BEGIN();

	while (1) {
		
		PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
// 		debug_time = rtimer_now();
// 		DEBUG(1, &debug_time, "Sender = %d, num_messages=%d, num_received=%d", print_stats.sender_id, print_stats.num_messages, print_stats.num_received);
		tos_to_nx(&(print_stats.sender_id));tos_to_nx(&(print_stats.num_messages));tos_to_nx(&(print_stats.num_received));
		write_tos_msg(AM_STATS_MSG, &print_stats, sizeof(print_stats));
	}

	PROCESS_END();
}

char scheduler(rtimer_t *rt) {
	
	PT_BEGIN(&pt);
	
	for (repetition=FL_REPETITIONS; repetition>0; repetition--) {
		for (sender=0; sender <= FL_MAX_NODE_ID; sender++) {
			roundstart = rt->time;
			stats.num_messages = FL_NUM_MESSAGES;
			stats.sender_id = sender;
			stats.num_received = 0;
			if (sender==node_id) {
				P1OUT |= BIT1;
				rtimer_schedule(0, rt->time + FL_GUARD_TIME, 0, scheduler);
				PT_YIELD(&pt);
				for (tx = FL_NUM_MESSAGES; tx>0; tx--) {
					pck->counter=tx-FL_NUM_MESSAGES;
					if (tx & 1) {
						P1OUT |= BIT0;
					}
					else {
						P1OUT &= ~BIT0;
					}
					broadcast_send(payload, sizeof(radio_msg_t));
					rtimer_schedule(0, rt->time + FL_MSG_INT, 0, scheduler);
					PT_YIELD(&pt);
				}
				P1OUT &= ~BIT1;
			}
			// next sender
			P1OUT |= BIT2;
			rtimer_schedule(0, roundstart + FL_NUM_MESSAGES * FL_MSG_INT + 2 * FL_GUARD_TIME, 0, scheduler);
			PT_YIELD(&pt);
			memcpy(&print_stats, &stats, sizeof(stats_msg_t));
			process_poll(&print_process);
			P1OUT &= ~BIT2;
		}
	}

	PT_END(&pt);
}

void unicast_received(rtimer_clock_t *timestamp, void *payload, uint8_t payload_len, addr_t source) {
}

void broadcast_received(rtimer_clock_t *timestamp, void *payload, uint8_t payload_len, addr_t source) {
	if (stats.sender_id == source)
		stats.num_received++;
	debug_time = rtimer_now();
	DEBUG(1, &debug_time, "Received Broadcast, Sender = %d", source);
}

/*---------------------------------------------------------------------------*/
PROCESS(flocklab_linktest_process, "Linktest");
AUTOSTART_PROCESSES(&flocklab_linktest_process, &print_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(flocklab_linktest_process, ev, data) {

	PROCESS_BEGIN();

	PT_INIT(&pt);
	
	uint8_t i;
	
	//set the TX power level to +10 dBm
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_30_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_12_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_MINUS_6_dBm);
	//rf1a_set_tx_power(RF1A_TX_POWER_0_dBm);
 	//rf1a_set_tx_power(RF1A_TX_POWER_PLUS_10_dBm);
	
	//  max: 0xc0
	//  7dBm: 0xca
	//  4dBm: 0x85
	//  1dBm: 0x8c
	static const uint8_t pa_values[6] = {TX_POWER, 0x25, 0x2d, 0x8d, 0xc3, 0xc0}; // for values see http://www.ti.com/lit/an/swra151a/swra151a.pdf
	write_data_to_register(PATABLE, (uint8_t *)pa_values, 6);
	rf1a_set_tx_power(0); // select index 0
	// set high power request
	PMMCTL0_H = 0xA5;
	PMMCTL0_L |= PMMHPMRE;
	PMMCTL0_H = 0x00;
	
	// set the maximum allowed packet length to 127 bytes
	rf1a_set_maximum_packet_length(FL_PACKET_LEN_MAX);
	rf1a_set_channel(10);
	
	PIN_SET_AS_OUTPUT(2, 7); // COM_SPI_MUX
	P2OUT |= BIT7;
// 	PIN_SET_AS_OUTPUT(1, 0); // LED1
// 	PIN_SET_AS_OUTPUT(1, 1); // LED2
// 	PIN_SET_AS_OUTPUT(1, 2); // LED3
// 	PIN_SET_AS_OUTPUT(3, 6); // INT1
// 	PIN_SET_AS_OUTPUT(3, 7); // INT2
// 	P1OUT &= ~(BIT0 | BIT1 | BIT2);

	// used for debugging
	//PIN_MAP_AS_OUTPUT(3, 0, PM_RFGDO2);
	//PIN_MAP_AS_OUTPUT(2, 6, PM_ACLK);
	
	// PIN 3.2 for GPS pulse capture (TA0CCR1A)
// 	P3SEL |= BIT2;
// 	P3DIR &= ~BIT2;
	
	// set the content of the payload
	pck = (radio_msg_t*)&payload;
	for (i = 0; i < PAYLOAD_LEN; i++) {
		payload[i] = i;
	}
#if 1
	rtimer_clock_t delay = rtimer_now() + RTIMER_SECOND;
	while(delay < rtimer_now());
	// configure the RST pin in NMI mode (default: pullup enabled, rising edge triggers)
	SFRRPCR |= SYSNMI;
	delay = rtimer_now() + 5 * RTIMER_SECOND;
	while (!(SFRIFG1 & NMIIFG) && delay < rtimer_now());
	SFRIFG1 &= ~NMIIFG;
	SFRRPCR &= ~SYSNMI;     // disable NMI mode
	rtimer_schedule(0, rtimer_now() + RTIMER_SECOND, 0, scheduler);
/*
	// wait for SIG1 interrupt on P1.3
	PIN_SET_AS_INPUT(1, 3); // SIG1
	P1REN &= ~(1<<3);
	P1IES &= ~(1<<3);
	P1IFG = 0;
	P1IE = 1<<3;*/
#else
	node_id = 0;
	rtimer_schedule(0, rtimer_now() + RTIMER_SECOND, 0, scheduler);
#endif
	PROCESS_END();

}


ISR(PORT1, irq_p1)  {
	// start link measurements
	rtimer_schedule(0, rtimer_now() + RTIMER_SECOND, 0, scheduler);
	P1IE &= ~(1<<3);
}
