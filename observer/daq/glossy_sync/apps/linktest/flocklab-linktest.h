#ifndef FLOCKLAB_LINKTEST_H
#define FLOCKLAB_LINKTEST_H

#define FL_PACKET_LEN_MAX 127
#define FL_GUARD_TIME (RTIMER_SECOND / 20)
#define FL_NUM_MESSAGES 100
#define FL_MSG_INT (RTIMER_SECOND / 200) // REPETITIONS * nodes *  (GUARD_TIME_MS * 2 + FL_NUM_MESSAGES * FL_MSG_INT) = 288
#define FL_MAX_NODE_ID 31 // starting from 0
#define FL_CHANNEL_MIN 0
#define FL_CHANNEL_MAX 12
#define FL_RSSI_MIN 0
#define FL_RSSI_MAX 100
#define FL_N_SCANS 10000U
#define FL_REPETITIONS 15

typedef struct radio_msg {
  uint16_t counter;
  uint8_t padding[20];
} __attribute__((packed)) radio_msg_t;

typedef struct stats_msg {
  uint16_t num_messages;
  uint16_t sender_id;
  uint16_t num_received;
} __attribute__((packed)) stats_msg_t;

typedef struct rssi_msg {
  uint8_t channel;
  uint16_t rssi_level;
  uint16_t rssi_count;
} __attribute__((packed)) rssi_msg_t;


enum {
  AM_RADIO_MSG = 6,
  AM_STATS_MSG = 7,
  AM_RSSI_MSG = 8,
};

#endif