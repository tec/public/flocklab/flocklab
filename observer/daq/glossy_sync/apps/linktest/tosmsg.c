#include "contiki.h"
#include "uart0.h"

#define SYNCH_BYTE 	0x7e
#define ESCAPE_BYTE	0x7d
#define PROT_NOACK	0x45
#define MSG_ID			0x41

/*---------------------------------------------------------------------------*/
static uint16_t
crc_byte(uint16_t crc, uint8_t b)
{
  crc = (uint8_t)(crc >> 8) | (crc << 8);
  crc ^= b;
  crc ^= (uint8_t)(crc & 0xff) >> 4;
  crc ^= crc << 12;
  crc ^= (crc & 0xff) << 5;
  return crc;
}
/*---------------------------------------------------------------------------*/
static uint16_t
writeb_crc(unsigned char c, uint16_t crc)
{
  /* Escape bytes:
        7d -> 7d 5d
        7e -> 7d 5e */
  if(c == ESCAPE_BYTE){
    uart0_writeb(ESCAPE_BYTE);
    uart0_writeb(0x5d);
  } else if(c == SYNCH_BYTE){
    uart0_writeb(ESCAPE_BYTE);
    uart0_writeb(0x5e);
  } else {
    uart0_writeb(c);
  }

  return crc_byte(crc, c);
}
/*---------------------------------------------------------------------------*/
void write_tos_msg(uint8_t msg_id, void * payload, uint8_t len)
{
  int i;
  uint16_t crc;
  /* Packetize and write buffered characters to serial port */

  /* Start of frame */
  crc = 0;
  uart0_writeb(SYNCH_BYTE);

  /* Protocol (noack) */
  crc = writeb_crc(PROT_NOACK, crc);

  /* Sequence */
  crc = writeb_crc(0x00, crc);

  /* Destination */
  crc = writeb_crc(0xFF, crc);
  crc = writeb_crc(0xFF, crc);

  /* Source */
  crc = writeb_crc(0x00, crc);
  crc = writeb_crc(0x00, crc);

  /* Payload length = full buffer size */
  crc = writeb_crc(len, crc);

  /* Group */
  crc = writeb_crc(0x00, crc);

  /* Message ID */
  crc = writeb_crc(msg_id, crc);

  /* Actual payload characters */
  for (i=0; i < len; i++) {
    crc = writeb_crc(*(char*)(payload + i), crc);
  }

  /* CRC */
  /* Note: calculating but ignoring CRC for these two... */
  writeb_crc((uint8_t) (crc & 0xFF), 0);
  writeb_crc((uint8_t) ((crc >> 8) & 0xFF), 0);

  /* End of frame */
  uart0_writeb(SYNCH_BYTE);

}

void tos_to_nx(uint16_t * v) {
  uint8_t swp;
  uint8_t * b = (uint8_t *) v;
  swp = *b;
  *b = *(b+1);
  *(b+1) = swp;
}
