#ifndef __TOSMSG__
#define __TOSMSG__

void write_tos_msg(uint8_t msg_id, void * payload, uint8_t len);

void tos_to_nx(uint16_t * v);

#endif