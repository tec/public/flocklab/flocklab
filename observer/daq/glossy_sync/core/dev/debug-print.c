#include "debug-print.h"
#include "rtimer.h"

#if DEBUG_PRINT_CONF_ON
MEMB(debug_print_memb, debug_print_t, N_DEBUG_MSG);
LIST(debug_print_list);

char content[MAX_CONTENT_LENGTH + 1];

uint16_t timerReg;

volatile uint8_t overflow_lock = 0;

PROCESS(debug_print_process, "Debug print process");
PROCESS_THREAD(debug_print_process, ev, data) {
	PROCESS_BEGIN();

	memb_init(&debug_print_memb);
	list_init(debug_print_list);
	
	rtimer_clock_t now = rtimer_now();
	DEBUG_PRINT_MSG(&now, "DebugPrint", "Started");

	while (1) {
		PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
		// wait until we are polled by somebody
		while (list_length(debug_print_list) > 0) {
		  //PIN_SET(3,6);
#ifdef WITH_RADIO
#ifdef WITH_GLOSSY
			// do not try to print anything over the UART while Glossy is active
			while (glossy_is_active()) {
				PROCESS_PAUSE();
			}
#else
			// do not try to print anything over the UART while the radio is busy
			while (rf1a_is_busy()) {
				PROCESS_PAUSE();
			}
#endif /* WITH_GLOSSY */
#endif /* WITH_RADIO */

			// print the first message in the queue
			debug_print_t *msg = list_head(debug_print_list);
			printf("%u %llu. %10s: %s.\n", node_id, RTIMER_TO_MS(msg->time), msg->protocol, msg->content);
			// remove it from the queue
			
			//printf("%s\n", msg->content);
			
			list_remove(debug_print_list, msg);
			memb_free(&debug_print_memb, msg);
			//PIN_CLEAR(3,6);
			PROCESS_PAUSE();
		}
	}

	PROCESS_END();
}

void debug_print_init(void) {
	process_start(&debug_print_process, NULL);
}

void debug_print_msg(rtimer_clock_t *time, char *protocol, char *content) {
	
	debug_print_t *msg = memb_alloc(&debug_print_memb);
	if (msg != NULL) {
		// construct the message struct
		//PIN_SET(3,7);
		msg->time = *time;
		snprintf(msg->protocol, MAX_PROTOCOL_LENGTH + 1, protocol);
		sprintf(msg->content, content);

		// add it to the list of messages ready to print
		list_add(debug_print_list, msg);
		//PIN_CLEAR(3,7);
		// poll the debug print process
		process_poll(&debug_print_process);
		
	}
}
#else
void debug_print_init(void) { }
void debug_print_msg(rtimer_clock_t *time, char *protocol, char *content) { }
#endif /* DEBUG_PRINT_CONF_ON */
