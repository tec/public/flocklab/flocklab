#ifndef __DEBUG_PRINT_H__
#define __DEBUG_PRINT_H__

#include "contiki.h"

#define MAX_PROTOCOL_LENGTH 7
#define MAX_CONTENT_LENGTH  71
#define N_DEBUG_MSG 10

extern char content[MAX_CONTENT_LENGTH + 1];

typedef struct debug_print_t {
	struct debug_print_t *next;
	rtimer_clock_t time;
	char protocol[MAX_PROTOCOL_LENGTH + 1];
	char content[MAX_CONTENT_LENGTH + 1];
} debug_print_t;

#if DEBUG_PRINT_CONF_ON
#define DEBUG_PRINT_MSG(t, p, ...) \
	do { \
		snprintf(content, MAX_CONTENT_LENGTH + 1, ##__VA_ARGS__); \
		debug_print_msg(t, p, content); \
	} while (0)
#else
#define DEBUG_PRINT_MSG(t, p, ...)
#endif /* DEBUG_PRINT_CONF_ON */

void debug_print_init(void);

void debug_print_msg(rtimer_clock_t *time, char *protocol, char *content);

#endif /* __DEBUG_PRINT_H__ */
