#include "clock.h"
#include "hal_pmm.h"

// TODO: periodically re-calibrate the DCO if not exited from LPM for a while

void clock_init(void) {

	// set clock sources:
	// FLLREFCLK <- XT1 (low-frequency crystal, 32,768 Hz)
	// ACLK      <- XT2 (high-frequency crystal, 26 MHz / 8 = 3.25 MHz)
	// SMCLK     <- XT1 (low-frequency crystal, 32,768 Hz)
	// MCLK      <- XT2 (high-frequency crystal, 26 MHz / 2 = 13 MHz)

	// set the supply voltage to the maximum
	SetVCore(PMMCOREV_3);

	// enable XT2
	ENABLE_XT2();

	// initially, use the internal REFO and DCODIV clock sources
	UCSCTL3 = SELREF__REFOCLK | FLLREFDIV_0;
	UCSCTL4 = SELA__DCOCLKDIV | SELS__REFOCLK | SELM__DCOCLKDIV;
	// wait until XT1, XT2, and DCO stabilize
	do {
		// clear XT1, XT2, and DCO fault flags
		UCSCTL7 &= ~(XT1LFOFFG + XT2OFFG + DCOFFG);
		// clear oscillator fault flag
	    SFRIFG1 &= ~OFIFG;
	} while (SFRIFG1 & OFIFG);
	// XT1 is now stable: reduce its drive strength to save power
	UCSCTL6 &= ~XT1DRIVE_3;

	// set the DCO frequency to 3.25 MHz
	// disable the FLL control loop
	DISABLE_FLL();
	// set the lowest possible DCOx, MODx
	UCSCTL0 = 0;
	// select the suitable DCO frequency range, DCORSEL_6 =>  10.7MHz < FDCO < 39MHz
	UCSCTL1 = DCORSEL_6;
	// set the FLL loop divider to 2
	
	// set the FLL loop multiplier N such that (N + 1) * f_FLLREF = f_DCO --> N = 396 for f_DC0 = 13Mhz
	UCSCTL2 = FLLD_0 + 396;
	
	// For a f_DCO = 20MHz: (N + 1) * 32768 approx. 20*10^6 --> N = 609 and f_DCO = 19'933'580Hz
	//UCSCTL2 = FLLD_0 + 609;
	
	// enable the FLL control loop
	ENABLE_FLL();
	
	// wait until the DCO stabilizes
	
	// (up to 1 x 32 x 32 x 13 MHz / 32,768 Hz = 406,250 DCO cycles)
	__delay_cycles(406250);
		
	// (up to 1 x 32 x 32 x 20 MHz / 32,768 Hz = 625000 DCO cycles)
	//__delay_cycles(625000);
	
	// finally, use the desired clock sources and speeds
	// FLL source = XT1 and divider = 1
	UCSCTL3 = SELREF__XT1CLK | FLLREFDIV_0;
	// ACLK source = SELA, SMCLK = SELS and MCLK  = SELM (defined in contiki-conf.h)
	UCSCTL4 = SELA | SELS | SELM;
	// same applies to the divider values
	UCSCTL5 = DIVA | DIVS | DIVM;

}
