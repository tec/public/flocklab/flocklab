#ifndef __CLOCK_H__
#define __CLOCK_H__

#include "contiki-conf.h"

// speed of XT1 (low-frequency crystal)
#define XT1CLK_SPEED 32768

// speed of XT2 (high-frequency crystal)
#define XT2CLK_SPEED 26000000LU

// check whether the high-frequency crystal XT2 is permanently enabled
#define IS_XT2_ENABLED() (~(UCSCTL6 & XT2OFF))

// permanently enable the high-frequency crystal XT2 (i.e., even if the radio is in SLEEP mode)
#define ENABLE_XT2() (UCSCTL6 &= ~XT2OFF)

// disable the high-frequency crystal XT2 (i.e., active only when the radio is active)
#define DISABLE_XT2() (UCSCTL6 |= XT2OFF)

// enable the FLL control loop
#define ENABLE_FLL() (__bic_status_register(SCG0))

// disable the FLL control loop
#define DISABLE_FLL() (__bis_status_register(SCG0))

// initialize the clock system
void clock_init(void);

clock_time_t clock_time(void);

#endif /* __CLOCK_H__ */
