#include "contiki.h"
#include "glossy-sync.h"


uint16_t TOS_NODE_ID =0x1122;
uint16_t node_id;

static void print_processes(struct process * const processes[]) {
	printf("Starting");
	while(*processes != NULL) {
		printf(" '%s'", (*processes)->name);
		processes++;
	}
	putchar('\n');
}

int main(int argc, char **argv) {

	// stop the watchdog
	watchdog_stop();

	// initialize hardware
	leds_init();

	leds_on(LEDS_ALL);

	clock_init();

	rtimer_init();

	uart0_init();
	uart0_set_input(serial_line_input_byte);

#ifdef WITH_RADIO
	rf1a_init();
#endif /* WITH_RADIO */

	leds_off(LEDS_ALL);

	node_id = TOS_NODE_ID;
	printf(CONTIKI_VERSION_STRING " started. ");
	if(node_id > 0) {
		printf("Node id is set to %u.\n", node_id);
	} else {
		printf("Node id is not set.\n");
	}
	print_processes(autostart_processes);

	process_init();
	process_start(&etimer_process, NULL);

	random_init(node_id * TA0R);
	serial_line_init();
	debug_print_init();
	
	glossy_debug_init();
	
	
	energest_init();
	ENERGEST_ON(ENERGEST_TYPE_CPU);

#ifdef WITH_NULLMAC
	nullmac_init();
#endif /* WITH_NULLMAC */

#if WATCHDOG_CONF_ON
	watchdog_start();
#endif /* WATCHDOG_CONF_ON */

	autostart_start(autostart_processes);

	while (1) {
		int r;
		do {
#if WATCHDOG_CONF_ON
			// reset the watchdog
			watchdog_periodic();
#endif /* WATCHDOG_CONF_ON */
			r = process_run();
		} while (r > 0);

		// idle processing
		// disable interrupts
		__dint(); __nop();
		if (process_nevents() != 0 || uart0_active()) {
			// re-enable interrupts
			__eint(); __nop();
		} else {
			// re-enable interrupts and go to sleep atomically
			ENERGEST_OFF(ENERGEST_TYPE_CPU);
#if WATCHDOG_CONF_ON
			watchdog_stop();
#endif /* WATCHDOG_CONF_ON */
			// LPM3
			__bis_status_register(GIE | SCG0 | SCG1 | CPUOFF);

#if WATCHDOG_CONF_ON
			watchdog_start();
#endif /* WATCHDOG_CONF_ON */
			ENERGEST_ON(ENERGEST_TYPE_CPU);
		}
	}

	return 0;

}
