#ifndef __CONTIKI_CONF_H__
#define __CONTIKI_CONF_H__

#include "msp430.h"
#include "isr_compat.h"
#include "string.h"

#define CLIF
#define CCIF


// source and speed of the Master Clock MCLK
#define SELM SELM__XT2CLK
#define DIVM DIVM__2
#define MCLK_SPEED (XT2CLK_SPEED / 2)

// source and speed of the Auxiliary Clock ACLK: 
#define SELA SELA__XT2CLK
#define DIVA DIVA__2
#define ACLK_SPEED (XT2CLK_SPEED / 2)

// source and speed of the Sub-System Master Clock SMCLK
#define SELS SELS__XT2CLK
#define DIVS DIVS__2
#define SMCLK_SPEED (XT2CLK_SPEED / 2)

typedef uint32_t clock_time_t;
#define CLOCK_SECOND 50 // corresponds to roughly 1.008246 seconds

#define UART0_BAUDRATE 115200LU

// RF1A configuration file
#include "rf1a_SmartRF_settings/868MHz_2GFSK_250kbps.h"

typedef uint16_t addr_t;

#ifndef ENERGEST_CONF_ON
#define ENERGEST_CONF_ON 0
#endif /* ENERGEST_CONF_ON */

#ifndef WATCHDOG_CONF_ON
#define WATCHDOG_CONF_ON 0
#endif /* WATCHDOG_CONF_ON */

#ifndef DEBUG_PRINT_CONF_ON
#define DEBUG_PRINT_CONF_ON 1
#endif /* DEBUG_PRINT_CONF_ON */

#endif /* __CONTIKI_CONF_H__ */
