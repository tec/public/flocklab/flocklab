#define INITIATOR_ID  24
// period of a glossy round (1s, because based on GPS pulse)
#define PERIOD        (RTIMER_SECOND)
#define DURATION      (RTIMER_SECOND / 15)
#define INIT_DURATION (RTIMER_SECOND / 4)
#define GUARD_TIME    (RTIMER_SECOND / 500)
#define SYNC_OFFSET   (RTIMER_SECOND / 1000)
#define PPS_FRAC      4
// hard coded value of 490237 clock cycles (0.03771053s @13MHz) compensates offset of 1 PPS output signal relative to 1 PPS signal of nodes which are synchronized with GPS directly
#define PPS_OFFSET    (RTIMER_SECOND / PPS_FRAC + 490237)
/* #define PPS_OFFSET    (RTIMER_SECOND / PPS_FRAC) */
#define PPS_GUARD     200
#define DELTA_GUARD	 1000
#define N_TX_MAX      5
#define PAYLOAD_LEN   4
// #define DEBUG_VARS    0
// #define NUM_LISTENERS 9


//*****//
// FUN //
//*****//
char set_pps(rtimer_t *rt);
// used for scheduling a glossy round
char glossy_scheduler(rtimer_t *rt);
// starts debug printing process
void glossy_debug_init(void);
// initiating a new glossy round
char glossy_init_scheduler(rtimer_t *rt);

//*****//
// VAR //
//*****//
// extern const uint8_t listeners_arr[NUM_LISTENERS];

// current timer count at gps pulse occurence
extern volatile rtimer_clock_t actVal;
// previous timer count at gps pulse occurence
extern volatile rtimer_clock_t prevVal;
// interval between two consecutive gps pulses (in clock ticks)
extern volatile rtimer_clock_t intervalGPS;
// flag, set if gps pulses occures in TAOCCR1 interrupt routine
extern volatile uint16_t gpsFlag;

extern volatile rtimer_clock_t t_rx_start[N_TX_MAX];
extern volatile rtimer_clock_t t_tx_start[N_TX_MAX];
// #if DEBUG_VARS == 1
// extern volatile rtimer_clock_t t_rx_end[N_TX_MAX];
// extern volatile rtimer_clock_t t_tx_end[N_TX_MAX];
// #endif

extern volatile rtimer_clock_t tau1;
extern volatile rtimer_clock_t tau1_Sum;
extern volatile rtimer_clock_t tau1_Cnt;

extern rtimer_clock_t clkDrift;
extern int t_ref_diff;
extern rtimer_clock_t t_ref_delta_ws;
extern uint8_t glossy_flood_success;

// extern rtimer_clock_t syncOffset_Sum;
// extern rtimer_clock_t syncOffset;
// extern uint16_t syncOffset_Cnt;

extern rtimer_clock_t weightedSum;

extern int pps_clkDrift_correction;

extern uint8_t  t_ref_old_cnt;
