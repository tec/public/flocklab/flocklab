#include "glossy.h"
#include "math.h"

#include "glossy-sync.h"


#define DEBUG_GLOSSY 0

// minimum and maximum number of slots after which the timeout expires, since the last transmission
// NOTE: values below 2 do not make sense, as there would be no chance to receive a packet in between
#define SLOT_TIMEOUT_MIN 2
#define SLOT_TIMEOUT_MAX 2
// number of extra ticks required by the timeout callback before starting the transmission
// (to keep synchronous transmissions and time synchronization as much accurate as possible)

#define TIMEOUT_EXTRA_TICKS 70

#define CLK_DRIFT_MEAS_CNT 120
#define CLK_MEAN_CNT 22

// maximum tolerance to accept measurements of T_slot when comparing to the theoretical value
// (value in clock ticks)
#define T_SLOT_TOLERANCE 20

#define DEBUG(l, t, ...)    if (DEBUG_GLOSSY >= l) { DEBUG_PRINT_MSG(t, "Glossy", __VA_ARGS__); }
#define LEDS_ON(l, ...)     if (DEBUG_GLOSSY >= l) { leds_on(__VA_ARGS__); }
#define LEDS_OFF(l, ...)    if (DEBUG_GLOSSY >= l) { leds_off(__VA_ARGS__); }
#define LEDS_TOGGLE(l, ...) if (DEBUG_GLOSSY >= l) { leds_toggle(__VA_ARGS__); }

typedef struct {
	rtimer_clock_t t_ref, t_tx_stop, t_rx_start, t_rx_stop, t_tx_start;
	rtimer_clock_t T_slot_sum;
	rtimer_clock_t T_slot_estimated;
	rtimer_clock_t t_timeout;
	uint8_t n_T_slot;
	glossy_header_t header;
	uint8_t *payload;
	uint8_t payload_len;
	uint8_t active;
	uint8_t n_rx;
	uint8_t n_tx;
	uint8_t relay_cnt_last_rx, relay_cnt_last_tx, relay_cnt_first_rx, relay_cnt_t_ref;
	uint8_t relay_cnt_timeout;
	uint8_t t_ref_updated;
	uint8_t header_ok;
} glossy_state_t;
static glossy_state_t g;

// glossy RX and TX timestamps
volatile rtimer_clock_t t_rx_start[N_TX_MAX] = {};
volatile rtimer_clock_t t_tx_start[N_TX_MAX] = {};
// #if DEBUG_VARS ==1
// volatile rtimer_clock_t t_rx_end[N_TX_MAX] = {};
// volatile rtimer_clock_t t_tx_end[N_TX_MAX] = {};
// #endif
// tau1 correction
volatile rtimer_clock_t tau1 = 0;
volatile rtimer_clock_t tau1_Sum = 0;
volatile rtimer_clock_t tau1_Cnt = 0;
// T_slot_debug = c * T_slot_sum, used for rounding correction
volatile rtimer_clock_t T_slot_debug;
// T_slot_floor = c * T_slot_sum / n, downwards rounded T_slot value
volatile rtimer_clock_t T_slot_floor;
// frequency (clkTicks between t_ref's) calculations
uint8_t clkDrift_cnt;
rtimer_clock_t clkDrift = RTIMER_SECOND;
rtimer_clock_t clkDrift_Sum = 0;
// t_ref values for frequency calculations
// array for sliding window
volatile rtimer_clock_t t_ref_old_arr[CLK_MEAN_CNT] = {};
uint8_t  t_ref_old_cnt;
uint8_t  history_fill_time = 0;
volatile rtimer_clock_t t_ref_old_ws = 0;
volatile rtimer_clock_t t_ref_old_cd = 0;
rtimer_clock_t t_ref_delta = 0;
rtimer_clock_t t_ref_delta_ws = 0;
uint8_t zero_cnt = 0;
// difference between t_ref and previous t_ref + 1sec
int t_ref_diff = 0;
// weigthed sum
rtimer_clock_t weightedSum = 0;
// round initiation offset on root 
rtimer_clock_t syncOffset_Sum = 0;
rtimer_clock_t syncOffset = 0;
uint16_t syncOffset_Cnt;
// successful flood => t_ref based on new timing values
uint8_t glossy_flood_success = 1;
int8_t rssi_start;
int16_t packet_rssi_sum;

/************************* Glossy helper functions **************************/

static inline uint8_t process_glossy_header(uint8_t *pkt, uint8_t pkt_len, uint8_t crc_ok) {
	// extract the Glossy header from the packet
	glossy_header_t *rcvd_header = (glossy_header_t *)pkt;

	if (!g.header_ok) {
		// we have not checked the header yet, so check it now

		if (GET_COMMON_HEADER(rcvd_header->pkt_type) != GLOSSY_COMMON_HEADER) {
			// keep processing only if the common header is correct
			return FAIL;
		}

		if ((GET_SYNC(g.header.pkt_type) != GLOSSY_UNKNOWN_SYNC) &&
				(GET_SYNC(g.header.pkt_type) != GET_SYNC(rcvd_header->pkt_type))) {
			// keep processing only if the local sync value is either unknown or it matches the received one
			return FAIL;
		}

		if ((GET_N_TX_MAX(g.header.pkt_type) != GLOSSY_UNKNOWN_N_TX_MAX) &&
				GET_SYNC(g.header.pkt_type) != GET_SYNC(rcvd_header->pkt_type)) {
			// keep processing only if the local n_tx_max value is either unknown or it matches the received one
			return FAIL;			
		}

		if ((g.header.initiator_id != GLOSSY_UNKNOWN_INITIATOR) &&
				(g.header.initiator_id != rcvd_header->initiator_id)) {
			// keep processing only if the local initiator_id value is either unknown or it matches the received one
			return FAIL;
		}

		if ((g.payload_len != GLOSSY_UNKNOWN_PAYLOAD_LEN) &&
				(g.payload_len != pkt_len - GLOSSY_HEADER_LEN(g.header.pkt_type))) {
			// keep processing only if the local payload_len value is either unknown or it matches the received one
			return FAIL;
		}

		// the header is ok
		g.header_ok = 1;
	}

	if (crc_ok) {
		// we have received the entire packet (and the CRC was ok)

		// store the received header (all the unknown values are also learned)
		g.header = *rcvd_header;
		// store the payload_len
		g.payload_len = pkt_len - GLOSSY_HEADER_LEN(g.header.pkt_type);
		// store the header_len
		rf1a_set_header_len_rx(GLOSSY_HEADER_LEN(g.header.pkt_type));
	}

	return SUCCESS;
}

static inline rtimer_clock_t estimate_T_slot(uint8_t pkt_len) {
	rtimer_clock_t T_tx_estim = T_TX_BYTE * (pkt_len + 3) + T_TX_OFFSET;
	return NS_TO_RTIMER_TICKS(T_tx_estim + T2R - TAU1);
}

static inline char timeout_expired(rtimer_t *rt) {
	if (!rf1a_is_busy()) {
		// we are not receiving anything: retransmit the packet
		rf1a_start_tx();
		g.header.relay_cnt = g.relay_cnt_timeout;
		rf1a_write_to_tx_fifo((uint8_t *)&g.header, GLOSSY_HEADER_LEN(g.header.pkt_type),
				(uint8_t *)g.payload, g.payload_len);
		g.t_timeout = rt->time;
	} else {
		// we are receiving a packet: postpone the timeout by one slot
		g.relay_cnt_timeout++;
		rtimer_schedule(3, rt->time + g.T_slot_estimated, 0, timeout_expired);
	}
	return 0;
}

static inline void schedule_timeout(void) {
	// number of slots after which the timeout will expire:
	// random number between SLOT_TIMEOUT_MIN and SLOT_TIMEOUT_MAX
	uint8_t slot_timeout = SLOT_TIMEOUT_MIN + (random_rand() % (SLOT_TIMEOUT_MAX - SLOT_TIMEOUT_MIN + 1));
	if (WITH_RELAY_CNT()) {
		// if the relay counter is sent, increment it by the chosen number of slots
		g.relay_cnt_timeout = g.header.relay_cnt + slot_timeout;
	}
	rtimer_schedule(3, g.t_timeout + slot_timeout * g.T_slot_estimated, 0, timeout_expired);
}

static inline void update_t_ref(rtimer_clock_t t_ref, uint8_t relay_cnt) {
	g.t_ref = t_ref;
	g.t_ref_updated = 1;
	g.relay_cnt_t_ref = relay_cnt;
}

static inline void add_T_slot_measurement(rtimer_clock_t T_slot_measured) {
	if ((T_slot_measured > g.T_slot_estimated - T_SLOT_TOLERANCE) &&
			(T_slot_measured < g.T_slot_estimated + T_SLOT_TOLERANCE)) {
		g.T_slot_sum += T_slot_measured;
		g.n_T_slot++;
	}
}

/***************************** Glossy interface *****************************/

void glossy_start(uint16_t initiator_id, uint8_t *payload, uint8_t payload_len,
		uint8_t n_tx_max, glossy_sync_t sync, glossy_rf_cal_t rf_cal) {

 	PIN_SET(1, 0);

	rtimer_clock_t now = rtimer_now();
	DEBUG(1, &now, "Started: in=%u, pl=%u, n=%u, s=%u",
			initiator_id, payload_len, n_tx_max, sync);

	// TODO: disable undesired interrupts

	g.active = 1;
	g.payload = payload;
	g.payload_len = payload_len;
	g.n_rx = 0;
	g.n_tx = 0;
	g.relay_cnt_last_rx = 0;
	g.relay_cnt_last_tx = 0;
	g.t_ref_updated = 0;
	g.T_slot_sum = 0;
	g.n_T_slot = 0;
	glossy_flood_success = 1;
	packet_rssi_sum = 0;
	
	// prepare the Glossy header, with the information known so far
	g.header.initiator_id = initiator_id;
	SET_PKT_TYPE(g.header.pkt_type, sync, n_tx_max);
	g.header.relay_cnt = 0;

	// automatically switch to TX at the end of RX
	rf1a_set_rxoff_mode(RF1A_OFF_MODE_TX);
	// automatically switch to RX at the end of TX
	rf1a_set_txoff_mode(RF1A_OFF_MODE_RX);
	// do not calibrate automatically
	rf1a_set_calibration_mode(RF1A_CALIBRATION_MODE_MANUAL);

	if (rf_cal == GLOSSY_WITH_RF_CAL) {
		// if instructed so, perform a manual calibration
		rf1a_manual_calibration();
	}

	rf1a_set_header_len_rx(GLOSSY_HEADER_LEN(g.header.pkt_type));

	rf1a_go_to_idle();
	
	
	if (IS_INITIATOR()) {
		// Glossy initiator
		if (GET_SYNC(g.header.pkt_type) == GLOSSY_UNKNOWN_SYNC) {
			// the initiator must know whether there will be synchronization or not!
			glossy_stop();
		} else {
			// start the first transmission
			g.t_timeout = rtimer_now() + TIMEOUT_EXTRA_TICKS;
			rf1a_start_tx();
			rf1a_write_to_tx_fifo((uint8_t *)&g.header, GLOSSY_HEADER_LEN(g.header.pkt_type),
					(uint8_t *)g.payload, g.payload_len);
			g.relay_cnt_timeout = 0;
		}
	} else {
		// Glossy receiver
		rf1a_start_rx();
		__delay_cycles(MCLK_SPEED / 2000); // wait 0.5 ms
		rssi_start = rf1a_get_rssi();
	}
}

uint8_t glossy_stop(void) {

 	PIN_CLEAR(1, 0);
	
	if (g.active) {
		// stop the timeout
		rtimer_stop(3);
		// flush both RX FIFO and TX FIFO and go to sleep
		rf1a_flush_rx_fifo();
		rf1a_flush_tx_fifo();
		rf1a_go_to_sleep();
// 		PIN_CLEAR(3, 6);
// 		PIN_CLEAR(3, 7);
		g.active = 0;
// 		PIN_TOGGLE(1,1);
		// tau1 calculation
		if (!IS_INITIATOR()){
		  uint8_t i;
		  uint8_t calcCnt = 0;
		  // calculate 2*tau1 using timestamps
		  for(i = 1; i <= 2*N_TX_MAX-2; i++){
		    if (i%2 == 1){  
		      // RX -> TX
		      tau1 = t_rx_start[calcCnt + 1] - 2*t_tx_start[calcCnt] + t_rx_start[calcCnt];
		    } else {
		      // TX -> RX
		      tau1 = 2*t_rx_start[calcCnt + 1] - t_tx_start[calcCnt + 1] - t_tx_start[calcCnt];
		      calcCnt += 1;
		    }
		    if ((tau1 > 2*NS_TO_RTIMER_TICKS(TAU1) - 20) && (tau1 < 2*NS_TO_RTIMER_TICKS(TAU1) + 20)){
		      tau1_Sum += tau1;
		      tau1_Cnt += 1;
		      }
		    }
		    if (tau1_Cnt != 0){
		      tau1 = tau1_Sum/(2*tau1_Cnt);
		    } else {
		     tau1 = NS_TO_RTIMER_TICKS(TAU1); 
		    }
		  
		 // rounding tau1
		  if (tau1_Sum - 2*tau1_Cnt*(tau1) >= tau1_Cnt){
		    tau1 += 1; 
		  }
		}
		
		
// 		PIN_TOGGLE(1,1);
		if (g.t_ref_updated) {
			if (g.n_T_slot > 0) {
				uint8_t i=0;
				T_slot_floor = (g.relay_cnt_t_ref * g.T_slot_sum) / g.n_T_slot;				
				// calculate reference time
				g.t_ref -= T_slot_floor;
				
				// adjust the reference time with the tau1 offset.
				if (!IS_INITIATOR()){
				 g.t_ref -= (int)(tau1 - NS_TO_RTIMER_TICKS(TAU1)); 
				}
				
				T_slot_debug = (g.relay_cnt_t_ref * g.T_slot_sum); 
				// check if relay*sum/n was rounded downwards instead of upwards, and if so decrease T_ref by one tick
				if (!IS_INITIATOR() && T_slot_debug - g.n_T_slot * T_slot_floor >= (g.n_T_slot/2 + g.n_T_slot%2)){
				    g.t_ref -= 1;  
				}
								
				rtimer_clock_t now = rtimer_now();
				zero_cnt = 0;
				//uint8_t i;
				if(t_ref_old_cnt == CLK_MEAN_CNT){
					history_fill_time = 0;
				  clkDrift_Sum = 0;
				  for (i = 0; i < CLK_MEAN_CNT ; i++){
					  if (t_ref_old_arr[i] != 0){
				   	clkDrift_Sum += t_ref_old_arr[i]; 
					  } else {
						  zero_cnt += 1;
					  }
				  }
					if (zero_cnt != CLK_MEAN_CNT){
						clkDrift = clkDrift_Sum/(CLK_MEAN_CNT - zero_cnt);
					}
					if ((clkDrift_Sum - CLK_MEAN_CNT*clkDrift) >= (CLK_MEAN_CNT/2 + CLK_MEAN_CNT%2)){
				      clkDrift += 1;
				   }
				}
				else
					history_fill_time++;
				// check if the difference between two consecutive Trefs lies in a feasbile range around the ACLK-frequency 
				// otherwise if a flood completely fails, t_ref_delta will be >= 2sec
				t_ref_delta = g.t_ref - t_ref_old_cd;
				if (t_ref_delta > RTIMER_SECOND - DELTA_GUARD && t_ref_delta < RTIMER_SECOND + DELTA_GUARD){
					for (i = CLK_MEAN_CNT-1; i > 0; i--){
						t_ref_old_arr[i] = t_ref_old_arr[i-1];
					}
					t_ref_old_arr[0] = t_ref_delta;
					if (t_ref_old_cnt < CLK_MEAN_CNT)
						t_ref_old_cnt++;
				}
				t_ref_old_cd = g.t_ref;
				if((g.t_ref - t_ref_old_ws < 1.5 * RTIMER_SECOND) & !IS_INITIATOR()){
				  // calculate the difference between the calculated reference value and the one if exactly one second would have passed since the last reference
					// t_ref_i:   g.t_ref
					// t_ref_i-1: t_ref_old_ws + clkDrift
					t_ref_diff = g.t_ref - t_ref_old_ws - clkDrift;
#ifdef GLOSSY_SYNC_WEIGHTED_SUM_FIXED_ALPHA
#warning **** using constant smoothing factor for jitter reduction ***
					// weightedSum = g.t_ref + (9 * (t_ref_old_ws + clkDrift - g.t_ref)) / 10;
					// weightedSum = g.t_ref - (9 * t_ref_diff) / 10;
					weightedSum = g.t_ref - ( (t_ref_diff > 0)?((9 * t_ref_diff + 5) / 10):((9 * t_ref_diff - 5) / 10) );
					g.t_ref = (rtimer_clock_t)weightedSum;
#else
				  t_ref_diff = abs(t_ref_diff);
				  if (t_ref_diff != 0){
				    // calculate a weighted sum, which demagnifies the variance of the glossy accuracy
				    weightedSum = (g.t_ref + t_ref_diff*(t_ref_old_ws + clkDrift)-(t_ref_old_ws + clkDrift))/t_ref_diff;
// 				    if (t_ref_diff > 1 && ((g.t_ref + t_ref_diff*(t_ref_old_ws + clkDrift)-(t_ref_old_ws + clkDrift)) - t_ref_diff*weightedSum) >= t_ref_diff/2 + t_ref_diff%2){
// 				      weightedSum += 1;
// 				    }
				    g.t_ref = (rtimer_clock_t)weightedSum;
				  }
#endif
				}
				
				
				// the time of the glossy flood start after a gps pulse is more or less constant, max +/-4 ticks, in a few cases it might happen that the flood starts later than the usual time
				// therefore the root keeps track of the average time it takes between a gps pulse and a glossy start, and if a new start is far off it can adjust its reference time with the offset, because
				// the receiver nodes will adjust their time as well due to the weighted sum
// 				if (IS_INITIATOR()){
// 				  syncOffset = g.t_ref-actVal; 
// 				  if (now > 2*RTIMER_SECOND && now < 60*RTIMER_SECOND){
// 				    syncOffset_Sum += (g.t_ref - actVal);
// 				    syncOffset_Cnt += 1;
// 				  } else {
// 				   if(abs((int)syncOffset - syncOffset_Sum/syncOffset_Cnt) > 5){
// 				      g.t_ref -= (int)syncOffset - syncOffset_Sum/syncOffset_Cnt;  				      
// 				   }
// 				  }
// 				}
				
			} else {
				g.t_ref -= g.relay_cnt_t_ref * g.T_slot_estimated;
			}
			
		} else {
#if 0
		  // if it was not possible to calculate an initial T_ref time, just take the the on from the last round and add the locally calculated second
		  // under the assumption that the clock is not drifting much in a short time, this will be relativily stable and accurate
			// NOTE: This is dangerous if the old reference time is wrong, e.g. if the GPS reciever had a hick-up.
		  if (clkDrift != 0){
		    update_t_ref(t_ref_old_ws + clkDrift, 0);
		  }
#endif
			t_ref_old_cnt = 0; // reset history
			history_fill_time++;
		  glossy_flood_success = 0;
			if (history_fill_time > 2 * CLK_MEAN_CNT) { // reset clock speed estimate, just in case we were really off..
				clkDrift = RTIMER_SECOND;
				history_fill_time = 0;
			}
		}
		rtimer_clock_t now = rtimer_now();
		t_ref_delta_ws =  g.t_ref - t_ref_old_ws;
		t_ref_old_ws = g.t_ref;
		
		// TODO: re-enable undesired interrupts
	}
	return g.n_rx;
}

// current timer count at gps pulse occurence
uint8_t glossy_is_active(void) {
	return g.active;
}

uint8_t glossy_get_n_rx(void) {
	return g.n_rx;
}

uint8_t glossy_get_n_tx(void) {
	return g.n_tx;
}

uint8_t glossy_get_payload_len(void) {
	return g.payload_len;
}

uint8_t glossy_is_t_ref_updated(void) {
	return g.t_ref_updated;
}

rtimer_clock_t glossy_get_t_ref(void) {
	return g.t_ref;
}

uint8_t glossy_get_relay_cnt_first_rx(void) {
	return g.relay_cnt_first_rx;
}

rtimer_clock_t glossy_get_T_slot(void) {
	return T_slot_floor;
}

uint8_t glossy_get_n_T_slot(void) {
	return g.n_T_slot; 
}

int8_t glossy_get_packet_rssi(void) {
	return packet_rssi_sum / g.n_rx;
}
int8_t glossy_get_noise_rssi(void) {
	return rssi_start;
}

/*********************** RF1A callback implementation ***********************/

void rf1a_cb_rx_started(rtimer_clock_t *timestamp) {
// 	PIN_SET(3, 6);
	// notify about the beginning of the reception
	DEBUG(2, timestamp, "RX started");

	RTIMER_DISABLE_OVERFLOW_INTERRUPTS();

	g.t_rx_start = *timestamp;
	g.header_ok = 0;
	
	if (g.n_rx < N_TX_MAX)
		t_rx_start[g.n_rx] = *timestamp;
	
	if (IS_INITIATOR()) {
		// we are the initiator and we have started a packet reception: stop the timeout
		rtimer_stop(3);
	}
}

void rf1a_cb_tx_started(rtimer_clock_t *timestamp) {
// 	PIN_SET(3, 7);
	// notify about the beginning of the transmission
	DEBUG(2, timestamp, "TX started");

	g.t_tx_start = *timestamp;
	
	if (g.n_tx < N_TX_MAX)
		t_tx_start[g.n_tx] = *timestamp;
	
	if (g.n_tx == 0) {
		// first transmission: estimate the slot length based on the packet length
		g.T_slot_estimated = estimate_T_slot(GLOSSY_HEADER_LEN(g.header.pkt_type) + g.payload_len);

	}
}

void rf1a_cb_header_received(rtimer_clock_t *timestamp, uint8_t *header, uint8_t packet_len) {
	if (process_glossy_header(header, packet_len, 0) != SUCCESS) {
		// the header is not ok: interrupt the reception and start a new attempt
		rf1a_cb_rx_failed(timestamp);
	}
}

void rf1a_cb_rx_ended(rtimer_clock_t *timestamp, uint8_t *pkt, uint8_t pkt_len) {
// 	PIN_CLEAR(3, 6);

	RTIMER_ENABLE_OVERFLOW_INTERRUPTS();
	g.t_rx_stop = *timestamp;

	if ((process_glossy_header(pkt, pkt_len, 1) == SUCCESS)) {
		// we received a correct packet, and the header has been stored into g.header
		uint8_t *payload = pkt + GLOSSY_HEADER_LEN(g.header.pkt_type);

		if (WITH_RELAY_CNT()) {
			// the relay counter is part of the header
			if (g.n_rx == 0) {
				// store the relay counter corresponding to the first reception
				g.relay_cnt_first_rx = g.header.relay_cnt;
			}
			// increment the relay counter
			g.header.relay_cnt++;
		}

		if ((GET_N_TX_MAX(g.header.pkt_type) == 0) || (g.n_tx < GET_N_TX_MAX(g.header.pkt_type))) {
			// if n_tx_max is either unknown or not yet reached, transmit the packet
			//BALZ: comment following line to stop receivers from transmiting received package			
			rf1a_write_to_tx_fifo((uint8_t *)&g.header, GLOSSY_HEADER_LEN(g.header.pkt_type), payload, g.payload_len);
		} else {
			// otherwise, stop Glossy
			glossy_stop();
		}
		
// 		  t_rx_end[g.n_rx] = *timestamp;
		
		// increment the reception counter
		g.n_rx++;
		packet_rssi_sum += rf1a_get_last_packet_rssi();

		if ((!IS_INITIATOR()) && (g.n_rx == 1)) {
			// we are a receiver and this was our first packet reception:
			// store the payload for the application
			memcpy((uint8_t *)g.payload, payload, g.payload_len);
		}

		if (WITH_SYNC()) {
			// store the relay counter of this last reception
			g.relay_cnt_last_rx = g.header.relay_cnt - 1;

			if (g.t_ref_updated == 0) {
				// t_ref has not been updated yet: update it
				update_t_ref(g.t_rx_start - NS_TO_RTIMER_TICKS(TAU1), g.header.relay_cnt - 1);
			}

			if ((g.relay_cnt_last_rx == g.relay_cnt_last_tx + 1) && (g.n_tx > 0)) {
				// this reception immediately followed a transmission: measure T_slot
				add_T_slot_measurement(g.t_rx_start - g.t_tx_start - NS_TO_RTIMER_TICKS(TAU1));
			}
		}

		// notify about the successful reception
		DEBUG(2, timestamp, "RX completed. Received a %u-byte packet with initiator %u.",
				pkt_len, g.header.initiator_id);
	} else {
		// some fields in the header were not correct: discard it
		rf1a_cb_rx_failed(timestamp);
	}
}

void rf1a_cb_tx_ended(rtimer_clock_t *timestamp) {
// 	PIN_CLEAR(3, 7);
	// notify about the successful transmission
	DEBUG(2, timestamp, "TX completed");

	g.t_tx_stop = *timestamp;
	

// 	t_tx_end[g.n_tx] = *timestamp;
	
	
	if (WITH_SYNC()) {
		// store the relay counter of this last transmission
		g.relay_cnt_last_tx = g.header.relay_cnt;

		if (g.t_ref_updated == 0) {
			// t_ref has not been updated yet: update it
			update_t_ref(g.t_tx_start, g.header.relay_cnt);
		}

		if ((g.relay_cnt_last_tx == g.relay_cnt_last_rx + 1) && (g.n_rx > 0)) {
			// this transmission immediately followed a reception: measure T_slot
			add_T_slot_measurement(g.t_tx_start - g.t_rx_start + NS_TO_RTIMER_TICKS(TAU1));
		}
	}
	
	// increment the transmission counter
	g.n_tx++;

	if ((g.n_tx == GET_N_TX_MAX(g.header.pkt_type)) &&
			(GET_N_TX_MAX(g.header.pkt_type) > 1 || (!IS_INITIATOR()))) {
		// we have reached N_tx_max and either N_tx_max > 1 or we are a receiver: stop Glossy
		glossy_stop();
	} else {
		if ((IS_INITIATOR()) && (g.n_rx == 0)) {
			// we are the initiator and we still have not received any packet: schedule the timeout
			schedule_timeout();
		}
	}
}

void rf1a_cb_rx_failed(rtimer_clock_t *timestamp) {
// 	PIN_CLEAR(3, 6);
	// notify about the failure, flush the RX FIFO and start a new reception attempt
	DEBUG(2, timestamp, "RX completed. Received a corrupted packet");

	RTIMER_ENABLE_OVERFLOW_INTERRUPTS();
	rf1a_flush_rx_fifo();
	rf1a_start_rx();
// 	PIN_TOGGLE(1, 2);
}

void rf1a_cb_rx_tx_error(rtimer_clock_t *timestamp) {
// 	PIN_CLEAR(3, 6);
// 	PIN_CLEAR(3, 7);
	// notify about the error
	DEBUG(2, timestamp, "RX/TX error (interference?)");

	RTIMER_ENABLE_OVERFLOW_INTERRUPTS();

	if (g.active) {
		// if Glossy is still active, flush both RX FIFO and TX FIFO and start a new reception attempt
		rf1a_flush_rx_fifo();
		rf1a_flush_tx_fifo();
		rf1a_start_rx();
// 		PIN_TOGGLE(1, 2);
	}
}

