#include "leds.h"

// leds in the EM430F5137RF900 (evaluation board for the CC430F5137)
// green led: P1.0
// red   led: P3.6

void leds_init(void) {
	// green led (P1.0)
	P1OUT &= ~BIT0;
	P1DIR |= BIT0;
	// red led (P3.6)
	P3OUT &= ~BIT6;
	P3DIR |= BIT6;
}	

void leds_on(uint16_t leds) {
	if (leds & LEDS_GREEN) {
		P1OUT |= BIT0;
	}
	if (leds & LEDS_RED) {
		P3OUT |= BIT6;
	}
}

void leds_off(uint16_t leds) {
	if (leds & LEDS_GREEN) {
		P1OUT &= ~BIT0;
	}
	if (leds & LEDS_RED) {
		P3OUT &= ~BIT6;
	}
}

void leds_toggle(uint16_t leds) {
	if (leds & LEDS_GREEN) {
		P1OUT ^= BIT0;
	}
	if (leds & LEDS_RED) {
		P3OUT ^= BIT6;
	}
}

void leds_set(uint16_t leds) {
	if (leds & LEDS_GREEN) {
		P1OUT |= BIT0;
	} else {
		P1OUT &= ~BIT0;
	}
	if (leds & LEDS_RED) {
		P3OUT |= BIT6;
	} else {
		P3OUT &= ~BIT6;
	}
}
