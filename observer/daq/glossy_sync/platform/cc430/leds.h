#ifndef __LEDS_H__
#define __LEDS_H__

#include "contiki-conf.h"

// leds in the EM430F5137RF900 (evaluation board for the CC430F5137)
// green led: P1.0
// red   led: P3.6

#define LEDS_GREEN 1
#define LEDS_RED   2
#define LEDS_ALL  (LEDS_GREEN + LEDS_RED)

void leds_init(void);
void leds_on(uint16_t leds);
void leds_off(uint16_t leds);
void leds_toggle(uint16_t leds);
void leds_set(uint16_t leds);

#endif /* __LEDS_H__ */
