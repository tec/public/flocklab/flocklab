#ifndef __PORTS_H__
#define __PORTS_H__

#include "contiki-conf.h"

#define PIN_TOGGLE(port, pin)        P##port##OUT ^=  BIT##pin
#define PIN_SET(port, pin)           P##port##OUT |=  BIT##pin
#define PIN_CLEAR(port, pin)         P##port##OUT &= ~BIT##pin
#define PIN_SELECT(port, pin)        P##port##SEL |=  BIT##pin
#define PIN_UNSELECT(port, pin)      P##port##SEL &= ~BIT##pin
#define PIN_SET_AS_OUTPUT(port, pin) P##port##DIR |=  BIT##pin
#define PIN_SET_AS_INPUT(port, pin)  P##port##DIR &= ~BIT##pin
#define PIN_MAP(port, pin, map)      P##port##MAP##pin = map

#define PIN_MAP_AS_OUTPUT(port, pin, map) do {\
	/* disable interrupts */ \
	__dint(); __nop(); \
	/* get write-access to the port mapping control registers (see 9.2.1) */ \
	PMAPKEYID = 0x02D52; \
	/* allow reconfiguration of port mapping */ \
	PMAPCTL |= PMAPRECFG; \
	PIN_SELECT(port, pin); \
	PIN_SET_AS_OUTPUT(port, pin); \
	PIN_MAP(port, pin, map); \
	/* lock write-access to the port mapping control registers (see 9.2.1) */ \
	PMAPKEYID = 0; \
	/* enable interrupts */ \
	__eint(); __nop(); \
} while (0)

#define PIN_MAP_AS_INPUT(port, pin, map) do {\
	/* disable interrupts */ \
	__dint(); __nop(); \
	/* get write-access to the port mapping control registers (see 9.2.1) */ \
	PMAPKEYID = 0x02D52; \
	/* allow reconfiguration of port mapping */ \
	PMAPCTL |= PMAPRECFG; \
	PIN_SELECT(port, pin); \
	PIN_SET_AS_INPUT(port, pin); \
	PIN_MAP(port, pin, map); \
	/* lock write-access to the port mapping control registers (see 9.2.1) */ \
	PMAPKEYID = 0; \
	/* enable interrupts */ \
	__eint(); __nop(); \
} while (0)

// NOTE: the following port macros work only for byte-wise ports (i.e., ports P1 to P5)
#define PORT_TOGGLE(port)            P##port##OUT ^= 0xff
#define PORT_SET(port)               P##port##OUT = 0xff
#define PORT_CLEAR(port)             P##port##OUT = 0x00
#define PORT_SELECT(port)            P##port##SEL = 0xff
#define PORT_UNSELECT(port)          P##port##SEL = 0x00
#define PORT_SET_AS_OUTPUT(port)     P##port##DIR = 0xff
#define PORT_SET_AS_INPUT(port)      P##port##DIR = 0x00

#endif /* __PORTS_H__ */
