#include "contiki.h"
#include "glossy-sync.h"


// rtimer structs
static rtimer_t rt[N_RTIMERS];
// SW extension for timer A0
volatile rtimer_clock_t ta0_sw_ext;

#define MAX_TICKS (~((clock_time_t)0) / 2)
static volatile clock_time_t count = 0;


#define DEBUG(l, t, ...) DEBUG_PRINT_MSG(t, "GT", __VA_ARGS__);


static inline uint16_t rtimer_now_16_bit(void) {
	// timer A0 is synchronous with the MCU (they are both sourced by XT2):
	// no need to take a majority vote!
	return TA0R;

//// code for majority vote:
//	uint16_t t1, t2;
//	do {
//		t1 = TA0R;
//		t2 = TA0R;
//	} while (t1 != t2);
//	return t1;
}

static inline void update_rtimer_state(uint16_t timer) {
	// update the state only if the rtimer has not been manually
	// stopped or re-scheduled by the callback function
	if (rt[timer].state == RTIMER_JUST_EXPIRED) {
		if (rt[timer].period > 0) {
			// if it is periodic, schedule the new expiration
			rt[timer].time += rt[timer].period;
			*(&TA0CCR0 + timer) += (uint16_t)(rt[timer].period);
			rt[timer].state = RTIMER_SCHEDULED;
		} else {
			// otherwise, just stop it
			*(&TA0CCTL0 + timer) = 0;
			rt[timer].state = RTIMER_INACTIVE;
		}
	}
}

#define RTIMER_CALLBACK(timer) \
	if ((rtimer_now() >= rt[timer].time) && (rt[timer].state == RTIMER_SCHEDULED)) { \
		/* the timer has expired! */ \
		rt[timer].state = RTIMER_JUST_EXPIRED; \
		/* execute the proper callback function */ \
		rt[timer].func(&rt[timer]); \
		/* update or stop the timer */ \
		update_rtimer_state(timer); \
		if (process_nevents() > 0) { \
			LPM4_EXIT; \
		} \
	}

void rtimer_init(void) {
	// initialize timer A0:
	// ACLK (3.25 MHz), continuous mode, clear TA0R, overflow interrupt
	ta0_sw_ext = 0;
	TA0CTL = TASSEL_1 | MC_2 | TACLR | TAIE;

	// initialize timer A1:
	// SMCLK (32 kHz), continuous mode, clear TA1R
	TA1CTL = TASSEL_2 | MC_2 | TACLR;

	// capture control: falling edge, cap on, synchronous capture
	//TA0CCTL0 = CM_0;
	TA0CCTL1 = CM_2 | CAP | SCS | CCIS_0 | CCIE;
	
	// compare for TA0CCR2
	TA0CCTL2 &= ~CAP;
	TA0CCTL2 = SCS | OUTMOD_1; 
	
	memset(rt, 0, sizeof(rt));
}

void rtimer_schedule(uint16_t timer, rtimer_clock_t start, rtimer_clock_t period, rtimer_callback_t func) {
	if ((timer < N_RTIMERS) && (rt[timer].state != RTIMER_SCHEDULED)) {
		rt[timer].func = func;
		rt[timer].period = period;
		rt[timer].time = start + period;
		rt[timer].state = RTIMER_SCHEDULED;
		*(&TA0CCR0 + timer) = (uint16_t)(start + period);
		*(&TA0CCTL0 + timer) = CCIE | OUTMOD_4;
	}
}

void rtimer_stop(uint16_t timer) {
	if (timer < N_RTIMERS) {
		*(&TA0CCTL0 + timer) = 0;
		rt[timer].state = RTIMER_INACTIVE;
	}
}

rtimer_clock_t rtimer_get_period(uint16_t timer) {
	if ((timer < N_RTIMERS) && (rt[timer].state != RTIMER_INACTIVE)) {
		return rt[timer].period;
	} else {
		return 0;
	}
}

rtimer_clock_t rtimer_get_expiration_time(uint16_t timer) {
	if ((timer < N_RTIMERS) && (rt[timer].state != RTIMER_INACTIVE)) {
		return rt[timer].time;
	} else {
		return 0;
	}
}

uint8_t __atomic_start() {
	uint8_t result = ((READ_SR & GIE) != 0);
	__dint(); __nop();
	asm volatile("" : : : "memory"); /* ensure atomic section effect visibility */
	return result;
}

rtimer_clock_t rtimer_now(void) {
	uint16_t hwdiff;
	uint8_t reenable_interrupts;
	// check whether interrupts must be re-enabled at the end of the function
	// depending on the current value of the GIE bit
	reenable_interrupts = __atomic_start();

	// take a snapshot of both the HW timer and the SW extension
	rtimer_clock_t sw = ta0_sw_ext;
	uint16_t hw = rtimer_now_16_bit();
	if (TA0CTL & TAIFG) {
		// in the meantime there has been an overflow of the HW timer:
		// manually increment the SW extension
		sw++;
		// and take a new snapshot of the HW timer
		hwdiff = rtimer_now_16_bit() - hw;
		hw = hw + hwdiff;
	}
	else {
		hwdiff = 0;
		// make both branches take equal amount of clock cycles for deterministic behaviour
		asm volatile("jmp $+2");
		asm volatile("jmp $+2");
		asm volatile("jmp $+2");
		asm volatile("jmp $+2");
		asm volatile("jmp $+2");
		asm volatile("jmp $+2");
	}
	// shift the SW extension to the left and append the HW timer
	rtimer_clock_t time = ((sw << 16) | hw) - hwdiff;

	if (reenable_interrupts) {
		// re-enable all interrupts
		__eint(); __nop();
	}

	return time;
}


// Timer A0, CCR0 interrupt service routine
ISR(TIMER0_A0, timer0_a0_interrupt) {

	ENERGEST_ON(ENERGEST_TYPE_CPU);

	RTIMER_CALLBACK(0);
	if (process_nevents() > 0) {
		LPM4_EXIT;
	}

	ENERGEST_OFF(ENERGEST_TYPE_CPU);

}

// Timer A0, CCR1-4 interrupt service routine
ISR(TIMER0_A1, timer0_a1_interrupt) {

	ENERGEST_ON(ENERGEST_TYPE_CPU);

	switch (TA0IV) {
	case TA0IV_TA0CCR1:
		// GPS pulse captured
		// take timestamp
		actVal = rtimer_now();
		
		if ((uint16_t)(actVal & 0xFFFF) >= TA0CCR1){
			actVal = ((actVal & ~((rtimer_clock_t)0xFFFF)) | TA0CCR1);
		} else {
			actVal = ((actVal & ~((rtimer_clock_t)0xFFFF)) | TA0CCR1) - 65535;
		}
		// set flag to inform that a gps pulse occured
		gpsFlag = 1;
		
		// reset capture overflow flag and capture flag
		if (TA0CCTL1 & COV){
		  TA0CCTL1 &= ~COV;
		}
		  TA0CCTL1 &= ~CCIFG;
		//RTIMER_CALLBACK(1);
		break;
	case TA0IV_TA0CCR2:
		RTIMER_CALLBACK(2);
		break;
	case TA0IV_TA0CCR3:
		RTIMER_CALLBACK(3);
		break;
#ifndef WITH_RADIO
	case TA0IV_TA0CCR4:
		RTIMER_CALLBACK(4);
		break;
#endif /* WITH_RADIO */
	case TA0IV_TA0IFG:
		// overflow of timer A0: increment its software extension
		ta0_sw_ext++;
		
		//PIN_TOGGLE(1, 1);
		// increment also the etimer count
		count++;
		// check whether there are etimers ready to be served
		overflow_lock = 1;
		if (etimer_pending() && (etimer_next_expiration_time() - count - 1) > MAX_TICKS) {
			etimer_request_poll();
			LPM4_EXIT;
		}
		break;
	}

	ENERGEST_OFF(ENERGEST_TYPE_CPU);

}

// Timer A1, CCR0 interrupt service routine
ISR(TIMER1_A0, timer1_a0_interrupt) {
}

// Timer A1, CCR1-2 interrupt service routine
ISR(TIMER1_A1, timer1_a1_interrupt) {

	ENERGEST_ON(ENERGEST_TYPE_CPU);

	switch (TA1IV) {
	case TA1IV_TA1CCR1:
		break;
	case TA1IV_TA1CCR2:
		break;
	}

	ENERGEST_OFF(ENERGEST_TYPE_CPU);

}

clock_time_t clock_time(void) {
	clock_time_t t1, t2;
	do {
		t1 = count;
		t2 = count;
	} while(t1 != t2);
	return t1;
}
