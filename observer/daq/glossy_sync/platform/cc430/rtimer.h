#ifndef __RTIMER_H__
#define __RTIMER_H__

#include "contiki-conf.h"

// number of usable rtimers
#ifdef WITH_RADIO
#define N_RTIMERS 4
#else
#define N_RTIMERS 5
#endif /* WITH_RADIO */

// number of rtimer ticks corresponding to 1 second
#define RTIMER_SECOND ((rtimer_clock_t)ACLK_SPEED)

typedef enum {
	RTIMER_INACTIVE = 0,
	RTIMER_SCHEDULED = 1,
	RTIMER_JUST_EXPIRED = 2
} rtimer_state_t;

typedef uint64_t rtimer_clock_t;
typedef struct rtimer_t rtimer_t;
// prototype of a rtimer callback function
typedef char (*rtimer_callback_t)(rtimer_t *rt);
// struct to store information about a certain rtimer
typedef struct rtimer_t {
	rtimer_callback_t func; // callback function to execute when the rtimer expires
	rtimer_state_t state;   // internal state of the rtimer
	rtimer_clock_t period;  // if period = 0: one-shot timer; otherwise: timer period in clock ticks
	rtimer_clock_t time;    // if state = RTIMER_SCHEDULED: next expiration time; otherwise: last expiration time
} rtimer_t;

extern volatile uint8_t overflow_lock;
#define RTIMER_ENABLE_OVERFLOW_INTERRUPTS()  (TA0CTL |= TAIE)
#define RTIMER_DISABLE_OVERFLOW_INTERRUPTS() (TA0CTL &= ~TAIE)

// initialize the rtimers
void rtimer_init(void);

// schedule rtimer timer to execute function func with a certain period, starting from start
// if period is 0, then func is executed only once
void rtimer_schedule(uint16_t timer, rtimer_clock_t start, rtimer_clock_t period, rtimer_callback_t func);

// stop rtimer timer
void rtimer_stop(uint16_t timer);

// get the period of rtimer timer
rtimer_clock_t rtimer_get_period(uint16_t timer);

// get the expiration time of rtimer timer
rtimer_clock_t rtimer_get_expiration_time(uint16_t timer);

// get the current rtimer time
rtimer_clock_t rtimer_now(void);


// convert rtimer time values from clock ticks to milliseconds
#define RTIMER_TO_MS(t) ((t) / (RTIMER_SECOND/1000))

// get the current rtimer time (macro used in several Contiki core files)
#define RTIMER_NOW()    (rtimer_now())

// get the current rtimer time in milliseconds
#define RTIMER_NOW_MS() (RTIMER_TO_MS(rtimer_now()))

#define NS_TO_RTIMER_TICKS(ns) (((rtimer_clock_t)(ns) * (rtimer_clock_t)RTIMER_SECOND) / (rtimer_clock_t)1000000000)

#endif /* __RTIMER_H__ */
