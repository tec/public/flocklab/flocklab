#include "contiki.h"

int
putchar(int c)
{
  uart0_writeb((char)c);
  return c;
}
