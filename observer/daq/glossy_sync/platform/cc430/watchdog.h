#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

#include "contiki-conf.h"

void watchdog_stop(void);

#endif /* __WATCHDOG_H__ */
