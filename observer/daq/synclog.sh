#! /bin/sh
# Author: Balz Maag <bmaag@ee.ethz.ch>
# based on skeleton from Debian GNU/Linux
# starts a socat process to log output from glossy sync node

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=synclog
DESC="Synchronization protocol logger"
MOUNT=/media/card

set -e

case "$1" in
  start)
        echo -n "Starting $DESC... "
        if [[ `mount | grep /media/card | wc -l` -eq 1 ]] && [[ `ls /dev/flocklab/usb/daq2 | wc -l` -eq 1 ]]
        then
                stty -F /dev/flocklab/usb/daq2 hupcl
                echo ' ' > /dev/flocklab/usb/daq2 # open and close dev
                sleep 1
                start-stop-daemon --start --background --pidfile /var/run/socat.$NAME.pid --make-pidfile --exec /usr/bin/socat -- -u /dev/flocklab/usb/daq2,b115200,raw,echo=0,crnl,cs8,parenb=0,ignoreeof exec:'logger -t synclog -p local0.info'
        fi
        echo "...Done"
        ;;
  stop)
        echo -n "Stopping $DESC... "
        if [[ `ls /var/run/socat.$NAME.pid | wc -l` -eq 1 ]]
        then
                start-stop-daemon --stop --pidfile /var/run/socat.$NAME.pid
                stty -F /dev/flocklab/usb/daq2 hup
                echo ' ' > /dev/flocklab/usb/daq2 # open and close dev
        fi
        echo "...Done"
        ;;
  *)
        N=/etc/init.d/$NAME
        echo "Usage: $N {start|stop}" >&2
        exit 1
        ;;
esac

exit 0
