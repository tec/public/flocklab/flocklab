#!/bin/sh
#
# Author: Christoph Walser, (walserc@ee.ethz.ch)
#
# Creation date: 2009/01/13
#
# Last changed: 2009/09/17
#
# Purpose: Script to set up flocklab database on the observer and starting related cronjobs.
#

# Constants:
SQLSCRIPT="Setup_DB_Observer_Arch_v1.5.sql"		# Name of the SQL script with the code for setting up the DB.
DBNAME="db_flocklab_observer.db"			# Name of the DB on the observer
DBPATH="/home/root/mmc/flocklab"			# Path to the DB without trailing backslash

echo "********************************************";
echo "Setting up database on observer.";
echo "********************************************";

# Determine whether the database already exists:
if [ -f "$DBPATH/$DBNAME" ]
then
	echo "Database $DBPATH/$DBNAME already exists. If you continue, the database will be deleted and setup from scratch.";
	ANS=""
	while [ "$ANS" != "y" ] && [ "$ANS" != "n" ]
	do
		echo "";
		echo -n "Do you want to continue? (y/n) ";
		read ANS;
	done
	if [[ $ANS = "n" ]]
	then
		echo "Setup cancelled.";
		exit;
	fi
	echo "Delete database $DBPATH/$DBNAME";
	rm "$DBPATH/$DBNAME";
fi

# Create the database and the database path (if it is not yet existing):
echo "Create database $DBPATH/$DBNAME";
mkdir -p $DBPATH;
touch "$DBPATH/$DBNAME";
	
# Run the SQL script to setup the database:
echo "Setting up database...";
sqlite3 $DBPATH/$DBNAME ".read $SQLSCRIPT";

# Check integrity of the database:
echo "Checking the integrity of the database...";
sqlite3 $DBPATH/$DBNAME 'PRAGMA integrity_check;';

# If the user wants to, register a cronjob that periodically compresses the database:
ANS="";
while [ "$ANS" != "y" ] && [ "$ANS" != "n" ]
do
	echo "";
	echo -n "Do you want to register a cronjob that periodically compresses the database? (y/n) ";
	read ANS;
done
if [ "$ANS" = "n" ]
then
	echo "Setup finished. Database $DBPATH/$DBNAME is ready to use";
	echo "********************************************";
	exit;
fi 
if [ "$ANS" = "y" ]
then
	echo "";
	echo -n "Enter the period of compression (as HH:MM, recommended: 00:15): ";
	read ANS;
	# Explode answer to its parts:
	HOURS=`echo "$ANS"|cut -d':' -f1`
	MINUTES=`echo "$ANS"|cut -d':' -f2`
		
	# Register cron job:
	echo "Register cron job..."
	CRONTEXT=""
	if [ "$MINUTES" != "00" ]
	then
		CRONTEXT="*/$MINUTES";
	else
		CRONTEXT="*";
	fi
	if [ "$HOURS" != "00" ]
	then
		CRONTEXT=$CRONTEXT" */$HOURS";
	else
		CRONTEXT=$CRONTEXT" *";
	fi
	CRONTEXT=$CRONTEXT" * * * sqlite3 $DBPATH/$DBNAME 'VACUUM;'>>/var/log/$DBNAME.cronlog 2>/dev/null";
			
	# Append the new job to the existing crontab:
	crontab -l > crontab.temp;
	sed -i '1,3d' crontab.temp;
	echo "$CRONTEXT" >> crontab.temp;
	# register new crontab:
	crontab -r;
	crontab crontab.temp;
	
	# Delete temporary file:
	rm crontab.temp;
		
	# Start cron service if not started yet:
	if [ "`ps -u $USER | grep cron`" = "" ]
	then
		# Start cron service
		echo "Start cron service..."
		/etc/init.d/cron start
	fi
fi 

echo "Setup finished. Database $DBPATH/$DBNAME is ready to use";
echo "********************************************";
exit;
