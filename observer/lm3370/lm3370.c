/**
 * @file
 *
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * *
 * @section LICENSE
 *
 * Copyright (c) 2011 ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA,
 * OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 *
 *
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/i2c.h>
#include <asm/uaccess.h>

#define MODULE_NAME "lm3370"

/* Registers on LM3370: */
#define LM3370_REGISTER_CONTROL  0x00
#define LM3370_REGISTER_BUCK1    0x01
#define LM3370_REGISTER_BUCK2    0x02
#define LM3370_FORCE_PWM         0x20

static int __devinit lm3370_probe(struct i2c_client *client,
		const struct i2c_device_id *id);
static int __devexit lm3370_remove(struct i2c_client *client);
static int lm3370_read(char *page, char **start, off_t offset, int count,
		int *eof, void *data);
static int lm3370_write(struct file *file, const char __user *buffer,
		unsigned long count, void *_data);

struct lm3370_driver_data {
	struct proc_dir_entry *proc_entry;
	struct i2c_client *i2c_clnt;
};

/* Device ID table */
static const struct i2c_device_id lm3370_id[] = {
		{ MODULE_NAME, 0x21 },
		{ }
};
MODULE_DEVICE_TABLE(i2c, lm3370_id);

static struct i2c_driver lm3370_driver_i2c = {
		.driver.name = MODULE_NAME,
		.probe = lm3370_probe,
		.remove = __devexit_p(lm3370_remove),
		.id_table = lm3370_id,
};

/*******************************************************************************************
 *
 * Function is executed when a user reads the proc file
 *
 * @param buf
 * @param start
 * @param offset
 * @param count
 * @param eof
 * @param data
 *
 * @return Number of written bytes to userspace on success, 0 or -EFAULT otherwise.
 *
 *******************************************************************************************/
static int lm3370_read(char *buf, char **start, off_t offset, int count, int *eof, void *data) {
	struct lm3370_driver_data *drv = data;
	int ret, len, force;

	// Get the register from the LM3370:
	ret = i2c_smbus_read_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2);
	// Check for errors.
	if (ret < 0) {
		// error:
		ret = -1;
		len = sprintf(buf, "%d\n", ret);
	} else {
		// No error. Convert the value to a voltage:
		force = ret & LM3370_FORCE_PWM;
		ret = (ret & 0x1F)+17;
		// Write data to the proc file:
		if (force)
			len = sprintf(buf, "%d force\n", ret);
		else
			len = sprintf(buf, "%d auto\n", ret);
	}

	*eof = 1;
	return len;
}

/*******************************************************************************************
 *
 * Function is executed when a user writes the proc file
 *
 * @param file
 * @param buffer
 * @param count
 * @param _data
 *
 * @return number of read bytes from userspace
 *
 *******************************************************************************************/
static int lm3370_write(struct file *file, const char __user *buffer, unsigned long count, void *_data) {
	struct lm3370_driver_data *drv = _data;
	char *lbuf;
	int val, conv_val, ret;

	lbuf = kzalloc(count+1, GFP_KERNEL);
	if (!lbuf)
		return -ENOMEM;
 	count -= copy_from_user(lbuf, buffer, count);
	if (count >= 5 && strncmp("force",lbuf,5) == 0) {
		kfree(lbuf);
		// set force bit
		ret = i2c_smbus_read_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2);
		if (ret < 0) {
			return ret;
		}
		else {
			ret = i2c_smbus_write_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2, ret | LM3370_FORCE_PWM);
		}
		if (ret == 0) {
			printk(KERN_INFO MODULE_NAME ": Set force PWM mode\n");
		} else {
			printk(KERN_INFO MODULE_NAME ": Could not set force PWM mode\n");
		}
	} else if (count >= 4 && strncmp(lbuf,"auto",4) == 0) {
		kfree(lbuf);
		// clear force bit
		ret = i2c_smbus_read_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2);
		if (ret < 0) {
			return ret;
		}
		else {
			ret = i2c_smbus_write_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2, ret & ~LM3370_FORCE_PWM);
		}
		if (ret == 0) {
			printk(KERN_INFO MODULE_NAME ": Set auto PWM mode\n");
		} else {
			printk(KERN_INFO MODULE_NAME ": Could not set auto PWM mode\n");
		}
	}
	else {
		val = simple_strtol(lbuf, NULL, 10);
		kfree(lbuf);
	
		if ((val < 18) || (val > 33)) {
			printk(KERN_WARNING MODULE_NAME ": Error when reading from userspace. Valid values are integers from 18 to 33 (1.8V - 3.3V) but read %d \n", val);
			return -EINVAL;
		} else {
			// Convert the value:
			conv_val = val-17;
			ret = i2c_smbus_read_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2);
			if (ret < 0) {
				return ret;
			}
			else {
				ret = i2c_smbus_write_byte_data(drv->i2c_clnt, LM3370_REGISTER_BUCK2, conv_val | (LM3370_FORCE_PWM & ret));
				if (ret == 0) {
					printk(KERN_INFO MODULE_NAME ": Voltage set to %u.%uV\n", val/10, val%10);
				} else {
					printk(KERN_INFO MODULE_NAME ": Voltage could not be set. Errno: %d\n", ret);
				}
			}
		}
	}
	return count;
}

/*******************************************************************************************
 *
 * lm3370_probe() - probe device
 * @client: I2C client device
 * @id: device ID
 *
 * Called by the I2C core when an entry in the ID table matches a
 * device's name.
 * Returns 0 on success.
 *
 *******************************************************************************************/
static int __devinit lm3370_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	struct lm3370_driver_data *drv;

	// Create driver struct:
	drv = kmalloc(sizeof(struct lm3370_driver_data), GFP_KERNEL);
	if (!drv) {
		printk(KERN_ERR MODULE_NAME ": Could not allocate memory for driver data.\n");
		return -ENOMEM;
	}
	i2c_set_clientdata(client, drv);
	// Register /proc file:
	drv->proc_entry = create_proc_entry(MODULE_NAME, 0, NULL);
	if (drv->proc_entry) {
		drv->proc_entry->data = drv;
		drv->proc_entry->read_proc = lm3370_read;
		drv->proc_entry->write_proc = lm3370_write;
		printk(KERN_INFO MODULE_NAME ": proc entry /proc/"MODULE_NAME" created.\n");
	} else {
		printk(KERN_ERR MODULE_NAME ": Could not create proc entry.\n");
		kfree(drv);
		return -ENODEV;
	}

	drv->i2c_clnt = client;

	dev_info(&client->dev, "initialized\n");

	return 0;
}

/*******************************************************************************************
 *
 * lm3370_remove() - remove device
 * @client: I2C client device
 *
 *******************************************************************************************/
static int __devexit lm3370_remove(struct i2c_client *client) {
	struct lm3370_driver_data *drv = i2c_get_clientdata(client);

	// Free driver data
	kfree(drv);

	// Remove /proc file:
	remove_proc_entry(MODULE_NAME, NULL);
	printk(KERN_INFO MODULE_NAME ": proc entry /proc/"MODULE_NAME" destroyed.\n");

	return 0;
}

/*******************************************************************************************
 *
 * Initialization function for the kernel module
 *
 *******************************************************************************************/
static int __init lm3370_init(void) {
	return i2c_add_driver(&lm3370_driver_i2c);
}
module_init(lm3370_init);

/*******************************************************************************************
 *
 * Destruction function for the kernel module
 *
 *******************************************************************************************/
static void __exit lm3370_exit(void) {
	i2c_del_driver(&lm3370_driver_i2c);
}
module_exit(lm3370_exit);

MODULE_AUTHOR("ETH Zurich 2011, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("LM3370 - Dual Synchronous Step-Down DC-DC Converter with Dynamic Voltage Scaling Function");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1.1");
