#!/bin/sh 
#
# Author: Christoph Walser, <walserc@tik.ee.ethz.ch>
# Date: 2011/10/14
#
# Modified on 2019-01-09 (WLAN0 related stuff removed).

# path is needed for ifdown / ifup
PATH=/usr/bin:/bin:/usr/sbin:/sbin
PINGPATH=/var/tmp/ping_watchdog

if [ $# -lt 2 ]
then
	echo "Provide at least one IP-address and a threshold as parameter."
	echo "Example: ping_watchdog.sh <ip1> <ip2> <threshold>"
	exit 0
fi

INTERFACE='eth0'

touch $PINGPATH
for TIMEOUT in $@; do true; done
let HALFTIME=$TIMEOUT/2
PINGOK=0

for IP in `echo $@ | sed 's/^\(.*\)\s[0-9]\+\s*$/\1/'`
do
    if ping -c 2 $IP >> /dev/null
    then
        PINGOK=1
        break
    fi
    /usr/bin/logger "ping_watchdog: $IP is not reachable"
done

if [ $PINGOK -eq 1 ]
then
	# IP is not offline, thus reset counter in file:
	echo "0" > $PINGPATH
	# Set LED 2 to indicate state:
	/bin/sh /etc/network/if-up.d/gumstix_leds_ifup
else
	# IP is offline. Increment counter. If counter is bigger than threshold, reboot:
	COUNTER=`cat $PINGPATH`
	if [ ! -s $PINGPATH ]
	then
		COUNTER=0
	fi
	
	# If threshold has been reached, reboot the machine:
	if [ $COUNTER -ge $TIMEOUT ]
	then
		/bin/rm -f $PINGPATH 
		/sbin/shutdown -r now "ping_watchdog: rebooting Gumstix after network has been down for $TIMEOUT checks."
	fi
	
	if [ $COUNTER -ge $HALFTIME ]
	then
		/usr/bin/logger "ping_watchdog: ifdown / ifup on eth0"
		/sbin/ifdown $INTERFACE
		/bin/sleep 5
		/sbin/ifup $INTERFACE
	fi
		
	# Increment counter:
	let COUNTER=$COUNTER+1
	echo $COUNTER > $PINGPATH
fi

