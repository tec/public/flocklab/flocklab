/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This kernelmodule provides functionality for the FlockLab v2 @ ETH Zurich,
 * Switzerland. It implements the GPIO monitoring service which allows a
 * user to monitor GPIO pins and optionally take defined actions whenever a
 * pin change is detected.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */




/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>					/* Needed by all modules */
#include <linux/kernel.h>					/* Needed for KERN_INFO */
#include <linux/list.h>						/* List mechanisms */
#include <linux/time.h>						/* Time functions */
#include <linux/interrupt.h>				/* Interrupt handling */
#include <mach/regs-ost.h>					/* Register definitions */
#include <mach/hardware.h>					/* Register functions */
#include <linux/gpio.h>						/* GPIO functions */
#include <linux/ioctl.h>					/* Needed for IOCTL */
#include <linux/fs.h>						/* Needed for device registering */
#include <linux/uaccess.h>					/* Needed for exchanging data between kernel and user space over /dev */
#include <linux/cdev.h>						/* Needed for registering device */
#include <linux/device.h>					/* Needed for registering device */
#include <linux/slab.h>						/* Needed for slab caches */
#include <linux/sched.h>					/* Tasklets */
#include <linux/kfifo.h>
#include <linux/clocksource.h>
#include <asm/io.h>
#include <asm/atomic.h>
#include "flocklab_gpio_setting.h"			/* Needed for callback */
#include "flocklab_powerprof.h"				/* Needed for callback */
#include "flocklab_gpio_monitor.h"
#include "flocklab_gpio_monitor_shared.h"	/* Contains shared definitions of usperspace and kernelspace programs for this service */
#include "ads1271.h"



/* ---- Module documentation and authoring information --------------------*/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("ETH Zurich, 2009-2012, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("GPIO monitor service for FlockLab v2 @ ETH Zurich, Switzerland.");
MODULE_SUPPORTED_DEVICE("FlockBoard rev. 1.1");
MODULE_VERSION("2.3");

#define MODULE_NAME GPIO_MONITOR_KM_NAME


/* ---- Global variables --------------------------------------------------*/
/**
 * Struct to keep all data associated with a specific GPIO monitor task. The struct inherits all members (but the list_head) from the struct monitor_job which is also used in user space.
 */
struct monitor_job_list {
	struct list_head list;									// Implements a double linked list.
	struct monitor_job job;									// Data for the monitoring job.
	unsigned int active;									// Indicates whether the monitor job is active (1) or inactive (0). the IRQ handler is only executed for active jobs.
#ifdef FLOCKLAB_CATCH_LEVEL_FLAG
#warning including experimental gpio level detection
	void __iomem	*gedr;									// pointer to GEDR register for this IO pin
#endif	
	void __iomem	*gplr;									// pointer to GPLR register for this IO pin
	unsigned long gmask;									// responsible bit in this register
	char name[30];
	atomic_t tasklets_avail;
};

struct monitor_job_bh {
	struct tasklet_struct tasklet;
	struct monitor_job_list *entry;
	u32 oscr_timestamp;
	unsigned long gpio_level;
#ifdef FLOCKLAB_CATCH_LEVEL_FLAG
	unsigned long gpio_level2, gedr;
#endif	
};

static struct list_head monitor_list;						// Create list head

// Variables for processed events ringbuffer:
static struct kfifo sample_fifo;
// Error handling:
static int kfifo_full_count = 0;									// Count for fifo full events.
static int irq_disabled_count = 0;								// Count for irq disable events.

// Global variables for list operations:
static int device_list_open = 0;							// Used to prevent opening the device multiple times:
static struct class *dev_list_class = NULL;					// List class need to register device
static dev_t dev_list_nr = 0;								// Device number needed to register device
static struct cdev dev_list_cdev;							// CDev needed to register device
static int dev_list_read;

// Global variables for dbbuf operations:
static int device_dbbuf_open = 0;							// Used to prevent opening the device multiple times:
static struct class *dev_dbbuf_class = NULL;				// List class need to register device
static dev_t dev_dbbuf_nr = 0;								// Device number needed to register device
static struct cdev dev_dbbuf_cdev;							// CDev needed to register device

// Memory related stuff:
static struct kmem_cache *monitor_job_bh_cache;

// Wait queue for processed events ringbuffer reading over /dev/gpiosetting_dbbuf:
static DECLARE_WAIT_QUEUE_HEAD(wq_dbbuf);

// os timer stuff
static unsigned int oscr_mult;
#define OSCR_SHIFT 20

/* ---- Prototypes --------------------------------------------------------*/
int init_module(void);
void cleanup_module(void);
void cleanup(int init_state);
static int list_remove(struct monitor_job *job_delete);
static int list_removeall(void);
static irqreturn_t irq_handler(int irq, void *dev_id);
static void tasklet_handler(unsigned long data);
static int gpio_monitor_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
static int gpio_monitor_dev_list_open(struct inode *inode, struct file *filp);
static ssize_t gpio_monitor_dev_list_read(struct file *filp, char *buffer, size_t length, loff_t *offset);
static int gpio_monitor_dev_list_release(struct inode *inode, struct file *filp);
static int gpio_monitor_dev_dbbuf_open(struct inode *inode, struct file *filp);
static ssize_t gpio_monitor_dev_dbbuf_read(struct file *filp, char *buffer, size_t length, loff_t *offset);
static int gpio_monitor_dev_dbbuf_release(struct inode *inode, struct file *filp);



/* ---- More global variables ---------------------------------------------*/
/**
 * Struct for accessing the /dev/gpiomonitor_list file
 */
static struct file_operations fops_list =
{
	.owner		= THIS_MODULE,
	.ioctl		= gpio_monitor_ioctl,
	.open		= gpio_monitor_dev_list_open,
	.read		= gpio_monitor_dev_list_read,
	.release	= gpio_monitor_dev_list_release
};

/**
 * Struct for accessing the /dev/gpiomonitor_dbbuf file
 */
static struct file_operations fops_dbbuf =
{
	.owner		= THIS_MODULE,
	.open		= gpio_monitor_dev_dbbuf_open,
	.read		= gpio_monitor_dev_dbbuf_read,
	.release	= gpio_monitor_dev_dbbuf_release
};

/* ---- Enums to for init / cleanup ---------------------------------------------*/
enum {
    INIT_GPIO,
    INIT_LIST_CHRDEV,
    INIT_LIST_ADD,
    INIT_LIST_CLASS,
    INIT_DBBUF_CHRDEV,
    INIT_DBBUF_ADD,
    INIT_DBBUF_CLASS,
    INIT_EVENT_QUEUE,
    INIT_SLAB_CACHE,
    INIT_KFIFO,
    INIT_DONE,
};
/**************************************************************************************//**
 *
 * Called to perform module initialization when the module is loaded
 *
 *****************************************************************************************/
int init_module(void)
{
    /* Local variables	*/
    int rs;

    //printk(KERN_INFO "%s: FlockLab service GPIO monitor starting...\n", GPIO_MONITOR_KM_NAME);

    /* Register the device for the list() command of the API */
    // Get a major number
    if (( rs = alloc_chrdev_region( &dev_list_nr, 0, 1, DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST )) < 0 ) {
        printk( KERN_ERR "%s: Unable to allocate major for list device. Error %d occurred.\n", GPIO_MONITOR_KM_NAME, rs );
        cleanup(INIT_GPIO);
        return rs;
    }
    printk(KERN_DEBUG "%s: Allocated device numbers for list device. Major:%d minor:%d\n", GPIO_MONITOR_KM_NAME, MAJOR( dev_list_nr ), MINOR( dev_list_nr ));
    // Register the device. The device becomes "active" as soon as cdev_add is called.
    cdev_init( &dev_list_cdev, &fops_list );
    dev_list_cdev.owner = THIS_MODULE;
    dev_list_cdev.ops = &fops_list;
    if (( rs = cdev_add( &dev_list_cdev, dev_list_nr, 1 )) != 0 ) {
        printk( KERN_ERR "%s: cdev_add for list device failed with error %d.\n", GPIO_MONITOR_KM_NAME, rs );
        cleanup(INIT_LIST_CHRDEV);
        return rs;
    }
    printk(KERN_DEBUG "%s: List device registered.\n", GPIO_MONITOR_KM_NAME);
    // Create a class, so that udev will make the /dev entry automatically:
    dev_list_class = class_create( THIS_MODULE, DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST );
    if ( IS_ERR( dev_list_class )) {
        printk( KERN_ERR "%s: Unable to create class for list device.\n", GPIO_MONITOR_KM_NAME );
        cleanup(INIT_LIST_ADD);
        return -1;
    }
    device_create( dev_list_class, NULL, dev_list_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST );
    printk(KERN_DEBUG "%s: List device node created.\n", GPIO_MONITOR_KM_NAME);

    /* Register the device for writing the processed events queue to the database in userspace */
    // Get a major number
    if (( rs = alloc_chrdev_region( &dev_dbbuf_nr, 0, 1, DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_DBBUF )) < 0 ) {
        printk( KERN_ERR "%s: Unable to allocate major for dbbuf device. Error %d occurred.\n", GPIO_MONITOR_KM_NAME, rs );
        cleanup(INIT_LIST_CLASS);
        return rs;
    }
    printk(KERN_DEBUG "%s: Allocated device numbers for dbbuf device. Major:%d minor:%d\n", GPIO_MONITOR_KM_NAME, MAJOR( dev_dbbuf_nr ), MINOR( dev_dbbuf_nr ));
    // Register the device. The device becomes "active" as soon as cdev_add is called.
    cdev_init( &dev_dbbuf_cdev, &fops_dbbuf );
    dev_dbbuf_cdev.owner = THIS_MODULE;
    dev_dbbuf_cdev.ops = &fops_dbbuf;
    if (( rs = cdev_add( &dev_dbbuf_cdev, dev_dbbuf_nr, 1 )) != 0 ) {
        printk( KERN_ERR "%s: cdev_add for dbbuf device failed with error %d.\n", GPIO_MONITOR_KM_NAME, rs );
        cleanup(INIT_DBBUF_CHRDEV);
        return rs;
    }
    printk(KERN_DEBUG "%s: Dbbuf device registered.\n", GPIO_MONITOR_KM_NAME);
    // Create a class, so that udev will make the /dev entry automatically:
    dev_dbbuf_class = class_create( THIS_MODULE, DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_DBBUF );
    if ( IS_ERR( dev_dbbuf_class )) {
        printk( KERN_ERR "%s: Unable to create class for dbbuf device.\n", GPIO_MONITOR_KM_NAME );
        cleanup(INIT_DBBUF_ADD);
        return -1;
    }
    device_create( dev_dbbuf_class, NULL, dev_dbbuf_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_DBBUF );
    printk(KERN_DEBUG "%s: Dbbuf device node created.\n", GPIO_MONITOR_KM_NAME);

    /* Initialize event queue */
    INIT_LIST_HEAD(&monitor_list);	// Initialize list head
    printk(KERN_DEBUG "%s: Monitor list initialized.\n", GPIO_MONITOR_KM_NAME);

    /* Create slab caches for frequently used structures to speed-up the memory allocation process. */
    monitor_job_bh_cache = kmem_cache_create("monitor_job_bh", sizeof(struct monitor_job_bh), 0, 0, 0);
    if (!monitor_job_bh_cache) {
        // There was not enough memory for creating the slab caches:
        printk( KERN_ERR "%s: Unable to create slab caches.\n", GPIO_MONITOR_KM_NAME );
        cleanup(INIT_EVENT_QUEUE);
        return -ENOMEM;
    }
    printk(KERN_DEBUG "%s: Slab caches created.\n", GPIO_MONITOR_KM_NAME);

    /* Initialize ringbuffer for processed events */
    if (kfifo_alloc(&sample_fifo, SIZEOFBUF*sizeof(struct monitor_job_report), GFP_KERNEL)) {
        printk(KERN_ERR GPIO_MONITOR_KM_NAME ": unable to allocate memory for kfifo.\n");
        cleanup(INIT_SLAB_CACHE);
        return -ENOMEM;
    }
    printk(KERN_DEBUG "%s: Kfifo initialized.\n", GPIO_MONITOR_KM_NAME);

    oscr_mult = clocksource_hz2mult(get_clock_tick_rate(), OSCR_SHIFT);
    //printk(KERN_INFO "%s: FlockLab service GPIO monitor started.\n", GPIO_MONITOR_KM_NAME);

    return 0;
} // init_module



/**************************************************************************************//**
 *
 * IOCTL handler for user space commands (for /dev/gpiomonitor_list)
 *
 * @param inode Inode
 * @param filp File pointer
 * @param cmd Command
 * @param arg Argument(s)
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int gpio_monitor_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Local variables */
	int ret_val;
	struct monitor_job_list *entry;
	void *callback_args_temp;
	unsigned long irq_flags;

	// Extract type and number bitfield. If wrong cmd, return ENOTTY (inappropriate ioctl):
	if (_IOC_TYPE(cmd) != GPIO_MONITOR_IOCTL_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > GPIO_MONITOR_IOCTL_MAXNR) return -ENOTTY;

	// See which cmd it was and act accordingly:
	switch (cmd) {
		case GPIO_MONITOR_IOCTL_ADD:
			// Allocate memory for the monitor_job. Free this memory when list is destroyed or job removed
			entry = (struct monitor_job_list*) kmalloc(sizeof(struct monitor_job_list), GFP_KERNEL);

			// Get the data from the userspace:
			ret_val = copy_from_user(&(entry->job), (unsigned int *) arg, sizeof(struct monitor_job));
			if (ret_val != 0) {
				printk(KERN_ERR "%s: Error getting the arguments.\n", GPIO_MONITOR_KM_NAME);
				kfree(entry);
				return -ENOTTY;
			}

			// Get the arguments of the callback:
			switch (entry->job.callback) {
				case gpio_set_add:
					// The arguments are stored in a struct callback_args_gpio_set_add:
					callback_args_temp = kmalloc(sizeof(struct callback_args_gpio_set_add), GFP_KERNEL);
					ret_val = copy_from_user(callback_args_temp, entry->job.callback_args, sizeof(struct callback_args_gpio_set_add));
					if (ret_val != 0) {
						printk(KERN_ERR "%s: Error getting arguments for gpio_set_add callback: %d\n", GPIO_MONITOR_KM_NAME, ret_val);
						kfree(callback_args_temp);
						kfree(entry);
						return -ENOTTY;
					}
					entry->job.callback_args = callback_args_temp;
					//DEBUG printk(KERN_INFO "%s: ------> Callback args: %i, %i, %lu\n", GPIO_MONITOR_KM_NAME, ((struct callback_args_gpio_set_add *) (entry->job.callback_args))->gpio, ((struct callback_args_gpio_set_add *) (entry->job.callback_args))->level, ((struct callback_args_gpio_set_add *) (entry->job.callback_args))->offset_us);
					break;

				case powerprof_add:
					callback_args_temp = kzalloc(sizeof(struct callback_args_powerprof_add), GFP_KERNEL);
					ret_val = copy_from_user(callback_args_temp, entry->job.callback_args, sizeof(struct callback_args_powerprof_add));
					if (ret_val != 0) {
						printk(KERN_ERR "%s: Error getting arguments for powerprof_add callback: %d\n", GPIO_MONITOR_KM_NAME, ret_val);
						kfree(callback_args_temp);
						kfree(entry);
						return -ENOTTY;
					}
					entry->job.callback_args = callback_args_temp;
					break;

				default:
					entry->job.callback = -1;
					entry->job.callback_args = NULL;
					break;
			}

			// If there is already a pin for the given GPIO and mode registered, delete it:
			list_remove(&(entry->job));

			// Activate the monitoring job:
			entry->active = 1;
			
			// set GEDR and GPIO address
#ifdef FLOCKLAB_CATCH_LEVEL_FLAG
			entry->gedr = (void __iomem *)GPIO_BANK(gpio_to_bank(entry->job.gpio)) + GEDR_OFFSET;
#endif			
			entry->gmask = GPIO_bit(entry->job.gpio);
			entry->gplr = (void __iomem *)GPIO_BANK(gpio_to_bank(entry->job.gpio)) + GPLR_OFFSET;
			atomic_set(&entry->tasklets_avail, MAX_MONITOR_TASKLETS);
			
			// Insert the struct into the list:
			list_add(&entry->list, &monitor_list);

			// Set IRQ flags:
			irq_flags = 0;
			irq_flags |= IRQF_SHARED;			// Register the interrupt as shared so multiple handlers can be defined per GPIO.
			irq_flags |= IRQF_TRIGGER_RISING;
			irq_flags |= IRQF_TRIGGER_FALLING;

			// Register and enable IRQ handler:
			// DEBUG printk(KERN_INFO "%s: ------> Register IRQ %i with dev_id: %p | irq_flags: %lu\n", GPIO_MONITOR_KM_NAME, IRQ_GPIO(entry->job.gpio), entry, irq_flags);
			snprintf(entry->name, sizeof(entry->name), "FlockLab GPIO Monitor %u %u %u", entry->job.gpio, entry->job.edge, entry->job.mode);
			if (( ret_val = request_irq(IRQ_GPIO(entry->job.gpio), irq_handler, irq_flags, entry->name, entry)) < 0 ) {
				printk(KERN_ERR "%s: Cannot register interrupt %d for GPIO %d because: %d\n", GPIO_MONITOR_KM_NAME, IRQ_GPIO(entry->job.gpio), entry->job.gpio, ret_val);
				return -ERRIRQNOTREG;
			}

			// Return data to userspace:
			return SUCCESS;
			break;

		case GPIO_MONITOR_IOCTL_REMOVE:
			entry = (struct monitor_job_list*) kmalloc(sizeof(struct monitor_job_list), GFP_KERNEL);

			// Get the data from the userspace:
			ret_val = copy_from_user(&(entry->job), (unsigned int *) arg, sizeof(struct monitor_job));
			if (ret_val != 0) {
				printk(KERN_ERR "%s: Error getting the arguments.\n", GPIO_MONITOR_KM_NAME);
				kfree(entry);
				return -ENOTTY;
			}

			// Remove the entry from the list:
			ret_val = list_remove(&(entry->job));

			// Free the used memory:
			kfree(entry);

			return ret_val;
			break;

		case GPIO_MONITOR_IOCTL_REMOVEALL:
			return list_removeall();
			break;

		case GPIO_MONITOR_IOCTL_FLUSH:
			kfifo_reset(&sample_fifo);
			break;

		case GPIO_MONITOR_IOCTL_ERRCNT:
			return kfifo_full_count;
			break;

		case GPIO_MONITOR_IOCTL_ERRRST:
			// Reset kfifo full counter:
			kfifo_full_count = 0;
			irq_disabled_count = 0;
			return SUCCESS;
			break;

		default:
			printk(KERN_ERR "%s: Requested IOCTL not found.\n", GPIO_MONITOR_KM_NAME);
			return -ENOTTY;
	}

	return SUCCESS;
} // gpio_monitor_ioctl



/**************************************************************************************//**
 *
 * Function is executed when a user opens the dev file for /dev/gpiomonitor_list
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int gpio_monitor_dev_list_open(struct inode *inode, struct file *filp)
{
	if (device_list_open)
		return -EBUSY;
	device_list_open++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	dev_list_read = 0;

	try_module_get(THIS_MODULE);

	return 0;
} // gpio_monitor_dev_list_open



/**************************************************************************************//**
 *
 * Function is executed when a user reads the dev file for /dev/gpiomonitor_list
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, negative error number otherwise
 *
 *****************************************************************************************/
static ssize_t gpio_monitor_dev_list_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	unsigned int ret, count = 0;
	struct monitor_job_list *entry_ptr;
	struct list_head *list_ptr, *list_safe_ptr;

	if (dev_list_read == 0) {
		if (!list_empty(&monitor_list)) {
			// Put list contents into buffer:
			list_for_each_safe(list_ptr, list_safe_ptr, &monitor_list) {
				entry_ptr = list_entry(list_ptr, struct monitor_job_list, list);
				// Do not list entries which are inactive:
				if (entry_ptr->active == 0)
					continue;
				if ((ret = copy_to_user(__user buffer+count, &(entry_ptr->job), sizeof(struct monitor_job))))
					return -EFAULT;
				count += sizeof(struct monitor_job)-ret;
			}
		}
		dev_list_read = 1;
	}

	return count;
} // gpio_monitor_dev_list_read



/**************************************************************************************//**
 *
 * Function is executed when a user closes the dev file for /dev/gpiomonitor_list
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int gpio_monitor_dev_list_release(struct inode *inode, struct file *filp)
{
	device_list_open--;

	module_put(THIS_MODULE);

	return 0;
} // gpio_monitor_dev_list_release



/**************************************************************************************//**
 *
 * Function is executed when a user opens the dev file for dbbuf
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int gpio_monitor_dev_dbbuf_open(struct inode *inode, struct file *filp)
{
	if (device_dbbuf_open)
		return -EBUSY;
	device_dbbuf_open++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	try_module_get(THIS_MODULE);

	return 0;
} // gpio_monitor_dev_dbbuf_open



/**************************************************************************************//**
 *
 * Function is executed when a user reads the dev file for dbbuf
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes
 *
 *****************************************************************************************/
static ssize_t gpio_monitor_dev_dbbuf_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	unsigned int copied;
	int ret;

	wait_event_interruptible(wq_dbbuf, !kfifo_is_empty(&sample_fifo));
	ret = kfifo_to_user(&sample_fifo, buffer, length, &copied);

	return ret ? ret : copied;
} // gpio_monitor_dev_dbbuf_read



/**************************************************************************************//**
 *
 * Function is executed when a user closes the dev file for dbbuf
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int gpio_monitor_dev_dbbuf_release(struct inode *inode, struct file *filp)
{
	device_dbbuf_open--;

	// Print kfifo_full count if it occurred:
	if (kfifo_full_count > 0) {
		printk(KERN_ERR "%s: kfifo_full_count=%u\n", GPIO_MONITOR_KM_NAME, kfifo_full_count);
	}
	if (irq_disabled_count > 0) {
		printk(KERN_ERR "%s: irq_disabled_count=%u\n", GPIO_MONITOR_KM_NAME, irq_disabled_count);
	}
	module_put(THIS_MODULE);

	return SUCCESS;
} // gpio_monitor_dev_dbbuf_release



/**************************************************************************************//**
 *
 * Interrupt handler to process all GPIO IRQ's
 * Time needed us(min, mean, max): 13.5, 15.12, 20.42
 *
 * @param irq IRQ number
 * @param dev_id Device ID
 *
 * @return Status of IRQ handling
 *
 *****************************************************************************************/
static irqreturn_t irq_handler(int irq, void *dev_id)
{
	/* Local variables	*/
	struct monitor_job_list *entry_ptr;
	struct monitor_job_bh bh_entry;
	struct monitor_job_bh *bh_entry_ptr;

	//DEBUG gpio_set_value(100,1);

	// Get timestamp 1.7us
	bh_entry.oscr_timestamp = OSCR;
	// gpio_set_value(100,0);

	// Get the entry that stores the data for the monitor job: 3.6us
	entry_ptr = (struct monitor_job_list *) dev_id;
	if (entry_ptr->active == 0)
		return IRQ_NONE;
	bh_entry.gpio_level =  __raw_readl(entry_ptr->gplr);
#ifdef FLOCKLAB_CATCH_LEVEL_FLAG	
	bh_entry.gedr =        __raw_readl(entry_ptr->gedr);
	bh_entry.gpio_level2 = __raw_readl(entry_ptr->gplr);
#endif	
	// gpio_set_value(100,1);
	// Process remaining task in tasklet 3us
	bh_entry_ptr = (struct monitor_job_bh *) kmem_cache_alloc(monitor_job_bh_cache, GFP_ATOMIC);
	if (bh_entry_ptr == NULL){
		printk(KERN_ERR "Not enough cache memory...");
		return IRQ_HANDLED;
	}
	// gpio_set_value(100,0);
	bh_entry.entry = entry_ptr; // 1.3 us
	memcpy(bh_entry_ptr, &bh_entry, sizeof(struct monitor_job_bh));
	// gpio_set_value(100,1);

	// Initialize the tasklet: 5.3us
	if (atomic_dec_and_test(&entry_ptr->tasklets_avail)) {
		// avoid too many scheduled tasklets
		disable_irq_nosync(irq);
		irq_disabled_count++;
	}
	tasklet_init(&(bh_entry_ptr->tasklet), tasklet_handler, (unsigned long) bh_entry_ptr);
	// Schedule the tasklet depending on the type of callback:
	if (entry_ptr->job.callback == -1) {
		// Schedule the tasklet with low priority:
		tasklet_schedule(&(bh_entry_ptr->tasklet));
	} else {
		// Schedule the tasklet with high priority:
		tasklet_hi_schedule(&(bh_entry_ptr->tasklet));
	}

	// If mode is set to be single, deactivate the monitoring job:
	if (entry_ptr->job.mode == S) {
		entry_ptr->active = 0;
		// DEBUG printk(KERN_INFO "%s: --> Removed single monitor job.\n", GPIO_MONITOR_KM_NAME);
	}

	//DEBUG gpio_set_value(100,0);
	
	return IRQ_HANDLED;
} // irq_handler



/**************************************************************************************//**
 *
 * Bottom half of IRQ handler
 *
 * @param data Data to handle
 *
 *****************************************************************************************/
static void tasklet_handler(unsigned long data) {
	/* Local variables */
	struct monitor_job_bh *bh_ptr;
	struct monitor_job_report sample;
	struct callback_args_gpio_set_add* gpioset_args;
	struct callback_args_powerprof_add* powerprof_args;
	struct timeval timetmp, now, timestamp;
	uint32_t oscr_interval;
	int32_t signed_usecs;
	int edge_detected;
	struct monitor_job_list *entry_ptr;
	
	// DEBUG printk(KERN_INFO "%s: --> Tasklet handler entered.\n", GPIO_MONITOR_KM_NAME);
	
	// Get the arguments:
	bh_ptr = (struct monitor_job_bh *) data;
	entry_ptr = bh_ptr->entry;
#ifdef FLOCKLAB_CATCH_LEVEL_FLAG
	if (bh_ptr->gedr & entry_ptr->gmask) {
		edge_detected = ((bh_ptr->gpio_level2 & entry_ptr->gmask) == 0);
		//printk(KERN_INFO "%s: --> flag detected, gpio %i.\n", GPIO_MONITOR_KM_NAME, entry_ptr->job.gpio);
	}
	else {
		edge_detected = (bh_ptr->gpio_level & entry_ptr->gmask) > 0;
	}
#else
	edge_detected = (bh_ptr->gpio_level & entry_ptr->gmask) > 0;
#endif
	// Check if IRQ was for us (shared IRQ's can only be registered for both edges. Thus we need to check which edge triggered the IRQ).
	// We have to check whether the current job is active and the GPIO edge that triggered the IRQ is the one we are looking for.
	if ( ((edge_detected != ((int) entry_ptr->job.edge)) && (entry_ptr->job.edge != B))) {
		//DEBUG gpio_set_value(60,0);
		// Free the memory used:
		kmem_cache_free(monitor_job_bh_cache, bh_ptr);
		return;
	}
	
	// calculate timestamp
	do_gettimeofday(&timestamp);
	oscr_interval = OSCR - bh_ptr->oscr_timestamp;
	oscr_interval = ((u32)(((u64)oscr_interval * (u64)oscr_mult) >> OSCR_SHIFT)) / 1000; // get uS
	signed_usecs = timestamp.tv_usec - oscr_interval;
	while (signed_usecs < 0) {
		signed_usecs += 1000000;
		timestamp.tv_sec--;
	}
	timestamp.tv_usec = signed_usecs;
	
	// Execute callback if any was given:
	switch(entry_ptr->job.callback) {
		case gpio_set_add:
			// Extract the callback arguments:
			gpioset_args = (struct callback_args_gpio_set_add*) (entry_ptr->job.callback_args);
			// Depending on the start time of the setting (immediately or in the future), set the timeval to the correct settings.
			// If offset and microseconds are 0, pass 0 to callback function. Otherwise get current time and add the offset.
			if ((gpioset_args->offset_s == 0) && (gpioset_args->offset_us == 0)) {
				timetmp.tv_sec = 0;
				timetmp.tv_usec = 0;
			} else {
				now = timestamp;
				// Add offset to current time, care for wrap of usec:
				if ( (now.tv_usec + gpioset_args->offset_us) >= 1000000) {
					timetmp.tv_sec  = now.tv_sec  + gpioset_args->offset_s  + 1;
					timetmp.tv_usec = now.tv_usec + gpioset_args->offset_us - 1000000;
				} else {
					timetmp.tv_sec  = now.tv_sec  + gpioset_args->offset_s;
					timetmp.tv_usec = now.tv_usec + gpioset_args->offset_us;
				}
			}
			//DEBUG printk(KERN_INFO "%s: --> Monitored timeval: %lu.%lu Offset: %lu Callback timeval:\t %lu.%lu.\n", GPIO_MONITOR_KM_NAME, bh_ptr->timestamp.tv_sec, bh_ptr->timestamp.tv_usec, ((struct callback_args_gpio_set_add*)callback_args_ptr)->offset_us, ((struct timeval*)temp)->tv_sec,((struct timeval*)temp)->tv_usec);
			// Call the exported function from the GPIO setting service to add the event:
			gpio_setting_add(gpioset_args->gpio, gpioset_args->level, timetmp, 0, 0);
			break;

		case powerprof_add:
			// Extract the callback arguments:
			powerprof_args = (struct callback_args_powerprof_add*)(entry_ptr->job.callback_args);
			// Depending on the start time of the profile (immediately or in the future), set the timeval to the correct settings.
			// If offset and microseconds are 0, pass 0 to callback function. Otherwise get current time and add the offset.
			if ((powerprof_args->offset_s == 0) && (powerprof_args->offset_us == 0)) {
				timetmp.tv_sec = 0;
				timetmp.tv_usec = 0;
			} else {
				now = timestamp;
				// Add offset to current time, care for wrap of usec:
				if ( (now.tv_usec + powerprof_args->offset_us) >= 1000000) {
					timetmp.tv_sec  = now.tv_sec  + powerprof_args->offset_s  + 1;
					timetmp.tv_usec = now.tv_usec + powerprof_args->offset_us - 1000000;
				} else {
					timetmp.tv_sec  = now.tv_sec  + powerprof_args->offset_s;
					timetmp.tv_usec = now.tv_usec + powerprof_args->offset_us;
				}
			}
			// Call the exported function from the powerprofiling service to add the event:
			powerprofile_add(powerprof_args->duration, timetmp, ADS1271_SAMPLE_DIVIDER_DEFAULT);
			break;

		default:
			//nop
			break;
	}
	
	// Insert element into processed events ringbuffer:
	sample.edge = edge_detected;
	sample.gpio = entry_ptr->job.gpio;
	sample.timestamp = timestamp;
	if (!kfifo_in(&sample_fifo, &sample, sizeof(struct monitor_job_report))) {
		kfifo_full_count++;
		//DEBUG printk(KERN_ALERT "%s: ----> WARNING: Kfifo full. Count: %u\n", GPIO_MONITOR_KM_NAME, kfifo_full_count);
	}
	
	if (entry_ptr->active == 0) {
		list_remove(&(entry_ptr->job));
	}

	// Wake up sleeping readers:
	wake_up_interruptible(&wq_dbbuf);

	// Free the memory used:
	kmem_cache_free(monitor_job_bh_cache, bh_ptr);
	if (atomic_inc_return(&entry_ptr->tasklets_avail)==1) {
		enable_irq(IRQ_GPIO(entry_ptr->job.gpio));
	}
} // tasklet_handler



/**************************************************************************************//**
 *
 * Remove a job from the monitoring list
 *
 * @param job_delete Struct holding the job to delete.
 *
 * @return 0 on success, error number else.
 *
 *****************************************************************************************/
static int list_remove(struct monitor_job *job_delete)
{
	/* Local variables */
	struct monitor_job_list *entry_ptr;
	struct list_head *list_ptr, *list_safe_ptr;

	// Check whether list is empty:
	if (list_empty(&monitor_list))
		return ERRLISTEMPTY;

	// Traverse list, check for each entry whether it matches the search. If it does, delete the entry from the list.
	list_for_each_safe(list_ptr, list_safe_ptr, &monitor_list) {
		entry_ptr = list_entry(list_ptr, struct monitor_job_list, list);
		if ( (entry_ptr->job.gpio == job_delete->gpio) && (entry_ptr->job.edge == job_delete->edge) ) {
			// Unregister IRQ:
			free_irq(IRQ_GPIO(entry_ptr->job.gpio), entry_ptr);
			// DEBUG printk(KERN_INFO "%s: --------> Freed IRQ %i with dev_id %p\n", GPIO_MONITOR_KM_NAME, IRQ_GPIO(entry_ptr->job.gpio), entry_ptr);
			// Delete the entry from the list:
			list_del(list_ptr);

			// Free all used memory:
			if ((entry_ptr->job.callback_args) != NULL) {
				kfree(entry_ptr->job.callback_args);
				entry_ptr->job.callback_args = NULL;
			}
			kfree(entry_ptr);

			return SUCCESS;
		}
	}

	// The entry was not found.
	return ERRNOTFOUND;
} // list_remove



/**************************************************************************************//**
 *
 * Remove all monitoring jobs from list and free memory
 *
 *****************************************************************************************/
static int list_removeall(void)
{
	/* Local variables */
	int rs = SUCCESS;
	struct monitor_job_list *entry_ptr;
	struct list_head *list_ptr, *list_safe_ptr;

	// Traverse list, remove each entry and free its memory:
	list_for_each_safe(list_ptr, list_safe_ptr, &monitor_list) {
		// Get the whole job:
		entry_ptr = list_entry(list_ptr, struct monitor_job_list, list);
		// Remove the job:
		rs = list_remove(&(entry_ptr->job));
	}

	return rs;
} // list_removeall



/**************************************************************************************//**
 *
 * Called to perform module cleanup when the module is unloaded
 *
 *****************************************************************************************/
void cleanup_module(void)
{
    cleanup(INIT_DONE);
}

void cleanup(int init_state)
{
    //printk(KERN_INFO "%s: FlockLab service GPIO monitor unloading...\n", GPIO_MONITOR_KM_NAME);
    switch(init_state) {
        case INIT_DONE:
        case INIT_KFIFO:
            // Free memory of ring buffer:
            kfifo_free(&sample_fifo);
            printk(KERN_DEBUG "%s: Kfifo destroyed and freed.\n", GPIO_MONITOR_KM_NAME);

        case INIT_SLAB_CACHE:
            // Destroy slab caches:
            kmem_cache_destroy(monitor_job_bh_cache);
            printk(KERN_DEBUG "%s: Slab caches destroyed.\n", GPIO_MONITOR_KM_NAME);

        case INIT_EVENT_QUEUE:
            // Destroy monitoring job list and free memory:
        	list_removeall();
            printk(KERN_DEBUG "%s: Monitoring job list destroyed.\n", GPIO_MONITOR_KM_NAME);
            
        // Unregister device /dev/gpiomonitor_dbbuf:
        case INIT_DBBUF_CLASS:
            device_destroy( dev_dbbuf_class, dev_dbbuf_nr );
            class_destroy( dev_dbbuf_class );
        case INIT_DBBUF_ADD:
            cdev_del( &dev_dbbuf_cdev );
        case INIT_DBBUF_CHRDEV:
            unregister_chrdev_region( dev_dbbuf_nr, 1 );
            printk(KERN_DEBUG "%s: Dbbuf device unregistered.\n", GPIO_MONITOR_KM_NAME);

        // Unregister device /dev/gpiomonitor_list:
        case INIT_LIST_CLASS:
            device_destroy( dev_list_class, dev_list_nr );
            class_destroy( dev_list_class );
        case INIT_LIST_ADD:
            cdev_del( &dev_list_cdev );
        case INIT_LIST_CHRDEV:
            unregister_chrdev_region( dev_list_nr, 1 );
            printk(KERN_DEBUG "%s: List device unregistered.\n", GPIO_MONITOR_KM_NAME);

        case INIT_GPIO:
            /* Free the GPIO pins used */
            // Not needed as they are only configured in init_module() and not requested via request_gpio()
            break;
    }
    //printk(KERN_INFO "%s: FlockLab service GPIO monitor unloaded.\n", GPIO_MONITOR_KM_NAME);

} // cleanup_module


