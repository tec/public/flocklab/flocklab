/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains shared definitions for the kernelmodule of the FlockLab service "GPIO monitor".
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_GPIO_MONITOR_H
#define FLOCKLAB_GPIO_MONITOR_H

#define MAX_MONITOR_TASKLETS 8

/* ---- Export functions for shared use in other kernel modules ---------------*/



/* ---- Include Guard -------------------------------------------------------- */
#endif
