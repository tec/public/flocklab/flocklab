/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 * This h-file contains shared definitions for both the userspace programs
 * and the kernelmodule of the flocklab service "GPIO monitor".
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_GPIO_MONITOR_SHARED_H
#define FLOCKLAB_GPIO_MONITOR_SHARED_H


/* ---- Include Files ---------------------------------------------------- */
#include "flocklab_shared.h"

/* ---- Constants ----------------------------------------------------------*/

// Magic number for this kernel module for registering ioctl commands
#define GPIO_MONITOR_IOCTL_MAGIC  	'O'
// Set up ioctl command numbers:
#define GPIO_MONITOR_IOCTL_ADD			_IOWR(GPIO_MONITOR_IOCTL_MAGIC,	0, long[6])		// IOCTL to add a GPIO monitoring job.
#define GPIO_MONITOR_IOCTL_REMOVE		_IOWR(GPIO_MONITOR_IOCTL_MAGIC,	1, long[4])		// IOCTL to remove a single GPIO monitoring job.
#define GPIO_MONITOR_IOCTL_REMOVEALL	_IOR (GPIO_MONITOR_IOCTL_MAGIC,	2, int)			// IOCTL to remove all GPIO monitoring jobs.
#define GPIO_MONITOR_IOCTL_FLUSH		_IOR (GPIO_MONITOR_IOCTL_MAGIC,	3, int)			// IOCTL to flush the output buffer for the monitoring results.
#define GPIO_MONITOR_IOCTL_ERRCNT		_IOR (GPIO_MONITOR_IOCTL_MAGIC,	4, int)			// IOCTL to show error statistics
#define GPIO_MONITOR_IOCTL_ERRRST		_IOR (GPIO_MONITOR_IOCTL_MAGIC,	5, int)			// IOCTL to reset error statistics
#define GPIO_MONITOR_IOCTL_MAXNR		6												// Denotes maximum number of cmd's above

// Size of ring buffers that holds the detected GPIO edges (should be power of 2):
#define SIZEOFBUF	512
// Return values of functions:
#ifndef SUCCESS
#define SUCCESS			0															// Indicates success of an operation.
#endif
#define ERRIRQNOTREG	1															// Indicates to api_add() that the requested IRQ could not be registered.

/* ---- Global variables ---------------------------------------------------*/
// Enumeration for monitoring mode:
#define EN_MODE C(C)C(S)		// C = Continuous, S = Single
#undef C
#define C(x) x,
enum en_mode { EN_MODE EN_MODE_TOP };
#undef C
#define C(x) #x,
char * en_mode[] = { EN_MODE };

// Enumeration for callback functions:
#define EN_CALLBACK C(gpio_set_add)C(powerprof_add)
#undef C
#define C(x) x,
enum en_callback { EN_CALLBACK EN_CALLBACK_TOP };
#undef C
#define C(x) #x,
char * en_callback[] = { EN_CALLBACK };

/**
 * Struct that holds all data needed for monitoring a pin. Is used in userspace and in kernel space.
 */
struct monitor_job {
	unsigned int gpio;								// Pin number which is to be monitored.
	enum en_edge edge;								// Edge which is to be monitored.
	enum en_mode mode;								// Mode of the monitoring.
	enum en_callback callback;						// Callback function which is to be executed upon pin detection.
	void *callback_args;							// Argument pointer for callback function.
};

/**
 * Struct that holds all data needed for reporting an event for a monitored pin. Is used in userspace and in kernel space.
 */
struct monitor_job_report {
	unsigned int gpio;								// Pin number which is to be monitored.
	enum en_edge edge;								// Edge which is to be monitored.
	struct timeval timestamp;						// Timestamp of occurrence of monitoring event.
};

/**
 * Structs for arguments of callbacks
 */
struct callback_args_gpio_set_add {
	unsigned int gpio;								// Pin number to set.
	enum en_level level;							// Level to set. Can be H or L.
	unsigned long offset_s;							// Second part of offset to wait before setting the pin.
	unsigned long offset_us;						// Microsecond part of offset to wait before setting the pin.
};

struct callback_args_powerprof_add {
	unsigned long duration;							// Duration of sampling
	unsigned long offset_s;							// Second part of offset to wait before setting the pin.
	unsigned long offset_us;						// Microsecond part of offset to wait before setting the pin.
};

/* ---- Include Guard ---------------------------------------------------- */
#endif


