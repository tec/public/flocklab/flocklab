/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This kernelmodule provides functionality for the FlockLab v2 @ ETH Zurich, Switzerland. It implements
 * the GPIO setting service which allows a user to define sets of GPIO pin settings/clearings at
 * predefined points of time.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>					/* Needed by all modules */
#include <linux/kernel.h>					/* Needed for KERN_INFO */
#include <linux/list.h>					/* List mechanisms */
#include <linux/time.h>					/* Time functions */
#include <linux/timer.h>					/* Kernel Timer functions */
#include <linux/clocksource.h>
#include <linux/sched.h>					/* Scheduling functions */
#include <linux/interrupt.h>				/* Interrupt handling */
#include <linux/kfifo.h>
#include <mach/regs-ost.h>					/* Register definitions */
#include <mach/hardware.h>					/* Register functions */
#include <linux/gpio.h>					/* GPIO functions */
#include <linux/ioctl.h>					/* Needed for IOCTL */
#include <linux/fs.h>						/* Needed for device registering, file writes for GPIO initialization */
#include <linux/fcntl.h>					/* Needed for file writes for GPIO initialization */
#include <linux/uaccess.h>					/* Needed for exchanging data between kernel and user space over /dev */
#include <linux/cdev.h>					/* Needed for registering device */
#include <linux/device.h>					/* Needed for registering device */
#include <linux/sched.h>					/* Tasklets */
#include "flocklab_gpio_setting.h"
#include "flocklab_gpio_setting_shared.h"
#include "flocklab_ostimer_fnct.h"



/* ---- Module documentation and authoring information --------------------*/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("ETH Zurich, 2009-2012, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("GPIO setting service for FlockLab v2 @ ETH Zurich, Switzerland.");
MODULE_SUPPORTED_DEVICE("FlockBoard rev. 1.1");
MODULE_VERSION("2.7");

#define MODULE_NAME GPIO_SETTING_KM_NAME


/* ---- Global variables --------------------------------------------------*/
/**
 * Struct which holds all data needed for a GPIO setting event. The struct has the same structure as the according DB table tbl_obs_gpio_setting.
 */
struct gpioset_event_list {
	struct list_head list;									// Implements a doubly linked list. Struct is defined in linux/list.h
	struct gpioset_event event;
	unsigned int interval;									// interval in us for periodic settings
	unsigned int count;									// number of repetition to do, for periodic settings, 1 if it is a one-time setting
};
static struct list_head event_queue;						// Create list head
static struct gpioset_event_list *cur_event_list = NULL;	// Pointer to the event that is currently scheduled and processed
spinlock_t list_lock;										// lock for list manipulations

// Variables for processed events kfifo:
static struct kfifo event_fifo;
static int kfifo_full_count = 0;							// Count for fifo full events.

// Global variables for list operations:
static int device_list_open = 0;							// Used to prevent opening the device multiple times:
static struct class *dev_list_class = NULL;					// List class need to register device
static dev_t dev_list_nr = 0;								// Device number needed to register device
static struct cdev dev_list_cdev;							// CDev needed to register device
static int dev_list_read;

// Global variables for dbbuf operations:
static int device_dbbuf_open = 0;							// Used to prevent opening the device multiple times:
static struct class *dev_dbbuf_class = NULL;				// List class need to register device
static dev_t dev_dbbuf_nr = 0;								// Device number needed to register device
static struct cdev dev_dbbuf_cdev;							// CDev needed to register device

// Wait queue for dbbuf reader processes:
static DECLARE_WAIT_QUEUE_HEAD(wq_dbbuf);

// Tasklet for bottom half of IRQ handler:
static struct tasklet_struct irq_bh_tasklet;

// Variables for the kernel timer:
static struct timer_list kerneltimer;						// Timer list to time the kernel timer events.


/* ---- Prototypes --------------------------------------------------------*/
int init_module(void);
void cleanup_module(void);
void cleanup(int init_state);
static int schedule_next_event(void);
static void list_add_sorted(struct gpioset_event_list *new);
static int list_remove(unsigned int gpio, int value, struct timeval time_planned);
static int list_removeall(void);
static void tasklet_handler(unsigned long);
static void set_kernel_timer(struct timeval tv_wait_time);
static int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
static int gpio_setting_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
static int gpio_setting_dev_list_open(struct inode *inode, struct file *filp);
static ssize_t gpio_setting_dev_list_read(struct file *filp, char *buffer, size_t length, loff_t *offset);
static int gpio_setting_dev_list_release(struct inode *inode, struct file *filp);
static int gpio_setting_dev_dbbuf_open(struct inode *inode, struct file *filp);
static ssize_t gpio_setting_dev_dbbuf_read(struct file *filp, char *buffer, size_t length, loff_t *offset);
static int gpio_setting_dev_dbbuf_release(struct inode *inode, struct file *filp);
static inline void __to_kfifo_and_reschedule(struct gpioset_event_list * event_list_item);
EXPORT_SYMBOL_GPL(gpio_setting_add);

/* ---- More Global variables ---------------------------------------------*/
// Variables for OS timer
static unsigned int oscr_mult;
#define OSCR_SHIFT 20

/**
 * Struct for accessing the list device file
 */
static struct file_operations fops_list =
{
		.owner		= THIS_MODULE,
		.ioctl		= gpio_setting_ioctl,
		.open		= gpio_setting_dev_list_open,
		.read		= gpio_setting_dev_list_read,
		.release	= gpio_setting_dev_list_release
};

/**
 * Struct for accessing the dbbuf device file
 */
static struct file_operations fops_dbbuf =
{
		.owner		= THIS_MODULE,
		.open		= gpio_setting_dev_dbbuf_open,
		.read		= gpio_setting_dev_dbbuf_read,
		.release	= gpio_setting_dev_dbbuf_release
};
/* ---- Enums to for init / cleanup ---------------------------------------------*/
enum {
	INIT_GPIO,
	INIT_LIST_CHRDEV,
	INIT_LIST_ADD,
	INIT_LIST_CLASS,
	INIT_DBBUF_CHRDEV,
	INIT_DBBUF_ADD,
	INIT_DBBUF_CLASS,
	INIT_EVENT_QUEUE,
	INIT_KFIFO,
	INIT_KERNEL_TIMER,
	INIT_DONE,
};

/**************************************************************************************//**
 *
 * Called to perform module initialization when the module is loaded
 *
 * @return Zero for success, negative error number otherwise
 *
 *****************************************************************************************/
int init_module(void)
{
	/* Local variables	*/
	int i, rs;
	struct file *fp;
	mm_segment_t oldfs;
	loff_t offset=0;
	char path[34];

	printk(KERN_INFO "%s: FlockLab service GPIO setting starting...\n", GPIO_SETTING_KM_NAME);

	/* Initialize GPIO's used. This can not be done using request_gpio() as this would conflict with the symlinked GPIO names in /sys/devices/platform/gpio/ */
	oldfs = get_fs();
	set_fs(get_ds());
	for (i=0;i<(sizeof(list_gpiopins_set)/sizeof(*list_gpiopins_set)); i++) {
		sprintf(path, "/sys/class/gpio/gpio%u/direction", list_gpiopins_set[i]);
		fp = filp_open(path, O_WRONLY, 0644);
		vfs_write(fp, "out", 3, &offset);
		filp_close(fp, NULL);

		sprintf(path, "/sys/class/gpio/gpio%u/value", list_gpiopins_set[i]);
		fp = filp_open(path, O_WRONLY, 0644);
		vfs_write(fp, "0", 1, &offset);
		filp_close(fp, NULL);

		printk(KERN_DEBUG "%s: Successfully initialized GPIO %i.\n", GPIO_SETTING_KM_NAME, list_gpiopins_set[i]);
	}
	set_fs(oldfs);

	/* Register the list device for the list() command of the API */
	// Get a major number
	if (( rs = alloc_chrdev_region( &dev_list_nr, 0, 1, DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST )) < 0 ) {
		printk( KERN_ERR "%s: Unable to allocate major for list device. Error %d occurred.\n", GPIO_SETTING_KM_NAME, rs );
		cleanup(INIT_GPIO);
		return rs;
	}
	printk(KERN_DEBUG "%s: Allocated device numbers for list device. Major:%d minor:%d\n", GPIO_SETTING_KM_NAME, MAJOR( dev_list_nr ), MINOR( dev_list_nr ));
	// Register the device. The device becomes "active" as soon as cdev_add is called.
	cdev_init( &dev_list_cdev, &fops_list );
	dev_list_cdev.owner = THIS_MODULE;
	dev_list_cdev.ops = &fops_list;
	if (( rs = cdev_add( &dev_list_cdev, dev_list_nr, 1 )) != 0 ) {
		printk( KERN_ERR "%s: cdev_add for list device failed with error %d\n.", GPIO_SETTING_KM_NAME, rs );
		cleanup(INIT_LIST_CHRDEV);
		return rs;
	}
	printk(KERN_DEBUG "%s: List device registered.\n", GPIO_SETTING_KM_NAME);
	// Create a class, so that udev will make the dev entry automatically:
	dev_list_class = class_create( THIS_MODULE, DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST );
	if ( IS_ERR( dev_list_class )) {
		printk( KERN_ERR "%s: Unable to create class for list device.\n", GPIO_SETTING_KM_NAME );
		cleanup(INIT_LIST_ADD);
		return -1;
	}
	device_create( dev_list_class, NULL, dev_list_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST );
	printk(KERN_DEBUG "%s: List device node created.\n", GPIO_SETTING_KM_NAME);

	/* Register the dbbuf device for writing the processed events queue to the database in userspace */
	// Get a major number
	if (( rs = alloc_chrdev_region( &dev_dbbuf_nr, 0, 1, DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_DBBUF )) < 0 ) {
		printk( KERN_ERR "%s: Unable to allocate major for dbbuf device. Error %d occurred.\n", GPIO_SETTING_KM_NAME, rs );
		cleanup(INIT_LIST_CLASS);
		return rs;
	}
	printk(KERN_DEBUG "%s: Allocated device numbers for dbbuf device. Major:%d minor:%d\n", GPIO_SETTING_KM_NAME, MAJOR( dev_dbbuf_nr ), MINOR( dev_dbbuf_nr ));
	// Register the device. The device becomes "active" as soon as cdev_add is called.
	cdev_init( &dev_dbbuf_cdev, &fops_dbbuf );
	dev_dbbuf_cdev.owner = THIS_MODULE;
	dev_dbbuf_cdev.ops = &fops_dbbuf;
	if (( rs = cdev_add( &dev_dbbuf_cdev, dev_dbbuf_nr, 1 )) != 0 ) {
		printk( KERN_ERR "%s: cdev_add for dbbuf device failed with error %d\n.", GPIO_SETTING_KM_NAME, rs );
		cleanup(INIT_DBBUF_CHRDEV);
		return rs;
	}
	printk(KERN_DEBUG "%s: Dbbuf device registered.\n", GPIO_SETTING_KM_NAME);
	// Create a class, so that udev will make the dev entry automatically:
	dev_dbbuf_class = class_create( THIS_MODULE, DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_DBBUF );
	if ( IS_ERR( dev_dbbuf_class )) {
		printk( KERN_ERR "%s: Unable to create class for dbbuf device.\n", GPIO_SETTING_KM_NAME );
		cleanup(INIT_DBBUF_ADD);
		return -1;
	}
	device_create( dev_dbbuf_class, NULL, dev_dbbuf_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_DBBUF );
	printk(KERN_DEBUG "%s: Dbbuf device node created.\n", GPIO_SETTING_KM_NAME);

	/* Initialize event queue */
	INIT_LIST_HEAD(&event_queue);	// Initialize list head
	spin_lock_init(&(list_lock));
	printk(KERN_DEBUG "%s: Event queue initialized.\n", GPIO_SETTING_KM_NAME);

	/* Initialize kfifo for processed events */
	if ((rs = kfifo_alloc(&event_fifo, SIZEOFEVENTBUF*sizeof(struct gpioset_event), GFP_KERNEL)) != 0) {
		printk(KERN_ERR GPIO_SETTING_KM_NAME ": unable to allocate memory for kfifo, error %d.\n", rs);
		cleanup(INIT_EVENT_QUEUE);
		return -ENOMEM;
	}
	printk(KERN_DEBUG "%s: Kfifo for processed events initialized.\n", GPIO_SETTING_KM_NAME);

	/* Register interrupt handler */
	// Init tasklet for bottom half of interrupt handler:
	tasklet_init(&irq_bh_tasklet, tasklet_handler, 0);
	printk(KERN_DEBUG "%s: Tasklet for OS timer interrupt registered.\n", GPIO_SETTING_KM_NAME);

	/* Initialize the kernel timer */
	oscr_mult = clocksource_hz2mult(get_clock_tick_rate(), OSCR_SHIFT);
	init_timer(&kerneltimer);
	kerneltimer.function = (void *) schedule_next_event;
	printk(KERN_DEBUG "%s: Kernel timer initialized.\n", GPIO_SETTING_KM_NAME);

	printk(KERN_INFO "%s: FlockLab service GPIO setting started.\n", GPIO_SETTING_KM_NAME);

	return 0;
} // init_module



/**************************************************************************************//**
 *
 * Get the next event from the event queue and schedule it in an appropriate timer.
 *
 * @return Zero for success, negative error number otherwise
 *
 *****************************************************************************************/
static int schedule_next_event(void)
{
	/* Local variables */
	struct timeval tv_now, tv_wait_time;
	unsigned long wait_time_us;
	int tv_diff;
	struct gpioset_event_list *old_cur_eventlist;
	struct gpioset_event *old_event, *cur_event;

	cur_event = &(cur_event_list->event);

	// Only proceed if the event queue is not empty:
	if (likely(!list_empty(&event_queue))) {
		// Get first event in event queue:
		old_cur_eventlist = list_entry((&event_queue)->next, struct gpioset_event_list, list);
		old_event = &(old_cur_eventlist->event);

		// If the currently scheduled event (stored in cur_eventlist) and the first event in the queue (old_curr_event_ptr) are the same, we do not need to do anything.
		// Furthermore, if the kernel timer is also running, we need not to do anything as well.
		// Otherwise, we need to reschedule;
		if ((cur_event_list != NULL) && (old_event->time_planned.tv_sec == cur_event->time_planned.tv_sec) && (old_event->time_planned.tv_usec == cur_event->time_planned.tv_usec) && (timer_pending(&kerneltimer)))
			return SUCCESS;
		cur_event_list = old_cur_eventlist;
		cur_event = &(cur_event_list->event);

		// Get timestamp:
		do_gettimeofday(&tv_now);

		// If the event is to be planned for the future, check if it is still schedulable:
		if ((cur_event->time_planned.tv_sec != 0) || (cur_event->time_planned.tv_usec != 0)) {
			// Check whether event can be scheduled or if we already missed it:
			tv_diff = timeval_compare(&(cur_event->time_planned), &tv_now);
			if (unlikely(tv_diff < 0)) {
				// We missed the event. If we just missed it close, do the setting anyway.
				if (unlikely( (tv_diff < -150) || (tv_diff == -1) )) {
					//The event was missed with more than 150us, do not execute it anymore.
					// Write 0 into timestamp field to indicate in the DB that the event was missed:
					cur_event->time_executed.tv_sec = 0;
					cur_event->time_executed.tv_usec = 0;
					// Insert element into processed events kfifo:
					__to_kfifo_and_reschedule(cur_event_list);
					
					// Wake up sleeping readers:
					wake_up_interruptible(&wq_dbbuf);

					// Schedule next event:
					return schedule_next_event();
				} else {
					// Schedule the event immediately:
					wait_time_us = 1;
				}
			} else {
				// We did not miss the event.
				// Calculate relative time we need to wait:
				timeval_subtract(&tv_wait_time, &(cur_event->time_planned), &tv_now);
				wait_time_us = (tv_wait_time.tv_sec*1000000)+tv_wait_time.tv_usec;
			}
		} else {
			// The event is planned for immediate execution. Thus set the wait-time to 1 us:
			wait_time_us = 1;
			cur_event->time_planned.tv_sec = tv_now.tv_sec;
			cur_event->time_planned.tv_usec = tv_now.tv_usec + wait_time_us;
		}
		
		// If the wait time is long, schedule the event using the kernel timer, otherwise use the OS timer:
		if ( wait_time_us <= SWITCHTIMER ) {
			// Set OS timer to current event:
			flocklab_ostimer_fnct_set(&kerneltimer, wait_time_us, GPIO_SETTING_OST_OFFSET);
		} else {
			// Set kernel timer for current event.
			// If the wait time is longer in the future than 100*SWITCHTIMER, set the timer to only half the time needed to wait to enforce re-scheduling.
			// This is needed for ensuring that time synchronisation (NTP) is taken into consideration when scheduling an event.
			if (wait_time_us > (100*SWITCHTIMER)) {
				tv_wait_time.tv_sec = (long) tv_wait_time.tv_sec/2;
			}
			set_kernel_timer(tv_wait_time);
		}
	}
	else
	{
		// The event queue is empty. Write a status message and exit the function:
		return ERRLISTEMPTY;
	}
	return SUCCESS;
} // schedule_next_event


/**************************************************************************************//**
 *
 * Insert new entry into sorted event queue
 *
 * @param new New event to insert into list.
 *
 *****************************************************************************************/
static void list_add_sorted(struct gpioset_event_list *new)
{
	/* Local variables */
	struct list_head *list_ptr;
	struct gpioset_event_list *entry;
	
	// Traverse list backwards as users will mostly insert first events first...
	list_for_each_prev(list_ptr, &event_queue) {
		entry = list_entry(list_ptr, struct gpioset_event_list, list);
		// If tv_sec part of event to insert is greater than current event, insert new event after current event. If tv_sec part is the same but tv_usec part is greater, insert as well. Otherwise, iterate.
		if ((new->event.time_planned.tv_sec > entry->event.time_planned.tv_sec) || ((new->event.time_planned.tv_sec == entry->event.time_planned.tv_sec) && (new->event.time_planned.tv_usec > entry->event.time_planned.tv_usec))) {
			// Add event after the current event:
			list_add(&new->list, list_ptr);
			// Not at first position, we do not need to change the schedule
			return;
		}
	}
	// Add event at first position in the queue:
	list_add(&new->list, &event_queue);
	// Schedule event queue:
	schedule_next_event();
} // list_add_sorted


/**************************************************************************************//**
 *
 * Wrapper for list_add_sorted() function. This function is exported so other kernel
 * modules can access it.
 *
 * @param gpio GPIO pin to set
 * @param value Value to set the pin to
 * @param time_planned Time for which the pin setting is planned. 0 for immediate start.
 *
 *****************************************************************************************/
void gpio_setting_add(unsigned int gpio, int value, struct timeval time_planned, unsigned int interval, unsigned int count)
{
	/* Local variables */
	struct gpioset_event_list *new_eventlist;
	unsigned long list_lock_flags;

	new_eventlist = kmalloc(sizeof(struct gpioset_event_list), GFP_ATOMIC);

	// Fill the event struct:
	new_eventlist->event.gpio = gpio;
	new_eventlist->event.value = value;
	new_eventlist->event.time_planned.tv_sec = time_planned.tv_sec;
	new_eventlist->event.time_planned.tv_usec = time_planned.tv_usec;
	new_eventlist->interval = interval;
	new_eventlist->count = count;

	// Add the event:
	
	spin_lock_irqsave(&(list_lock), list_lock_flags);
	list_add_sorted(new_eventlist);
	spin_unlock_irqrestore(&(list_lock), list_lock_flags);
	
} // gpio_setting_add




/**************************************************************************************//**
 *
 * Remove the first occurrence of an entry from the event queue
 *
 * @param gpio GPIO pin to set
 * @param value Value to set the pin to
 * @param time_planned Time for which the pin setting is planned
 *
 * @return Zero for success, error number otherwise
 *
 *****************************************************************************************/
static int list_remove(unsigned int gpio, int value, struct timeval time_planned)
{
	/* Local variables */
	struct gpioset_event_list *entry;
	struct list_head *list, *list_safe;
	unsigned int reschedule = 0;
	
	// Check whether list is empty:
	if (list_empty(&event_queue))
		return ERRLISTEMPTY;

	// Traverse list, check for each entry whether it matches the search. If it does, delete the entry from the list.
	list_for_each_safe(list, list_safe, &event_queue) {
		entry = list_entry(list, struct gpioset_event_list, list);
		if ((entry->event.gpio == gpio)
				&& (entry->event.value == value)
				&& (entry->event.time_planned.tv_sec == time_planned.tv_sec)
				&& (entry->event.time_planned.tv_usec == time_planned.tv_usec)
		)
		{
			if (entry == cur_event_list) {
				// remove currently scheduled event
				cur_event_list = NULL;
				reschedule = 1;
			}
			list_del(list);	// Only delete first occurrence of search.
			kfree(entry);

			// If the queue is empty now, disable the timer (because the event that was deleted was the last one and so it's timer is set which we have to undo now):
			if (list_empty(&event_queue)) {
				// Disable the OS timer:
				flocklab_ostimer_fnct_disable(GPIO_SETTING_OST_OFFSET);

				// Disable the kernel timer if it was set:
				if ( timer_pending(&kerneltimer) ) {
					del_timer(&kerneltimer);
				}
			}

			// Reschedule the event queue in case we deleted the first entry in the list that was scheduled to be next:
			if (reschedule)
				schedule_next_event();
			return SUCCESS;
		}
	}
	
	// The entry was not found.
	return ERRNOTFOUND;
} // list_remove


/**************************************************************************************//**
 *
 * Destroy event queue and free memory
 *
 *****************************************************************************************/
static int list_removeall(void)
{
	/* Local variables */
	int rs = SUCCESS;
	struct gpioset_event_list *entry_ptr;
	struct list_head *list_ptr, *list_safe_ptr;
	
	// Traverse list, remove each entry and free its memory:
	list_for_each_safe(list_ptr, list_safe_ptr, &event_queue) {
		// Get the whole event::
		entry_ptr = list_entry(list_ptr, struct gpioset_event_list, list);
		// Remove the job:
		rs = list_remove(entry_ptr->event.gpio, entry_ptr->event.value, entry_ptr->event.time_planned);
	}

	return rs;
} // list_removeall


/**************************************************************************************//**
 *
 * Function for setting a kernel timer.
 * This function configures the kernel timer for the desired wait time.
 *
 * @param tv_wait_time Time to set the timer to
 *
 *****************************************************************************************/
static void set_kernel_timer(struct timeval tv_wait_time)
{
	/* Local variables */

	// Check if a timer is already running. If yes, abort it:
	if ( timer_pending(&kerneltimer) ) {
		del_timer(&kerneltimer);
	}

	// Configure the kernel timer. Calculate the jiffies value at which the timer should expire. The timer should expire SWITCHTIMER microseconds before the event so it can be rescheduled using the OS timer (which is more accurate).
	kerneltimer.expires = jiffies + timeval_to_jiffies(&tv_wait_time) - (SWITCHTIMER*HZ/1000000);

	// Start the kernel timer:
	add_timer(&kerneltimer);
} // set_kernel_timer



/**************************************************************************************//**
 *
 * Interrupt handler for OS timer
 *
 *
 *****************************************************************************************/
void gpio_setting_ostimer_expired(void)
{
	/* Local variables	*/
	struct gpioset_event *cur_event;

	if (unlikely(cur_event_list == NULL))
		return;
	
	cur_event = &(cur_event_list->event);

	// Set the gpio pin to the value planned for the current event. If the value indicates toggling, do so:
	if (unlikely((cur_event->value & T) > 0)) {
		// Toggle the GPIO.
		// Get current value of GPIO and invert it:
		if (gpio_get_value(cur_event->gpio)) {
			gpio_set_value(cur_event->gpio, 0);
			cur_event->value = T | 0;
		} else {
			gpio_set_value(cur_event->gpio, 1);
			cur_event->value = T | 1;
		}
	} else {
		// No toggling, so just set the GPIO:
		gpio_set_value(cur_event->gpio, cur_event->value);
	}

	// Get timestamp:
	irq_bh_tasklet.data = OSCR;

	tasklet_schedule(&irq_bh_tasklet);
} // gpio_setting_ostimer_expired
EXPORT_SYMBOL_GPL(gpio_setting_ostimer_expired);

/**************************************************************************************//**
 *
 * helper function to handle executed events
 * 
 * events are removed or rescheduled
 *
 *****************************************************************************************/
static inline void __to_kfifo_and_reschedule(struct gpioset_event_list * event_list_item) {
	struct gpioset_event *cur_event;
	int gpio_val;
	
	cur_event = &(event_list_item->event);
	
	// write to fifo
	gpio_val = cur_event->value;
	cur_event->value &= ~T;
	if (unlikely(!kfifo_in(&event_fifo, cur_event, sizeof(struct gpioset_event)))) {
		kfifo_full_count++;
	}
	
	// Remove event from event queue:
	if (&(event_list_item->list) !=(&event_queue)->next)
		printk("BUG: current event != first entry\n");
	else
		list_del((&event_queue)->next);
	
	// Re-schedule periodic events
	if (event_list_item->count > 1) {
		cur_event->value = gpio_val;
		event_list_item->count--;
		cur_event->time_planned.tv_usec += event_list_item->interval % 1000000;
		cur_event->time_planned.tv_sec += event_list_item->interval / 1000000;
		if (cur_event->time_planned.tv_usec >= 1000000) {
			cur_event->time_planned.tv_sec++;
			cur_event->time_planned.tv_usec -= 1000000;
		}
		// add it again
		list_add_sorted(event_list_item);
	}
	else {
		//remove event
		kfree(event_list_item);
	}
	// Schedule next event:
	schedule_next_event();

}

/**************************************************************************************//**
 *
 * Bottom half of IRQ handler
 *
 * @param data Data to handle
 *
 *****************************************************************************************/
static void tasklet_handler(unsigned long data) {
	struct gpioset_event *cur_event;
	struct timeval tv_now;
	uint32_t oscr_overhead, interval_secs, interval_usecs;
	cur_event = &(cur_event_list->event);

	// Calculate timestamp:
	do_gettimeofday(&tv_now);
	oscr_overhead = OSCR - data;
	oscr_overhead = ((u32)(((u64)oscr_overhead * (u64)oscr_mult) >> OSCR_SHIFT)) / 1000;
	interval_secs = oscr_overhead / 1000000;
	interval_usecs = oscr_overhead - (interval_secs*1000000);
	cur_event->time_executed.tv_sec = tv_now.tv_sec - interval_secs;
	if (tv_now.tv_usec < interval_usecs) {
		cur_event->time_executed.tv_sec --;
		cur_event->time_executed.tv_usec = 1000000 + (tv_now.tv_usec - interval_usecs);
	} else {
		cur_event->time_executed.tv_usec = tv_now.tv_usec - interval_usecs;
	}

	__to_kfifo_and_reschedule(cur_event_list);

	// Wake up sleeping readers:
	wake_up_interruptible(&wq_dbbuf);
} // tasklet_handler



/**************************************************************************************//**
 *
 * Subtract the `struct timeval' values X and Y, Store the result in RESULT.
 *
 * @param[in] x Minor
 * @param[in] y Diminor
 * @param[out] result Result of the subtraction
 *
 * @return Return 1 if the difference is negative, otherwise 0
 *
 *****************************************************************************************/
static int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
	// Perform the carry for the later subtraction by updating y.
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	//Compute the time remaining to wait. tv_usec is certainly positive.
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	// Return 1 if result is negative.
	return x->tv_sec < y->tv_sec;
}


/**************************************************************************************//**
 *
 * IOCTL handler for user space commands (for list device file)
 *
 * @param inode Inode
 * @param filp File pointer
 * @param cmd Command to give
 * @param arg Argument(s) *
 *
 * @return Zero for success, negative error number otherwise
 *
 *****************************************************************************************/
static int gpio_setting_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Local variables */
	int ret_val;
	long *arg_val;
	struct timeval tv;
	struct gpioset_event_list *eventlist;
	struct gpioset_event *event;
	struct timeval tv_now;
	unsigned long list_lock_flags;

	// Extract type and number bitfield. If wrong cmd, return ENOTTY (inappropriate ioctl):
	if (_IOC_TYPE(cmd) != GPIO_SETTING_IOCTL_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > GPIO_SETTING_IOCTL_MAXNR) return -ENOTTY;

	// See which cmd it was and act accordingly:
	switch (cmd) {
	case GPIO_SETTING_IOCTL_ADD:
		// Get the array with the integers:
		arg_val = (long *) arg;

		// Fill the struct:
		eventlist = kmalloc(sizeof(struct gpioset_event_list), GFP_KERNEL);
		if (eventlist == NULL)
			return -ENOMEM;
		event = &(eventlist->event);
		event->gpio = (int) arg_val[0];
		event->value = (int) arg_val[1];
		event->time_planned.tv_sec = arg_val[2];
		event->time_planned.tv_usec = arg_val[3];
		eventlist->interval = arg_val[4];
		eventlist->count = arg_val[5];

		// Check the planned time for the job: it either has to be in the future or planned for immediate start. If it is planned for the future, check if it is valid:
		if ((event->time_planned.tv_sec != 0) || (event->time_planned.tv_usec != 0)) {
			// Job is planned for the future:
			do_gettimeofday(&tv_now);
			if ((tv_now.tv_sec > event->time_planned.tv_sec) || ((tv_now.tv_sec == event->time_planned.tv_sec) && (tv_now.tv_usec > event->time_planned.tv_usec))) {
				// There is no use to schedule the event because we already missed it. Hence, return:
				kfree(eventlist);
				return ERRMISSEDEVENT;
			}
		}

		// Insert the struct into the list (this will also trigger a scheduling of the event queue):
		spin_lock_irqsave(&(list_lock), list_lock_flags);
		list_add_sorted(eventlist);
		spin_unlock_irqrestore(&(list_lock), list_lock_flags);
		return SUCCESS;
		break;

	case GPIO_SETTING_IOCTL_REMOVE:
		// Get the array with the integers:
		arg_val = (long *) arg;

		// Fill the timeval struct:
		tv.tv_sec = arg_val[2];
		tv.tv_usec = arg_val[3];

		// Remove the struct from the event queue (this will also trigger a scheduling of the event queue):
		spin_lock_irqsave(&(list_lock), list_lock_flags);
		ret_val = list_remove((int) arg_val[0], (int) arg_val[1], tv);
		spin_unlock_irqrestore(&(list_lock), list_lock_flags);

		// Return data to userspace:
		return ret_val;
		break;

	case GPIO_SETTING_IOCTL_REMOVEALL:
		spin_lock_irqsave(&(list_lock), list_lock_flags);
		ret_val = list_removeall();
		spin_unlock_irqrestore(&(list_lock), list_lock_flags);
		return ret_val;
		break;

	case GPIO_SETTING_IOCTL_FLUSH:
		// lock list, just to be sure that the interrupt handler does not access the fifo
		spin_lock_irqsave(&(list_lock), list_lock_flags);
		kfifo_reset(&event_fifo);
		spin_unlock_irqrestore(&(list_lock), list_lock_flags);
		break;

	case GPIO_SETTING_IOCTL_ERRCNT:
		return kfifo_full_count;
		break;

	case GPIO_SETTING_IOCTL_ERRRST:
		// Reset kfifo full counter:
		kfifo_full_count = 0;
		return SUCCESS;
		break;

	default:
		printk(KERN_ERR "%s: ERROR: Requested IOCTL not found.\n", GPIO_SETTING_KM_NAME);
		return -ENOTTY;
	}

	return 0;
} // gpio_setting_ioctl


/**************************************************************************************//**
 *
 * Function is executed when a user opens the list device file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Zero for success, negative error number otherwise
 *
 *****************************************************************************************/
static int gpio_setting_dev_list_open(struct inode *inode, struct file *filp)
{
	if (device_list_open)
		return -EBUSY;
	device_list_open++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	dev_list_read = 0;

	try_module_get(THIS_MODULE);

	return 0;
} // gpio_setting_dev_list_open


/**************************************************************************************//**
 *
 * Function is executed when a user reads the list device file
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length length
 * @param offset Offset
 *
 * @return On success, return read bytes. Otherwise return negative error code
 *
 *****************************************************************************************/
static ssize_t gpio_setting_dev_list_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	unsigned int ret, count = 0;
	struct gpioset_event_list *eventlist_ptr;
	struct list_head *list_ptr, *list_safe_ptr;
	unsigned long list_lock_flags;

	if (dev_list_read == 0) {
		spin_lock_irqsave(&(list_lock), list_lock_flags);
		if (!list_empty(&event_queue)) {
			// Put list contents into buffer:
			list_for_each_safe(list_ptr, list_safe_ptr, &event_queue) {
				eventlist_ptr = list_entry(list_ptr, struct gpioset_event_list, list);
				if ((ret = copy_to_user(__user buffer+count, &(eventlist_ptr->event), sizeof(struct gpioset_event)))) {
					spin_unlock_irqrestore(&(list_lock), list_lock_flags);
					return -EFAULT;
				}
				count += sizeof(struct gpioset_event)-ret;
			}
		}
		dev_list_read = 1;
		spin_unlock_irqrestore(&(list_lock), list_lock_flags);
	}

	return count;
} // gpio_setting_dev_list_read


/**************************************************************************************//**
 *
 * Function is executed when a user closes the list device file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int gpio_setting_dev_list_release(struct inode *inode, struct file *filp)
{
	device_list_open--;

	module_put(THIS_MODULE);

	return 0;
} // gpio_setting_dev_list_release



/**************************************************************************************//**
 *
 * Function is executed when a user opens the dbbuf device file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Zero for success, negative error number otherwise
 *
 *****************************************************************************************/
static int gpio_setting_dev_dbbuf_open(struct inode *inode, struct file *filp)
{
	if (device_dbbuf_open)
		return -EBUSY;
	device_dbbuf_open++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	try_module_get(THIS_MODULE);

	return 0;
} // gpio_setting_dev_dbbuf_open


/**************************************************************************************//**
 *
 * Function is executed when a user reads the dbbuf device file
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length length
 * @param offset Offset
 *
 * @return On success, return read bytes. Otherwise return negative error code
 *
 *****************************************************************************************/
static ssize_t gpio_setting_dev_dbbuf_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	unsigned int copied;
	int ret;

	wait_event_interruptible(wq_dbbuf, !kfifo_is_empty(&event_fifo));
	ret = kfifo_to_user(&event_fifo, buffer, length, &copied);

	return ret ? ret : copied;
} // gpio_setting_dev_dbbuf_read


/**************************************************************************************//**
 *
 * Function is executed when a user closes the dbbuf device file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int gpio_setting_dev_dbbuf_release(struct inode *inode, struct file *filp)
{
	device_dbbuf_open--;

	// Print kfifo_full count if it occurred:
	if (kfifo_full_count > 0) {
		printk(KERN_ERR "%s: kfifo_full_count=%u\n", GPIO_SETTING_KM_NAME, kfifo_full_count);
	}

	module_put(THIS_MODULE);

	return 0;
} // gpio_setting_dev_dbbuf_release


/**************************************************************************************//**
 *
 * Called to perform module cleanup when the module is unloaded
 *
 *****************************************************************************************/
void cleanup_module(void)
{
	cleanup(INIT_DONE);
}

void cleanup(int init_state)
{
	printk(KERN_INFO "%s: FlockLab service GPIO setting unloading...\n", GPIO_SETTING_KM_NAME);

	switch (init_state){
	case INIT_DONE:
	case INIT_KERNEL_TIMER:
		// Disable the OS timer:
		flocklab_ostimer_fnct_disable(GPIO_SETTING_OST_OFFSET);
		printk(KERN_DEBUG "%s: OS timer disabled.\n", GPIO_SETTING_KM_NAME);

		// Disable the kernel timer:
		if ( timer_pending(&kerneltimer) ) {
			del_timer(&kerneltimer);
			printk(KERN_DEBUG "%s: Kernel timer reset.\n", GPIO_SETTING_KM_NAME);
		}

	case INIT_KFIFO:
		// Free memory of ring buffer:
		kfifo_free(&event_fifo);
		printk(KERN_DEBUG "%s: Kfifo destroyed and freed.\n", GPIO_SETTING_KM_NAME);

	case INIT_EVENT_QUEUE:
		// Destroy event queue:
		list_removeall();
		printk(KERN_DEBUG "%s: Event queue destroyed.\n", GPIO_SETTING_KM_NAME);

		// Unregister dbbuf device:
	case INIT_DBBUF_CLASS:
		device_destroy( dev_dbbuf_class, dev_dbbuf_nr );
		class_destroy( dev_dbbuf_class );
	case INIT_DBBUF_ADD:
		cdev_del( &dev_dbbuf_cdev );
	case INIT_DBBUF_CHRDEV:
		unregister_chrdev_region( dev_dbbuf_nr, 1 );
		printk(KERN_DEBUG "%s: Dbbuf device unregistered.\n", GPIO_SETTING_KM_NAME);

		// Unregister list device:
	case INIT_LIST_CLASS:
		device_destroy( dev_list_class, dev_list_nr );
		class_destroy( dev_list_class );
	case INIT_LIST_ADD:
		cdev_del( &dev_list_cdev );
	case INIT_LIST_CHRDEV:
		unregister_chrdev_region( dev_list_nr, 1 );
		printk(KERN_DEBUG "%s: List device unregistered.\n", GPIO_SETTING_KM_NAME);

	case INIT_GPIO:
		/* Free the GPIO pins used */
		// Not needed as they are only configured in init_module() and not requested via request_gpio()
		break;
	}
	printk(KERN_INFO "%s: FlockLab service GPIO setting unloaded.\n", GPIO_SETTING_KM_NAME);
} // cleanup_module
