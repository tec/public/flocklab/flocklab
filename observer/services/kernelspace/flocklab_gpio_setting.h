/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains definitions for the kernelspace programs of the
 * FlockLab service "GPIO setting".
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_GPIO_SETTING_H
#define FLOCKLAB_GPIO_SETTING_H



/* ---- Public Prototypes --------------------------------------------------*/
void gpio_setting_add(unsigned int gpio, int value, struct timeval time_planned, unsigned int period, unsigned int count);									// Wrapper for list_add_sorted()
void gpio_setting_ostimer_expired(void);



/* ---- Include Guard ---------------------------------------------------- */
#endif
