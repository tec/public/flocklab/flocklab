/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains shared definitions for both the userspace and the
 * kernelspace programs of the FLOCKLAB service "GPIO setting".
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_GPIO_SETTING_SHARED_H
#define FLOCKLAB_GPIO_SETTING_SHARED_H


/* ---- Include Files ---------------------------------------------------- */
#include "flocklab_shared.h"

/* ---- Constants ----------------------------------------------------------*/

// Magic number for this kernel module for registering ioctl commands
#define GPIO_SETTING_IOCTL_MAGIC  	'O'
// Set up ioctl command numbers:
#define GPIO_SETTING_IOCTL_ADD			_IOWR(GPIO_SETTING_IOCTL_MAGIC,	0, long[4])		// IOCTL to add a GPIO setting job.
#define GPIO_SETTING_IOCTL_REMOVE		_IOWR(GPIO_SETTING_IOCTL_MAGIC,	1, long[4])		// IOCTL to remove a single GPIO setting job.
#define GPIO_SETTING_IOCTL_REMOVEALL	_IOR (GPIO_SETTING_IOCTL_MAGIC,	2, int)			// IOCTL to remove all GPIO setting jobs.
#define GPIO_SETTING_IOCTL_FLUSH		_IOR (GPIO_SETTING_IOCTL_MAGIC,	3, int)			// IOCTL to flush the output buffer for the setting results.
#define GPIO_SETTING_IOCTL_ERRCNT		_IOR (GPIO_SETTING_IOCTL_MAGIC,	4, int)			// IOCTL to show error statistics
#define GPIO_SETTING_IOCTL_ERRRST		_IOR (GPIO_SETTING_IOCTL_MAGIC,	5, int)			// IOCTL to reset error statistics
#define GPIO_SETTING_IOCTL_MAXNR		6												// Denotes maximum number of cmd's above

// Timers:
#define SWITCHTIMER		1000000	// Time in [us] after which the kernel timer should be used instead of the OS timer.

// Size of ring buffers that holds the processed events:
#define SIZEOFEVENTBUF	409
// Return values of functions:
#ifndef SUCCESS
#define SUCCESS			0															// Indicates success of an operation.
#endif
#define ERRMISSEDEVENT	2															// Returned by schedule_next_event()  or gpio_setting_ioctl() if we miss an event because it is already too late to process it.


/* ---- Global variables ---------------------------------------------------*/

/**
 * Struct that holds all data needed for setting a pin. Is used in userspace and in kernel space.
 */
struct gpioset_event {
	unsigned int gpio;										// Pin number which is to be set.
	int value;												// Value the pin should be set to. Should be 0(clear) or 1(set).
	struct timeval time_planned;							// Absolute time at which the pin is planned to be set.
	struct timeval time_executed;							// Timestamp at which the pin was actually set. This field is filled by the functions irq_handler() and schedule_next_event().
};



/* ---- Include Guard ---------------------------------------------------- */
#endif

