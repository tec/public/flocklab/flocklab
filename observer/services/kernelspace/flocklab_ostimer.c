/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This kernelmodule provides OS timer functionality to the FlockLab v2 @ ETH Zurich,
 * Switzerland.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */




/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>					/* Needed by all modules */
#include <linux/kernel.h>					/* Needed for KERN_INFO */
#include <linux/time.h>					/* Time functions */
#include <linux/timer.h>					/* Kernel timer functions */
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <mach/regs-ost.h>					/* Register definitions */
#include <mach/hardware.h>					/* Register functions */
#include "flocklab_shared.h"
#include "flocklab_ostimer.h"
#include "flocklab_powerprof.h"
#include "flocklab_gpio_setting.h"


/* ---- Global variables --------------------------------------------------*/
#define MODULE_NAME OSTIMER_KM_NAME


/* ---- Prototypes --------------------------------------------------------*/
static irqreturn_t __ossr_interrupt_handler(int, void*);



/**************************************************************************************//**
 *
 * Interrupt handler which is called when the OS kernel timer fires.
 *
 * @param irq IRQ number
 * @param dev_id Device ID
 *
 * @return no return value
 *
 *****************************************************************************************/
static irqreturn_t __ossr_interrupt_handler(int irq, void *dev_id)
{
	int was_for_us = 0;
	// Check who caused the interrupt and call the respective handlers:
	if (read_bit_OSSR(POWERPROF_STRT_OST_OFFSET)) {
		powerprof_start_timer_expired();
		was_for_us = 1;
	}
	if (read_bit_OSSR(POWERPROF_STP_OST_OFFSET)) {
		powerprof_stop_timer_expired();
		was_for_us = 1;
	}
	if (read_bit_OSSR(GPIO_SETTING_OST_OFFSET)) {
		gpio_setting_ostimer_expired();
		was_for_us = 1;
	}

	if (was_for_us) {
		// Clear OSSR bit by setting any bit:
		set_bit_OSSR(0);
		return IRQ_HANDLED;
	} else {
		return IRQ_NONE;
	}
} // __ossr_interrupt_handler


/* ---- Init and Cleanup ---------------------------------------------------------*/
/**
 * Initialization function for the kernel module
 */
static int __init ostimer_init(void)
{
	/* Local variables	*/
	int rs;

	printk(KERN_INFO MODULE_NAME ": Module starting...\n");

	// Register interrupt handler for os timer
	if (( rs = request_irq(7, __ossr_interrupt_handler, 0, "FlockLab OSTimer", NULL)) < 0 ) {
		printk(KERN_ERR MODULE_NAME ": Cannot register interrupt line 7. Module could not start.\n");
		return 0;
	}
	printk(KERN_DEBUG MODULE_NAME ": Interrupt handler for IRQ 7 registered.\n");


	printk(KERN_INFO MODULE_NAME ": Module started.\n");

	return 0;
} // init_module
module_init(ostimer_init);


/**
 * Destruction function for the kernel module
 */
static void __exit ostimer_exit(void)
{
	printk(KERN_INFO MODULE_NAME ": Module unloading...\n");

	// Free interrupt line 7:
	free_irq(7, NULL);
	printk(KERN_DEBUG MODULE_NAME ": IRQ released.\n");
	printk(KERN_INFO MODULE_NAME ": Module unloaded.\n");

} // cleanup_module
module_exit(ostimer_exit);


/* ---- Module documentation and authoring information --------------------*/
MODULE_AUTHOR("ETH Zurich, 2012, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("OS timer interrupt handling module for FlockLab v2 @ ETH Zurich, Switzerland.");
MODULE_SUPPORTED_DEVICE("FlockBoard rev. 1.1");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
