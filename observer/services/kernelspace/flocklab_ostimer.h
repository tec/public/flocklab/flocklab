/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains definitions for the OS timer convenience kernel module
 * of the FlockLab testbed.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_OSTIMER_H
#define FLOCKLAB_OSTIMER_H

#include <linux/io.h>

#define DEFINE_OS_REG_SINGLE(reg) \
static inline u32 read_bit_##reg(u8 off) \
{ return (reg & (1<<(4+off))); } \
\
static inline void set_bit_##reg(u8 off) \
{ reg |= (1<<(4+off)); } \
\
static inline void clear_bit_##reg(u8 off) \
{ reg &= ~(1<<(4+off)); }
DEFINE_OS_REG_SINGLE(OSSR)
DEFINE_OS_REG_SINGLE(OIER)

#define DEFINE_OS_REG_MULTI(reg) \
static inline void set_bit_offset_##reg(u8 reg_off, u8 bit_off) \
{ __raw_writel(__raw_readl(((void __iomem *)&reg)+(4*reg_off)) | (1<<bit_off), ((void __iomem *)&reg)+(4*reg_off)); }\
\
static inline void clear_bit_offset_##reg(u8 reg_off, u8 bit_off) \
{ __raw_writel(__raw_readl(((void __iomem *)&reg)+(4*reg_off)) & (~(1<<bit_off)), ((void __iomem *)&reg)+(4*reg_off)); }\
\
static inline void write_offset_##reg(u8 off, u32 val) \
{ __raw_writel(val, ((void __iomem *)&reg)+(4*off)); }
DEFINE_OS_REG_MULTI(OSMR4)
DEFINE_OS_REG_MULTI(OMCR4)
DEFINE_OS_REG_MULTI(OSCR4)

/* ---- Include Guard -------------------------------------------------------- */
#endif
