/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This kernelmodule provides OS timer functionality to the FlockLab v2 @ ETH Zurich,
 * Switzerland.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */




/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>					/* Needed by all modules */
#include <linux/kernel.h>					/* Needed for KERN_INFO */
#include <linux/time.h>					/* Time functions */
#include <linux/timer.h>					/* Kernel timer functions */
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <mach/regs-ost.h>					/* Register definitions */
#include <mach/hardware.h>					/* Register functions */
#include "flocklab_shared.h"
#include "flocklab_ostimer.h"
#include "flocklab_ostimer_fnct.h"


/* ---- Global variables --------------------------------------------------*/
#define MODULE_NAME OSTIMERFNCT_KM_NAME



/**************************************************************************************//**
 *
 * Function for setting an OS timer.
 *
 * This function configures the requested OS timer for the desired wait time.
 * The register descriptions and definitions can be found in
 * "Intel(r) PXA27x Processor Family Developer's Manual"
 *
 * @param kerneltimer Optional kernel timer to delete
 * @param wait_time_us Time to set the timer to
 * @param ost_reg_offset OS timer register offset as defined in flocklab_shared.h
 *
 *****************************************************************************************/
void flocklab_ostimer_fnct_set(struct timer_list *kerneltimer, unsigned long wait_time_us, int ost_reg_offset)
{
	// If a kernel timer is given and set, delete it:
	if ( kerneltimer && timer_pending(kerneltimer) ) {
		del_timer(kerneltimer);
	}

	// Set bit in OS timer interrupt enable register (OIER). If set, a match between OSRMx and the OS timer results in an assertion of the interrupt bit Mx in the OSSR.
	set_bit_OIER(ost_reg_offset);

	//Write OS timer match register (OSMRx). This is the register where we can set the wait time for the timer.
	write_offset_OSMR4(ost_reg_offset, wait_time_us);

	// Set OS match control register (OMCRx). This register is used to configure the timer.
	set_bit_offset_OMCR4(ost_reg_offset, 7);		// Channel x is compared to OSCRx and a write to OSCRx starts the channel
	clear_bit_offset_OMCR4(ost_reg_offset, 6);		// The channel stops incrementing after detecting a match
	set_bit_offset_OMCR4(ost_reg_offset, 3);		// Reset OSCRx on match
	set_bit_offset_OMCR4(ost_reg_offset, 2);		// Set timer to microsecond resolution (clock increments are derived from 13Mhz clock). Bits 2:0 are set to 0b100
	clear_bit_offset_OMCR4(ost_reg_offset, 1);		// Set timer to microsecond resolution (clock increments are derived from 13Mhz clock). Bits 2:0 are set to 0b100
	clear_bit_offset_OMCR4(ost_reg_offset, 0);		// Set timer to microsecond resolution (clock increments are derived from 13Mhz clock). Bits 2:0 are set to 0b100

	// Write to OSCRx to activate timer:
	write_offset_OSCR4(ost_reg_offset, 0);

} // flocklab_ostimer_fnct_set
EXPORT_SYMBOL_GPL(flocklab_ostimer_fnct_set);




/**************************************************************************************//**
 *
 * Function for disabling an OS timer.
 *
 * The register descriptions and definitions can be found in
 * "Intel(r) PXA27x Processor Family Developer's Manual"
 *
 * @param ost_reg_offset OS timer register offset as defined in flocklab_shared.h
 *
 *****************************************************************************************/
void flocklab_ostimer_fnct_disable(int ost_reg_offset)
{
	// Clear bit in OS timer interrupt enable register (OIER).
	clear_bit_OIER(ost_reg_offset);

	// Reset timer resolution bits in OS match control register (OMCRx) to 0b000. This disables the timer:
	clear_bit_offset_OMCR4(ost_reg_offset, 2);
	clear_bit_offset_OMCR4(ost_reg_offset, 1);
	clear_bit_offset_OMCR4(ost_reg_offset, 0);

} // flocklab_ostimer_fnct_disable
EXPORT_SYMBOL_GPL(flocklab_ostimer_fnct_disable);



/* ---- Init and Cleanup ---------------------------------------------------------*/
/**
 * Initialization function for the kernel module
 */
static int __init ostimer_init(void)
{
	printk(KERN_INFO MODULE_NAME ": Module starting...\n");

	printk(KERN_INFO MODULE_NAME ": Module started.\n");

	return 0;
} // init_module
module_init(ostimer_init);


/**
 * Destruction function for the kernel module
 */
static void __exit ostimer_exit(void)
{
	printk(KERN_INFO MODULE_NAME ": Module unloading...\n");

	printk(KERN_INFO MODULE_NAME ": Module unloaded.\n");

} // cleanup_module
module_exit(ostimer_exit);


/* ---- Module documentation and authoring information --------------------*/
MODULE_AUTHOR("ETH Zurich, 2012, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("OS timer convenience functions module for FlockLab v2 @ ETH Zurich, Switzerland.");
MODULE_SUPPORTED_DEVICE("FlockBoard rev. 1.1");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
