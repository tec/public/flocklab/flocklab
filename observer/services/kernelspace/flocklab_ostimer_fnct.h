/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains definitions for the OS timer convenience kernel module
 * of the FlockLab testbed.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_OSTIMER_FNCT_H
#define FLOCKLAB_OSTIMER_FNCT_H

/* ---- Prototypes of exported functions for shared use in other kernel modules ---------------*/
void flocklab_ostimer_fnct_set(struct timer_list*, unsigned long, int);
void flocklab_ostimer_fnct_disable(int);


/* ---- Include Guard -------------------------------------------------------- */
#endif
