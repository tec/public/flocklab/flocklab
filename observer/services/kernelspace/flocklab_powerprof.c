/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This kernelmodule provides functionality for the FlockLab v2 @ ETH Zurich,
 * Switzerland. It implements the powerprofiling service which allows a
 * user to monitor the power consumption of attached target nodes.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */




/* ---- Include Files ---------------------------------------------------- */
#include <linux/module.h>					/* Needed by all modules */
#include <linux/kernel.h>					/* Needed for KERN_INFO */
#include <linux/time.h>					/* Time functions */
#include <linux/timer.h>					/* Kernel timer functions */
#include <linux/string.h>					/* String handling */
#include <linux/ioctl.h>					/* Needed for IOCTL */
#include <linux/fs.h>						/* Needed for device registering */
#include <linux/uaccess.h>					/* Needed for exchanging data between kernel and user space over /dev */
#include <linux/cdev.h>					/* Needed for registering device */
#include <linux/device.h>					/* Needed for registering device */
#include <linux/gpio.h>					/* GPIO functions */
#include <linux/slab.h>					/* Needed for slab cache */
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/spi/spi.h>
#include <linux/interrupt.h>
#include <linux/vmalloc.h>
#include <mach/regs-ost.h>					/* Register definitions */
#include <mach/hardware.h>					/* Register functions */
#include "flocklab_shared.h"
#include "flocklab_powerprof.h"
#include "flocklab_ostimer_fnct.h"
#include "ads1271.h"


/* ---- Module documentation and authoring information --------------------*/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("ETH Zurich, 2009-2012, Christoph Walser <walserc@tik.ee.ethz.ch>");
MODULE_DESCRIPTION("Powerprofiling service for FlockLab v2 @ ETH Zurich, Switzerland.");
MODULE_SUPPORTED_DEVICE("FlockBoard rev. 1.1 with Analog-Digital-Converter ADS1271");
MODULE_VERSION("3.2");

#define MODULE_NAME POWERPROF_KM_NAME

/* ---- Global variables --------------------------------------------------*/
static struct powerprof_dev devlist;
static struct powerprof_dev devdbbuf;
static struct powerprof_drv pp_drv;



/* ---- Prototypes --------------------------------------------------------*/
void cleanup(int init_state);
static int powerprof_dev_list_open(struct inode*, struct file*);
static int powerprof_dev_list_release(struct inode*, struct file*);
static ssize_t powerprof_dev_list_read(struct file*, char*, size_t, loff_t*);
static int powerprof_dev_ioctl(struct inode*, struct file*, unsigned int, unsigned long);
static int powerprof_dev_dbbuf_open(struct inode*, struct file*);
static int powerprof_dev_dbbuf_release(struct inode*, struct file*);
static ssize_t powerprof_dev_dbbuf_read(struct file*, char*, size_t, loff_t*);
static int powerprof_schedule_remove(struct powerprof_job*);
static int __match_ads1271_dev(struct device*, void*);
static int __timeval_subtract(struct timeval*, struct timeval*, struct timeval*);
static int __packet_fetcher_kthread(void*);
static void __list_add_sorted(struct powerprof_job*);
static int __schedule_next_starttimer(void);
static void __schedule_next_stoptimer(void);


/* ---- Enums to for init / cleanup ---------------------------------------------*/
enum {
    INIT_START,
    INIT_LIST_CHRDEV,
    INIT_LIST_ADD,
    INIT_LIST_CLASS,
    INIT_DBBUF_CHRDEV,
    INIT_DBBUF_ADD,
    INIT_DBBUF_CLASS,
    INIT_FIFOLISTS,
    INIT_ADS1271,
    INIT_KTHREAD,
    INIT_EVENT_QUEUE,
    INIT_TIMER,
    INIT_DONE,
};


/* ---- Helper functions --------------------------------------------------*/

/**************************************************************************************//**
 *
 * Subtract the struct timeval values x and y (x-y), store the result in result.
 *
 * @param[in] x Minor
 * @param[in] y Diminor
 * @param[out] result Result of the subtraction
 *
 * @return Return 1 if the difference is negative, otherwise 0
 *
 *****************************************************************************************/
static int __timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
  // TODO: do not modify pointer values!
  // Perform the carry for the later subtraction by updating y.
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (x->tv_usec - y->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  //Compute the time remaining to wait. tv_usec is certainly positive.
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  // Return 1 if result is negative.
  return x->tv_sec < y->tv_sec;
} // __timeval_subtract


/**************************************************************************************//**
 *
 * Find the ads1271 device
 *
 * @param[in] dev Device to try to match
 * @param[in] data
 * @param[out] result Result of the subtraction
 *
 * @return Return 1 if the difference is negative, otherwise 0
 *
 *****************************************************************************************/
static int __match_ads1271_dev(struct device *dev, void *data){
	if (strncmp(dev_name(dev), data, strlen(data)) != 0) {
		(&pp_drv)->ads1271_dev = device_find_child(dev, data, __match_ads1271_dev);
		return 0;
	} else {
		return dev->devt;
	}
} // __match_ads1271_dev


/**************************************************************************************//**
 *
 * Kthread which retrieves packets from the ADS1271 driver
 *
 *
 *****************************************************************************************/
static int __packet_fetcher_kthread(void *data)
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_sample_pckt *sample_packet;
  struct powerprof_buffer_list *dst;
	int ret;
	int pck_len;

    printk(KERN_INFO "%s: Kthread started.\n", POWERPROF_KM_NAME);
	sample_packet = vmalloc(sizeof(struct powerprof_sample_pckt));

	// Get samples from ADS1271 driver as long as needed:
	while (!kthread_should_stop()) {
		// Get data from ADS1271 driver:
		ret = ads1271_read_internal(drv->ads1271_driver, (char *) &(sample_packet->sample_buf), sizeof(sample_packet->sample_buf));

		if (likely(ret == 0)) {
			continue;
		} else if (unlikely(ret < 0)) {
			printk(KERN_ERR POWERPROF_KM_NAME ": ADS1271 driver returned error %d.\n", ret);
			continue;
		}

		// Get timestamps:
		if (drv->ads1271_driver->packet_start.tv_sec != 0) {
			sample_packet->sampling_start.tv_sec = drv->ads1271_driver->packet_start.tv_sec;
			sample_packet->sampling_start.tv_usec = drv->ads1271_driver->packet_start.tv_usec;
		} else {
			sample_packet->sampling_start.tv_sec = 0;
		}
		if (drv->ads1271_driver->packet_end.tv_sec != 0) {
			sample_packet->sampling_end.tv_sec = drv->ads1271_driver->packet_end.tv_sec;
			sample_packet->sampling_end.tv_usec = drv->ads1271_driver->packet_end.tv_usec;
		} else {
			sample_packet->sampling_end.tv_sec = 0;
		}

		// Calculate sample count:
		sample_packet->sample_count = ret/sizeof(u32);
		pck_len = offsetof(struct powerprof_sample_pckt, sample_buf) + ret;

		// Put packet into fifo (drop packet if not enough space in fifo):    
    mutex_lock(&(drv->list_mutex));
		if (list_empty(&(drv->samplebuf_free))) {
			drv->kfifo_full_occurred++;
      mutex_unlock(&(drv->list_mutex));
			continue;
		}
		else {
      dst = list_first_entry(&(drv->samplebuf_free), struct powerprof_buffer_list, list);
      memcpy(&(dst->pckt), sample_packet, pck_len);
      list_move_tail(&(dst->list), &(drv->samplebuf_filled));
      // printk(KERN_DEBUG POWERPROF_KM_NAME ": fill buffer, %d bytes.\n", pck_len);
		}
		mutex_unlock(&(drv->list_mutex));

		// Wake up readers of FIFO:
		wake_up_interruptible(&(drv->samplepacket_fifo_wq));
	}

	vfree(sample_packet);

	printk(KERN_INFO "%s: Kthread stopped.\n", POWERPROF_KM_NAME);
	complete_and_exit(&drv->fetcher_completion, 0);
	return 0;
} // __packet_fetcher_kthread


/**************************************************************************************//**
 *
 * IOCTL handler for /dev/powerprof_list
 *
 * @param inode Inode
 * @param filp File pointer
 * @param cmd Command number of the ioctl to perform
 * @param arg Arguments passed from user space ioctl() function
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int powerprof_dev_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Local variables */
	struct powerprof_job job_temp;
	struct powerprof_job_conf jobconf;
	struct powerprof_drv *drv = &pp_drv;
	struct timeval now, diff;
	struct powerprof_errorstats errstats;
	int rs;

	// Extract type and number bitfield. If wrong cmd, return ENOTTY (inappropriate ioctl):
	if (_IOC_TYPE(cmd) != POWERPROF_IOCTL_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > POWERPROF_IOCTL_MAXNR) return -ENOTTY;

	// See which cmd it was and act accordingly:
	switch (cmd) {
		case POWERPROF_IOCTL_SCHEDULER_ADD:
			// Get the arguments from the user:
			if ( (copy_from_user(&jobconf, (unsigned int __user *) arg, sizeof(struct powerprof_job_conf))) != 0) {
				printk(KERN_ERR "%s: Error getting the arguments for POWERPROF_IOCTL_SCHEDULER_ADD.\n", POWERPROF_KM_NAME);
				return -ENOTTY;
			}
			// Check the planned time for the job: it either has to be in the future or be planned for immediate start. If it is planned for the future, check if it is valid:
			if ((jobconf.time_start.tv_sec != 0) || (jobconf.time_start.tv_usec != 0)) {
				// Job is planned for the future:
				do_gettimeofday(&now);
				__timeval_subtract(&diff, &jobconf.time_start, &now);
				if ((diff.tv_sec < 0) || ((diff.tv_sec == 0) && (diff.tv_usec <= 0))) {
					printk(KERN_ERR "%s: Error checking the arguments for POWERPROF_IOCTL_SCHEDULER_ADD: illegal value for time_start\n", POWERPROF_KM_NAME);
					return -EINVAL;
				}
			}
			// Set nthsample to default if not set:
			if (jobconf.nth_sample == 0) {
				jobconf.nth_sample = ADS1271_SAMPLE_DIVIDER_DEFAULT;
			}

			// Everything looks ok, thus add the job:
			powerprofile_add(jobconf.sampling_duration_us, jobconf.time_start, jobconf.nth_sample);

			return SUCCESS;
			break;

		case POWERPROF_IOCTL_SCHEDULER_REMOVE:
			// Get the arguments from the user:
			if ( (copy_from_user(&job_temp.conf, (unsigned int __user *) arg, sizeof(struct powerprof_job_conf))) != 0) {
				printk(KERN_ERR "%s: Error getting the arguments for POWERPROF_IOCTL_SCHEDULER_REMOVE.\n", POWERPROF_KM_NAME);
				return -ENOTTY;
			}

			// Remove the job from the schedule:
			rs = powerprof_schedule_remove(&job_temp);

			return rs;
			break;

		case POWERPROF_IOCTL_SCHEDULER_REMOVEALL:
			return powerprof_schedule_remove(NULL);
			break;

		case POWERPROF_IOCTL_BUFFER_FLUSH:
			rs = ads1271_flush(drv->ads1271_driver);
			// Wake up readers of FIFO:
			wake_up_interruptible(&(drv->samplepacket_fifo_wq));
			return rs;
			break;

		case POWERPROF_IOCTL_ERRCNT:
			errstats.kfifo_full_occured			= drv->ads1271_driver->kfifo_full_occured;
			errstats.rx_fifo_overrun_occured	= drv->ads1271_driver->rx_fifo_overrun_occured;
			errstats.rx_fifo_empty_occured		= drv->ads1271_driver->rx_fifo_empty_occured;
			if ((rs = copy_to_user((unsigned int __user *) arg, &errstats, sizeof(struct powerprof_errorstats)))) {
				return -EFAULT;
			} else {
				return SUCCESS;
			}
			break;

		case POWERPROF_IOCTL_ERRRST:
			// Reset error counters:
			drv->ads1271_driver->kfifo_full_occured			= 0;
			drv->ads1271_driver->rx_fifo_overrun_occured	= 0;
			drv->ads1271_driver->rx_fifo_empty_occured		= 0;
			return SUCCESS;
			break;

		default:
			printk(KERN_ERR "%s: Requested IOCTL not found.\n", POWERPROF_KM_NAME);
			return -ENOTTY;
	}

	return SUCCESS;
} // powerprof_dev_ioctl

/**************************************************************************************//**
 *
 * Function is executed when a user opens the dev file
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int powerprof_dev_list_open(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct powerprof_dev *dev = &devlist;

	if (dev->device_open_count)
		return -EBUSY;
	dev->device_open_count++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	// Reset the read flag:
	dev->dev_read = 0;

	try_module_get(THIS_MODULE);

	return SUCCESS;
} // powerprof_dev_list_open



/**************************************************************************************//**
 *
 * Function is executed when a user reads the dev file
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, negative error number otherwise
 *
 *****************************************************************************************/
static ssize_t powerprof_dev_list_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_dev *dev = &devlist;
	struct powerprof_job *job;
	struct list_head *iterator, *iterator_safe;
	unsigned int ret, count = 0;


	// Only output once: This is needed to prevent the file to be read over and over if accessed with 'cat'.
	if (dev->dev_read == 0) {
		if (!list_empty(&drv->schedule)) {
			// Put list contents into buffer:
			list_for_each_safe(iterator, iterator_safe, &drv->schedule) {
				job = list_entry(iterator, struct powerprof_job, schedule);
				if ((ret = copy_to_user(__user buffer+count, &(job->conf), sizeof(struct powerprof_job_conf))))
					return -EFAULT;
				count += sizeof(struct powerprof_job_conf)-ret;
			}
		}
		// Mark the device as read.
		dev->dev_read = 1;
	}
	return count;


} // powerprof_dev_list_read



/**************************************************************************************//**
 *
 * Function is executed when a user closes the dev file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int powerprof_dev_list_release(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct powerprof_dev *dev = &devlist;

	dev->device_open_count--;

	module_put(THIS_MODULE);

	return SUCCESS;
} // powerprof_dev_list_release


/**************************************************************************************//**
 *
 * Function is executed when a user opens the dev file
 *
 * @param inode Inode
 * @param filp file pointer
 *
 * @return 0 on success, negative error number else.
 *
 *****************************************************************************************/
static int powerprof_dev_dbbuf_open(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct powerprof_dev *dev = &devdbbuf;

	if (dev->device_open_count)
		return -EBUSY;
	dev->device_open_count++;

	// mark as non-seekable
	nonseekable_open(inode, filp);
	// mark as read-only -> necessary??
	filp->f_mode &= ~(FMODE_WRITE);

	// Reset the read flag:
	dev->dev_read = 0;

	try_module_get(THIS_MODULE);

	return SUCCESS;
} // powerprof_dev_dbbuf_open



/**************************************************************************************//**
 *
 * Function is executed when a user reads the dev file
 *
 * @param filp File pointer
 * @param buffer Buffer
 * @param length Length
 * @param offset Offset
 *
 * @return Number of read bytes on success, negative error number otherwise
 *
 *****************************************************************************************/
 
static ssize_t powerprof_dev_dbbuf_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_sample_pckt * pck;
  struct powerprof_buffer_list * src;
	int ret;
  int hasdata = 0;
	unsigned int tocopy;

	// Check if buffer passed by user is big enough:
	if (length < sizeof(struct powerprof_sample_pckt)) {
		printk(KERN_ERR POWERPROF_KM_NAME ": Buffer passed to %s is not large enough. Size was %d bytes but has to be at least %d bytes.\n", __FUNCTION__, length, sizeof(struct powerprof_sample_pckt));
		return -EFAULT;
	}

	// Wait for data and pass it to user:
	while (!hasdata) {
    ret = wait_event_interruptible(drv->samplepacket_fifo_wq, !list_empty(&(drv->samplebuf_filled)));
    if (ret < 0) { // interrupted
      printk(KERN_ERR POWERPROF_KM_NAME ": wait_event interrupted in %s.\n", __FUNCTION__);
      return 0;
    }
    mutex_lock(&(drv->list_mutex));
    hasdata = !list_empty(&(drv->samplebuf_filled));
    if (!hasdata) {
      mutex_unlock(&(drv->list_mutex));
    }
  }
	pck = (struct powerprof_sample_pckt *) buffer;
  src = list_first_entry(&(drv->samplebuf_filled), struct powerprof_buffer_list, list);
  
  tocopy = offsetof(struct powerprof_sample_pckt, sample_buf) + src->pckt.sample_count * sizeof(int);
  ret = copy_to_user(buffer, &(src->pckt), tocopy);
  if (ret > 0) { // not all copied
    mutex_unlock(&(drv->list_mutex));
    // this should not happen
    printk(KERN_ERR POWERPROF_KM_NAME ": Could not copy buffer (%d bytes) to user space in %s (remaining %d bytes).\n", tocopy, __FUNCTION__, ret);
    return -EFAULT;
  }
  list_move_tail(&(src->list), &(drv->samplebuf_free));
  mutex_unlock(&(drv->list_mutex));
  
	return tocopy;
} // powerprof_dev_dbbuf_read



/**************************************************************************************//**
 *
 * Function is executed when a user closes the dev file
 *
 * @param inode Inode
 * @param filp File pointer
 *
 * @return Always returns 0
 *
 *****************************************************************************************/
static int powerprof_dev_dbbuf_release(struct inode *inode, struct file *filp)
{
	/* Local variables */
	struct powerprof_dev *dev = &devdbbuf;

	dev->device_open_count--;

	module_put(THIS_MODULE);

	return SUCCESS;
} // powerprof_dev_dbbuf_release



/**************************************************************************************//**
 *
 * Bottom half of IRQ handler
 *
 * @param data Data to handle
 *
 *****************************************************************************************/
int __trigger_collection(unsigned long data)
{
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_job *job = drv->curr_sched_start;
	//DEBUG struct timeval now;

	// Remove the job from the schedule list:
	list_del(&job->schedule);

	// Change sampling rate if needed:
	if (unlikely(drv->ads1271_driver->nth_sample != job->conf.nth_sample)) {
		ads1271_change_samplingrate(drv->ads1271_driver, job->conf.nth_sample);
	}


	//DEBUG do_gettimeofday(&now);
	//DEBUG printk(KERN_ERR "Start at: %lu.%lu\t Planned at: %lu.%lu\tStop planned at %lu.%lu\n", now.tv_sec, now.tv_usec, job->conf.time_start.tv_sec, job->conf.time_start.tv_usec, job->conf.time_stop.tv_sec, job->conf.time_stop.tv_usec);

	// Start getting samples:
	ads1271_start_sampling(drv->ads1271_driver);

	drv->curr_sched_stop = job;
	drv->curr_sched_start = NULL;

	__schedule_next_stoptimer();
	__schedule_next_starttimer();

	return SUCCESS;
} // __trigger_collection



/**************************************************************************************//**
 *
 * This function is called when the start os timer fires.
 * Function is called from __ossr_interrupt_handler in flocklab_ostimer.c
 *
 * @return no return value
 *
 *****************************************************************************************/
void powerprof_start_timer_expired(void)
{
	struct powerprof_drv *drv = &pp_drv;

	// Schedule bottom half:
	tasklet_schedule(&drv->start_bh_tasklet);
} // powerprof_start_timer_expired
EXPORT_SYMBOL_GPL(powerprof_start_timer_expired);



/**************************************************************************************//**
 *
 * This function is called when a kernel timer fires. It stops sampling of the ADS1271.
 * Function is called from __ossr_interrupt_handler in flocklab_ostimer.c
 *
 *
 * @param ads1271_drv
 *
 * @return no return value
 *
 *****************************************************************************************/
void powerprof_stop_timer_expired(void)
{
	struct powerprof_drv *drv = &pp_drv;
	//DEBUG struct timeval now;

	//DEBUG do_gettimeofday(&now);
	//DEBUG printk(KERN_ERR "Stop at: %lu.%lu\n", now.tv_sec, now.tv_usec);

	// Free the memory for the job:
	kfree(drv->curr_sched_stop);
	ads1271_stop_sampling(drv->ads1271_driver);

} // powerprof_stop_timer_expired
EXPORT_SYMBOL_GPL(powerprof_stop_timer_expired);



/**************************************************************************************//**
 *
 * Function to (re-)schedule the stop of a sampling job.
 *
 * @return no return value
 *
 *****************************************************************************************/
static void __schedule_next_stoptimer()
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct timer_list *stoptimer = drv->stop_timer;
	struct timeval time_stop = drv->curr_sched_stop->conf.time_stop;
	unsigned long wait_time_us;
	struct timeval now, diff;

	// Start new timer for stop event after measurement.
	// Calculate the expiration of the timer in jiffies:
	do_gettimeofday(&now);

	__timeval_subtract(&diff, &time_stop, &now);
	wait_time_us = (diff.tv_sec*1000000)+diff.tv_usec;
	// If the wait time is long, schedule the event using the kernel timer, otherwise use the OS timer:
	if ( wait_time_us <= ADS1271_SWITCHTIMER ) {
		// Set OS timer to current event:
		flocklab_ostimer_fnct_set(stoptimer, wait_time_us, POWERPROF_STP_OST_OFFSET);
	} else {
		// If the wait time is longer in the future than 100*SWITCHTIMER, set the timer to only half the time needed to wait to enforce re-scheduling.
		// This is needed for ensuring that time synchronisation is taken into consideration when scheduling an event.
		if (wait_time_us > (100*ADS1271_SWITCHTIMER)) {
			diff.tv_sec = (long) diff.tv_sec/2;
		}
		// Check if a timer is already running. If yes, abort it:
		if ( unlikely(timer_pending(stoptimer)) ) {
			del_timer(stoptimer);
		}

		// Configure and start the kernel timer. Calculate the jiffies value at which the timer should expire. The timer should expire SWITCHTIMER microseconds before the event so it can be rescheduled using the OS timer (which is more accurate).
		stoptimer->expires = jiffies + timeval_to_jiffies(&diff) - (ADS1271_SWITCHTIMER*HZ/1000000);
		add_timer(stoptimer);
	}
} // __schedule_next_stoptimer


/**************************************************************************************//**
 *
 * Function to (re-)schedule the start of a sampling job.
 *
 * @return no return value
 *
 *****************************************************************************************/
static int __schedule_next_starttimer()
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_job *first_event;
	struct timeval now, diff;
	unsigned long wait_time_us;

	// Only proceed if the scheduling queue is not empty:
	if (likely(!list_empty(&drv->schedule))) {
		// Get first event from queue:
		first_event = list_entry((&drv->schedule)->next, struct powerprof_job, schedule);

		// If the currently scheduled event and the first event in the queue are the same, we do not need to do anything.
		// Furthermore, if the kernel timer is also running, we need not to do anything as well.
		// Otherwise, we need to reschedule;
		if ((drv->curr_sched_start != NULL) && (first_event->conf.time_start.tv_sec == drv->curr_sched_start->conf.time_start.tv_sec) && (first_event->conf.time_start.tv_usec == drv->curr_sched_start->conf.time_start.tv_usec) && (timer_pending(drv->start_timer)))
			return SUCCESS;
		//Re-schedule:
		drv->curr_sched_start = first_event;
		// If the job is in the future, start a timer. Otherwise, start sampling immediately.
		if ((drv->curr_sched_start->conf.time_start.tv_sec != 0) || (drv->curr_sched_start->conf.time_start.tv_usec != 0)) {
			// Job should start in the future:
			// Calculate the expiration of the timer in jiffies:
			do_gettimeofday(&now);
			__timeval_subtract(&diff, &drv->curr_sched_start->conf.time_start, &now);
			wait_time_us = (diff.tv_sec*1000000)+diff.tv_usec;
		} else {
			// The event is planned for immediate execution. Thus set the wait-time to 1 us:
			wait_time_us = 1;
		}

		// If the wait time is long, schedule the event using the kernel timer, otherwise use the OS timer:
		if ( wait_time_us <= ADS1271_SWITCHTIMER ) {
			// Set OS timer to current event:
			flocklab_ostimer_fnct_set(drv->start_timer, wait_time_us + ads1271_get_filter_delay_us(drv->ads1271_driver), POWERPROF_STRT_OST_OFFSET);
		} else {
			// Set kernel timer for current event.
			// If the wait time is longer in the future than 100*SWITCHTIMER, set the timer to only half the time needed to wait to enforce re-scheduling.
			// This is needed for ensuring that time synchronisation is taken into consideration when scheduling an event.
			if (wait_time_us > (100*ADS1271_SWITCHTIMER)) {
				diff.tv_sec = (long) diff.tv_sec/2;
			}
			// Check if a timer is already running. If yes, abort it:
			if ( timer_pending(drv->start_timer) ) {
				del_timer(drv->start_timer);
			}

			// Configure and start the kernel timer. Calculate the jiffies value at which the timer should expire. The timer should expire SWITCHTIMER microseconds before the event so it can be rescheduled using the OS timer (which is more accurate).
			drv->start_timer->expires = jiffies + timeval_to_jiffies(&diff) - (ADS1271_SWITCHTIMER*HZ/1000000);
			add_timer(drv->start_timer);
		}
	} else {
		// The event queue is empty. Write a status message and exit the function:
		return ERRLISTEMPTY;
	}
	return SUCCESS;

} // __schedule_next_starttimer



/**************************************************************************************//**
 *
 * Insert new entry into sorted scheduling queue
 *
 * @param new New event to insert into list.
 *
 *****************************************************************************************/
static void __list_add_sorted(struct powerprof_job *new)
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct list_head *list_ptr;
	struct powerprof_job *entry;

	// Traverse list backwards as users will mostly insert first events first...
	list_for_each_prev(list_ptr, &drv->schedule) {
		entry = list_entry(list_ptr, struct powerprof_job, schedule);
		// If tv_sec part of event to insert is greater than current event, insert new event after current event. If tv_sec part is the same but tv_usec part is greater, insert as well. Otherwise, iterate.
		if ((new->conf.time_start.tv_sec > entry->conf.time_start.tv_sec) || ((new->conf.time_start.tv_sec == entry->conf.time_start.tv_sec) && (new->conf.time_start.tv_usec > entry->conf.time_start.tv_usec))) {
			// Add event after the current event:
			list_add(&new->schedule, list_ptr);
			// Schedule event queue:
			__schedule_next_starttimer();
			return;
		}
	}
	// Add event at first position in the queue:
	list_add(&new->schedule, &drv->schedule);
	// Schedule event queue:
	__schedule_next_starttimer();
} // list_add_sorted



/**************************************************************************************//**
 *
 * Wrapper for powerprof_schedule_add() function. This function is exported so other kernel
 * modules can access it.
 *
 * @param sampling_duration_us How long should be sampled
 * @param time_start Time for which the power profile is planned. 0 for immediate start
 * @param divider Sapmling divider for ADS1271 driver
 *
 *****************************************************************************************/
void powerprofile_add(unsigned long sampling_duration_us, struct timeval time_start, unsigned int divider)
{
	/* Local variables */
	struct powerprof_job *new_job;
	unsigned long sampling_duration_s, stop_us;

	new_job = kmalloc(sizeof(struct powerprof_job), GFP_ATOMIC);

	new_job->conf.sampling_duration_us	= sampling_duration_us;
	new_job->conf.time_start.tv_sec		= time_start.tv_sec;
	new_job->conf.time_start.tv_usec	= time_start.tv_usec;
	new_job->conf.nth_sample			= divider;

	// Calculate the stop time:
	sampling_duration_s = sampling_duration_us / 1000000;
	new_job->conf.time_stop.tv_sec = new_job->conf.time_start.tv_sec + sampling_duration_s;
	stop_us = new_job->conf.time_start.tv_usec + (sampling_duration_us - (sampling_duration_s*1000000));
	if (unlikely(stop_us > 999999)) {
		stop_us -= 1000000;
		new_job->conf.time_stop.tv_sec++;
	}
	new_job->conf.time_stop.tv_usec = stop_us;

	// Schedule job:
	__list_add_sorted(new_job);

} // powerprofile_add
EXPORT_SYMBOL_GPL(powerprofile_add);


/**************************************************************************************//**
 *
 * This function removes a powerprofiling job from the schedule. The schedule is a
 * timer kernel list which holds all timers for this module.
 * Additionally, the job is removed from the linked list. This list is needed for
 * deletion and listing of the jobs. If a NULL pointer is passed to the function,
 * all entries in the list are removed.
 *
 * @param job 	Pointer to the struct holding all information needed to collect data. If
 * 				passed NULL, the whole list is deleted.
 *
 * @return 0 on success, negative error value otherwise.
 *
 *****************************************************************************************/
static int powerprof_schedule_remove(struct powerprof_job *job)
{
	/* Local variables */
	struct powerprof_drv *drv = &pp_drv;
	struct powerprof_job *job_ptr;
	struct list_head *list_ptr, *list_safe_ptr;
	int rs = ERRNOTFOUND;

	// Check whether list is empty:
	if (list_empty(&drv->schedule))
		return -ERRLISTEMPTY;

	// Traverse list, check for each entry whether it matches the search. If it does, delete the entry from the list.
	list_for_each_safe(list_ptr, list_safe_ptr, &drv->schedule) {
		job_ptr = list_entry(list_ptr, struct powerprof_job, schedule);
		if ( 	(job == NULL) ||
				(
						(job_ptr->conf.time_start.tv_sec == job->conf.time_start.tv_sec) &&
						(job_ptr->conf.time_start.tv_usec == job->conf.time_start.tv_usec)
				)) {
			list_del(&job_ptr->schedule);
			kfree(job_ptr);

			// If the queue is empty now, disable the timer (because the event that was deleted was the last one and so it's timer is set which we have to undo now):
			if (list_empty(&drv->schedule)) {
				// Disable the OS timer:
				flocklab_ostimer_fnct_disable(POWERPROF_STRT_OST_OFFSET);

				// Disable the start kernel timer if it was set. Do not disable any running stop timer as this would lead to infinite long sampling.
				if ( timer_pending(drv->start_timer) ) {
					del_timer(drv->start_timer);
				}
			}

			// Reschedule the event queue in case we deleted the first entry in the list that was scheduled to be next:
			__schedule_next_starttimer();

			// Set the return value:
			rs = SUCCESS;
		}
	}

	if (rs != SUCCESS)
		return -ERRNOTFOUND;
	else
		return SUCCESS;
} // powerprof_schedule_remove


/**************************************************************************************//**
 *
 * Called to perform module initialization when the module is loaded
 *
 *****************************************************************************************/
static int __init powerprof_init(void)
{
    /* Local variables	*/
    int rs, i;
    struct powerprof_buffer_list * mem;
    struct powerprof_dev *dev_list = &devlist;
    struct powerprof_dev *dev_dbbuf = &devdbbuf;
    struct powerprof_drv *drv = &pp_drv;
    struct spi_master *ads1271_spi_master;
    char ads1271_devname[11];

    printk(KERN_INFO "%s: FlockLab powerprofiling service starting...\n", POWERPROF_KM_NAME);

    /* Register the list device */
    // Prepare the device struct
    dev_list->devname           = DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST;
    dev_list->device_open_count = 0;
    dev_list->fops.owner        = THIS_MODULE;
    dev_list->fops.ioctl        = powerprof_dev_ioctl;
    dev_list->fops.open         = powerprof_dev_list_open;
    dev_list->fops.read         = powerprof_dev_list_read;
    dev_list->fops.release      = powerprof_dev_list_release;
    // Get a major number
    if (( rs = alloc_chrdev_region( &dev_list->dev_nr, 0, 1, dev_list->devname )) < 0 ) {
        printk( KERN_ERR "%s: Unable to allocate major for list device. Error %d occurred.\n", POWERPROF_KM_NAME, rs );
        cleanup(INIT_START);
        return rs;
    }
    printk(KERN_DEBUG "%s: Allocated device numbers for list device. Major:%d minor:%d\n", POWERPROF_KM_NAME, MAJOR( dev_list->dev_nr ), MINOR( dev_list->dev_nr ));
    // Register the device. The device becomes "active" as soon as cdev_add is called.
    cdev_init( &dev_list->dev_cdev, &dev_list->fops );
    dev_list->dev_cdev.owner = THIS_MODULE;
    dev_list->dev_cdev.ops = &dev_list->fops;
    if (( rs = cdev_add( &dev_list->dev_cdev, dev_list->dev_nr, 1 )) != 0 ) {
        printk( KERN_ERR "%s: cdev_add for list device failed with error %d.\n", POWERPROF_KM_NAME, rs );
        cleanup(INIT_LIST_CHRDEV);
        return rs;
    }
    printk(KERN_DEBUG "%s: List device registered.\n", POWERPROF_KM_NAME);
    // Create a class, so that udev will make the /dev entry automatically:
    dev_list->dev_class = class_create( THIS_MODULE, dev_list->devname );
    if ( IS_ERR( dev_list->dev_class )) {
        printk( KERN_ERR "%s: Unable to create class for list device.\n", POWERPROF_KM_NAME );
        cleanup(INIT_LIST_ADD);
        return -1;
    }
    device_create( dev_list->dev_class, NULL, dev_list->dev_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST );
    init_waitqueue_head(&dev_list->devreaders);
    printk(KERN_DEBUG "%s: List device node created.\n", POWERPROF_KM_NAME);


    /* Register the dbbuf device */
    // Prepare the device struct
    dev_dbbuf->devname                  = DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_DBBUF;
    dev_dbbuf->device_open_count        = 0;
    dev_dbbuf->fops.owner               = THIS_MODULE;
    dev_dbbuf->fops.open                = powerprof_dev_dbbuf_open;
    dev_dbbuf->fops.read                = powerprof_dev_dbbuf_read;
    dev_dbbuf->fops.release             = powerprof_dev_dbbuf_release;
    // Get a major number
    if (( rs = alloc_chrdev_region( &dev_dbbuf->dev_nr, 0, 1, dev_dbbuf->devname )) < 0 ) {
        printk( KERN_ERR "%s: Unable to allocate major for dbbuf device. Error %d occurred.\n", POWERPROF_KM_NAME, rs );
        cleanup(INIT_LIST_CLASS);
        return rs;
    }
    printk(KERN_DEBUG "%s: Allocated device numbers for dbbuf device. Major:%d minor:%d\n", POWERPROF_KM_NAME, MAJOR( dev_dbbuf->dev_nr ), MINOR( dev_dbbuf->dev_nr ));
    // Register the device. The device becomes "active" as soon as cdev_add is called.
    cdev_init( &dev_dbbuf->dev_cdev, &dev_dbbuf->fops );
    dev_dbbuf->dev_cdev.owner = THIS_MODULE;
    dev_dbbuf->dev_cdev.ops = &dev_dbbuf->fops;
    if (( rs = cdev_add( &dev_dbbuf->dev_cdev, dev_dbbuf->dev_nr, 1 )) != 0 ) {
        printk( KERN_ERR "%s: cdev_add for dbbuf device failed with error %d.\n", POWERPROF_KM_NAME, rs );
        cleanup(INIT_DBBUF_CHRDEV);
        return rs;
    }
    printk(KERN_DEBUG "%s: Dbbuf device registered.\n", POWERPROF_KM_NAME);
    // Create a class, so that udev will make the /dev entry automatically:
    dev_dbbuf->dev_class = class_create( THIS_MODULE, dev_dbbuf->devname );
    if ( IS_ERR( dev_dbbuf->dev_class )) {
        printk( KERN_ERR "%s: Unable to create class for dbbuf device.\n", POWERPROF_KM_NAME );
        cleanup(INIT_DBBUF_ADD);
        return -1;
    }
    device_create( dev_dbbuf->dev_class, NULL, dev_dbbuf->dev_nr, NULL, DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_DBBUF );
    init_waitqueue_head(&dev_dbbuf->devreaders);
    printk(KERN_DEBUG "%s: Dbbuf device node created.\n", POWERPROF_KM_NAME);

    /* Initialize the queue for sample packets */
    INIT_LIST_HEAD(&(drv->samplebuf_free));
    INIT_LIST_HEAD(&(drv->samplebuf_filled));
    for (i=0; i<SAMPLEPACKET_QUEUE_SIZE; i++) {
      mem = vmalloc(sizeof(struct powerprof_buffer_list));
      if (!mem) {
        printk(KERN_ERR POWERPROF_KM_NAME ": unable to allocate memory for FIFO.\n");
        cleanup(INIT_FIFOLISTS);
        return -ENOMEM;
      }
      list_add(&(mem->list), &(drv->samplebuf_free));
    }
    init_waitqueue_head(&drv->samplepacket_fifo_wq);
    mutex_init(&(drv->list_mutex));

    /* Find the ADS1271 device and initialize it */
    ads1271_spi_master = spi_busnum_to_master(ADS1271_SPI_BUS_NUM);
    sprintf(ads1271_devname, "ads1271-%d", ADS1271_SPI_BUS_NUM);
    device_find_child((ads1271_spi_master->dev.parent), ads1271_devname, __match_ads1271_dev);
    printk(KERN_DEBUG "%s: Found ADC ADS1271 device with name %s\n", POWERPROF_KM_NAME, dev_name(drv->ads1271_dev));
    drv->ads1271_driver = ads1271_prepare_internal(drv->ads1271_dev->devt);
    printk(KERN_DEBUG "%s: ADC ADS1271 prepared and reserved\n", POWERPROF_KM_NAME);

    /* Start the Kthread for fetching sample packets from the ADS1271 Driver */
    init_completion(&drv->fetcher_completion);
    drv->fetcher_kthread = kthread_run(&__packet_fetcher_kthread, NULL, "pp_packet_fetch");
    if (IS_ERR(drv->fetcher_kthread)) {
    	cleanup(INIT_KTHREAD);
    	return -EFAULT;
    }

    /* Initialize schedule queue */
    INIT_LIST_HEAD(&drv->schedule);
    printk(KERN_DEBUG "%s: Schedule initialized.\n", POWERPROF_KM_NAME);

    /* Initialize the timers */
    drv->start_timer = kmalloc(sizeof(struct timer_list), GFP_KERNEL);
    if (!drv->start_timer) {
        printk( KERN_ERR "%s: Unable to allocate memory for start timer.\n", POWERPROF_KM_NAME );
        cleanup(INIT_EVENT_QUEUE);
        return -ENOMEM;
    }
    init_timer(drv->start_timer);
    drv->start_timer->function = (void *) __schedule_next_starttimer;        // Function to call when timer expires
    printk(KERN_DEBUG "%s: Start timer initialized.\n", POWERPROF_KM_NAME);

    drv->stop_timer = kmalloc(sizeof(struct timer_list), GFP_KERNEL);
    if (!drv->stop_timer) {
        printk( KERN_ERR "%s: Unable to allocate memory for stop timer.\n", POWERPROF_KM_NAME );
        cleanup(INIT_EVENT_QUEUE);
        return -ENOMEM;
    }
    init_timer(drv->stop_timer);
    drv->stop_timer->function = (void *) __schedule_next_stoptimer;        // Function to call when timer expires
    printk(KERN_DEBUG "%s: Stop timer initialized.\n", POWERPROF_KM_NAME);

    // Register tasklet for start timer
    tasklet_init(&drv->start_bh_tasklet, (void *) __trigger_collection, 0);
    printk(KERN_DEBUG "%s: Tasklet for OS timer interrupt registered.\n", POWERPROF_KM_NAME);

    printk(KERN_INFO "%s: FlockLab powerprofiling service started.\n", POWERPROF_KM_NAME);

    return SUCCESS;
} // init_module




/**************************************************************************************//**
 *
 * Called to perform module cleanup when the module is unloaded
 *
 *****************************************************************************************/
static void __exit powerprof_exit(void)
{
    cleanup(INIT_DONE);
}

void cleanup(int init_state)
{
    /* Local variables */
    struct powerprof_dev *dev_list = &devlist;
    struct powerprof_dev *dev_dbbuf = &devdbbuf;
    struct powerprof_drv *drv = &pp_drv;
    struct list_head *list, *list_safe;

    printk(KERN_INFO "%s: FlockLab powerprofiling service unloading...\n", POWERPROF_KM_NAME);
    printk(KERN_DEBUG "%s: kfifo_full_occurred = %d\n", POWERPROF_KM_NAME, drv->kfifo_full_occurred);

    switch(init_state) {
        case INIT_DONE:

        case INIT_TIMER:
        	// Disable the OS timers:
        	flocklab_ostimer_fnct_disable(POWERPROF_STRT_OST_OFFSET);
        	flocklab_ostimer_fnct_disable(POWERPROF_STP_OST_OFFSET);
        	printk(KERN_DEBUG "%s: OS timers disabled.\n", POWERPROF_KM_NAME);

            // Destroy kernel timers
        	if (timer_pending(drv->start_timer)) {
        		del_timer(drv->start_timer);
        		printk(KERN_DEBUG "%s: Deleted pending start timer.\n", POWERPROF_KM_NAME);
        	}
        	kfree(drv->start_timer);

            if (timer_pending(drv->stop_timer)) {
                del_timer(drv->stop_timer);
                printk(KERN_DEBUG "%s: Deleted pending stop timer.\n", POWERPROF_KM_NAME);
            }
            kfree(drv->stop_timer);

        case INIT_EVENT_QUEUE:
            // Remove all entries in the schedule:
            powerprof_schedule_remove(NULL);
            printk(KERN_DEBUG "%s: Removed all entries from scheduler.\n", POWERPROF_KM_NAME);
            // Stop the kthread:
            kthread_stop(drv->fetcher_kthread);
            printk(KERN_DEBUG "%s: Stopping the kthread. Waiting for completion...\n", POWERPROF_KM_NAME);
            wait_for_completion(&drv->fetcher_completion);

        case INIT_KTHREAD:
        	// Nothing to do

        case INIT_ADS1271:
            // Release ADS1271:
            ads1271_release_internal(drv->ads1271_driver);
            put_device(drv->ads1271_dev);
            printk(KERN_DEBUG "%s: ADC ADS1271 released\n", POWERPROF_KM_NAME);

        case INIT_FIFOLISTS:
          // Destroy fifo:
          list_for_each_safe(list, list_safe, &(drv->samplebuf_free)) {
            vfree(list_entry(list, struct powerprof_buffer_list, list));
          }
          list_for_each_safe(list, list_safe, &(drv->samplebuf_filled)) {
            vfree(list_entry(list, struct powerprof_buffer_list, list));
          }
          printk(KERN_DEBUG "%s: packet sample FIFO freed\n", POWERPROF_KM_NAME);

        // Unregister devices            
        case INIT_DBBUF_CLASS:
            device_destroy( dev_dbbuf->dev_class, dev_dbbuf->dev_nr );
            class_destroy( dev_dbbuf->dev_class );
        case INIT_DBBUF_ADD:
            cdev_del( &dev_dbbuf->dev_cdev );
        case INIT_DBBUF_CHRDEV:
            unregister_chrdev_region( dev_dbbuf->dev_nr, 1 );
            printk(KERN_DEBUG "%s: Dbbuf device unregistered.\n", POWERPROF_KM_NAME);

        case INIT_LIST_CLASS:
            device_destroy( dev_list->dev_class, dev_list->dev_nr );
            class_destroy( dev_list->dev_class );
        case INIT_LIST_ADD:
            cdev_del( &dev_list->dev_cdev );
        case INIT_LIST_CHRDEV:
            unregister_chrdev_region( dev_list->dev_nr, 1 );
            printk(KERN_DEBUG "%s: List device unregistered.\n", POWERPROF_KM_NAME);

        case INIT_START:
            break;
    }
    printk(KERN_INFO "%s: FlockLab powerprofiling service unloaded.\n", POWERPROF_KM_NAME);

} // cleanup_module


/* ---- Init and Cleanup ----------------------------------------------------------*/
module_init(powerprof_init);
module_exit(powerprof_exit);
