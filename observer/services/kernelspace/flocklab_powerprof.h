/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains shared definitions for the kernelmodule of the FlockLab
 * powerprofiling service.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_POWERPROF_H
#define FLOCKLAB_POWERPROF_H

/* ---- Includes --------------------------------------------------------- */
#include "flocklab_powerprof_shared.h"


/* ---- Constants -------------------------------------------------------- */
#define ADS1271_SPI_BUS_NUM			2		// Bus number of SPI BUS to which ADS1271 ADC is connected.
#define ADS1271_SWITCHTIMER			1000000	// Time in [us] after which the kernel timer should be used instead of the OS timer.
// Kernel timer stuff:
#define OMCR5           			__REG(0x40A000C4)
#define OSMR5           			__REG(0x40A00084)
#define OSCR5           			__REG(0x40A00044)


/* ---- Export functions for shared use in other kernel modules ---------------*/
void powerprofile_add(unsigned long sampling_duration_us, struct timeval time_start, unsigned int divider);		// Wrapper for powerprof_schedule_add()
void powerprof_start_timer_expired(void);
void powerprof_stop_timer_expired(void);

/* ---- Structs ---------------------------------------------------------- */
/**
 * Struct holding all data associated to a sampling job for the data collector
 */
struct powerprof_job {
	struct list_head schedule;				///< Used internally for inserting the job into the schedule. Do not change!
	struct powerprof_job_conf conf;
};


/**
 * Struct holding the data used for sampling the adc
 */
struct powerprof_drv {
	struct list_head schedule;					///< List holding all scheduled profiling jobs. Do not change!
	struct powerprof_job *curr_sched_start;		///< Pointer to the currently scheduled power profile to be started
	struct powerprof_job *curr_sched_stop;		///< Pointer to the currently scheduled power profile to be stopped
	struct device *ads1271_dev;					///< Pointer to the ADS1271_device struct.
	struct ads1271_driver_data *ads1271_driver;	///< Pointer to the ADS1271 driver struct
	struct timer_list *start_timer;				///< Start timer for scheduler
	struct tasklet_struct start_bh_tasklet;		///< Tasklet data for bottom half of start timer handler
	struct timer_list *stop_timer;				///< Stop timer for scheduler
	struct work_struct wq_stop_timer;			///< Workqueue for bottom half of stop timer
	struct list_head samplebuf_free;					///< List holding free buffers
	struct list_head samplebuf_filled;					///< List holding filled buffers, received from ADS1271 driver
	struct mutex list_mutex;
	wait_queue_head_t samplepacket_fifo_wq;		///< Waitqueue for FIFO
	struct task_struct *fetcher_kthread;		///< Kthread which fetches sample packets from ADS1271 driver
	struct completion fetcher_completion;		///< Completion object for exiting kthread
	unsigned int kfifo_full_occurred;			///< Counter for statistics
};

/**
 * Struct holding all variables associated to a device file
 */
struct powerprof_dev {
	char *devname;								///< Name of the device
	dev_t dev_nr;								///< Device number to which device is registered to
	struct cdev dev_cdev;
	struct class *dev_class;
	int device_open_count;						///< Used to prevent opening the device multiple times
	int dev_read;								///< Flag to indicate that the device has already been read.
	struct file_operations fops;
	wait_queue_head_t devreaders;				///< Waitqueue for blocking I/O to dev readers
};

/**
 * struct for data buffer queues
 */
struct powerprof_buffer_list {
	struct list_head list;
	struct powerprof_sample_pckt pckt;
};

/* ---- Public Prototypes --------------------------------------------------*/

/* ---- Include Guard -------------------------------------------------------- */
#endif
