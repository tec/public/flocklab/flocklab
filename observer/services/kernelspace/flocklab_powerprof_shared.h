/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 * This h-file contains shared definitions for both the userspace programs
 * and the kernelmodule of the flocklab service "powerprofiling".
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_POWERPROF_SHARED_H
#define FLOCKLAB_POWERPROF_SHARED_H


/* ---- Include Files ---------------------------------------------------- */


/* ---- Constants ----------------------------------------------------------*/

// Magic number for this kernel module for registering ioctl commands
#define POWERPROF_IOCTL_MAGIC  	'O'
// Set up ioctl command numbers:
#define POWERPROF_IOCTL_SCHEDULER_ADD			_IOWR	(POWERPROF_IOCTL_MAGIC,	0,	int	)	// IOCTL to add a power profiling job.
#define POWERPROF_IOCTL_SCHEDULER_REMOVE		_IOWR	(POWERPROF_IOCTL_MAGIC,	1,	int	)	// IOCTL to remove a single power profiling job.
#define POWERPROF_IOCTL_SCHEDULER_REMOVEALL		_IOR	(POWERPROF_IOCTL_MAGIC,	2,	int	)	// IOCTL to remove all power profiling jobs.
#define POWERPROF_IOCTL_BUFFER_FLUSH			_IOR	(POWERPROF_IOCTL_MAGIC,	3,	int	)	// IOCTL to flush the output buffer for the power profiling results.
#define POWERPROF_IOCTL_ERRCNT					_IOR 	(POWERPROF_IOCTL_MAGIC,	4,	long)	// IOCTL to show error statistics
#define POWERPROF_IOCTL_ERRRST					_IOR 	(POWERPROF_IOCTL_MAGIC,	5,	int	)	// IOCTL to reset error statistics
#define POWERPROF_IOCTL_MAXNR					6											// Denotes maximum number of cmd's above


// Return values of functions:
#ifndef SUCCESS
#define SUCCESS			0															// Indicates success of an operation.
#endif

// Size of ADS1271 FIFO which stores samples. Length must be a power of 2.
// Has to be synchronized with SAMPLE_FIFO_LENGTH in ads1271.h
#define ADS1271_SAMPLE_FIFO_LENGTH				8191 / sizeof(int)
// Size of the FIFO which holds sample packets retrieved from the ADS1271 driver. Should be a power of 2
#define SAMPLEPACKET_QUEUE_SIZE 32


/* ---- Global variables ---------------------------------------------------*/
/**
 * Struct holding all configuration data from/to userspace associated to a sampling job for the data collector
 */
struct powerprof_job_conf {
	struct timeval time_start;							///< Time at which the sampling should start
	struct timeval time_stop;							///< Time at which the sampling should stop
	unsigned long sampling_duration_us;				///< How long should be sampled (in us)
	unsigned int nth_sample;							///< Sampling rate divider of ads1271 driver
};

/**
 * Struct holding a packet with measured samples from the ADS1271 which are going to be passed to userspace
 */
struct powerprof_sample_pckt {
	struct timeval sampling_start;						///< Time at which the sampling started. Only valid for the first packet of a measurement series.
	struct timeval sampling_end;						///< Time at which the sampling stopped. Only valid for the last packet of a measurement series.
	unsigned int sample_count;							///< Number of samples stored at the location pointed to by sample_buf.
	uint32_t sample_buf[ADS1271_SAMPLE_FIFO_LENGTH];	///< Buffer where sample_count measurement values are stored.
};

/**
 * Struct for error statistics from the ADS1271 which can be passed to userspace
 */
struct powerprof_errorstats {
	unsigned int kfifo_full_occured;					///< See ads1271.h, struct ads1271_driver_data
	unsigned int rx_fifo_overrun_occured;				///< See ads1271.h, struct ads1271_driver_data
	unsigned int rx_fifo_empty_occured;				///< See ads1271.h, struct ads1271_driver_data
};



/* ---- Include Guard ---------------------------------------------------- */
#endif


