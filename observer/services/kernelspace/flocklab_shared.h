/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This h-file contains shared definitions for both the userspace programs
 * and the kernel modules which have all flocklab services in common.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */



/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_SHARED_H
#define FLOCKLAB_SHARED_H

/* ---- Include Files ---------------------------------------------------- */


/* ---- Constants ----------------------------------------------------------*/
#define SIZEOFDBBUFBUF							8192						// Size of buffers to read out.

// Device names:
#define DEVICE_PARENT_FOLDER					"flocklab"					// All devices shall be place under /dev/flocklab
#define DEVICE_PARENT_GPIO_MONITOR				"gpiomonitor"				// Parent folder for this service.
#define DEVICE_PARENT_GPIO_SETTING				"gpiosetting"				// Parent folder for this service.
#define DEVICE_PARENT_POWERPROF					"powerprof"					// Parent folder for this service.
#define DEVICE_PARENT_SERIAL_READER				"serialreader"				// Parent folder for this service.
#define DEVICE_NAME_LIST						"list"						// List device
#define DEVICE_NAME_DBBUF						"dbbuf"						// DB buffer device
#define DEVICE_ADS1271							"daq_spi-2"

// Socket names:
#define SOCKET_DIR								"/tmp/flocklab"
#define SOCKET_NAME_GPIO_MONITOR				"dbd_gm_out.sock"
#define SOCKET_NAME_GPIO_SETTING				"dbd_gs_out.sock"
#define SOCKET_NAME_POWERPROF					"dbd_pp_out.sock"
#define SOCKET_NAME_FLOCKDAQ					"dbd_daq_out.sock"

// Names of kernel modules:
#define GPIO_MONITOR_KM_NAME					"flocklab_gpio_monitor"
#define GPIO_SETTING_KM_NAME 					"flocklab_gpio_setting"
#define POWERPROF_KM_NAME						"flocklab_powerprof"
#define OSTIMER_KM_NAME							"flocklab_ostimer"
#define OSTIMERFNCT_KM_NAME						"flocklab_ostimer_fnct"

// Names of API's and userspace programs
#define GPIO_MONITOR_API_NAME					"flocklab_gpiomonitor"
#define GPIO_SETTING_API_NAME					"flocklab_gpiosetting"
#define POWERPROF_API_NAME						"flocklab_powerprofiling"

// Path to the database daemon:
#define	 DBDAEMON_PATH							"/usr/bin"
#define DBDAEMON_NAME							"flocklab_dbd"
// Names for the different database daemon instances. This is always the prefix 'dbd_' followed by the first 8 characters of the service name (as defined e.g. in GPIO_MONITOR_SERVICE_NAME)
#define GPIO_MONITOR_DBD_NAME					"dbd_gpio_mon"
#define GPIO_SETTING_DBD_NAME					"dbd_gpio_set"
#define POWERPROF_DBD_NAME						"dbd_powerpro"
#define FLOCKDAQ_DBD_NAME						"dbd_flockdaq"

// Names of the different services:
#define GPIO_MONITOR_SERVICE_NAME				"gpio_monitor"
#define GPIO_SETTING_SERVICE_NAME				"gpio_setting"
#define POWERPROF_SERVICE_NAME					"powerprofiling"
#define SERIAL_READER_SERVICE_NAME				"serial_reader"
#define FLOCKDAQ_SERVICE_NAME						"flockdaq"

// Offsets for OS timer registers. The first usable OS timer is timer channel 4 for which the offset is 0. Useable channels are 4-11 with offsets 0-7
#define GPIO_SETTING_OST_OFFSET 				0	// OS timer 4
#define POWERPROF_STRT_OST_OFFSET				1	// OS timer 5
#define POWERPROF_STP_OST_OFFSET				2	// OS timer 6

#define ADS1271_FLOCKDAQ_BLOCK_SIZE			8188
// service info for dbd
struct service_info {
	char *name;     // Name of the service
	int fk;         // Foreign key of the service
	char *device;   // Device to read out
	char *socket;	// Socket to output data in addition to file
	char *dbd_name;	// Name of dbd
};

static const struct service_info flocklab_services[4] =
{
{ GPIO_MONITOR_SERVICE_NAME,   1, "/dev/" DEVICE_PARENT_FOLDER "/" DEVICE_PARENT_GPIO_MONITOR "/" DEVICE_NAME_DBBUF,	SOCKET_DIR "/" SOCKET_NAME_GPIO_MONITOR,	GPIO_MONITOR_DBD_NAME },
{ GPIO_SETTING_SERVICE_NAME,   2, "/dev/" DEVICE_PARENT_FOLDER "/" DEVICE_PARENT_GPIO_SETTING "/" DEVICE_NAME_DBBUF,	SOCKET_DIR "/" SOCKET_NAME_GPIO_SETTING,	GPIO_SETTING_DBD_NAME },
{ POWERPROF_SERVICE_NAME,      3, "/dev/" DEVICE_PARENT_FOLDER "/" DEVICE_PARENT_POWERPROF "/" DEVICE_NAME_DBBUF,		SOCKET_DIR "/" SOCKET_NAME_POWERPROF,		POWERPROF_DBD_NAME },
{ FLOCKDAQ_SERVICE_NAME,       4, "/dev/" DEVICE_ADS1271,																SOCKET_DIR "/" SOCKET_NAME_FLOCKDAQ,		FLOCKDAQ_DBD_NAME },
};

//Name and path of the database:
#define DBPATH 									"/home/root/mmc/flocklab/db/"		// Path to the DB WITH trailing backslash


// Return values of functions:
#ifndef SUCCESS
#define SUCCESS						0												// Indicates success of an operation.
#endif
#define ERRNOTFOUND 				900												// File or entry not found
#define ERRLISTEMPTY				901												// Returned if the list is empty.

// Signals:
#define SIGFLUSH					SIGUSR1											// Signal which is sent to the database daemon to flush its buffer to the database.
#define SIGRECON					SIGUSR2											// Signal which causes the database daemon to reconnect to the database.




/* ---- Global variables ---------------------------------------------------*/
/* List of setable pins:
 * 		75: TARGET_SIG1: 		Signal line from Gumstix to target node
 * 		74: TARGET_SIG1: 		Signal line from Gumstix to target node
 * 		60: TARGET_RST:			Reset line for target node (needed for switching on/off target node)
 */
int list_gpiopins_set[] = {60, 74, 75};

/* List of monitorable pins:
 * 		113: TARGET_INT1
 * 		87 : TARGET_INT2
 * 		71 : TARGET_LED1
 * 		70 : TARGET_LED2
 * 		69 : TARGET_LED3
 */
int list_gpiopins_monitor[] = {69, 70, 71, 87, 113};

// Enumeration for edge types:
#define EN_EDGE C(F)C(R)C(B)	// F = Falling, R = Rising, B = Both
#undef C
#define C(x) x,
enum en_edge { EN_EDGE EN_EDGE_TOP };
#undef C
#define C(x) #x,
char * en_edge[] = { EN_EDGE };

// Enumeration for level types:
#define EN_LEVEL C(L)C(H)C(T)	// H = high, L = low, T = toggle
#undef C
#define C(x) x,
enum en_level { EN_LEVEL EN_LEVEL_TOP };
#undef C
#define C(x) #x,
char * en_level[] = { EN_LEVEL };

// Temp pathes for FlockLab, flag files, ...
char *path_flocklab_tmp		=	"/var/tmp/flocklab";
char *flagfile_gpioset_nofl	=	"/var/tmp/flocklab/flag_gpiosetting_nofl";			// Path to the flag file which indicates that the -noflocklab option is set.
char *flagfile_gpiomon_nofl	=	"/var/tmp/flocklab/flag_gpiomonitoring_nofl";		// Path to the flag file which indicates that the -noflocklab option is set.


/* ---- Include Guard ---------------------------------------------------- */
#endif
