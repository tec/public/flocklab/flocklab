#! /usr/bin/env python
from serial.serialutil import SerialException

__author__ 		= "Christoph Walser <walser@tik.ee.ethz.ch>"
__copyright__ 	= "Copyright 2010, ETH Zurich, Switzerland, Christoph Walser"
__license__ 	= "GPL"
__version__ 	= "$Revision$"
__date__ 		= "$Date$"
__id__ 			= "$Id$"
__source__ 		= "$URL$"

"""
This file belongs to /usr/bin/ on the observer
"""

import os, sys, getopt, signal, socket, time, subprocess, errno, Queue, serial, select, multiprocessing, threading, traceback, __main__
from syslog import *
from struct import *
# Import local libraries:
sys.path.append('/usr/lib/flocklab/python/')
import daemon
from flocklab import SUCCESS
import flocklab


### Global variables ###
###
version = filter(str.isdigit, __version__)
scriptname = os.path.basename(__main__.__file__)
name = "flocklab_serial"
###
pidfile					= None
config					= None
isdaemon 				= False
debug					= False
port_list 				= ('usb', 'serial')									# List of possible ports to receive serial data from. 'usb' -> /dev/flocklab/usb/targetx, 'serial' -> /dev/ttyS1
baudrate_list 			= (2400, 4800, 9600, 19200, 38400, 57600, 115200)	# List of allowed baud rates for general targets
baudrate_contiki2_list 	= (19200, 38400, 57600, 115200)						# List of allowed baud rates for contiki2 targets
mode_list 				= ('pck', 'ascii', 'raw')							# List with all possible output modes
targetos_list 			= ('tinyos', 'contiki', 'other')					# List with all possible target operating systems
proc_list	 			= []												# List with all running processes
dbbuf_proc 				= []												# Dbbuf process
msgQueueDbBuf			= None												# Queue used to send data to the DB buffer


##############################################################################
#
# Error classes
#
##############################################################################
class Error(Exception):
	"""Base class for exceptions in this module."""
	pass

class SerialError(Error):
	"""Exception raised for errors in the serial communication."""
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)
### END Error classes



##############################################################################
#
# sigterm_handler
#
##############################################################################
def sigterm_handler(signum, frame):
	"""If the program is terminated by sending it the signal SIGTERM
	(e.g. by executing 'kill') or SIGINT (pressing ctrl-c),
	this signal handler is invoked for cleanup."""
	syslog(LOG_INFO, "Main process received SIGTERM signal")
	# Close serial forwarder object:
	retval = stop_on_sig(SUCCESS)
	sys.exit(retval)
### END sigterm_handler()



##############################################################################
#
# ServerSockets class
#
##############################################################################
class ServerSockets():
	def __init__(self, port):
		self.sock					= None
		self.sock_host				= ''
		self.sock_port				= port
		self.sock_rx_waittime		= 5.0
		self.sock_rx_bufsize		= 4096
		self.sock_listen_timeout = 0.2
		self.connection				= None
		self.address				= None

	def start(self):
		try:
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			self.sock.bind((self.sock_host, self.sock_port))
			self.sock.settimeout(self.sock_listen_timeout)
			syslog(LOG_INFO, "Started socket %s:%d"%(self.sock_host, self.sock_port))
		except:
			self.sock = None
			syslog(LOG_INFO, "Encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

	def stop(self):
		if self.sock != None:
			try:
				self.sock.shutdown(socket.SHUT_RDWR)
				self.sock.close()
				syslog(LOG_INFO, "Stopped socket %s:%d"%(self.sock_host, self.sock_port))
			except:
				syslog(LOG_INFO, "Could not stop socket %s:%d due to error in line %d: %s: %s"%(self.sock_host, self.sock_port, traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
			finally:
				self.connection = None
				self.address = None
				self.sock = None

	def waitForClient(self):
		if self.sock != None:
			# syslog(LOG_INFO, "Waiting for clients on socket %s:%d"%(self.sock_host, self.sock_port))
			try:
				self.sock.listen(1)
				self.connection, self.address = self.sock.accept()
				self.connection.setblocking(0)
				self.connection.settimeout(self.sock_rx_waittime)
				syslog(LOG_INFO, "Client %s:%d connected to socket %s:%d"%(self.address[0], self.address[1], self.sock_host, self.sock_port))
			except socket.timeout:
				self.connection = None
			return self.connection
		else:
			raise socket.error

	def disconnectClient(self):
		if self.connection != None:
			syslog(LOG_INFO, "Disconnect client %s:%d from socket %s:%d"%(self.address[0], self.address[1], self.sock_host, self.sock_port))
			self.connection.close()
			self.connection = None
			self.address = None

	def send(self, data):
		if self.connection != None:
			return self.connection.send(data)
		else:
			raise socket.error

	def recv(self, bufsize=None):
		if ((self.sock != None) and (self.connection != None)):
			if bufsize == None:
				bufsize = self.sock_rx_bufsize
			return self.connection.recv(bufsize)
		else:
			raise socket.error

	def isRunning(self):
		if self.sock != None:
			return True
		return False

	def clientConnected(self):
		if self.connection != None:
			return True
		return False
### END ServerSockets()



##############################################################################
#
# SerialForwarderStandard class
#
##############################################################################
class SerialForwarderStandard():
	def __init__(self, slotnr, serialdev, baudrate):
		self.slotnr				= slotnr
		self.serialdev			= os.path.realpath(serialdev)
		self.baudrate			= baudrate
		self.read_timeout 		= 1.0			# Timeout in seconds for reads from serial forwarder
		self.num_elements_rcv 	= 0				# Number of packets received by the process. For statistical information.
		self.num_elements_snd 	= 0				# Number of packets sent by the process. For statistical information.
		self.sf					= None

	def open(self):
		try:
			self.sf = serial.Serial(port=self.serialdev, baudrate=self.baudrate, timeout=self.read_timeout)
		except serial.SerialException, err:
			syslog(LOG_ERR, "SerialForwarderStandard could not initialize serial connection because: %s" % (str(sys.exc_info()[1])))
			return None
		syslog(LOG_INFO, "SerialForwarderStandard opened serial device %s with baudrate %d and timeout %d"%(self.serialdev, self.baudrate, self.read_timeout))

	def close(self):
		try:
			if self.sf != None:
				self.sf.close()
		except:
			syslog(LOG_ERR, "SerialForwarderStandard could not close serial connection because: %s" % (str(sys.exc_info()[1])))
		finally:
			self.sf = None
		syslog(LOG_INFO, "SerialForwarderStandard stopped...%d packets received, %d packets sent."%(self.num_elements_rcv, self.num_elements_snd))

	def isRunning(self):
		if self.sf != None:
			return True
		return False

	def read(self):
		ret = None
		try:
			data = self.sf.read(1)
			time.sleep(0.005)
			inbuf = self.sf.inWaiting()
			if inbuf:
				data = data + self.sf.read(inbuf)
			timestamp = time.time()
			ret = [data, timestamp]
			if (data not in (None, '')):
				self.num_elements_rcv = self.num_elements_rcv + 1
		except select.error, err:
			if (err[0] == 4):
				syslog(LOG_INFO, "SerialForwarderStandard interrupted due to caught stop signal.")
				return None
		except OSError, err:
			syslog(LOG_ERR, "SerialForwarderStandard error while reading from serial forwarder: %s" %str(err))
			self.close()
		except SerialException, err:
			syslog(LOG_ERR, "SerialForwarderStandard error while reading from serial forwarder: %s" %str(err))
			self.close()
		except:
			syslog(LOG_ERR, "SerialForwarderStandard encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return ret

	def write(self, data):
		try:
			rs = self.sf.write(data)
			if rs != len(data):
				syslog(LOG_ERR, "SerialForwarderStandard error while writing: no of bytes written (%d) != no of bytes in data (%d)." %str(rs, len(data)))
			self.num_elements_snd = self.num_elements_snd + 1
		except select.error, err:
			if (err[0] == 4):
				syslog(LOG_INFO, "SerialForwarderStandard interrupted due to caught stop signal.")
				return None
		except OSError, err:
			syslog(LOG_ERR, "SerialForwarderStandard error while writing to serial forwarder: %s" %str(err))
			self.close()
		except:
			syslog(LOG_ERR, "SerialForwarderStandard encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return None
### END SerialForwarderStandard



##############################################################################
#
# SerialForwarderTinyOS class
#
##############################################################################
class SerialForwarderTinyOS():
	def __init__(self, slotnr, serialdev, baudrate):
		self.slotnr				= slotnr
		self.serialdev			= os.path.realpath(serialdev)
		self.baudrate			= baudrate
		self.control_port		= 9010+int(self.slotnr)
		self.listen_port		= 9020+int(self.slotnr)
		self.read_timeout 		= 1.0			# Timeout in seconds for reads from serial forwarder
		self.num_elements_rcv 	= 0				# Number of packets received by the process. For statistical information.
		self.num_elements_snd 	= 0				# Number of packets sent by the process. For statistical information.
		self.remainder			= ''
		self.sf					= None
		self.sf_proc			= None
		self.blocksize			= config.getint("serial", "tinyos_blocksize")
		self.sf_proto_len		= -1

	def open(self):
		try:
			self.sf_proc = subprocess.Popen(["sf", "control-port", str(self.control_port), "daemon"], close_fds = True)
			time.sleep(2)
			syslog(LOG_INFO, "SerialForwarderTinyOS started TinyOS serial forwarder with control-port %d and PID %s"%(self.control_port, str(self.sf_proc.pid)))
			_sf = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			_sf.connect(('localhost', self.control_port))
			_sf.send('start %d %s %d\nclose\n'%(self.listen_port, self.serialdev, self.baudrate))
			_sf.shutdown(socket.SHUT_RDWR)
			_sf.close()
			time.sleep(2)
			self.sf = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.sf.connect(('localhost', self.listen_port))
			self.sf.settimeout(self.read_timeout)
			syslog(LOG_INFO, "SerialForwarderTinyOS connected to SF server socket")
			# Do handshake:
			data = self.sf.recv(self.blocksize)
			if data == 'U ':
				# SF initialization string. Return it to SF to complete handshake:
				self.sf.send(data)
				syslog(LOG_INFO, "SerialForwarderTinyOS completed handshake with SF server")
			else:
				raise Exception
			syslog(LOG_INFO, "SerialForwarderTinyOS started SF server for device %s with baudrate %d and timeout %d at port %d"%(self.serialdev, self.baudrate, self.read_timeout, self.listen_port))
		except Exception, err:
			syslog(LOG_ERR, "SerialForwarderTinyOS could not initialize SF server because: %s" % (str(sys.exc_info()[1])))
			return None
		syslog(LOG_INFO, "SerialForwarderTinyOS opened SF server.")

	def close(self):
		try:
			if self.sf != None:
				self.sf.shutdown(socket.SHUT_RDWR)
				self.sf.close()
				_sf = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				_sf.connect(('localhost', self.control_port))
				_sf.send('info %d\nexit\n'%self.listen_port)
				data = _sf.recv(self.blocksize)
				_sf.shutdown(socket.SHUT_RDWR)
				_sf.close()
				syslog(LOG_INFO, "SerialForwarderTinyOS sent exit signal to SF socket, output was %s"%data)
				syslog(LOG_INFO, "SerialForwarderTinyOS trying to kill TinyOS serial forwarder (PID %s)..."%(str(self.sf_proc.pid)))
				self.sf_proc.kill()
				syslog(LOG_INFO, "SerialForwarderTinyOS stopped TinyOS serial forwarder")
		except:
			syslog(LOG_ERR, "SerialForwarderTinyOS could not close TinyOS serial forwarder because of error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		finally:
			self.sf = None
		syslog(LOG_INFO, "SerialForwarderTinyOS stopped...%d packets received, %d packets sent."%(self.num_elements_rcv, self.num_elements_snd))

	def isRunning(self):
		if self.sf != None:
			return True
		return False

	def check_sf_proc(self):
		_ret = self.sf_proc.poll()
		if _ret != None:
			syslog(LOG_INFO, "SerialForwarderTinyOS realized that TinyOS serial forwarder is not running anymore. Return code %d. Trying to restart it..."%_ret)
			try:
				self.close()
				self.open()
			except:
				syslog(LOG_ERR, "SerialForwarderTinyOS could not restart TinyOS serial forwarder because of error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

	def read(self):
		ret = None
		try:
			if (self.sf_proto_len == -1):
				# Get length of next packet:
				data = self.sf.recv(1)
			else:
				# Get packet:
				data = self.sf.recv(self.sf_proto_len)
			if data == '':
				# Check if process is still running:
				self.check_sf_proc()
				self.sf_proto_len = -1
			else:
				if (self.sf_proto_len == -1):
					# Get length of packet
					self.sf_proto_len = ord(data)
				else:
					# Useful data was retrieved, insert it into queue:
					timestamp = time.time()
					self.sf_proto_len = -1
					self.num_elements_rcv = self.num_elements_rcv + 1
					ret = [data.encode("hex"), timestamp]
		except socket.timeout:
			pass
		except select.error, err:
			if (err[0] == 4):
				syslog(LOG_INFO, "SerialForwarderTinyOS interrupted due to caught stop signal.")
		except:
			syslog(LOG_ERR, "SerialForwarderTinyOS encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return ret

	def write(self, data):
		try:
			rs = self.sf.send(data)
			if rs != len(data):
				syslog(LOG_ERR, "SerialForwarderTinyOS error while writing: no of bytes written (%d) != no of bytes in data (%d)." %str(rs, len(data)))
			self.num_elements_snd = self.num_elements_snd + 1
		except socket.error, err:
			syslog(LOG_ERR, "SerialForwarderTinyOS error while writing to serial forwarder: %s" %str(err))
			self.close()
		except:
			syslog(LOG_ERR, "SerialForwarderTinyOS encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return None
### END SerialForwarderTinyOS



##############################################################################
#
# SerialForwarderContiki class
#
##############################################################################
class SerialForwarderContiki():
	def __init__(self, slotnr, serialdev, baudrate):
		self.serialdump_path	= '/usr/bin/contiki-serialdump'
		self.slotnr				= slotnr
		self.serialdev			= os.path.realpath(serialdev)
		self.baudrate			= baudrate
		self.num_elements_rcv 	= 0				# Number of packets received by the process. For statistical information.
		self.num_elements_snd 	= 0				# Number of packets sent by the process. For statistical information.
		self.sf_proc			= None

	def open(self):
		try:
			self.sf_proc = subprocess.Popen([self.serialdump_path, '-b%d'%self.baudrate, '-s', '%s'%self.serialdev], stdout=subprocess.PIPE, stdin=subprocess.PIPE, close_fds=False)
			syslog(LOG_INFO, "SerialForwarderContiki started contiki serialdump for device %s with baudrate %d"%(self.serialdev, self.baudrate))
		except Exception, err:
			syslog(LOG_ERR, "SerialForwarderContiki could not start contiki serialdump because: %s" % (str(sys.exc_info()[1])))
			return None
		syslog(LOG_INFO, "SerialForwarderContiki opened.")

	def close(self):
		try:
			if self.sf_proc != None:
				self.sf_proc.terminate()
				self.sf_proc.wait()
				syslog(LOG_INFO, "SerialForwarderContiki stopped contiki serialdump")
		except:
			syslog(LOG_ERR, "SerialForwarderContiki could not stop contiki serialdump because of error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		finally:
			self.sf_proc = None
		syslog(LOG_INFO, "SerialForwarderContiki stopped...%d packets received, %d packets sent."%(self.num_elements_rcv, self.num_elements_snd))

	def isRunning(self):
		if ((self.sf_proc != None) and (self.sf_proc.poll() == None)):
			return True
		return False

	def read(self):
		ret = None
		# Get data from serialdump:
		try:
			data = self.sf_proc.stdout.readline().strip()
			if (data != ''):
				# Useful data was retrieved, insert it into queue:
				timestamp = time.time()
				self.num_elements_rcv = self.num_elements_rcv +1
				ret = [data.encode("hex"), timestamp]
		except select.error, err:
			if (err[0] == 4):
				syslog(LOG_INFO, "SerialForwarderContiki interrupted due to caught stop signal.")
		except:
			syslog(LOG_ERR, "SerialForwarderContiki encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return ret

	def write(self, data):
		try:
			rs = self.sf_proc.stdin.write(data)
			if rs != len(data):
				syslog(LOG_ERR, "SerialForwarderContiki error while writing: no of bytes written (%d) != no of bytes in data (%d)." %str(rs, len(data)))
			self.num_elements_snd = self.num_elements_snd + 1
		except socket.error, err:
			syslog(LOG_ERR, "SerialForwarderContiki error while writing to serial forwarder: %s" %str(err))
			self.close()
		except:
			syslog(LOG_ERR, "SerialForwarderContiki encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		return None
### END SerialForwarderContiki



##############################################################################
#
# ThreadSerialReader thread
#
##############################################################################
def ThreadSerialReader(sf, mode, msgQueueDbBuf, msgQueueSockBuf, stopLock):
	lastTimestamp	= None
	maxCollectTime	= 2.0		# How long to collect data in ASCII/raw mode before sending it to DB buffer
	maxCollectSize	= 1024		# How many bytes to collect in ASCII/raw mode before sending it to DB buffer
	elem			= None
	data			= ''
	lastTimestamp	= time.time()
	timestamp		= time.time()
	sf_err_back_init= 0.5		# Initial time to wait after error on opening serial port
	sf_err_back_step= 0.5		# Time to increase backoff time to wait after error on opening serial port
	sf_err_back_max	= 5.0		# Maximum backoff time to wait after error on opening serial port
	sf_err_backoff  = sf_err_back_init # Time to wait after error on opening serial port

	syslog(LOG_ERR, "ThreadSerialReader started.")
	while stopLock.acquire(False):
		stopLock.release()
		if not sf.isRunning():
			rs = sf.open()
			if rs == None:
				# There was an error opening the serial device. Wait some time before trying again:
				time.sleep(sf_err_backoff)
				# Increase backoff time to wait
				sf_err_backoff = sf_err_backoff + sf_err_back_step
				if sf_err_backoff > sf_err_back_max:
					sf_err_backoff = sf_err_back_max
			else:
				sf_err_backoff = sf_err_back_init
			data = ''
			timestamp = lastTimestamp = 0
		if sf.isRunning():
			# Read data:
			try:
				elem = sf.read()
				if elem != None:
					# Data has been received.
					if elem[0] is None:
						elem[0]=''
					data = data + elem[0]
					timestamp = elem[1]
					if len(elem[0]) > 0:
						# Data is put directly onto the buffer queue for the socket:
						try:
							msgQueueSockBuf.put(elem[0], False)
						except Queue.full:
							syslog(LOG_ERR, "Queue msgQueueSockBuf full in ThreadSerialReader, dropping data.")
					if lastTimestamp == 0:
						lastTimestamp = timestamp
					#syslog(LOG_INFO, "---> ------------------------------------------")
					#if elem is None:
						#syslog(LOG_INFO, "---> Read data from SF: None")
					#else:
						#syslog(LOG_INFO, "---> Read data from SF: %s: >%s<"%(str(timestamp), str(elem[0])))
					#syslog(LOG_INFO, "---> data is now: >%s<"%(str(data)))
					#syslog(LOG_INFO, "---> timestamp: %s, lastTimestamp: %s"%(str(timestamp), str(lastTimestamp)))
					if len(data)>0:
						try:
							# In packet mode, the data can be handled directly. For ASCII/raw mode, data is collected for some time before being
							# put onto the DB buffer queue (to reduce the lines being generated in the CSV report file).
							if mode == 'pck':
								# Send data directly:
								if len(data) > 0:
									# Signal with 0, that data is from reader (use 1 for writer):
									msgQueueDbBuf.put([0,data,timestamp], False)
									data = ''
									timestamp = lastTimestamp = 0

							elif mode == 'ascii':
								# Wait for more data to come. Process data if it either contains newline characters, becomes to big or
								# if no new data is coming in for some time.
								if ((data.find('\r') != -1) or (data.find('\n') != -1)) or (len(data)>maxCollectSize) or ((timestamp-lastTimestamp)>maxCollectTime):
									if len(data) > 0:
										# Search for tails which have no newline at the end:
										newline_pos = max(data.rfind('\r'), data.rfind('\n'))
										if (newline_pos != -1) and (newline_pos != (len(data)-1)):
											# Leave the tail in the data:
											lines = data[:newline_pos].splitlines(False)
											data = data[newline_pos+1:]
										else:
											lines = data.splitlines(False)
											data = ''
										#if debug:
										#	syslog(LOG_INFO, "---> lines: >%s<"%(str(lines)))
										#	syslog(LOG_INFO, "---> data-tail: >%s<"%(str(data)))
										for line in lines:
											# Signal with 0, that data is from reader (use 1 for writer):
											msgQueueDbBuf.put([0,line,timestamp], False)
										if data == '':
											timestamp = lastTimestamp = 0

							elif mode == 'raw':
								# Wait for more data to come. Process data if it becomes to big or if no new data is coming in for some time.
								if (len(data)>maxCollectSize) or ((timestamp-lastTimestamp)>maxCollectTime):
									if len(data) > 0:
										# Signal with 0, that data is from reader (use 1 for writer):
										msgQueueDbBuf.put([0,data,timestamp], False)
										data = ''
										timestamp = lastTimestamp = 0
						except:
							syslog(LOG_ERR, "ThreadSerialReader could not insert data into queues because: %s: %s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1])))
							data = ''
							timestamp = lastTimestamp = 0
					else:
						lastTimestamp = 0
			except:
				syslog(LOG_ERR, "ThreadSerialReader encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
				sf.close()
	# Stop thread:
	syslog(LOG_ERR, "ThreadSerialReader stopping...")
	if sf.isRunning():
		sf.close()
	syslog(LOG_ERR, "ThreadSerialReader stopped.")

### END ThreadSerialReader()


##############################################################################
#
# ThreadSocketProxy thread
#
##############################################################################
def ThreadSocketProxy(msgQueueSockBuf, ServerSock, sf, msgQueueDbBuf, mode, stopLock):
	poll_timeout		= 1000
	READ_ONLY			= select.POLLIN | select.POLLPRI | select.POLLHUP | select.POLLERR
	READ_WRITE			= READ_ONLY | select.POLLOUT
	message_queues		= {}
	connection			= None
	fd_to_socket		= {}


	try:
		syslog(LOG_INFO, "ThreadSocketProxy started")

		# Initialize poller:
		poller = select.poll()
		poller.register(msgQueueSockBuf._reader, READ_ONLY)
		fd_to_socket[msgQueueSockBuf._reader.fileno()] = msgQueueSockBuf

		# Let thread run until stopLock is acquired:
		while stopLock.acquire(False):
			stopLock.release()
			try:
				if not ServerSock.isRunning():
					ServerSock.start()
				if not ServerSock.clientConnected():
					connection = ServerSock.waitForClient()
					if ServerSock.clientConnected():
						fd_to_socket[connection.fileno()] = connection
						poller.register(connection, READ_ONLY)
				# Wait for data:
				# drop data if client is not connected
				events = poller.poll(poll_timeout)

				for fd, flag in events:
					# Retrieve the actual socket from its file descriptor
					s = fd_to_socket[fd]
					# Handle inputs
					if flag & (select.POLLIN | select.POLLPRI):
						if s is connection:
							data = ServerSock.recv()
							timestamp = time.time()
							#if debug:
							#	syslog(LOG_INFO, "---> Received data from socket: %s: >%s<"%(str(timestamp), str(data)))
							if data == '':
								# That can only mean that the socket has been closed.
								poller.unregister(s)
								if ServerSock.isRunning():
									ServerSock.disconnectClient()
									continue
							if ((sf.__class__.__name__ == 'SerialForwarderTinyOS') and (mode == 'pck') and (data == 'U ')):
								# SF initialization string for TinyOS. Return it to socket to complete handshake:
								ServerSock.send(data)
								syslog(LOG_INFO, "SerialForwarderTinyOS completed handshake with SF server")
								continue
							# Send received data to serial forwarder and the DB buffer
							if not sf.isRunning():
								sf.close()
								sf.open()
							if sf.isRunning():
								sf.write(data)
								#if debug:
								#	syslog(LOG_INFO, "<--- Wrote data to SF: >%s<"%(str(data)))
								# Signal with 1, that data is from writer (use 0 for reader):
								try:
                							dataSanList = data.replace(b'\r', b'').split('\n')
                            						for i, dataSan in enumerate(dataSanList):
                                        					ts = timestamp + i * 0.000001 # with sligthly different timestamps we make sure that ordering is preserved
										msgQueueDbBuf.put([1, dataSan, ts], False)
								except Queue.full:
									syslog(LOG_ERR, "Queue msgQueueDbBuf full in ThreadSocketProxy, dropping data.")
								except Exception:
									syslog(LOG_ERR, "Serial data could not be sanitized, dropping data.")
						elif s is msgQueueSockBuf:
							# Retrieve element from queue:
							item = msgQueueSockBuf.get()
							# Forward element to socket:
							if ((ServerSock.isRunning()) and (ServerSock.clientConnected())):
								try:
									rs = ServerSock.send(item)
									#if debug:
									#	syslog(LOG_INFO, "<--- Sent data to socket (rs: %s, len: %d)"%(str(rs), len(item)))
									if rs != len(item):
										raise socket.error
								except socket.error, err:
									syslog(LOG_WARNING, "ThreadSocketProxy could not send data to socket because of encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
									poller.unregister(connection)
									if ServerSock.clientConnected():
										ServerSock.disconnectClient()
										continue
					elif flag & select.POLLHUP:
						# The POLLHUP flag indicates a client that "hung up" the connection without closing it cleanly.
						if ((s is connection) and (ServerSock.isRunning())):
							poller.unregister(s)
							if ServerSock.clientConnected():
								ServerSock.disconnectClient()
								continue
					elif flag & select.POLLERR:
						if ((s is connection) and (ServerSock.isRunning())):
							poller.unregister(s)
							ServerSock.disconnectClient()
							ServerSock.stop()
							continue
			except:
				syslog(LOG_ERR, "ThreadSocketProxy encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		# Stop the thread
		syslog(LOG_INFO, "ThreadSocketProxy stopping...")
		try:
			if ServerSock.isRunning():
				ServerSock.disconnectClient()
				ServerSock.stop()
		except:
			pass

	except:
		syslog(LOG_ERR, "ThreadSocketProxy encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

	syslog(LOG_INFO, "ThreadSocketProxy stopped.")
### END ThreadSocketProxy()



##############################################################################
#
# ProcDbBuf
#
##############################################################################
def ProcDbBuf(msgQueueDbBuf, stopLock, testid):
	_num_elements  = 0
	_dbfile        = None
	_dbfile_creation_time = 0
	_dbflushinterval = config.getint("serial", "dbflushinterval")
	_obsdbfolder	= "%s/%d" % (os.path.realpath(config.get("observer", 'obsdbfolder')),testid)

	def _get_db_file_name():
		return "%s/serial_%s.db" % (_obsdbfolder, time.strftime("%Y%m%d%H%M%S", time.gmtime()))

	try:
		syslog(LOG_INFO, "ProcDbBuf started")
		# set lower priority
		os.nice(1)

		# Let process run until stoplock is acquired:
		while stopLock.acquire(False):
			stopLock.release()
			try:
				# Wait for data in the queue:
				_waittime = _dbfile_creation_time + _dbflushinterval - time.time()
				if _waittime <= 0:
					if _dbfile is not None:
						_dbfile.close()
						syslog(LOG_INFO, "ProcDbBuf closed dbfile %s" % _dbfilename)
					_dbfilename = _get_db_file_name()
					_dbfile = open(_dbfilename, "wb")
					_dbfile_creation_time = time.time()
					_waittime = _dbflushinterval
					syslog(LOG_INFO, "ProcDbBuf opened dbfile %s" % _dbfilename)
				_service, _data, _ts = msgQueueDbBuf.get(True, _waittime)
				try:
					_len = len(_data)
				except:
					continue
				if _len > 0:
					_ts_sec = int(_ts)
					# Write to dbfile:
					if _dbfile is None:
						_dbfilename = _get_db_file_name()
						_dbfile = open(_dbfilename, "wb")
						_dbfile_creation_time = time.time()
						syslog(LOG_INFO, "ProcDbBuf opened dbfile %s" % _dbfilename)
					packet = pack("<Illl%ds" % _len,_len + 12, _service, _ts_sec, int((_ts - _ts_sec) * 1e6), _data)
					_dbfile.write(packet)
					_num_elements = _num_elements+1
			except Queue.Empty:
				continue
			except IOError, err:
				if (err[0] == 4):
					syslog(LOG_INFO, "ProcDbBuf interrupted due to caught stop signal.")
					continue
			except:
				syslog(LOG_ERR, "ProcDbBuf encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

		# Stop the process
		syslog(LOG_INFO, "ProcDbBuf stopping... %d elements received "%_num_elements)
	except:
		syslog(LOG_ERR, "ProcDbBuf encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

	# flush dbfile and errorfile
	try:
		if _dbfile is not None:
			_dbfile.close()
			syslog(LOG_INFO, "ProcDbBuf closed dbfile %s" % _dbfilename)
		syslog(LOG_INFO, "ProcDbBuf stopped.")
	except:
		syslog(LOG_ERR, "ProcDbBuf encountered error in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))

### END ProcDbBuf



##############################################################################
#
# stop_on_sig
#
##############################################################################
def stop_on_sig(ret_val=SUCCESS):
	"""Stop all serial forwarder threads and the output socket
	and exit the application.
	Arguments:
		ret_val:		Return value to exit the program with.
	"""
	global proc_list

	# Close all threads:
	syslog(LOG_INFO, "Closing %d processes/threads..."% len(proc_list))
	for (proc,stopLock) in proc_list:
		try:
			stopLock.acquire()
		except:
			syslog(LOG_ERR, "Could not acquire stop lock for process/thread.")
	syslog(LOG_INFO, "Joining %d processes/threads..."% len(proc_list))
	for (proc,stopLock) in proc_list:
		try:
			proc.join(10)
		except:
			syslog(LOG_ERR, "Could not stop process/thread.")
		if proc.is_alive():
			syslog(LOG_ERR, "Could not stop process/thread.")

	# Stop dbbuf process:
	syslog(LOG_INFO, "Closing ProcDbBuf process...")
	try:
		dbbuf_proc[1].acquire()
	except:
		syslog(LOG_ERR, "Could not acquire stoplock for ProcDbBuf process.")
	# Send some dummy data to the queue of the DB buffer to wake it up:
	msgQueueDbBuf.put([None, None, None])
	syslog(LOG_INFO, "Joining ProcDbBuf process...")
	try:
		dbbuf_proc[0].join(30)
	except:
		syslog(LOG_ERR, "Could not stop ProcDbBuf process.")
	if dbbuf_proc[0].is_alive():
		syslog(LOG_ERR, "Could not stop ProcDbBuf process.")

	# Remove the PID file if it exists:
	if os.path.exists(pidfile):
		try:
			os.remove(pidfile)
		except:
			syslog(LOG_WARN, "Could not remove pid file.")

	syslog(LOG_INFO, "FlockLab serial service stopped.")
	# Close the syslog and terminate the program
	closelog()
	return ret_val
### END stop_on_sig()


##############################################################################
#
# stop_on_api
#
##############################################################################
def stop_on_api():
	"""Stop all already running serial reader processes
	"""
	# Get PID of running serial reader (if any) from pidfile and send it the terminate signal.
	try:
		pid = int(open(pidfile,'r').read())
		# Signal the process to stop:
		if (pid > 0):
			syslog(LOG_INFO, "Main process: sending SIGTERM signal to process %d" %pid)
			try:
				os.kill(pid, signal.SIGTERM)
			except OSError:
				os.remove(pidfile)
				raise
			try:
				os.waitpid(pid, 0)
			except OSError:
				pass
		return SUCCESS
	except (IOError, OSError):
		#DEBUG syslog(LOG_ERR, "----->Main process: Error while trying to kill main process in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		# The pid file was most probably not present. This can have two causes:
		#   1) The serial reader service is not running.
		#   2) The serial reader service did not shut down correctly the last time.
		# As consequence, try to kill all remaining serial reader servce threads (handles 1)) and if that
		# was not successful (meaning cause 2) takes effect), return ENOPKG.
		try:
			patterns = ['flocklab_serial.py',]
			ownpid = str(os.getpid())
			suc = False
			for pattern in patterns:
				p = subprocess.Popen(['pgrep', '-f', pattern], stdout=subprocess.PIPE)
				out, err = p.communicate(None)
				if (out != None):
					for pid in out.split('\n'):
						if ((pid != '') and (pid != ownpid)):
							syslog(LOG_INFO, "Main process: Trying to kill process %s" %pid)
							os.kill(int(pid), signal.SIGKILL)
					suc = True
			if (suc == True):
				return SUCCESS
			else:
				return errno.ENOPKG
		except (OSError, ValueError):
			syslog(LOG_ERR, "Main process: Error while trying to kill old zombie threads in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
			return errno.EINVAL
### END stop_on_api()




##############################################################################
#
# Usage
#
##############################################################################
def usage():
	print "Usage: %s --targetos=<string> --testid=<int> [--port=<string>] [--baudrate=<int>] [--mode=<string>] [--socketport=<int>] [--stop] [--daemon] [--debug] [--help] [--version]" %sys.argv[0]
	print "Options:"
	print "  --targetos=<string>\t\tOperating system running on the target node."
	print "\t\t\t\tPossible values are: %s" %(str(targetos_list))
	print "  --testid=<int>\t\tID of the test."
	print "  --port=<string>\t\tOptional. Port over which serial communication is done."
	print "\t\t\t\tPossible values are: %s" %(str(port_list))
	print "  --baudrate=<int>\t\tOptional. Baudrate of serial device. Default is 115200."
	print "\t\t\t\tPossible values for targetos=contiki are: %s" %(str(baudrate_contiki2_list))
	print "\t\t\t\tPossible values for other targets are: %s" %(str(baudrate_list))
	print "  --mode=<string>\t\tOptional. Defines output mode. Default is pck. Possible values are: %s" %(str(mode_list))
	print "\t\t\t\tFor targetos=tinyos, specifies whether serial data transmitted will be as ASCII strings or packed in TOS messages."
	print "\t\t\t\tFor targetos=contiki, specifies whether serial data transmitted will be as ASCII strings or packed in SLIP packages."
	print "  --socketport=<int>\t\tOptional. If set, a server socket will be created on the specified port."
	print "  --stop\t\t\tOptional. Causes the program to stop a possibly running instance of the serial reader service."
	print "  --daemon\t\t\tOptional. If set, program will run as a daemon. If not specified, all output will be written to STDOUT and STDERR."
	print "  --debug\t\t\tOptional. Print debug messages to log."
	print "  --help\t\t\tOptional. Print this help."
	print "  --version\t\t\tOptional. Print version number of software and exit."
	return(0)
### END usage()



##############################################################################
#
# Main
#
##############################################################################
def main(argv):

	global proc_list
	global isdaemon
	global dbbuf_proc
	global pidfile
	global config
	global debug
	global msgQueueDbBuf

	port 		= 'usb'		# Standard port. Can be overwritten by the user.
	serialdev 	= None
	baudrate 	= 115200	# Standard baudrate. Can be overwritten by the user.
	mode 		= 'pck'		# Standard mode for tinyos and contiki. Can be overwritten by the user.
	targetos 	= None
	slotnr 		= None
	testid		= None
	socketport  = None
	stop		= False

	# Open the syslog:
	openlog('flocklab_serial', LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER)

	# Get config:
	config = flocklab.get_config()
	if not config:
		logger.warn("Could not read configuration file. Exiting...")
		sys.exit(errno.EAGAIN)

	# Get command line parameters.
	try:
		opts, args = getopt.getopt(argv, "ehvqdt:p:m:b:i:l:", ["stop", "help", "version", "daemon", "debug", "targetos=", "port=", "mode=", "baudrate=", "testid=", "socketport="])
	except getopt.GetoptError, err:
		syslog(LOG_ERR, str(err))
		usage()
		sys.exit(errno.EINVAL)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit(SUCCESS)
		elif opt in ("-v", "--version"):
			print version
			sys.exit(SUCCESS)
		elif opt in ("-d", "--debug"):
			syslog(LOG_INFO, "Debug option detected.")
			debug = True
		elif opt in ("-q", "--daemon"):
			isdaemon = True
		elif opt in ("-e", "--stop"):
			stop = True
		elif opt in ("-t", "--targetos"):
			targetos = arg
			if targetos not in targetos_list:
				err = "Wrong API usage: targetos not valid. Possible values are: %s" %(str(targetos_list))
				syslog(LOG_ERR, str(err))
				sys.exit(errno.EINVAL)
		elif opt in ("-b", "--baudrate"):
			if int(arg) not in baudrate_list:
				err = "Wrong API usage: baudrate not valid. Check help for possible baud rates."
				syslog(LOG_ERR, str(err))
				sys.exit(errno.EINVAL)
			else:
				baudrate = int(arg)
		elif opt in ("-p", "--port"):
			if arg not in port_list:
				err = "Wrong API usage: port not valid. Possible values are: %s" %(str(port_list))
				syslog(LOG_ERR, str(err))
				sys.exit(errno.EINVAL)
			else:
				port = arg
		elif opt in ("-m", "--mode"):
			if arg not in mode_list:
				err = "Wrong API usage: mode not valid. Possible values are: %s" %(str(mode_list))
				syslog(LOG_ERR, str(err))
				sys.exit(errno.EINVAL)
			else:
				mode = arg
		elif opt in ("-i", "--testid"):
			testid = int(arg)
		elif opt in ("-l", "--socketport"):
			socketport = int(arg)
		else:
			print "Wrong API usage"
			syslog(LOG_ERR, "Wrong API usage")
			usage()
			sys.exit(errno.EINVAL)

	# Check if the mandatory parameter --testid is set:
	if testid==None:
		print "Wrong API usage"
		syslog(LOG_ERR, "Wrong API usage")
		usage()
		sys.exit(errno.EINVAL)

	pidfile = "%s/%s" %(config.get("observer", "pidfolder"), "flocklab_serial_%d.pid" % testid)

	if stop:
		rs = stop_on_api()
		sys.exit(rs)

	# Check if the mandatory parameter --targetos is set:
	if targetos==None:
		print "Wrong API usage"
		syslog(LOG_ERR, "Wrong API usage")
		usage()
		sys.exit(errno.EINVAL)

	# Check that if targetos=contiki, the baudrate is correct:
	if targetos=='contiki':
		if baudrate not in baudrate_contiki2_list:
			err = "Wrong API usage: baudrate not valid. Check help for possible baud rates."
			print err
			syslog(LOG_ERR, err)
			sys.exit(errno.EINVAL)

	""" Check if daemon option is on. If on, reopen the syslog without the ability to write to the console.
		If the daemon option is on, later on the process will also be daemonized.
	"""
	if isdaemon:
		closelog()
		openlog('flocklab_serial', LOG_CONS | LOG_PID, LOG_USER)
		daemon.daemonize(pidfile=pidfile, closedesc=True)
		syslog(LOG_INFO, "Daemonized process")
	else:
		open(pidfile,'w').write("%d"%(os.getpid()))


	# Find out which target interface is currently activated.
	slotnr = flocklab.tg_interface_get()
	if not slotnr:
		err = "No interface active. Please activate one first."
		syslog(LOG_ERR, err)
		sys.exit(errno.EINVAL)
	syslog(LOG_INFO, "Active target interface detected as %d"%slotnr)
	# Make sure the right power source is on:
	if port == 'usb':
		serialdev = '/dev/flocklab/usb/target%d' %slotnr
		flocklab.tg_usbpwr_set(slotnr, 1)
		time.sleep(2.0)
	elif port == 'serial':
		serialdev = '/dev/ttyS1'

	# Initialize message queues ---
	msgQueueDbBuf = multiprocessing.Queue()
	msgQueueSockBuf = multiprocessing.Queue()

	# Initialize socket ---
	if not socketport is None:
		ServerSock = ServerSockets(socketport)

	# Initialize serial forwarder ---
	if (targetos == 'tinyos'):
		if (mode == 'pck'):
			sf = SerialForwarderTinyOS(slotnr,serialdev,baudrate)
		elif (mode in ('ascii', 'raw')):
			sf = SerialForwarderStandard(slotnr,serialdev,baudrate)
	elif (targetos == 'contiki'):
		if (mode == 'pck'):
			sf = SerialForwarderContiki(slotnr,serialdev,baudrate)
		elif (mode in ('ascii', 'raw')):
			sf = SerialForwarderStandard(slotnr,serialdev,baudrate)
	elif (targetos == 'other'):
		sf = SerialForwarderStandard(slotnr,serialdev,baudrate)

	# Start process for DB buffer ---
	stopLock = multiprocessing.Lock()
	p =  multiprocessing.Process(target=ProcDbBuf, args=(msgQueueDbBuf,stopLock, testid), name="ProcDbBuf")
	try:
		p.daemon = True
		p.start()
		time.sleep(1)
		if p.is_alive():
			dbbuf_proc = [p, stopLock]
			if debug:
				syslog(LOG_INFO, "DB buffer process running.")
		else:
			syslog(LOG_INFO, "DB buffer process is not running anymore.")
			raise Error
	except:
		syslog(LOG_ERR, "Error when starting DB buffer process in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		stop_on_sig(SUCCESS)
		sys.exit(errno.ECONNABORTED)

	# Start thread for serial reader ---
	stopLock = multiprocessing.Lock()
	p =  threading.Thread(target=ThreadSerialReader, args=(sf,mode,msgQueueDbBuf,msgQueueSockBuf,stopLock))
	try:
		p.daemon = True
		p.start()
		time.sleep(1)
		if p.is_alive():
			proc_list.append((p, stopLock))
			if debug:
				syslog(LOG_INFO, "Serial reader thread running.")
		else:
			syslog(LOG_INFO, "Serial reader thread is not running anymore.")
			raise Error
	except:
		syslog(LOG_ERR, "Error when starting serial reader thread in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		stop_on_sig(SUCCESS)
		sys.exit(errno.ECONNABORTED)

	# Start thread for socket proxy ---
	if not socketport is None:
		stopLock = multiprocessing.Lock()
		p =  threading.Thread(target=ThreadSocketProxy, args=(msgQueueSockBuf,ServerSock,
		sf,msgQueueDbBuf,mode,stopLock))
		try:
			p.daemon = True
			p.start()
			time.sleep(1)
			if p.is_alive():
				proc_list.append((p, stopLock))
				if debug:
					syslog(LOG_INFO, "Socket proxy thread running.")
			else:
				syslog(LOG_INFO, "Socket proxy thread is not running anymore.")
				raise Error
		except:
			syslog(LOG_ERR, "Error when starting socket proxy thread in line %d: %s: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
			stop_on_sig(SUCCESS)
			sys.exit(errno.ECONNABORTED)

	# Catch kill signal and ctrl-c
	signal.signal(signal.SIGTERM, sigterm_handler)
	signal.signal(signal.SIGINT, sigterm_handler)
	syslog(LOG_INFO, "Signal handler registered")

	syslog(LOG_INFO, "FlockLab serial service started.")

	""" Enter an infinite loop which hinders the program from exiting.
		This is needed as otherwise the thread list would get lost which would make it
		impossible to stop all threads when the service is stopped.
		The loop is stopped as soon as the program receives a stop signal.
	"""
	while 1:
		# Wake up once every now and then:
		time.sleep(10)

	sys.exit(SUCCESS)
### END main()


if __name__ == "__main__":
	try:
		main(sys.argv[1:])
	except SystemExit:
		pass
	except:
		syslog(LOG_ERR, "Encountered error in line %d: %s: %s: %s\n\n--- traceback ---\n%s--- end traceback ---\n\nCommandline was: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1]), str(traceback.print_tb(sys.exc_info()[2])), traceback.format_exc(), str(sys.argv)))
		sys.exit(errno.EAGAIN)
