/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * H file for shared library libflocklab.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Guard ---------------------------------------------------- */
#ifndef FLOCKLAB_H
#define FLOCKLAB_H

/* ---- Prototypes ----------------------------------------------------------*/
int is_module_loaded(char *modulename);											// Test whether kernel module is loaded at all
int is_instance_running(char *programname);										// Checks whether an instance of a user space program is already running or not.
int is_numeric(char *testvalue);												// Checks whether an argument is numeric or not.
unsigned long long int str_to_epoch(char *testvalue);							// Checks whether an argument is a date time string or not. Returns the extracted value in a timeval struct if it is a date time
int is_pin_setable(int pin);													// Checks whether a pin is in the list of GPIO pins allowed to be set
int is_pin_monitorable(int pin);												// Checks whether a pin is in the list of GPIO pins allowed to be monitored
int str_to_enum(char * str, char * enum_array[], int size );					// Returns the integer affiliated to an enum string.


/* ---- Include Guard ---------------------------------------------------- */
#endif

