/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * Library for functions used in several flocklab userspace programs.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>									/* Needed for string operations*/
#include <time.h>									/* Needed for time operations*/
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include "flocklab_shared.h"
#include "flocklab.h"
#include "flocklab_powerprof_shared.h"


/**************************************************************************************//**
 *
 * Checks whether the kernel module is loaded or not.
 *
 * @param modulename Name of the Moduel to check
 *
 * @return Returns 1 if module is loaded, 0 otherwise.
 *
 *****************************************************************************************/
int is_module_loaded(char *modulename)
{
	/* Local variables */
	FILE *fp;
	int buf_size = 80;
	char buf[buf_size];

	/* Test whether module is already loaded */
	fp = fopen("/proc/modules", "r");
	while ( (fgets(buf, buf_size, fp)) != NULL) {
		// Check whether the module name is contained in the line:
		if (strstr(buf, modulename)) {
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
} // is_module_loaded



/**************************************************************************************//**
 *
 * Checks whether an instance of a user space program is already running or not. This
 * 	excludes the instance which is calling the function (which allows to check whether
 * 	a program is already running when starting it up).
 *
 * @param programname Name of the program to check
 *
 * 	@return Returns 1 if an instance of the program is running, 0 otherwise.
 *
 *****************************************************************************************/
int is_instance_running(char *programname)
{
	/* Local variables */
	char *cmd;
	int len, ret_val = 1;

	// Allocate memory for the command line:
	len = strlen(programname) + 26;
	cmd = malloc(len*sizeof(char));

	// Generate the command line:
	snprintf(cmd, len, "pidof %s -o %i>/dev/null", programname, getpid());
	// Execute the command. Return 1 if program is not running, 0 if one or more instances are running.
	ret_val ^= system(cmd)/256;

	// Free memory and return:
	free(cmd);
	return ret_val;
} // is_instance_running



/**************************************************************************************//**
 *
 * Checks whether an argument is numeric or not.
 *
 * @param testvalue Value to check
 *
 * @return Returns 0 if not numeric, 1 otherwise.
 *
 *****************************************************************************************/
int is_numeric(char *testvalue)
{
	int i = 0;
	// Check the first value: it could be a minus sign:
	if ((testvalue[0] != '-') && ((testvalue[0] < '0') || (testvalue[0] > '9')))
		return 0;
	// Test the remaining characters:
	if (strlen(testvalue) > 1) {
		for (i=1; testvalue[i] != '\0'; i++) {
			// Test whether the current character is a number.
			if ((testvalue[i] < '0') || (testvalue[i] > '9')) {
				return 0;
			}
		}
	}
	return 1;
} // is_numeric




/**************************************************************************************//**
 *
 * Checks whether string 'testvalue' is a date time string of format %Y/%m/%d %H:%M:%s
 *    (e.g. 2009/02/22 23:59:00) in UTC
 *
 * @param testvalue Value to test
 *
 * @return -1 if string is not convertable, epoch value (unix timestamp) if string is convertable
 *
 *****************************************************************************************/
unsigned long long int str_to_epoch(char *testvalue)
{
	char *tmp, *testvalue_temp;
	struct tm tm_tmp;

	// Set local time to UTC:
	putenv("TZ=UTC");
	tzset();

	testvalue_temp = (char *) malloc(strlen(testvalue));
	testvalue_temp = strcpy(testvalue_temp, testvalue);

	// Check the length of the string: it has to be exactly 19:
	if (strlen(testvalue_temp) != 19) {
		free(testvalue_temp);
		return -1;
	} else {
		// Try to convert the datetime string into a timeval structure:
		tmp = (char *) strptime(testvalue_temp, "%Y/%m/%d %T", &tm_tmp);
		free(testvalue_temp);
		if ( tmp == NULL)
			return -1;
	}

	return mktime(&tm_tmp);
} // is_datetime


/**************************************************************************************//**
 *
 * Return the integer affiliated to an enum string.
 *
 * @param str The string to convert
 * @param enum_array The name of the enum array that holds all enums
 * @param top The top element in the enum
 *
 * @return -1 if the string provided is not part of the enum. Return the integer
 * 			corresponding to the enum otherwise.
 *
 *****************************************************************************************/
int str_to_enum(char * str, char * enum_array[], int top )
{
	/* Local variables	*/
	int i;

	for( i = 0; i < top; ++i )
		if( !strcmp( str, enum_array[i]))return i;
	return -1;
}



/**************************************************************************************//**
 *
 * Checks whether a pin is in the list of GPIO pins allowed to be set
 * (defined in flocklab_shared.h)
 *
 * @param pin Pin to check
 *
 * @return Return values: 0 if pin is not setable, 1 otherwise.
 *
 ****************************************************************************************/
int is_pin_setable(int pin)
{
	/* Local variables */
	int i;

	// Loop through the array and check if pin is in list:
	for (i=0;i<(sizeof(list_gpiopins_set)/sizeof(*list_gpiopins_set)); i++) {
		if (list_gpiopins_set[i] == pin)
			return 1;
	}

	return 0;
} // is_pin_setable



/**************************************************************************************//**
 *
 * Checks whether a pin is in the list of GPIO pins allowed to be monitored
 * (defined in flocklab_shared.h)
 *
 * @param pin Pin to check
 *
 * @return Return values: 0 if pin is not monitorable, 1 otherwise.
 *
 ****************************************************************************************/
int is_pin_monitorable(int pin)
{
	/* Local variables */
	int i;

	// Loop through the array and check if pin is in list:
	for (i=0;i<(sizeof(list_gpiopins_monitor)/sizeof(*list_gpiopins_monitor)); i++) {
		if (list_gpiopins_monitor[i] == pin)
			return 1;
	}

	return 0;
} // is_pin_monitorable







