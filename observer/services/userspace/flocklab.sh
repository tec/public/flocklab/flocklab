#! /bin/sh
# Author: Christoph Walser <walser@tik.ee.ethz.ch>
# Licence. GPL
# $Revision: 1775 $
# $Date: 2012-05-21 11:23:09 +0200 (Mon, 21 May 2012) $
# $Id: flocklab_gpiomonitor.sh 1775 2012-05-21 09:23:09Z walserc $
# $URL: svn://svn.ee.ethz.ch/flocklab/trunk/observer/services/userspace/flocklab_gpiomonitor.sh $
# based on skeleton from Debian GNU/Linux

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=flocklab.sh
DESC="FlockLab services"

set -e

case "$1" in
  start)
	echo -n "Starting $DESC... "
	start-stop-daemon --start --exec /usr/bin/flocklab_gpiomonitor -- "-start"
	/usr/bin/xc3sprog -c ft4232h -v -p 0 /lib/firmware/DAQ_Board_rev12.bit 2>&1 | logger
	echo "...Done"
	;;
  stop)
	echo -n "Stopping $DESC... "
	start-stop-daemon --start --exec /usr/bin/flocklab_gpiomonitor -- "-stop"
	start-stop-daemon --start --exec /sbin/modprobe -- "-r" "flocklab_ostimer"
	echo "...Done"
	;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop}" >&2
	exit 1
	;;
esac

exit 0

