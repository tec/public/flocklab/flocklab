/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 * @author Roman Lim <lim@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * This user program is a daemon which writes output from FlockLab services
 * to the local database on the Gumstix computer.
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */

/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <sys/signal.h>								/* Needed for catching the termination signal from flocklab_gpio_setting_stop*/
#include <string.h>									/* Needed for string operations*/
#include "flocklab_shared.h"						/* Contains shared definitions of userspace and kernelspace programs for all services */
#include "flocklab_powerprof_shared.h"
#include "flocklab_gpio_monitor_shared.h"
#include "flocklab_gpio_setting_shared.h"
#include <sys/ioctl.h>

//DEBUG #define free(x) printf("freeing "#x" %p at %s: %d\n", x, __FILE__, __LINE__);

/* ---- Global variables ---------------------------------------------------*/
#define VERSION 			3.02										// Version of the database daemon. Has to be incremented manually.
#define DBPATH 				"/home/root/mmc/flocklab/db/"				// Path to the DB WITH trailing backslash
#define EXIT_FAILURE 		1
#define ENTER_ATOMIC sigprocmask (SIG_BLOCK, &signal_mask, NULL)
#define EXIT_ATOMIC sigprocmask (SIG_UNBLOCK, &signal_mask, NULL)
#define MY_MACIG 'G'
#define READ_TRAILING _IOW(MY_MACIG, 1, int)
static volatile sig_atomic_t isTerminating = 0;							// Flag set if program is terminating (used to flush buffer)
static time_t time_last_written;										// Time at which the last write to the database occurred. At program startup, it holds the time when the while-loop was entered
static time_t time_last_flushed;
struct error_log_entry {
	struct timeval timestamp;
	int service_fk;
	char errormessage[1024];
};

struct varlen_payload {
	int len;
	char data[];
};

static const struct service_info * service;								// Variables naming the device which the daemon is working on
static sig_atomic_t useSocket;
short sock_error = 1;
struct sockaddr_un serv_addr, devserv_addr;
static int collect_num;													// Number of elements to collect before writing to database.
static int stdin_dup, stdout_dup, stderr_dup;							// Variables used for reopening STDIN, STDOUT and STDERR
static char *daemon_name = NULL;										// Name of daemon.
static char **collect_buf;												// Buffer to collect the data
static int collect_buf_pos_write = 0;									// Points to the last written entry in the buffer:
static sigset_t signal_mask;
static FILE * dbfile = NULL;
static char *dbfilename;
static FILE * errorfile = NULL;
static char * errorfilename;
static char *dbfolder;
static int testid;
int trailing_requested = 0;

/* ---- Prototypes ----------------------------------------------------------*/
static void daemonize(int quiet);
static void write_to_log(char *, int, int);
static void signal_handler(int);
static void write_db(void);
static short collect_powerprofile(int, int);
static short collect_gpiomonitor(int, int);
static short collect_gpiosetting(int, int);
static short collect_flockdaq(int, int);
static void getTimeString(char * name);
void api_usage();

/**************************************************************************************//**
 *
 * Main
 *
 * @param argc Argument count
 * @param argv Argument list
 *
 * @return 0 on success, errno otherwise
 *
 *****************************************************************************************/
int main( int argc, char *argv[] ) {
	/* Local variables */
	int i, switch1;
	int quiet = 0;
	int stop = 0;
	int argv0size;											// Size of process name argument (needed for renaming the daemon).
	int fd, sock, rs;
	struct stat stats;
	int sock_optval = 1;
	int sock_optlen = sizeof (int);
	short listdev_error = 1;
	char cmd[40];

	// Determine whether the quiet option is set and open the syslog accordingly:
	for (i = 1; i < argc; i++) {
		if (strcmp( argv[i], "--quiet") == 0) {
			quiet = 1;
			break;
		}
	}
	if (quiet) {
		openlog("flocklab_dbd", LOG_PID, LOG_USER);
	} else {
		openlog("flocklab_dbd", LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER);
	}

	// Set local time to UTC:
	putenv("TZ=UTC");
	tzset();

	// If there are less than 2 arguments, we can abort immediately:
	if ( argc < 2 ) {
		api_usage();
		syslog(LOG_ERR, "Wrong API use.");
		closelog();
		return EINVAL;
	}

	// Test whether the given API command is in the set of allowed API commands:
		if( strcmp( argv[1], "-start" ) == 0)				switch1 = 1;
		else if( strcmp( argv[1], "-stop" ) == 0)			switch1 = 2;
		else if( strcmp( argv[1], "-help" ) == 0)			switch1 = 3;
		else if( strcmp( argv[1], "-version" ) == 0)		switch1 = 4;

		switch( switch1 ) {
			case 1:	// -start
						if (!validate_api(argc, argv, switch1)) {
							syslog(LOG_ERR, "Wrong API usage");
							return EINVAL;
						}
						break;

			case 2:	// -stop
						if (!validate_api(argc, argv, switch1)) {
							syslog(LOG_ERR, "Wrong API usage");
							return EINVAL;
						} else {
							stop = 1;
						}
						break;

			case 3:	// -help
						api_usage();
						return SUCCESS;
						break;

			case 4:	// -version
						printf("%.2f\n", VERSION);
						return SUCCESS;
						break;

			default:
						syslog(LOG_ERR, "Command not recognized. For help, use flocklab_gpiomonitor -help");
						return EINVAL;
						break;
		}


	// If dbd is to be stopped, do it now and exit:
	if (stop){
		// TODO: kill only dbd if belonging to test
		sprintf(cmd, "pkill -SIGINT -f %s\0", service->dbd_name);
		system(cmd);
		write_to_log("Sent stop signal to any running database daemon instances...", 0, LOG_INFO);
		return SUCCESS;
	}

	// create empty error_log
	errorfilename = (char *) malloc(strlen(dbfolder)+strlen(service->name)+6+19);
	snprintf(errorfilename, strlen(dbfolder)+strlen(service->name)+6+19, "%serror_%s", dbfolder, service->name);
	getTimeString(errorfilename);
	
	/* Daemonize the process */
	write_to_log("Database daemon started", 0, LOG_INFO);
	daemonize(quiet);
	// Rename the daemonized process to reflect the service it is working for. This name will be seen when running 'ps -x' on the Gumstix
	//DEBUG syslog(LOG_ERR, "dbd_name: >%s<, strlen: %i, strlen argv[0]: %i", service->dbd_name, strlen(service->dbd_name), strlen(argv[0]));
	strncpy(argv[0], service->dbd_name, strlen(service->dbd_name));

	/* Catch signals */
	signal(SIGINT, signal_handler);
	sigemptyset(&signal_mask);
	sigaddset(&signal_mask, SIGINT);
	ENTER_ATOMIC;

	// Initialize the collection buffer:
	collect_buf = malloc(collect_num*sizeof(char *));

	// Reset the variables for the transaction calculation:
	time_last_written = time(NULL);
	time_last_flushed = time_last_written;

	if (useSocket) {
		// Create socket address to send to:
		bzero((char *) &serv_addr, sizeof(struct sockaddr_un));
		serv_addr.sun_family = AF_UNIX;
		strcpy(serv_addr.sun_path, service->socket);
	}

	// Enter the loop which holds the actual tasks of the daemon:
	while (isTerminating == 0) {
		// Try to open the list device for reading: it is either a file, a character device or a socket.
		if (listdev_error) {
			stat(service->device, &stats);
			if (S_ISREG(stats.st_mode)) {
				// Device is a regular file:
				rs = open(service->device, O_RDONLY);
				fd = rs;
			} else if (S_ISCHR(stats.st_mode)){
				// Device is a character device:
				rs = open(service->device, O_RDONLY);
				fd = rs;
			} else if (S_ISSOCK(stats.st_mode)) {
				// Device is a socket:
				devserv_addr.sun_family = AF_UNIX;
				strcpy(devserv_addr.sun_path, service->device);
				fd = socket(AF_UNIX, SOCK_STREAM, 0);
				rs = connect(fd, (struct sockaddr *) &devserv_addr, sizeof(devserv_addr));
			} else {
				// Device is neither a file nor a socket, thus return an error:
				write_to_log("Device file is neither a file nor a socket. Can thus not open it. Aborting daemon.", 1, LOG_ERR);
				//DEBUG syslog(LOG_CRIT, "%i cannot open %s because: %s", __LINE__, service->device, strerror(errno));
				return ENOENT;
			}
			// Check if device opened correctly:
			if (rs < 0) {
				write_to_log("Cannot open device file. Aborting daemon.", 1, LOG_ERR);
				//DEBUG syslog(LOG_CRIT, "%i cannot open %s because: %s", __LINE__, service->device, strerror(errno));
				return ENOENT;
			}
		}

		/* Socket output handling */
		if ( useSocket && sock_error) {
			// Create socket:
			close(sock);
			sock = socket(AF_UNIX, SOCK_DGRAM | SOCK_NONBLOCK, 0);
			if (sock < 0) {
				write_to_log("Cannot create outgoing socket. Aborting daemon...", 1, LOG_ERR);
				return ENOENT;
			}
			setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *) &sock_optval, sock_optlen);

			sock_error = 0;
		}

		/*
		 * Read the file which contains the contents of the processed events buffer. The file is read line by line and
		 * the contents are inserted into a ringbuffer. This has to be done as efficiently as possible as the runtime
		 * of the database daemon highly depends on the runtime of this loop.
		 *
		 * A separate function for each service should be implemented.
		 */
		switch(service->fk) {
		case 1:	// gpio monitor
			listdev_error = collect_gpiomonitor(fd, sock);
			break;
		case 2:	// gpio setting
			listdev_error = collect_gpiosetting(fd, sock);
			break;
		case 3: // powerprofiling
			listdev_error = collect_powerprofile(fd, sock);
			break;
		case 4: // flockdaq
			listdev_error = collect_flockdaq(fd, sock);
			break;
		}
		// Close the device (this is the same operation for both files and sockets):
		if (listdev_error) {
			close(fd);
		}
		if (sock_error) {
			close(sock);
			sock = -1;
		}

		/* Write all pending entries to the database if one of the following cases is true:
		      - There are collect_num pending entries in the buffer
		      - There are pending entries in the buffer and the last time data was written is at least 5 minutes ago
		      - The program is terminating and thus signal_handler() has set isTerminating to 1
		 */
		if ((collect_buf_pos_write >= collect_num) || ((collect_buf_pos_write > 0) && (difftime(time(NULL), time_last_written) >= 300.0)) || (isTerminating == 1)) {
			//DEBUG syslog(LOG_ERR, "--> %i: going into write_db()", __LINE__);
			write_db();
			//DEBUG syslog(LOG_ERR, "--> %i: got out of write_db()", __LINE__);
		}
	}
	
	// if we use the flockdaq, we need to read the device once again for any trailing bytes and if they haven't been received yet
	if(service->fk == 4) {
		if (fd >= 0) {
			rs = ioctl(fd, READ_TRAILING, "test");
			if (rs < 0) {
				syslog(LOG_ERR, "error first ioctl, return val = %d, error = %s\n", rs, strerror(errno));
			}
			else {
				write_to_log("Requested trailing bytes...", 0, LOG_INFO);
				trailing_requested = 1;
				collect_flockdaq(fd, sock);
			}
		} else {
			syslog(LOG_ERR, "device not open, fd =%d\n", fd);
		}
	}
	
	if (!listdev_error) {
		close(fd);
	}
	close(sock);

	/* The isTerminating flag has been set to 1, thus cleanup and exit the program: */
	// If there is data in the buffer, write it to the database:
	//DEBUG syslog(LOG_ERR, "buffer: %d", collect_buf_pos_write);
	if (collect_buf_pos_write > 0) {
		write_to_log("Flushing processed events buffer...", 0, LOG_INFO);
		write_db();
		write_to_log("Processed events buffer flushed", 0, LOG_INFO);
	}

	if (dbfile!=NULL) {
		fflush(dbfile);
		fsync(fileno(dbfile));
		fclose(dbfile);
	}

	// Free memory:
	if (dbfilename!=NULL) {
		free(dbfilename);
	}
	free(daemon_name);
	free(collect_buf);
	free(errorfilename);
	
	// Restore the standard file descriptors
	dup2(stdin_dup, STDIN_FILENO);
	if (!quiet) {
		dup2(stdout_dup, STDOUT_FILENO);
		dup2(stderr_dup, STDERR_FILENO);
	}

	write_to_log("Database daemon stopped", 0, LOG_INFO);

	return SUCCESS;
} // main



/**************************************************************************************//**
 *
 * Function to validate user input for API-commands "start".
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 * 			On successful validation, the needed global variables are filled with their
 * 			respective values.
 *
 * @param argc Argument count from main()
 * @param argv Argument list from main()
 * @param api_command Depicts the type of validation:
 * 					  1 is 'start'
 * 					  2 is 'stop'
 *
 ****************************************************************************************/
int validate_api(int argc, char *argv[], int api_command)
{
	/* Local variables */
	int i;
	int switch1 = -1;
	char *temp;
	// Check for right api_command:
	if ((api_command != 1) && (api_command != 2)) {
		syslog(LOG_ERR, "Illegal api_command.");
		// Unknown command:
		return 0;
	}
	// Check number of parameters:
	if (api_command == 1) {	// 'start' command
		// There need to be at least 3 parameters and at most 5 parameters:
		if ((argc < 5) || (argc > 7)) {
			syslog(LOG_ERR, "Wrong number of arguments: %d",argc);
			return 0;
		}
	} else if (api_command == 2) {	// 'stop' command
		// There need to be at least 2 parameter and at most 3 parameters:
		if ((argc < 4) || (argc > 5)) {
			syslog(LOG_ERR, "Wrong number of arguments: %d",argc);
			return 0;
		}
	}
	// Reset all global variables:
	useSocket = 0;
	collect_num = -1;
	service = NULL;

	// Fill the global variables with their respective arguments:
	for (i=2; i < argc; i++){
		// Test if the argument is in the list of expected arguments and assign it a number for the switching process:
		if( strncmp( 		argv[i], "--service=",			strlen("--service=")) == 0)			switch1 = 0; // Do NOT change the value for switch1 as it is used in validate_value()
		if( strncmp( 		argv[i], "--testid=",			strlen("--testid=")) == 0)			switch1 = 1; // Do NOT change the value for switch1 as it is used in validate_value()
		else if( strncmp( 	argv[i], "--threshold=", 		strlen("--threshold=")) == 0)		switch1 = 2; // Do NOT change the value for switch1 as it is used in validate_value()
		else if( strncmp( 	argv[i], "--socket",			strlen("--socket")) == 0)			switch1 = 3; // Do NOT change the value for switch1 as it is used in validate_value()
		else if( strcmp( 	argv[i], "--quiet") == 0)											switch1 = 4; // Do NOT change the value for switch1 as it is used in validate_value()
		if (switch1 < 0) {
			syslog(LOG_ERR, "Illegal argument detected.");
			return 0;
		}
		// Extract the argument value (except for options without value). If it is empty, abort.
		temp = strtok(argv[i],"=");
		temp = strtok(NULL,"=");
		if ((temp == NULL) && (switch1 < 3)) {
			return 0;
		}
		if (validate_value(temp, switch1) == 0) {
			return 0;
		}
	}
	// Check whether all needed variables have been set. If not, abort:
	if (api_command == 1) {	// 'start' command
		if ((service == NULL) || (collect_num == -1)) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		}
	} else if (api_command == 2) { // 'stop' command
		if ((service == NULL)) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		}
	}

	// We made it that far, so all arguments are valid.
	return 1;

} // validate_api



/**************************************************************************************//**
 *
 * Function to validate a single value for -start or -stop API commands
 *
 * @param temp Value to validate
 * @param argument Argument for switching
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 * 	       On successful validation, the respective global variable is filled with its value.
 *
 *****************************************************************************************/
int validate_value(char *temp, int argument)
{
	/* Local variables */
	char *temp_param;
	int i, rs;

	// Validate the argument and fill the value into the right global variable:
	switch( argument ) {
		case 0:
			// Service. Needs to be a string and in the list of allowed services:
			for (i=0;i< (sizeof flocklab_services / sizeof flocklab_services[0]);i++) {
				if (strcmp(temp, flocklab_services[i].name) == 0) {
					service = &flocklab_services[i];
					break;
				}
			}
			if (service == NULL) {
				syslog(LOG_ERR, "Service is not valid.");
				return 0;
			}
			break;
		case 1: // testid. Needs to be an integer:
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "Testid is not numeric.");
				return 0;
			} else if (atoi(temp) <= 0) {
				syslog(LOG_ERR, "Testid is not greater than 0.");
				return 0;
			} else {
				dbfolder = malloc(strlen(DBPATH)+strlen(temp)+2);
				testid = atoi(temp);
				snprintf(dbfolder, strlen(DBPATH)+strlen(temp)+2, DBPATH "%s/", temp);
			}
			break;
		case 2:
			// Threshold. Needs to be an integer:
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "Threshold is not numeric.");
				return 0;
			} else if (atoi(temp) <= 0) {
				syslog(LOG_ERR, "Threshold is not greater than 0.");
				return 0;
			} else {
				collect_num = atoi(temp);
			}
			break;

		case 3:
			// Socket. Just needs to be present:
			useSocket = 1;
			break;

		case 4:
			// Quiet option. Can be ignored as it was already detected in main();
			break;

		default:
			return 0;
	}

	// We made it that far, so the data is valid.
	return 1;
} // validate_value



/**************************************************************************************//**
 *
 * Writes all pending entries to the database.
 *
 *****************************************************************************************/
static void write_db(void)
{
	/* Local variables */
	int i;
	uint32_t collect_size;
	int written;
	struct powerprof_sample_pckt *packet;
	void * buf;
	
	if (collect_buf_pos_write > 0) {
		// open file if not already open
		if (dbfile==NULL) {
			if (dbfilename!=NULL)
				free(dbfilename);
			dbfilename = (char *) malloc(strlen(dbfolder)+strlen(service->name)+19);
			snprintf(dbfilename, strlen(dbfolder)+strlen(service->name)+19, "%s%s", dbfolder, service->name);
			getTimeString(dbfilename);
			dbfile = fopen(dbfilename, "wb");
			if (dbfile==NULL) {
				// it did not work
				syslog(LOG_ERR, "Could not open database file for writing.");
				return;
			}			
		}
		for (i = 0; i < collect_buf_pos_write; i++) {
			// write entries to file, according to this format:
			// <uint32_t> size of buffer, <char[]> content of buffer
			switch (service->fk) {
				case 1: // gpio monitor
					collect_size = sizeof(struct monitor_job_report);
					buf = collect_buf[i];
					break;
				case 2: // gpio setting
					collect_size = sizeof(struct gpioset_event);
					buf = collect_buf[i];
					break;
				case 3: // powerprofiling
					packet = (struct powerprof_sample_pckt *) collect_buf[i];
					collect_size = offsetof(struct powerprof_sample_pckt, sample_buf) + packet->sample_count * sizeof(packet->sample_buf[0]);
					buf = collect_buf[i];
					break;
				case 4:
					collect_size = ((struct varlen_payload *)collect_buf[i])->len;
					buf = ((struct varlen_payload *)collect_buf[i])->data;
					break;
			}
			written = 0;
			written += fwrite(&collect_size, sizeof collect_size, 1, dbfile);
			written += fwrite(buf, collect_size, 1, dbfile);
			// Check for errors in the binding process:
 			if (written != 2) {
				syslog(LOG_ERR, "Could not write to database file, written %i of 2 items.", written);
 				return;
 			}
		}

		// Free all memory used by the collection buffer entries:
		for (i = 0; i < collect_buf_pos_write; i++) {
			free(collect_buf[i]);
		}

		// Reset all variables:
		collect_buf_pos_write = 0;
		time_last_written = time(NULL);
		
	}
	// change file if needed
	if (difftime(time(NULL), time_last_flushed) >= 300.0) {
		if (dbfile != NULL) {
			fflush(dbfile);
			fsync(fileno(dbfile));
			fclose(dbfile);
			dbfile = NULL;
		}
		// error file
		snprintf(errorfilename, strlen(dbfolder)+strlen(service->name)+6+19, "%serror_%s", dbfolder, service->name);
		getTimeString(errorfilename);
		time_last_flushed = time(NULL);
	}
	
	return;
} // write_db


/**************************************************************************************//**
 *
 * Creates a daemon out of the process.
 *
 *****************************************************************************************/
static void daemonize(int quiet)
{
	/* Local variables */
	pid_t pid, sid;		// Process and session ID


	// Are we already a daemon?
	if ( getppid() == 1 ) return;

	// Fork off the parent process
	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}
	// If we got a good PID, then we can exit the parent process.
	if (pid > 0) {
		exit(SUCCESS);
	}

	/* At this point we are executing as the child process */

	// Change the file mode mask
	umask(0);

	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
		write_to_log("SID could not be created.", 1, LOG_ERR);
		exit(EXIT_FAILURE);
	}

	// Change the current working directory.  This prevents the current directory from being locked; hence not being able to remove it.
	if ((chdir("/")) < 0) {
		write_to_log("Working directory could not be changed to root.", 1, LOG_ERR);
		exit(EXIT_FAILURE);
	}

	// Close standard file descriptors as a daemon does not communicate over the terminal:
	stdin_dup = dup(STDIN_FILENO);
	close(STDIN_FILENO);
	if (!quiet) {
		stdout_dup = dup(STDOUT_FILENO);
		stderr_dup = dup(STDERR_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
	}

} // daemonize


/**************************************************************************************//**
 *
 * Writes the provided text together with a timestamp to the logfile.
 *
 * @param logtext String to log
 * @param log_to_db If 'log_to_db' is 1, the logmessage is also written to the database (tbl_obs_errorlog)
 * @param severity 'Severity' indicates the level of the logmessage when writing it to syslog.
 *		  Possible values are: LOG_DEBUG -> LOG_INFO -> LOG_NOTICE -> LOG_WARNING
 *		  -> LOG_ERR -> LOG_CRIT -> LOG_ALERT
 *
 *****************************************************************************************/
static void write_to_log(char *logtext, int log_to_db, int severity)
{
	/* Local variables */
	struct error_log_entry * error_log;
	int written = 0;
	uint32_t errorsize;
	int textlen = strlen(logtext);

	// Write Message to syslog:
	syslog(severity, "%s: %s", service->name, logtext);

	// Write logmessage to database if wanted:
	if (log_to_db == 1) {
		// mask signal because of write function
		if (textlen > 1024)
			textlen = 1024;
		error_log = (struct error_log_entry *) malloc(sizeof(struct error_log_entry));
		// Generate a timestamp:		
		gettimeofday(&(error_log->timestamp), NULL);		
		memcpy(&(error_log->errormessage), logtext, textlen);
		error_log->service_fk = service->fk;
		errorsize = offsetof(struct error_log_entry, errormessage) + textlen;
		// Open the database:
		errorfile = fopen(errorfilename, "ab");
		if (errorfile!=NULL) {
			written += fwrite(&errorsize, sizeof errorsize, 1, errorfile);
			written += fwrite(error_log, errorsize, 1, errorfile);
			free(error_log);
			if (written!=2) {
				syslog(LOG_ERR, "%s: %s", service->name, "Could not write to error log dbfile.");
			}
			fclose(errorfile);
		}
	}
} // write_to_log

/**************************************************************************************//**
 *
 * Signal handler that catches the kill and flush signals and acts accordingly.
 *
 * @param sig Signal to handle
 *
 *****************************************************************************************/
static void signal_handler(int sig)
{
	switch(sig) {
		case SIGINT:
			// Set the termination flag for main()
			isTerminating = 1;
			break;
		default:
			break;
	}
	return;
} // signal_handler


/**************************************************************************************//**
 *
 * Data collection routine for the flockdaq service (all three together)
 *
 * @param file File descriptor to the opened device file.
 * @param file Socket descriptor to the opened outgoing socket (if it exists)
 *
 *****************************************************************************************/
static short collect_flockdaq(int file, int sock)
{
	/* Local variables */
	unsigned char *buffer;
	int nread;
	

	// Allocate memory for the buffer:
	buffer = malloc(ADS1271_FLOCKDAQ_BLOCK_SIZE);
	if (buffer==NULL) {
		syslog(LOG_ERR, "collect_flockdaq: could not allocate tmpbuffer of size %d", ADS1271_FLOCKDAQ_BLOCK_SIZE);
		return 0;
	}	
  
	nread = 0;
	while ((collect_buf_pos_write < collect_num) && (isTerminating == 0 || trailing_requested)) {
		EXIT_ATOMIC;
		nread = read(file, buffer, ADS1271_FLOCKDAQ_BLOCK_SIZE);
		ENTER_ATOMIC;
		//syslog(LOG_INFO, "collect_flockdaq: nread = %d", nread);
		if (nread <= ADS1271_FLOCKDAQ_BLOCK_SIZE && nread != 0) {
			// Allocate memory for the new entry and copy the buffer contents to it:
			// syslog(LOG_ERR, "collect_flockdaq: block size= %d", nread);
			collect_buf[collect_buf_pos_write] = malloc(nread + sizeof(int));
			if (collect_buf[collect_buf_pos_write] != NULL) {
				memcpy(((struct varlen_payload*)collect_buf[collect_buf_pos_write])->data, buffer, nread);
				((struct varlen_payload*)collect_buf[collect_buf_pos_write])->len = nread;
				// If the outbound socket is to be used, send the data to it:
				if (!sock_error) { // It is sufficient to just trigger on sock_error as this variable is only set to 0 if useSock is set as well.
				  if ( sendto(sock, ((struct varlen_payload*)collect_buf[collect_buf_pos_write])->data, nread, MSG_DONTWAIT, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
						// Ignore errors which are caused by a non-running receiving server:
						if ( (errno!=EAGAIN) && (errno!=EWOULDBLOCK) && (errno!=ECONNREFUSED) && (errno!=ENOENT) ) {
						  sock_error = 1;
						  syslog(LOG_ERR, "Socket error %i (%s) occurred.", errno, strerror(errno));
						}
				  }
				}
				collect_buf_pos_write++;
		  } else {
				syslog(LOG_ERR, "collect_flockdaq: could not allocate buffer of size %d", nread);
		  }
		}
		if (trailing_requested) {
			trailing_requested = 0;
			write_to_log("received trailing bytes", 0, LOG_INFO);
		}
	}
	// Free memory:
	free(buffer);
	return (nread==0) && (isTerminating == 0);
} // collect_powerprofile

/**************************************************************************************//**
 *
 * Data collection routine for the powerprofiling service
 *
 * @param file File descriptor to the opened device file.
 * @param file Socket descriptor to the opened outgoing socket (if it exists)
 *
 *****************************************************************************************/
static short collect_powerprofile(int file, int sock)
{
	/* Local variables */
	struct powerprof_sample_pckt *buffer;
	int nread;

	// Allocate memory for the buffer:
	buffer = malloc(sizeof(struct powerprof_sample_pckt));
	if (buffer==NULL) {
		syslog(LOG_ERR, "collect_powerprofile: could not allocate tmpbuffer of size %d", sizeof(struct powerprof_sample_pckt));
		return 0;
	}	

	while ((collect_buf_pos_write < collect_num) && (isTerminating == 0)) {
		EXIT_ATOMIC;
		nread = read(file, buffer, sizeof(struct powerprof_sample_pckt));
		ENTER_ATOMIC;
		if (nread >= offsetof(struct powerprof_sample_pckt, sample_buf)) {
			nread = offsetof(struct powerprof_sample_pckt, sample_buf) + (buffer->sample_count) * sizeof(buffer->sample_buf[0]);
			if (nread <= sizeof(struct powerprof_sample_pckt)) {
				// Allocate memory for the new entry and copy the buffer contents to it:
				collect_buf[collect_buf_pos_write] = malloc(nread);
				if (collect_buf[collect_buf_pos_write] != NULL) {
					memcpy(collect_buf[collect_buf_pos_write], buffer, nread);

					// If the outbound socket is to be used, send the data to it:
					if (!sock_error) { // It is sufficient to just trigger on sock_error as this variable is only set to 0 if useSock is set as well.
						if ( sendto(sock, collect_buf[collect_buf_pos_write], nread, MSG_DONTWAIT, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
							// Ignore errors which are caused by a non-running receiving server:
							if ( (errno!=EAGAIN) && (errno!=EWOULDBLOCK) && (errno!=ECONNREFUSED) && (errno!=ENOENT) ) {
								sock_error = 1;
								syslog(LOG_ERR, "Socket error %i (%s) occurred.", errno, strerror(errno));
							}
						}
					}
					collect_buf_pos_write++;
				}
				else {
					syslog(LOG_ERR, "collect_powerprofile: could not allocate buffer of size %d", nread);
				}
			}
			else {
				syslog(LOG_ERR, "collect_powerprofile: sample count %d too big", buffer->sample_count);
				nread=0;
				break;
			}
		}
		else {
			if (nread > 0)
				syslog(LOG_ERR, "collect_powerprofile: Could not read full packet header, nread=%d (should be %d)", nread, offsetof(struct powerprof_sample_pckt, sample_buf));
			nread=0;
			break;
		}
	}

	// Free memory:
	free(buffer);

	return nread==0;
} // collect_powerprofile



/**************************************************************************************//**
 *
 * Data collection routine for the gpio monitoring service
 *
 * @param file File descriptor to the opened device file.
 * @param file Socket descriptor to the opened outgoing socket (if it exists)
 *
 *****************************************************************************************/
static short collect_gpiomonitor(int file, int sock)
{
	/* Local variables */
	struct monitor_job_report *buffer;
	int i, nread, nelem;

	// Allocate memory for the buffer:
	buffer = malloc(collect_num*sizeof(struct monitor_job_report));
	if (buffer==NULL) {
		syslog(LOG_ERR, "collect_gpiomonitor: could not allocate tmpbuffer of size %d", collect_num*sizeof(struct monitor_job_report));
		return 0;
	}	

	while ((collect_buf_pos_write < collect_num) && (isTerminating == 0)) {
		EXIT_ATOMIC;
		nread = read(file, buffer, (collect_num-collect_buf_pos_write)*sizeof(struct monitor_job_report));
		ENTER_ATOMIC;
		if (nread > 0) {
			// Allocate memory for every new entry and copy the buffer contents to it:
			nelem = nread / sizeof(struct monitor_job_report);
			//DEBUG printf("nelem: %d, nread: %d, sizeof: %d\n", nelem, nread, sizeof(struct gpioset_event));
			for (i=0; i<nelem; i++) {
				collect_buf[collect_buf_pos_write] = malloc(sizeof(struct monitor_job_report));
				if (collect_buf[collect_buf_pos_write]!=NULL) {
					memcpy(collect_buf[collect_buf_pos_write], &(buffer[i]), sizeof(struct monitor_job_report));
					// If the outbound socket is to be used, send the data to it:
					if (!sock_error) { // It is sufficient to just trigger on sock_error as this variable is only set to 0 if useSock is set as well.
						if ( sendto(sock, collect_buf[collect_buf_pos_write], sizeof(struct monitor_job_report), MSG_DONTWAIT, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
							// Ignore errors which are caused by a non-running receiving server:
							if ( (errno!=EAGAIN) && (errno!=EWOULDBLOCK) && (errno!=ECONNREFUSED) && (errno!=ENOENT) ) {
								sock_error = 1;
								syslog(LOG_ERR, "Socket error %i (%s) occurred.", errno, strerror(errno));
							}
						}
					}
					collect_buf_pos_write++;
				}
				else {
					syslog(LOG_ERR, "collect_gpiomonitor: could not allocate buffer of size %d", sizeof(struct monitor_job_report));
				}
			}
			if (nread % sizeof(struct monitor_job_report) != 0) {
				syslog(LOG_ERR, "collect_gpiomonitor: packet size %d did not match", nread);
				nread=0;
				break;
			}
		}
	}
	// Free memory:
	free(buffer);

	return nread==0;
} // collect_gpiomonitor


/**************************************************************************************//**
 *
 * Data collection routine for the gpio setting service
 *
 * @param file File descriptor to the opened device file.
 * @param file Socket descriptor to the opened outgoing socket (if it exists)
 *
 *****************************************************************************************/
static short collect_gpiosetting(int file, int sock)
{
	/* Local variables */
	struct gpioset_event *buffer;
	int i, nread, nelem;

	// Allocate memory for the buffer:
	buffer = malloc(collect_num*sizeof(struct gpioset_event));
	if (buffer==NULL) {
		syslog(LOG_ERR, "collect_gpiosetting: could not allocate tmpbuffer of size %d", collect_num*sizeof(struct gpioset_event));
		return 0;
	}

	while ((collect_buf_pos_write < collect_num) && (isTerminating == 0)) {
		EXIT_ATOMIC;
		nread = read(file, buffer, (collect_num-collect_buf_pos_write)*sizeof(struct gpioset_event));
		ENTER_ATOMIC;
		if (nread > 0) {
			// Allocate memory for every new entry and copy the buffer contents to it:
			nelem = nread / sizeof(struct gpioset_event);
			//DEBUG printf("nelem: %d, nread: %d, sizeof: %d\n", nelem, nread, sizeof(struct gpioset_event));
			for (i=0; i<nelem; i++) {
				collect_buf[collect_buf_pos_write] = malloc(sizeof(struct gpioset_event));
				if (collect_buf[collect_buf_pos_write]!=NULL) {
					memcpy(collect_buf[collect_buf_pos_write], &(buffer[i]), sizeof(struct gpioset_event));
					// If the outbound socket is to be used, send the data to it:
					if (!sock_error) { // It is sufficient to just trigger on sock_error as this variable is only set to 0 if useSock is set as well.
						if ( sendto(sock, collect_buf[collect_buf_pos_write], sizeof(struct gpioset_event), MSG_DONTWAIT, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
							// Ignore errors which are caused by a non-running receiving server:
							if ( (errno!=EAGAIN) && (errno!=EWOULDBLOCK) && (errno!=ECONNREFUSED) && (errno!=ENOENT) ) {
								sock_error = 1;
								syslog(LOG_ERR, "Socket error %i (%s) occurred.", errno, strerror(errno));
							}
						}
					}
					collect_buf_pos_write++;
				}
				else {
					syslog(LOG_ERR, "collect_gpiosetting: could not allocate buffer of size %d", sizeof(struct gpioset_event));
				}
			}
			if (nread % sizeof(struct gpioset_event) != 0) {
				syslog(LOG_ERR, "collect_gpiosetting: packet size %d did not match", nread);
				nread=0;
				break;
			}
		}
	}

	// Free memory:
	free(buffer);

	return nread==0;
} // collect_gpiosetting


/**************************************************************************************//**
 *
 * Append unique time string to filename (YYYYMMDDHHmmss.db)
 *
 * @param name Name of the db file, must have enough space for time and ending (name + 19 bytes _20120413131600.db)
 *
 *****************************************************************************************/
static void getTimeString(char * name) {
	time_t t = time(NULL);   // get time now
	struct tm * now = gmtime( & t );
	snprintf(name+strlen(name), 19, "_%04d%02d%02d%02d%02d%02d.db", now->tm_year + 1900, now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
}



/**************************************************************************************//**
 *
 * Show usage of API to user.
 *
 *****************************************************************************************/
void api_usage()
{
	/* Local variables */
	int i;

	printf("/---------------------------------------------------------------------------------------\\\n");
	printf("|Usage: flocklab_dbd command [options]\t\t\t\t\t\t\t|\n");
	printf("|Commands:\t\t\t\t\t\t\t\t\t\t|\n");
	printf("|  -start\t\t\tStart the FlockLab database daemon.\t\t\t|\n");
	printf("|  -stop\t\t\tStop the FlockLab database daemon.\t\t\t|\n");
	printf("|  -help\t\t\tPrint this message.\t\t\t\t\t|\n");
	printf("|  -version\t\t\tPrint software version number.\t\t\t\t|\n");
	printf("\\---------------------------------------------------------------------------------------/\n\n");

	printf("\nUsage for start: flocklab_dbd -start --service=<string> --threshold=<int> --testid=<testid> [--socket] [--quiet]\n");
	printf("--------------------\n");
	printf("Options for start:\n");
	printf("  --service=<string>\t\t Name of service for which database daemon is to be started. Allowed values are:");
	for (i=0; i<(sizeof(flocklab_services)/sizeof(*flocklab_services)); i++)
			printf(" %s", flocklab_services[i].name);
	printf(".\n");
	printf("  --threshold=<int>\t\t Number of elements to collect before writing them to the database.\n");
	printf("  --testid=<int>\t\t ID of the test.\n");
	printf("  --socket\t\t\t Optional. If specified, output will be additionally output to UNIX socket.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for stop: flocklab_dbd -stop --service=<string> [--quiet]\n");
	printf("--------------------\n");
	printf("Options for stop:\n");
	printf("  --service=<string>\t\t Name of service for which database daemon is to be stopped. Allowed values are:");
	for (i=0; i<(sizeof(flocklab_services)/sizeof(*flocklab_services)); i++)
			printf(" %s", flocklab_services[i].name);
	printf(".\n");
	printf("  --testid=<int>\t\t ID of the test.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");
} // api_usage
