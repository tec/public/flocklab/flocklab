/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * Userspace API implementation for the FlockLab service "GPIO monitoring"
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include "flocklab.h"
#include "flocklab_gpio_monitor_shared.h"			/* Contains shared definitions of userspace and kernelspace programs for this service */
#include "flocklab_powerprof_shared.h"				/* Needed for callback */



/* ---- Global variables ---------------------------------------------------*/
#define VERSION 			2.1								// Version. Has to be incremented manually.


static int busy_retry;
static int backoff_time = 1000000;								// Time in us for exponential backoff when trying to write to the database.
static int quiet = 1;											// If set, all output is put in syslog, otherwise on console and syslog
static int noflocklab = 0;										// If set, restrictions which are specific to the FlockLab testbed (such as GPIO mappings) are lifted
static int arg_pin;												// Program argument
static enum en_edge arg_edge;									// Program argument
static enum en_mode arg_mode;									// Program argument
static enum en_callback arg_callback;							// Program argument
static void *arg_callbackparams;								// Program argument
static int arg_callbackparams_enum;								// Indicates which type of callback the callback parameters are refering to.
struct add_list {												// Struct for holding lines of the batch file that can be provided for multiple events with parameter -addbatch
	struct add_list *next;
	struct monitor_job job;
};
typedef struct add_list item;

/* ---- Prototypes ----------------------------------------------------------*/
static int api_list();
static int api_start(int);
static int api_stop();
static int api_add(int, enum en_edge, enum en_mode, enum en_callback, void*);
static int api_addbatch(int, char*[]);
static int api_remove(int, enum en_edge);
static int api_removeall();
static int api_flush();
static int api_errorstats_get();
static int api_errorstats_reset();
static int api_read(char*);
int validate_api(int, char*[], int);
int validate_value(char*, int);
int validate_callback_params(char*);
void api_usage();
void free_linked_add_list(item*);



/************************************************************************************//**
 *
 * Main
 *
 * @param argc Argument count
 * @param argv Argument list
 *
 * @return Returns 0 on success and errno otherwise.
 *
 ***************************************************************************************/
int main(int argc, char *argv[]){
	/* Local variables */
	int i, switch1, rs;
	FILE *file_ptr;
	char *filepath = NULL;

	// Determine whether the quiet option is set and open the syslog accordingly:
	quiet = 0;
	for (i = 1; i < argc; i++) {
		if (strcmp( argv[i], "--quiet") == 0) {
				quiet = 1;
			break;
		}
	}
	if (quiet) {
		openlog("flocklab_gpiomonitor", LOG_CONS | LOG_PID, LOG_USER);
	} else {
		openlog("flocklab_gpiomonitor", LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER);
	}
	// Set local time to UTC:
	putenv("TZ=UTC");
	tzset();

	// If there are less than 2 arguments, we can abort immediately:
	if ( argc < 2 ) {
		api_usage();
		syslog(LOG_ERR, "Wrong API use.");
		closelog();
		return EINVAL;
	}

	// Find out whether noflocklab flag is already set:
	file_ptr = fopen(flagfile_gpiomon_nofl, "r");
	if (file_ptr) {
		noflocklab = 1;
		fclose(file_ptr);
	}

	// Test whether the given API command is in the set of allowed API commands:
	if( strcmp( argv[1], "-list" ) == 0)				switch1 = 1;
	else if( strcmp( argv[1], "-start" ) == 0)			switch1 = 2;
	else if( strcmp( argv[1], "-stop" ) == 0)			switch1 = 3;
	else if( strcmp( argv[1], "-add" ) == 0)			switch1 = 4;
	else if( strcmp( argv[1], "-remove" ) == 0)			switch1 = 5;
	else if( strcmp( argv[1], "-addbatch") == 0)		switch1 = 6;
	else if( strcmp( argv[1], "-help" ) == 0)			switch1 = 7;
	else if( strcmp( argv[1], "-version" ) == 0)		switch1 = 8;
	else if( strcmp( argv[1], "-read" ) == 0)			switch1 = 9;
	else if( strcmp( argv[1], "-removeall" ) == 0)		switch1 = 10;
	else if( strcmp( argv[1], "-flush" ) == 0)			switch1 = 11;
	else if( strcmp( argv[1], "-errorstats" ) == 0)		switch1 = 12;

	switch( switch1 ) {
		case 1:	// -list
					// No parameters are to be given to this API command.
					if( argc > 2 )
						rs = EINVAL;
					else
						rs = api_list();
					break;

		case 2:	// -start
					//Check if there are too many parameters:
					if (argc > 5) {
						rs = EINVAL;
						break;
					}
					// Check for the optional --database and --noflocklab parameter:
					for (i = 2; i < argc; i++) {
						if (strcmp( argv[i], "--noflocklab" ) == 0) {
							noflocklab = 1;
						}
					}
					rs = api_start(noflocklab);
					break;

		case 3:	// -stop
					// Check for the right number of parameters:
					if( argc > 3 ) {
						// Too many parameters.
						rs = EINVAL;
						break;
					}
					rs = api_stop();
					break;

		case 4:	// -add
					if (validate_api(argc, argv, switch1)) {
						rs = api_add(arg_pin, arg_edge, arg_mode, arg_callback, arg_callbackparams);
					} else {
						rs = EINVAL;
					}
					if (arg_callbackparams) free(arg_callbackparams);
					break;

		case 5:	// -remove
					if (validate_api(argc, argv, switch1)) {
						rs = api_remove(arg_pin, arg_edge);
					} else {
						rs = EINVAL;
					}
					break;

		case 6:	// -addbatch
					rs = api_addbatch(argc, argv);
					break;

		case 7:	// -help
					api_usage();
					rs = SUCCESS;
					break;

		case 8:	// -version
					printf("%.2f\n", VERSION);
					rs = SUCCESS;
					break;

		case 9:	// -read
					// Check for the additional filename provided:
					if ((argv[2] != NULL) && (strncmp(argv[2], "--file=",	strlen("--file=")) == 0)) {
						// Extract the file path:
						filepath = strtok(argv[2],"=");
						filepath = strtok(NULL,"=");
					}
					rs = api_read(filepath);
					break;

		case 10:	// -removeall
					rs = api_removeall();
					break;

		case 11:	// -flush
					rs = api_flush();
					break;

		case 12:	// -errorstats
					rs = EINVAL;
					for (i = 2; i < argc; i++) {
						if (strcmp( argv[i], "--get" ) == 0) {
							rs = api_errorstats_get();
							break;
						} else if (strcmp( argv[i], "--reset" ) == 0) {
							rs = api_errorstats_reset();
							break;
						}
					}
					break;

		default:
					syslog(LOG_ERR, "Command not recognized. For help, use flocklab_gpiomonitor -help");
					rs = EINVAL;
					break;
	}

	if (rs != SUCCESS) {
		if (rs == EINVAL) {
			syslog(LOG_ERR, "Wrong API use");
		} else {
			syslog(LOG_ERR, "Error %i occurred", rs);
		}
	}

	// Close the syslog:
	closelog();

	return rs;
} // main



/**************************************************************************************//**
 *
 * Function to validate user input for API-commands "add" and "remove".
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 * 			On successful validation, the needed global variables are filled with their
 * 			respective values.
 *
 * @param argc Argument count from main()
 * @param argv Argument list from main()
 * @param api_command Depicts the type of validation:
 * 					  4 is 'add'
 * 					  5 is 'remove'
 *
 ****************************************************************************************/
int validate_api(int argc, char *argv[], int api_command)
{
	/* Local variables */
	int i;
	int switch1 = -1;
	char *temp;
	// Check for right api_command:
	if ((api_command != 4) && (api_command != 5)) {
		syslog(LOG_ERR, "Illegal api_command.");
		// Unknown command:
		return 0;
	}
	// Check number of parameters:
	if (api_command == 4) {	// 'add' command
		// There need to be at least 3 parameters and at most 6 parameters:
		if ((argc < 5) || (argc > 7)) {
			syslog(LOG_ERR, "Wrong number of arguments: %d");
			return 0;
		}
	} else if (api_command == 5) { // 'remove' command
		// There need to be either 2 parameters or 3 parameters (if used with --quiet option):
		if ((argc != 4) && (argc != 5)) {
			syslog(LOG_ERR, "Wrong number of arguments");
			return 0;
		}
	}
	// Reset all global variables:
	arg_pin = -1;
	arg_edge = -1;
	arg_mode = -1;
	arg_callback = -1;
	arg_callbackparams = NULL;
	arg_callbackparams_enum = -1;

	// Fill the global variables with their respective arguments:
	for (i=2; i < argc; i++){
		// Test if the argument is in the list of expected arguments and assign it a number for the switching process:
		if( strncmp( 		argv[i], "--pin=", 				strlen("--pin=")) == 0)				switch1 = 0; // Do NOT change the value for switch1 as it is used in api_addbatch() and validate_value()
		else if( strncmp( 	argv[i], "--edge=", 			strlen("--edge=")) == 0)			switch1 = 1; // Do NOT change the value for switch1 as it is used in api_addbatch() and validate_value()
		else if( strncmp( 	argv[i], "--mode=", 			strlen("--mode=")) == 0)			switch1 = 2; // Do NOT change the value for switch1 as it is used in api_addbatch() and validate_value()
		else if( strncmp( 	argv[i], "--callback=", 		strlen("--callback=")) == 0)		switch1 = 3; // Do NOT change the value for switch1 as it is used in api_addbatch() and validate_value()
		else if( strcmp( 	argv[i], "--quiet") == 0)											switch1 = 5; // Do NOT change the value for switch1 as it is used in api_addbatch() and validate_value()
		if (switch1 < 0) {
			syslog(LOG_ERR, "Illegal argument detected.");
			return 0;
		}
		// Extract the argument value (except for --quiet option). If it is empty, abort.
		temp = strtok(argv[i],"=");
		temp = strtok(NULL,"=");
		if ((temp == NULL) && (switch1 != 5)) {
			return 0;
		}
		if (validate_value(temp, switch1) == 0) {
			return 0;
		}
	}
	// Check whether all needed variables have been set. If not, abort:
	if (api_command == 4) {	// 'add' command
		// Combination 1: no callback provided. Nevertheless pin, edge and mode have to be present.
		if ((arg_pin == -1) || (arg_edge == -1) || (arg_mode == -1)) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		} else if ((arg_callback != -1) && (arg_callbackparams_enum != arg_callback)) {
			// Combination 2: callback provided. Now, combination 1 has to be present and the right callback params have to be given.
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		}
	} else if (api_command == 5) { // 'remove' command
		if ((arg_pin == -1) || (arg_edge == -1)) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		}
	}

	// We made it that far, so all arguments are valid.
	return 1;

} // validate_api



/**************************************************************************************//**
 *
 * Function to validate a single value for -add, -remove or -addbatch API commands
 *
 * @param temp Value to validate
 * @param argument Argument for switching
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 * 	       On successful validation, the respective global variable is filled with its value.
 *
 *****************************************************************************************/
int validate_value(char *temp, int argument)
{
	/* Local variables */
	char *temp_param;
	int i, rs;

	// Validate the argument and fill the value into the right global variable:
	switch( argument ) {
		case 0:
			// Pin. Needs to be a number and in the list of monitorable pins (if used in FlockLab):
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "Pin is not numeric:  %s", temp);
				return 0;
			} else {
				arg_pin = atoi(temp);
				if (!noflocklab) {
					if (!is_pin_monitorable(arg_pin)) {
						syslog(LOG_ERR, "Pin is not monitorable: %s", temp);
						return 0;
					}
				}
			}
			break;

		case 1:
			// Edge. Needs to be an enum en_edge and in the list of valid edges:
			arg_edge = str_to_enum(temp, en_edge, EN_EDGE_TOP);
			if ((int) arg_edge == -1) {
				syslog(LOG_ERR, "Edge is not enum: %s", temp);
				return 0;
			}
			break;

		case 2:
			// Mode. Needs to be an enum en_mode and in the list of valid modes:
			arg_mode = str_to_enum(temp, en_mode, EN_MODE_TOP);
			if ((int) arg_mode == -1) {
				syslog(LOG_ERR, "Edge is not enum: %s", temp);
				return 0;
			}
			break;

		case 3:
			// Callback. Has to be an enum en_callback and in the list of valid callbacks.
			// The parameter are appended (comma seperated) to the callback.

			rs = validate_callback_params(temp);
			if (rs != 0) {
				syslog(LOG_ERR, "Error in callback: %s", temp);
				return 0;
			}
			break;

		case 5:
			// Quiet option. Can be ignored as it was already detected in main();
			break;

		default:
			return 0;
	}

	// We made it that far, so the data is valid.
	return 1;
} // validate_value



/**************************************************************************************//**
 *
 * Find out if string temp_callback is a list of arguments for the callback function provided
 * in the argument 'callback'. On success, the void arg_callbackparams will be a pointer to
 * the according struct	with the callback parameters. The variable arg_callbackparams_enum
 * is then filled with the type of callback the arg_callbackparams expect.
 *
 * @param temp_callback Parameter to validate
 *
 * @return Returns 0 on successful validation, negative error number otherwise
 *
 ****************************************************************************************/
int validate_callback_params(char *temp_callback)
{
	/* Local variables */
	char *testvalue;
	char params_cpy[strlen(temp_callback)+1];
	int rs = 0;
	unsigned long temp;

	// Copy the temp string:
	strncpy(params_cpy, temp_callback, strlen(temp_callback));
	params_cpy[strlen(temp_callback)] = '\0';
	//printf("%d: params_cpy: %s, len: %d\n", __LINE__, params_cpy, strlen(params_cpy));

	// Find out the callback which is stored in the first value (comma separated) of the string:
	testvalue = strtok(params_cpy,",");
	arg_callback = str_to_enum(testvalue, en_callback, EN_CALLBACK_TOP);
	if ((int) arg_callback == -1) {
		syslog(LOG_ERR, "Callback is not enum: %s", testvalue);
		return -1;
	}

	// Now check the callback arguments:
	switch(arg_callback) {
		case gpio_set_add:
			/* gpio_set_add: defined in struct callback_args_gpio_set_add */
			// Allocate memory:
			arg_callbackparams = malloc(sizeof(struct callback_args_gpio_set_add));

			// First argument is pin. Needs to be a number and in the list of setable pins (if used in FlockLab):
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "Pin is not numeric");
				rs = -1;
			} else {
				((struct callback_args_gpio_set_add*) arg_callbackparams)->gpio = atoi(testvalue);
				if (!noflocklab) {
					if (!is_pin_setable(((struct callback_args_gpio_set_add*) arg_callbackparams)->gpio)) {
						syslog(LOG_ERR, "Pin is not setable");
						rs = -1;
					}
				}
			}

			// Second argument is level. Needs to be an enum en_level and in the list of valid levels:
			testvalue = strtok(NULL,",");
			if (testvalue == NULL) {
				syslog(LOG_ERR, "Level cannot be null");
				rs = -1;
			}
			((struct callback_args_gpio_set_add*) arg_callbackparams)->level = str_to_enum(testvalue, en_level, EN_LEVEL_TOP);
			if ((int) (((struct callback_args_gpio_set_add*) arg_callbackparams)->level) == -1) {
				syslog(LOG_ERR, "Level is not enum");
				rs = -1;
			}

			// Third argument is offset in s. Needs to be an integer between 0 and 3600
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "offset_s is not numeric");
				rs = -1;
			} else {
				((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_s = atol(testvalue);
				if ((((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_s < 0) || (((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_s > 3600)) {
					syslog(LOG_ERR, "Error in range of offset_s");
					rs = -1;
				}
			}

			// Fourth argument is offset in us. Needs to be an integer between 0 and 999999
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "offset_us is not numeric");
				rs = -1;
			} else {
				((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_us = atol(testvalue);
				if ((((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_us < 0) || (((struct callback_args_gpio_set_add*) arg_callbackparams)->offset_us > 999999)) {
					syslog(LOG_ERR, "Error in range of offset_us");
					rs = -1;
				}
			}

			// No more arguments should be given:
			if (strtok(NULL, ",") != NULL) {
				syslog(LOG_ERR, "Too many arguments");
				rs = -1;
			}
			break;

		 	case powerprof_add:
			// Allocate memory:
			arg_callbackparams = malloc(sizeof(struct callback_args_powerprof_add));

			// First argument is duration in ms: Needs to be between 300 and 3600000:
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "Duration is not numeric");
				rs = -1;
			} else {
				temp = atol(testvalue);
				if ((temp < 300) || (temp > 3600000)) {
					syslog(LOG_ERR, "Illegal value for duration.");
					rs = -1;
				} else {
					((struct callback_args_powerprof_add*) arg_callbackparams)->duration = temp*1000;
				}
			}
			// Second argument is offset in s. Needs to be an integer between 0 and 3600
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "offset_s is not numeric");
				rs = -1;
			} else {
				temp = atol(testvalue);
				if ((temp < 0) || (temp > 3600)) {
					syslog(LOG_ERR, "Error in range of offset_s");
					rs = -1;
				} else {
					((struct callback_args_powerprof_add*) arg_callbackparams)->offset_s = temp;
				}
			}

			// Third argument is offset in us. Needs to be an integer between 0 and 999999
			testvalue = strtok(NULL,",");
			if ((testvalue == NULL) || (!is_numeric(testvalue))) {
				syslog(LOG_ERR, "offset_us is not numeric");
				rs = -1;
			} else {
				temp = atol(testvalue);
				if ((temp < 0) || (temp > 999999)) {
					syslog(LOG_ERR, "Error in range of offset_us");
					rs = -1;
				} else {
					((struct callback_args_powerprof_add*) arg_callbackparams)->offset_us = temp;
				}
			}

			// No more arguments should be given:
			if (strtok(NULL, ",") != NULL){
				syslog(LOG_ERR, "Too many arguments");
				rs = -1;
			}
			break;

		default:
			rs = -1;
			break;
	}

	// If rs == 0 this means that at least one test failed. If rs == 1 all tests passed.
	if (rs != 0) {
		free(arg_callbackparams);
		arg_callbackparams = NULL;
	} else {
		arg_callbackparams_enum = arg_callback;
	}
	return rs;
} // validate_callback_params



/**************************************************************************************//**
 *
 * Show usage of API to user.
 *
 *****************************************************************************************/
void api_usage()
{
	/* Local variables */
	int i;

	printf("/---------------------------------------------------------------------------------------\\\n");
	printf("|Usage: flocklab_gpiomonitor command [options]\t\t\t\t\t\t|\n");
	printf("|Commands:\t\t\t\t\t\t\t\t\t\t|\n");
	printf("|  -start\t\t\tStart the FlockLab GPIO monitoring service.\t\t|\n");
	printf("|  -stop\t\t\tStop the FlockLab GPIO monitoring service.\t\t|\n");
	printf("|  -list\t\t\tList all GPIO pins currently monitored.\t\t\t|\n");
	printf("|  -add\t\t\t\tAdd a new GPIO pin to monitor.\t\t\t\t|\n");
	printf("|  -addbatch\t\t\tRead a file with multiple pins to monitor.\t\t|\n");
	printf("|  -remove\t\t\tRemove a monitored GPIO pin.\t\t\t\t|\n");
	printf("|  -removeall\t\t\tRemove all monitored GPIO pins.\t\t\t\t|\n");
	printf("|  -flush\t\t\tFlush output buffer for monitoring results.\t\t|\n");
	printf("|  -errorstats\t\t\tGet/reset error statistics.\t\t\t\t|\n");
	printf("|  -read\t\t\tGet monitoring results and display them on standard out.|\n");
	printf("|  -help\t\t\tPrint this message.\t\t\t\t\t|\n");
	printf("|  -version\t\t\tPrint software version number.\t\t\t\t|\n");
	printf("\\---------------------------------------------------------------------------------------/\n\n");

	printf("\nUsage for start: flocklab_gpiomonitor -start [--noflocklab] [--quiet]\n");
	printf("--------------------\n");
	printf("Options for start:\n");
	printf("  --noflocklab\t\t\t Optional. If specified, restrictions which are specific to the FlockLab testbed (such as GPIO mappings) are lifted. Note that in this mode, callbacks shall not be used.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");


	printf("\nUsage for stop: flocklab_gpiomonitor -stop [--quiet]\n");
	printf("--------------------\n");
	printf("Options for stop:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for add: flocklab_gpiomonitor -add --pin=<int> --edge=<char> --mode=<char> [--callback=<string>] [--quiet]\n");
	printf("--------------------\n");
	printf("Options for add:\n");
	printf("  --pin=<int>\t\t\t Pin number of the GPIO pin to monitor. Usable pins are:");
	for (i=0; i<(sizeof(list_gpiopins_monitor)/sizeof(*list_gpiopins_monitor)); i++)
		printf(" %i", list_gpiopins_monitor[i]);
	printf(".\n");
	printf("  --edge=<char>\t\t\t Edge to be monitored. Possible values are R (rising), F (falling) or B (both).\n");
	printf("  --mode=<char>\t\t\t Mode for monitoring. Can be S (single) or C (continuous). Single mode stops the monitoring after the first detected edge while continuous mode monitors infinitely.\n");
	printf("  --callback=<string>\t\t Optional. Callback function and parameters (comma separated) If not specified, no callback function is invoked. \n");
	printf("\t\t\t\t Possible callback functions are:\n");
	printf("\t\t\t\t\t gpio_set_add:\t A GPIO setting will be scheduled.\n");
	printf("\t\t\t\t\t\t\t Parameter for gpio_set_add:\n");
	printf("\t\t\t\t\t\t\t\t pin:\t\t Pin to set. Refer to flocklab_gpiosetting help for setable pins.\n");
	printf("\t\t\t\t\t\t\t\t level:\t\t Level to set pin to. Refer to flocklab_gpiosetting help for possible values.\n");
	printf("\t\t\t\t\t\t\t\t offset_s:\t Offset in s to schedule pin setting after having detected the monitored edge. Minimum is 0, maximum is 3600 (1 hour).\n");
	printf("\t\t\t\t\t\t\t\t offset_us:\t Offset in us to schedule pin setting after having detected the monitored edge. Minimum is 0, maximum is 999999.\n");
	printf("\t\t\t\t\t powerprof_add:\t A power profile will be acquired.\n");
	printf("\t\t\t\t\t\t\t Parameter for powerprof_add:\n");
	printf("\t\t\t\t\t\t\t\t duration:\t Duration of sampling in ms. Refer to flocklab_powerprofiling help for possible values.\n");
	printf("\t\t\t\t\t\t\t\t offset_s:\t Offset in s to schedule power profile after having detected the monitored edge. Minimum is 0, maximum is 3600 (1 hour).\n");
	printf("\t\t\t\t\t\t\t\t offset_us:\t Offset in us to schedule power profile after having detected the monitored edge. Minimum is 0, maximum is 999999.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for addbatch: flocklab_gpiomonitor -addbatch --file=<path> [--quiet]\n");
	printf("--------------------\n");
	printf("  --file=<path>\t\t\t Path to where the file is stored without whitespaces. Each line of this file has to contain one set of options as for the add-command in the following form:\n");
	printf("\t\t\t\t\t pin;edge;mode;[callback,callback_params];\n");
	printf("\t\t\t\t Note that the specification of a callback function and its parameters is optional.\n");
	printf("\t\t\t\t Note that multiple values in callback_params have to be separated by commas (no whitespaces).\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for remove: flocklab_gpiomonitor -remove --pin=<int> --edge=<char> [--quiet]\n");
	printf("--------------------\n");
	printf("Options for remove:\n");
	printf("  --pin=<int>\t\t\t Pin number of the GPIO pin to remove from being monitored. Usable pins are:");
	for (i=0; i<(sizeof(list_gpiopins_monitor)/sizeof(*list_gpiopins_monitor)); i++)
		printf(" %i", list_gpiopins_monitor[i]);
	printf(".\n");
	printf("  --edge=<char>\t\t\t Edge to remove from being monitored. Possible values are R (rising), F (falling) or B (both).\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for read: flocklab_gpiomonitor -read [--file=<path>]\n");
	printf("--------------------\n");
	printf("Options for read:\n");
	printf("  --file=<path>\t\t\t Optional. Path to where all read values should be stored. If absent, data will only be written to standard out.\n");

	printf("\nUsage for removeall: flocklab_gpiomonitor -removeall [--quiet]\n");
	printf("--------------------\n");
	printf("Options for removeall:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for flush: flocklab_gpiomonitor -flush [--quiet]\n");
	printf("--------------------\n");
	printf("Options for flush:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for errorstats: flocklab_gpiomonitor -errorstats {--get | --reset} [--quiet]\n");
	printf("--------------------\n");
	printf("Options for errorstats:\n");
	printf("  --get\t\t\t\t Get error counts.\n");
	printf("  --reset\t\t\t Reset error counts.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");
} // api_usage




/**************************************************************************************//**
 *
 * api_start processes the API command 'start'. The command starts the service
 *
 * @param noflocklab If argument 'noflocklab' is set to 1, only the GPIO monitoring kernel module
 * 					is started and no additional modules. Hence, no callbacks can be used.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_start(int noflocklab)
{
	/* Local variables */
	FILE *file_ptr;

	syslog(LOG_INFO, "FlockLab service for GPIO monitoring starting...");

	/* Test whether module is already loaded. If yes, give out an error. */
	if ( is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_INFO, GPIO_MONITOR_KM_NAME " is already running and can therefore not be started again");
	} else {
		system("modprobe "OSTIMERFNCT_KM_NAME);
		system("modprobe "GPIO_SETTING_KM_NAME);
		system("modprobe "POWERPROF_KM_NAME);
		system("modprobe "GPIO_MONITOR_KM_NAME);
		system("modprobe "OSTIMER_KM_NAME);
	}

	if (noflocklab) {
		// Create a file to indicate the use of the -noflocklab option to the api:
		mkdir(path_flocklab_tmp, 0755);
		file_ptr = fopen(flagfile_gpiomon_nofl, "w");
		fclose(file_ptr);
		syslog(LOG_INFO, "No FlockLab flag file created");
	}

	syslog(LOG_INFO, "FlockLab service for GPIO monitoring started");

	return SUCCESS;

} // api_start



/**************************************************************************************//**
 *
 * api_stop processes the API command 'stop'. The command stops the service
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_stop()
{
	/* Local variables */
	FILE *file_ptr;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not running and can therefore not be stopped");
		return ENOPKG;
	}

	syslog(LOG_INFO, "FlockLab service for GPIO monitoring stopping...");

	/* Stop kernel module */
	// If the kernel module can not be unloaded now, wait some time and try again:
	busy_retry = 1;
	while ((system("rmmod "GPIO_MONITOR_KM_NAME) != 0) && (busy_retry<=5)) {
		syslog(LOG_WARNING, "Kernel module cannot be removed. Waiting for retry number %i", busy_retry);
		usleep(busy_retry*backoff_time);
		busy_retry++;
	}
	//If we cannot remove the kernel module now, print an error and abort;
	if (busy_retry > 5) {
		syslog(LOG_ERR, "Kernel module cannot be removed. Aborting stop command");
		return EBUSY;
	} else {
		syslog(LOG_INFO, "Kernel module removed");
	}

	// Delete the noflocklab flag file:
	if (noflocklab) {
		remove(flagfile_gpiomon_nofl);
		syslog(LOG_INFO, "No FlockLab flag file deleted");
	}

	syslog(LOG_INFO, "FlockLab service for GPIO monitoring stopped");

	return SUCCESS;
} // api_stop



/**************************************************************************************//**
 *
 * api_list processes the API command 'list'.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_list()
{
	/* Local variables */
	struct monitor_job* buf;
	int fd, nread;
	unsigned int n=0, nent=0, rs=SUCCESS;
	unsigned int bufsize = 10000;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Allocate memory for the buffer */
	buf = malloc(bufsize*sizeof(struct monitor_job));
	if (buf == NULL) {
		return ENOMEM;
	}

	/* Open the list device for reading */
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_ERR, "Cannot open list device file because: %s", strerror(fd));
		free(buf);
		return EIO;
	}

	/* Read the file which contains the list. Read line by line and output the entries */
	while (n < bufsize) {
		nread = (read(fd, buf+nent, 100000));
		n += nread;
		if (nread == 0)
			break;
		nent += nread/sizeof(struct monitor_job);
	}
	close(fd);

	// Output the buffer:
	if (nent > 0) {
		printf("GPIO, Edge, Mode, Callback\n");
		for (n = 0; n < nent; n++) {
			printf("%u, %s, %s, ", buf[n].gpio, en_edge[buf[n].edge], en_mode[buf[n].mode]);
			switch (buf[n].callback) {
				case gpio_set_add:
					printf("%s\n", en_callback[buf[n].callback]);
					break;

				case powerprof_add:
					printf("%s\n", en_callback[buf[n].callback]);
					break;

				default:
					// NOP
					printf("none\n");
					break;
			}
		}
	} else {
		printf("No jobs scheduled.\n");
		rs = -ENODATA;
	}


	fflush(NULL);
	free(buf);

	return rs;

} // api_list

/**************************************************************************************//**
 *
 * api_read reads out the device with the pin monitoring data and writes it to standard
 * out. This is done in order to replace the gpio-event module on the Gumstix which
 * fulfills exactly the same purpose.
 *
 * @param	logfile	Path to the file where the received data should be stored. If NULL,
 * 					only output on standard out.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_read(char *logfile)
{
	/* Local variables */
	struct monitor_job_report* databuf;
	int nread, i;
	FILE *logfil;
	int fp;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// Try to open the logfile:
	if (logfile != NULL) {
		if ((logfil = fopen(logfile, "w")) == NULL) {
			syslog(LOG_ERR, "Cannot open log file. Aborting.");
			return EIO;
		}
	}

	// Read data from device file:
	databuf = malloc(SIZEOFBUF*sizeof(struct monitor_job_report));
	if (databuf == NULL) {
		return ENOMEM;
	}
	if ((fp = open("/dev/" DEVICE_PARENT_FOLDER "/" DEVICE_PARENT_GPIO_MONITOR "/" DEVICE_NAME_DBBUF, O_RDONLY)) < 0) {
		syslog(LOG_ERR, "Cannot open device file. Aborting.");
		free(databuf);
		return EIO;
	}
	if (logfile) {
		fprintf(logfil, "GPIO, Edge, Time\n");
	} else {
		printf("GPIO, Edge, Time\n");
	}
	while (1) {
		// Collect data in blocks of SIZEOFBUF values:
		// TODO: Shouldn't we read a multiple of sizeof(struct monitor_job_report) ?
		i = read(fp, databuf, SIZEOFBUF);
		if (i == 0) {
			continue;
		} else {
			nread = i / sizeof(struct monitor_job_report);
			//DEBUG printf("nread: %i, sizeof struct: %d\n", nread, sizeof(struct monitor_job_report));
			for (i=0; i<nread; i++) {
				if (logfile) {
					fprintf(logfil, "%d,%s,%lu.%06lu\n", databuf[i].gpio, en_edge[databuf[i].edge], databuf[i].timestamp.tv_sec, databuf[i].timestamp.tv_usec);
				} else {
					printf("%d %s %lu.%06lu\n", databuf[i].gpio, en_edge[databuf[i].edge], databuf[i].timestamp.tv_sec, databuf[i].timestamp.tv_usec);
				}
				/* DEBUG
				if (databuf[i].gpio == 0) {
					syslog(LOG_ERR, "Buffer overrun occurred in gpio_monitor kernel module. Processed event data has been lost.");
					continue;
				}*/
				if (logfile)
					fflush(logfil);
			}
		}
	}
	close(fp);
	free(databuf);
	return SUCCESS;
} // api_read


/**************************************************************************************//**
 *
 * api_add processes the API command 'add'.
 *
 * @param pin Pin number
 * @param edge Edge to detect
 * @param mode Mode for detection. Can be single or continuous
 * @param callback Callback function to execute after detection
 * @param callbackarg_struct Struct for arguments of callback
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_add(int pin, enum en_edge edge, enum en_mode mode, enum en_callback callback, void *callbackarg_struct)
{
	/* Local variables */
	int file_desc, rs;
	struct monitor_job job;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// Fill values into the struct:
	job.gpio = pin;
	job.edge = edge;
	job.mode = mode;
	job.callback = callback;
	job.callback_args = callbackarg_struct;

	/* Open device for IOCTL and submit the monitor job to kernel module: */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}
	// Send command and data to kernel module:
	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_ADD, &job);
	close(file_desc);
	switch (rs) {
		case SUCCESS:
			//syslog(LOG_INFO, "Monitor job added successfully");
			break;
		case ERRIRQNOTREG:
			syslog(LOG_ERR, "The IRQ handler for the requested GPIO could not be registered");
			rs = EACCES;
			break;
		default:
			syslog(LOG_ERR, "Unknown return status %d", rs);
			rs = ENOEXEC;
			break;
	}

	return rs;
} // api_add



/**************************************************************************************//**
 *
 * api_remove processes the API command 'remove'.
 *
 * @param pin Pin number
 * @param edge Edge to detect
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_remove(int pin, enum en_edge edge)
{
	/* Local variables */
	int i;
	int file_desc, rs;
	struct monitor_job job_delete;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// Fill values into the struct:
	job_delete.gpio = pin;
	job_delete.edge = edge;
	job_delete.mode = -1;
	job_delete.callback = -1;
	job_delete.callback_args = NULL;

	/* Open device for IOCTL and submit event to kernel module for inserting it into the event queue: */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_REMOVE, &job_delete);
	close(file_desc);
	switch (rs) {
		case SUCCESS:
			//syslog(LOG_INFO, "Monitoring job removed successfully");
			break;
		case ERRLISTEMPTY:
		case ERRNOTFOUND:
			syslog(LOG_ERR, "The monitoring job has not been found in the list");
			rs = ENOENT;
			break;
		default:
			syslog(LOG_ERR, "Unknown error %d occurred", rs);
			perror("Error was: ");
			rs = ENOEXEC;
			break;
	}

	return rs;

} // api_remove


/**************************************************************************************//**
 *
 * api_removeall processes the API command 'removeall'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_removeall()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_REMOVEALL);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "All monitoring jobs have been removed successfully");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_removeall


/**************************************************************************************//**
 *
 * api_flush processes the API command 'flush'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_flush()
{
	/* Local variables */
	int file_desc, rs;
	char cmd[40];

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_FLUSH);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "Output buffer for monitoring results has been flushed.");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_flush



/**************************************************************************************//**
 *
 * api_errorstats_get processes the API command 'errcnt'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_get()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_ERRCNT);
	close(file_desc);
	if (rs >= 0) {
		syslog(LOG_INFO, "Error statistics: kfifo_full_count: %i", rs);
	} else {
		syslog(LOG_ERR, "Unknown error occurred when trying to get error statistics. Ioctl returned %i", rs);
	}

	return SUCCESS;

} // api_errorstats_get



/**************************************************************************************//**
 *
 * api_errorstats_reset processes the API command 'errrst'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_reset()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_MONITOR"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_MONITOR_IOCTL_ERRRST);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "Error statistics reset successfully.");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred when trying to reset error statistics.", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_errorstats_reset


/**************************************************************************************//**
 *
 * api_addbatch processes the API command 'addbatch'. The command processes a file with a
 * number of pin monitoring jobs in it.
 *
 * @param argc Argument count from main()
 * @param argv Argument list from main()
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_addbatch(int argc, char *argv[])
{
	/* Local variables */
	int i, m, rs;
	int min_elem_per_line = 3;	// Minimum number of semicolon-separated elements per line in the batchfile
	int max_elem_per_line = 5;	// Maximum number of semicolon-separated elements per line in the batchfile
	int elem_on_curr_line = 0;
	char *filename, *tmp[max_elem_per_line];
	FILE *file_ptr;
	int max_file_line_size = 100;
	char file_line[max_file_line_size];
	item *curr, *head;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// There need to be either 1 parameter or 2 parameters (if used with --quiet option)
	if ((argc != 3) && (argc != 4))
		return EINVAL;
	// Initialize the linked list:
	head = NULL;
	// Check the arguments:
	for (i=2; i < argc; i++){
		if( strncmp( argv[i], "--file=", 7) == 0) {
			// Extract the file name. If it is empty, abort.
			filename = strtok(argv[i],"=");
			filename = strtok(NULL,"=");
			if (filename == NULL)
				return ERRNOTFOUND;
			// Now we have the filename. Lets try to open that file in read-mode:
			file_ptr = fopen(filename, "r");
			if (file_ptr == NULL)
				return ERRNOTFOUND;
			while(fgets(file_line, max_file_line_size, file_ptr) != NULL) {
				// Parse the line and insert the tokens into the array for validation:
				if ((tmp[0] = strtok(file_line, ";\n")) == NULL) {
					free_linked_add_list(head);
					fclose(file_ptr);
					return EINVAL;
				}
				for (m=1; m < max_elem_per_line; m++) {
					tmp[m] = strtok(NULL, ";\n");
					if (tmp[m] == NULL) {
						break;
					}
				}
				elem_on_curr_line = m;
				//DEBUG printf("Elements on current line: %i\n", elem_on_curr_line);

				// Get the number of elements on the current line. If it is smaller than the minimal expected amount, abort:
				if (elem_on_curr_line < min_elem_per_line) {
					free_linked_add_list(head);
					fclose(file_ptr);
					return EINVAL;
				}

				// Reset all global variables:
				arg_pin = -1;
				arg_edge = -1;
				arg_mode = -1;
				arg_callback = -1;
				arg_callbackparams = NULL;
				arg_callbackparams_enum = -1;

				// Validate the data.
				for (m=0; m < elem_on_curr_line; m++) {
					//DEBUG printf("validate_value(%s, %i)\n", tmp[m], m);
					if (!validate_value(tmp[m], m)) {
						free_linked_add_list(head);
						fclose(file_ptr);
						return EINVAL;
					}
				}

				// Check whether all variables have been set. If not, abort:
				// Combination 1: no callback provided. Nevertheless pin, edge and mode have to be present.
				if ((arg_pin == -1) || (arg_edge == -1) || (arg_mode == -1)) {
					free_linked_add_list(head);
					fclose(file_ptr);
					return EINVAL;
				} else if ((arg_callback != -1) && (arg_callbackparams_enum != arg_callback)) {
					// Combination 2: callback provided. Now, combination 1 has to be present and the right callback params have to be given.
					free_linked_add_list(head);
					fclose(file_ptr);
					return EINVAL;
				}

				// The data is good, thus fill it into the linked list:
				curr 					= (item *) malloc(sizeof(item));
				curr->next 				= head;
				curr->job.gpio 			= arg_pin;
				curr->job.edge			= arg_edge;
				curr->job.mode			= arg_mode;
				curr->job.callback		= arg_callback;
				curr->job.callback_args	= arg_callbackparams;
				head 					= curr;
			}

			fclose(file_ptr);
			//Insert data with api_add and free memory of linked list:
			curr = head;
			rs = 0;
			while ((curr) && (rs == 0)) {
				rs = api_add(curr->job.gpio, curr->job.edge, curr->job.mode, curr->job.callback, curr->job.callback_args);
				curr = curr->next;
			}
			free_linked_add_list(head);
			return rs;
		}
	}

	return SUCCESS;
} // api_addbatch



/**************************************************************************************//**
 *
 * free_linked_add_list frees all memory that was allocated when building up a linked
 * list of type add_list.
 *
 * @param head Head of the linked list
 *
 *****************************************************************************************/
void free_linked_add_list(item *head) {
	/* Local variables */
	item *next, *this;

	this = head;
	// Traverse linked list and delete all elements:
	while (this) {
		next = this->next;
		free(this);
		this = next;
	}
} // free_linked_add_list



