/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * Userspace API implementation for the FlockLab service "GPIO setting"
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>									/* Needed for string operations*/
#include <time.h>									/* Needed for time operations*/
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <syslog.h>
#include "flocklab.h"
#include "flocklab_gpio_setting_shared.h"			/* Contains shared definitions of usperspace and kernelspace programs for this service */



/* ---- Global variables ---------------------------------------------------*/
#define VERSION 			2.5								// Version of the database daemon. Has to be incremented manually.


struct gpiosetting_job_conf {
	int pin;
	enum en_level level;
	struct timeval time_planned;
	unsigned int interval;
	unsigned int count;
};

struct add_list {												// Struct for holding lines of the batch file that can be provided for multiple gpiosetting events with parameter -addbatch
	struct add_list *next;
	struct gpiosetting_job_conf job_conf;
};

struct arg_detect {
	unsigned pin:		1;
	unsigned level:		1;
	unsigned time:		1;
	unsigned offset:	1;
	unsigned us:		1;
	unsigned interval:	1;
	unsigned count:		1;
	unsigned :			0;
	unsigned long offset_val;
	struct timeval tv_time;
	unsigned int us_val;
};

static int busy_retry;
static int backoff_time = 1000000;								// Time in us for exponential backoff when trying to write to the database.
static int quiet = 0;
static int noflocklab = 0;										// If set, restrictions which are specific to the FlockLab testbed (such as GPIO mappings) are lifted
typedef struct add_list item;


/* ---- Prototypes ----------------------------------------------------------*/
static int api_list();
static int api_start();
static int api_stop();
static int ioctl_add(struct gpiosetting_job_conf *job_conf, int fd);
static int api_add(struct gpiosetting_job_conf*);
static int api_remove(struct gpiosetting_job_conf*);
static int api_addbatch(int, char*[]);
static int api_read(char*);
static int api_removeall();
static int api_flush();
static int api_errorstats_get();
static int api_errorstats_reset();
int validate_and_convert_args(int, char*[], struct gpiosetting_job_conf*, int);
int validate_and_convert_value(char*, int, struct gpiosetting_job_conf*, struct arg_detect*);
void api_usage();
void free_linked_add_list(item *head);
void reset_arg_detect(struct arg_detect*);
void gpiosetting_cpy_job_conf(struct gpiosetting_job_conf*, struct gpiosetting_job_conf*);


/************************************************************************************//**
 *
 * Main
 *
 * @param argc Argument count
 * @param argv Argument list
 *
 * @return Returns 0 on success and errno otherwise.
 *
 ***************************************************************************************/
int main(int argc, char *argv[]){
	/* Local variables */
	int i, switch1, rs;
	struct gpiosetting_job_conf job_conf;
	FILE *file_ptr;
	char *filepath = NULL;

	// Set job_conf to default values:
	gpiosetting_cpy_job_conf(&job_conf, NULL);

	// Determine whether the quiet option is set and open the syslog accordingly:
	// Set local time to UTC:
	putenv("TZ=UTC");
	tzset();
	quiet = 0;
	for (i = 1; i < argc; i++) {
		if (strcmp( argv[i], "--quiet") == 0) {
			quiet = 1;
			break;
		}
	}
	if (quiet) {
		openlog("flocklab_gpiosetting", LOG_CONS | LOG_PID, LOG_USER);
	} else {
		openlog("flocklab_gpiosetting", LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER);
	}


	// If there are less than 2 arguments, we can abort immediately:
	if ( argc < 2 ) {
		syslog(LOG_ERR, "Wrong API use: specify a command.");
		closelog();
		return EINVAL;
	}

	// Find out whether noflocklab flag is already set:
	file_ptr = fopen(flagfile_gpioset_nofl, "r");
	if (file_ptr) {
		noflocklab = 1;
		fclose(file_ptr);
	}

	// Test whether the given API command is in the set of allowed API commands:
	if( strcmp( argv[1], "-list" ) == 0)				switch1 = 1;
	else if( strcmp( argv[1], "-start" ) == 0)			switch1 = 2;
	else if( strcmp( argv[1], "-stop" ) == 0)			switch1 = 3;
	else if( strcmp( argv[1], "-add" ) == 0)			switch1 = 4;
	else if( strcmp( argv[1], "-remove" ) == 0)			switch1 = 5;
	else if( strcmp( argv[1], "-addbatch") == 0)		switch1 = 6;
	else if( strcmp( argv[1], "-help" ) == 0)			switch1 = 7;
	else if( strcmp( argv[1], "-version" ) == 0)		switch1 = 8;
	else if( strcmp( argv[1], "-read" ) == 0)			switch1 = 9;
	else if( strcmp( argv[1], "-removeall" ) == 0)		switch1 = 10;
	else if( strcmp( argv[1], "-flush" ) == 0)			switch1 = 11;
	else if( strcmp( argv[1], "-errorstats" ) == 0)		switch1 = 12;


	switch( switch1 ) {
		case 1:	// -list
					// No parameters are to be given to this API command.
					if( argc > 2 ) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
					}
					else
						rs = api_list();
					break;

		case 2:	// -start
					//Check if there are too many parameters:
					if (argc > 5) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
						break;
					}
					// Check for the optional --database and --noflocklab parameter:
					for (i = 2; i < argc; i++) {
						if (strcmp( argv[i], "--noflocklab" ) == 0) {
							noflocklab = 1;
						}
					}
					rs = api_start();
					break;

		case 3:	// -stop
					// Check if there are enough parameters:
					if( argc > 3 ) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
						break;
					}
					rs = api_stop();
					break;

		case 4:	// -add
					// Cut the first 2 arguments away as this is the program name and the '-add' command:
					gpiosetting_cpy_job_conf(&job_conf, NULL);
					if (validate_and_convert_args(argc-2, &argv[2], &job_conf, 1)) {
						rs = api_add(&job_conf);
					} else {
						rs = EINVAL;
					}
					break;

		case 5:	// -remove
					// Cut the first 2 arguments away as this is the program name and the '-remove' command:
					gpiosetting_cpy_job_conf(&job_conf, NULL);
					if (validate_and_convert_args(argc-2, &argv[2], &job_conf, 1)) {
						rs = api_remove(&job_conf);
					} else {
						rs = EINVAL;
					}
					break;

		case 6:	// -addbatch
					rs = api_addbatch(argc-2, &argv[2]);
					break;

		case 7:	// -help
					api_usage();
					rs = SUCCESS;
					break;

		case 8:	// -version
					printf("%.2f\n", VERSION);
					rs = SUCCESS;
					break;

		case 9:	// -read
					// Check for the additional filename provided:
					if ((argv[2] != NULL) && (strncmp(argv[2], "--file=",	strlen("--file=")) == 0)) {
						// Extract the file path:
						filepath = strtok(argv[2],"=");
						filepath = strtok(NULL,"=");
					}
					rs = api_read(filepath);
					break;

		case 10: // -removeall
					rs = api_removeall();
					break;

		case 11: // -flush
					rs = api_flush();
					break;

		case 12:	// -errorstats
					rs = EINVAL;
					for (i = 2; i < argc; i++) {
						if (strcmp( argv[i], "--get" ) == 0) {
							rs = api_errorstats_get();
							break;
						} else if (strcmp( argv[i], "--reset" ) == 0) {
							rs = api_errorstats_reset();
							break;
						}
					}
					break;

		default:
					syslog(LOG_ERR, "Command not recognized. For help, use flocklab_gpiosetting -help");
					rs = EINVAL;
					break;
	}

	if (rs != SUCCESS) {
		if (rs == EINVAL) {
			syslog(LOG_ERR, "Wrong API use.");
		} else {
			syslog(LOG_ERR, "Error %i occurred!", rs);
		}
	}

	// Close the syslog:
	closelog();

	return rs;
} // main



/**************************************************************************************//**
 *
 * Function to validate user input for API-commands.
 *
 * @param argc_stripped Argument count for the command
 * @param argv_stripped Argument list for the command
 * @param job_conf Pointer to the struct holdig the configuration data
 * @param command Depicting which command wants validation:
 * 					1 -> add or remove
 *
 * @return 	Returns 1 on successful validation, 0 otherwise.
 *
 *****************************************************************************************/
int validate_and_convert_args(int argc_stripped, char *argv_stripped[], struct gpiosetting_job_conf *job_conf, int command)
{
	/* Local variables */
	int i, rs;
	int switch1 = -1;
	char *temp;
	struct arg_detect arg_det;
	struct timeval time_temp;

	// Reset the arg detect struct:
	reset_arg_detect(&arg_det);

	// Check the number of parameters: this is dependent on the command:
	if (command == 1) {
		// For the add/remove command, there need to be between 3 and 5 parameters
		if ((argc_stripped < 3) || (argc_stripped > 7)) {
			syslog(LOG_ERR, "Not enough arguments");
			return 0;
		}
	}
	
	// Fill the global variables with their respective arguments:
	for (i=0; i < argc_stripped; i++){
		// Test if the argument is in the list of expected arguments and assign it a number for the switching process:
		if( strncmp( 		argv_stripped[i], "--pin=",			strlen("--pin=")) == 0)		switch1 = 0; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--level=",		strlen("--level=")) == 0)	switch1 = 1; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--time=", 		strlen("--time=")) == 0)	switch1 = 2; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--us=", 			strlen("--us=")) == 0)		switch1 = 3; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--interval=", 	strlen("--interval=")) == 0)	switch1 = 4; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--count=", 		strlen("--count=")) == 0)	switch1 = 5; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strcmp( 	argv_stripped[i], "--quiet"			) == 0)						switch1 = 6; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--offset=", 		strlen("--offset")) == 0)	switch1 = 7; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		if (switch1 < 0) {
			syslog(LOG_ERR, "Illegal argument detected.");
			return 0;
		}
		// Extract the argument value. If it is empty, abort.
		temp = strtok(argv_stripped[i],"=");
		temp = strtok(NULL,"=");
		if ((temp == NULL) && (switch1 != 6)) {
			syslog(LOG_ERR, "Argument empty");
			return 0;
		}

		if (validate_and_convert_value(temp, switch1, job_conf, &arg_det) == 0)
			return 0;

	}

	/*
	 * Check whether all variables have been set. This is dependent on the command.
	 */
	if (command == 1) {
		// For add/remove command, needed are: pin, level and either offset or time.
		if ((arg_det.pin == 0) || (arg_det.level == 0) || ((arg_det.time == 0) && (arg_det.offset == 0)) ) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		} else {
			// check if interval and count are given
			if ((arg_det.count != arg_det.interval) && (arg_det.count + arg_det.interval > 0)) {
				syslog(LOG_ERR, "Interval and count must be specified for periodic events.");
				return 0;
			}
			else {
				// Check which timeval to take: if time was given, take this. Otherwise take offset:
				if (arg_det.time == 1) {
					job_conf->time_planned.tv_sec = arg_det.tv_time.tv_sec;
					job_conf->time_planned.tv_usec = arg_det.us_val;
				} else {
					// Take offset. If offset and microseconds are 0, pass 0 to kernel space. Otherwise get current time and add the offset.
					if ((arg_det.offset_val == 0) && (arg_det.us_val == 0)) {
						job_conf->time_planned.tv_sec = 0;
						job_conf->time_planned.tv_usec = 0;
					} else {
						gettimeofday(&time_temp, NULL);
						// Add offset to current time, care for wrap of usec:
						if ( (time_temp.tv_usec + arg_det.us_val) >= 1000000) {
							job_conf->time_planned.tv_sec = time_temp.tv_sec + arg_det.offset_val + 1;
							job_conf->time_planned.tv_usec = time_temp.tv_usec + arg_det.us_val - 1000000;
						} else {
							job_conf->time_planned.tv_sec = time_temp.tv_sec + arg_det.offset_val;
							job_conf->time_planned.tv_usec = time_temp.tv_usec + arg_det.us_val;
						}
					}
				}
			}
		}
	}

	return 1;
} // validate_and_convert_args



/**************************************************************************************//**
 *
 * Function to validate a single value for -add, -remove or -addbatch API commands
 *
 * @param temp Value to validate
 * @param argument Argument to do the switch on
 * @param job_conf Pointer to the struct where the converted values should be written to
 * @param arg_det Pointer to the struct used for detection of already written values.
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 *
 *****************************************************************************************/
int validate_and_convert_value(char *temp, int argument, struct gpiosetting_job_conf *job_conf, struct arg_detect *arg_det)
{
	/* Local variables */
	int itemp = 0;
	long ltemp = 0;

	// Validate the argument and fill the value into the job conf:
	switch( argument ) {
		case 0:		// pin
			// Pin. Needs to be a number and in the list of setable pins (if used with FlockLab):
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "Pin is not numeric: %s\n", temp);
				return 0;
			} else {
				itemp = atoi(temp);
				if (!noflocklab) {
					if (!is_pin_setable(itemp)) {
						syslog(LOG_ERR, "Pin is not setable: %s\n", temp);
						return 0;
					}
				}
				job_conf->pin = itemp;
				arg_det->pin = 1;
			}
			break;

		case 1:		// level
			// Level: Needs to be an enum en_level and in the list of valid levels:
			itemp = (int) str_to_enum(temp, en_level, EN_LEVEL_TOP);
			if (itemp == -1) {
				syslog(LOG_ERR, "Level is not enum: %s\n", temp);
				return 0;
			}
			job_conf->level = itemp;
			arg_det->level = 1;
			break;

		case 2:		// time
			// Needs to be a valid time value: This can either be an absolute UTC time or a relative unix timestamp.
			// The time is stored in the special struct arg_det as it is not known at this time whether time or ofset are used.
			if (is_numeric(temp)) {
				// The value is a unix timestamp. Thus, add it to the current time:
				arg_det->tv_time.tv_sec = atol(temp);
			} else {
				// Try to convert from a UTC time:
				arg_det->tv_time.tv_sec = str_to_epoch(temp);
				if (arg_det->tv_time.tv_sec < 0) {
					// Conversion from UTC time failed.
					syslog(LOG_ERR, "Time is not UTC: %s\n", temp);
					return 0;
				}
			}
			arg_det->time = 1;
			break;

		case 3:		// us
			// Microseconds. Needs to be an integer between 0 and 999999
			// The time is stored in the special struct arg_det as it is not known at this time whether time or ofset are used.
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "us is not numeric: %s\n", temp);
				return 0;
			} else {
				itemp = atoi(temp);
				if ((itemp < 0) || (itemp > 999999)) {
					syslog(LOG_ERR, "us is not in range (0-999999): %s\n", itemp);
					return 0;
				}
				// Everything is ok. Thus, add the microseconds to the struct.
				arg_det->us_val = itemp;
			}
			arg_det->us = 1;
			break;

		case 4:		// interval
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "interval is not numeric: %s\n", temp);
				return 0;
			} else {
				itemp = atoi(temp);
				if (itemp < 0) {
					syslog(LOG_ERR, "interval is not positive: %s\n", itemp);
					return 0;
				}
				// Everything is ok.
				job_conf->interval = itemp;
				arg_det->interval = 1;
			}
			break;

		case 5:		// count
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "count is not numeric: %s\n", temp);
				return 0;
			} else {
				itemp = atoi(temp);
				if (itemp < 1) {
					syslog(LOG_ERR, "count is not positive: %s\n", itemp);
					return 0;
				}
				// Everything is ok.
				job_conf->count = itemp;
				arg_det->count = 1;
			}
			break;

		case 6:		// quiet
			// Quiet option. Can be ignored as it was already detected in main();
			break;

		case 7:		// offset
			// Offset in seconds to schedule the job. Needs to be a positive integer.
			// The time is stored in the special struct arg_det as it is not known at this time whether time or offset are used.
			// Only taken into consideration if --time is not present.
			if (arg_det->time == 0) {
				if (!is_numeric(temp)) {
					syslog(LOG_ERR, "offset is not numeric: %s\n", temp);
					return 0;
				} else {
					ltemp = atol(temp);
					if ((ltemp < 0) || (ltemp > 3600)) {
						syslog(LOG_ERR, "Illegal value for offset: %s\n", temp);
						return 0;
					} else {
						arg_det->offset_val = ltemp;
						arg_det->offset = 1;
					}
				}
			} else {
				syslog(LOG_ERR, "Offset option ignored because time option has higher priority.");
			}
			break;

		default:
			return 0;
	}

	return 1;
} // validate_and_convert_value


/**************************************************************************************//**
 *
 * Show usage of API to user.
 *
 *****************************************************************************************/
void api_usage()
{
	/* Local variables */
	int i;

	printf("/---------------------------------------------------------------------------------------\\\n");
	printf("|Usage: flocklab_gpiosetting command [options]\t\t\t\t\t\t|\n");
	printf("|Commands:\t\t\t\t\t\t\t\t\t\t|\n");
	printf("|  -start\t\t\tStart the FlockLab GPIO setting service.\t\t|\n");
	printf("|  -stop\t\t\tStop the FlockLab GPIO setting service.\t\t\t|\n");
	printf("|  -list\t\t\tList all scheduled pin settings.\t\t\t|\n");
	printf("|  -add\t\t\t\tAdd a new time and pin to set.\t\t\t\t|\n");
	printf("|  -addbatch\t\t\tRead a file with multiple times and pins to set.\t|\n");
	printf("|  -remove\t\t\tRemove a scheduled pin setting.\t\t\t\t|\n");
	printf("|  -removeall\t\t\tRemove all scheduled pin settings.\t\t\t|\n");
	printf("|  -flush\t\t\tFlush output buffer for setting results.\t\t|\n");
	printf("|  -errorstats\t\t\tGet/reset error statistics.\t\t\t\t|\n");
	printf("|  -read\t\t\tGet setting results and display them on standard out.\t|\n");
	printf("|  -help\t\t\tPrint this message.\t\t\t\t\t|\n");
	printf("|  -version\t\t\tPrint software version number.\t\t\t\t|\n");
	printf("\\---------------------------------------------------------------------------------------/\n\n");

	printf("\nUsage for start: flocklab_gpiosetting -start [--noflocklab] [--quiet]\n");
	printf("---------------------\n");
	printf("Options for start:\n");
	printf("  --noflocklab\t\t\t Optional. If specified, restrictions which are specific to the FlockLab testbed (such as GPIO mappings) are lifted.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for stop: flocklab_gpiosetting -stop [--quiet]\n");
	printf("---------------------\n");
	printf("Options for stop:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for add/remove: flocklab_gpiosetting {-add | -remove} --pin=<int> --level=<char> {--time=<string> | --offset=<long>} [--us=<int>] [--interval=<int>] [--count=<int>] [--quiet]\n");
	printf("---------------------\n");
	printf("Options for add/remove:\n");
	printf("  --pin=<int>\t\t\t Pin number of the GPIO pin to set. Usable pins are:");
	for (i=0; i<(sizeof(list_gpiopins_set)/sizeof(*list_gpiopins_set)); i++)
		printf(" %i", list_gpiopins_set[i]);
	printf(".\n");
	printf("  --level=<char>\t\t Level to set the pin to. Can be L (low), H (high) or T (toggle).\n");
	printf("  --time=<string>\t\t Absolute time to schedule the pin setting. Format: either time in UTC YYYY/MM/DD hh:mm:ss (for example 2009/11/23 12:00:00) or unix timestamp.\n");
	printf("\t\t\t\t Note that microseconds have to be defined separately.\n");
	printf("\t\t\t\t Note that if both --time and --offset are present, --time is used.\n");
	printf("  --offset=<long>\t\t Relative time in seconds to schedule the pin setting job. The job is inserted relative to the time when the command is invoked.\n");
	printf("\t\t\t\t If job is to be started immediately, insert 0. Maximum is 3600 (1 hour).\n");
	printf("\t\t\t\t Note that microseconds have to be defined separately.\n");
	printf("\t\t\t\t Note that if both --time and --offset are present, --time is used.\n");
	printf("\t\t\t\t Note that removals of jobs always have to be done with absolute times using --time.\n");
	printf("  --us=<int>\t\t\t Optional. Microsecond offset for the pin setting (relative or absolute). Can take values from 0 - 999999. If absent, 0 will be assumed.\n");
	printf("  --interval=<int>\t\t Optional. Interval in microseconds for periodic events.\n");
	printf("  --count=<int>\t\t\t Optional. Number of repetitions for periodic events.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for addbatch: flocklab_gpiosetting -addbatch --file=<string> [--quiet]\n");
	printf("---------------------\n");
	printf("  --file=<string>\t\t Path to where the file is stored without whitespaces. Each line of this file has to contain one set of options as for the\n");
	printf("\t\t\t\t add-command in the following form:\n");
	printf("\t\t\t\t\t pin;level;time;us;\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for read: flocklab_gpiosetting -read [--file=<path>]\n");
	printf("--------------------\n");
	printf("Options for read:\n");
	printf("  --file=<path>\t\t\t Optional. Path to where all read values should be stored. If absent, data will only be written to standard out.\n");

	printf("\nUsage for removeall: flocklab_gpiosetting -removeall [--quiet]\n");
	printf("--------------------\n");
	printf("Options for removeall:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for flush: flocklab_gpiosetting -flush [--quiet]\n");
	printf("--------------------\n");
	printf("Options for flush:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for errorstats: flocklab_gpiosetting -errorstats {--get | --reset} [--quiet]\n");
	printf("--------------------\n");
	printf("Options for errorstats:\n");
	printf("  --get\t\t\t\t Get error counts.\n");
	printf("  --reset\t\t\t Reset error counts.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");
} // api_usage



/**************************************************************************************//**
 *
 * api_start processes the API command 'start'. The command starts the service.
 *
 *  @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_start()
{
	/* Local variables */
	FILE *file_ptr;

	syslog(LOG_INFO, "FlockLab service for GPIO setting starting...");

	if ( is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_INFO, GPIO_SETTING_KM_NAME " is already running and can therefore not be started again.");
	} else {
		system("modprobe "OSTIMERFNCT_KM_NAME);
		system("modprobe "GPIO_SETTING_KM_NAME);
		system("modprobe "OSTIMER_KM_NAME);
	}

	if (noflocklab) {
		// Create a file to indicate the use of the -noflocklab option to the api:
		mkdir(path_flocklab_tmp, 0755);
		file_ptr = fopen(flagfile_gpioset_nofl, "w");
		fclose(file_ptr);
		syslog(LOG_INFO, "No FlockLab flag file created");
	}

	syslog(LOG_INFO, "FlockLab service for GPIO setting started.");

	return SUCCESS;

} // api_start



/**************************************************************************************//**
 *
 * api_stop processes the API command 'stop'. The command stops the service.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_stop()
{
	/* Local variables */
	FILE *file_ptr;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not running and can therefore not be stopped");
		return ENOPKG;
	}

	/* Test if the flocklab service GPIO monitor is running which relies on GPIO setting. If so, do not stop the service */
	if ( is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service GPIO monitor which relies on this service is running. Therefore, this service cannot be stopped");
		return EPERM;
	}

	/* If the powerprofiling is running, the module OSTIMER_KN_NAME cannot be removed as it depends on it. */
	if ( is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service powerprofiling which relies on modules used by this service is running. Use 'modprobe -r %s' before trying again.", OSTIMER_KM_NAME);
		return EPERM;
	}

	syslog(LOG_INFO, "FlockLab service for GPIO setting stopping...");

	/* Stop kernel modules */
	busy_retry = 1;
	while ((system("rmmod "OSTIMER_KM_NAME) != 0) && (busy_retry<=5)) {
		syslog(LOG_WARNING, OSTIMER_KM_NAME " cannot be removed. Waiting for retry number %i", busy_retry);
		usleep(busy_retry*backoff_time);
		busy_retry++;
	}
	if (busy_retry > 5) {
		syslog(LOG_ERR, OSTIMER_KM_NAME " cannot be removed. Aborting stop command");
		return EBUSY;
	} else {
		syslog(LOG_INFO, OSTIMER_KM_NAME " removed");
	}
	busy_retry = 1;
	while ((system("rmmod "GPIO_SETTING_KM_NAME) != 0) && (busy_retry<=5)) {
		syslog(LOG_WARNING, GPIO_SETTING_KM_NAME " cannot be removed. Waiting for retry number %i", busy_retry);
		usleep(busy_retry*backoff_time);
		busy_retry++;
	}
	if (busy_retry > 5) {
		syslog(LOG_ERR, GPIO_SETTING_KM_NAME " cannot be removed. Aborting stop command");
		return EBUSY;
	} else {
		syslog(LOG_INFO, GPIO_SETTING_KM_NAME " removed");
	}

	// Delete the noflocklab flag file:
	if (noflocklab) {
		remove(flagfile_gpioset_nofl);
		syslog(LOG_INFO, "No FlockLab flag file deleted");
	}

	syslog(LOG_INFO, "FlockLab service for GPIO setting stopped");

	return SUCCESS;
} // api_stop



/**************************************************************************************//**
 *
 * api_list processes the API command 'list'. The command lists all scheduled pin setting
 * events.
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_list()
{
	/* Local variables */
	struct gpioset_event* buf;
	int fd, nread;
	unsigned int n=0, nent=0, rs=SUCCESS;
	unsigned int bufsize = 10000;
	char timestr[33];
	time_t time_now;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Allocate memory for the buffer */
	buf = malloc(bufsize*sizeof(struct gpioset_event));
	if (buf == NULL) {
		return ENOMEM;
	}

	/* Open the list device for reading */
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_ERR, "Cannot open list device file because: %s", strerror(fd));
		free(buf);
		return EIO;
	}

	/* Read the file which contains the list. Read line by line and output the entries */
	while (n < bufsize) {
		nread = (read(fd, buf+nent, 100000));
		n += nread;
		if (nread == 0)
			break;
		nent += nread/sizeof(struct gpioset_event);
	}
	close(fd);

	// Set local timezone settings:
	tzset();

	// Output the buffer:
	if (nent > 0) {
		time_now = time(NULL);
		strftime(timestr, 20,"%Y/%m/%d %T", localtime(&time_now));
		printf("GPIO, Level, Time planned (Time now is: %s)\n", timestr);
		for (n = 0; n < nent; n++) {
			strftime(timestr, 20,"%Y/%m/%d %T", localtime(&(buf[n].time_planned.tv_sec)));
			printf("%u, %u, %s.%06lu\n", buf[n].gpio, buf[n].value, timestr, buf[n].time_planned.tv_usec);
		}
	} else {
		printf("No jobs scheduled.\n");
		rs = -ENODATA;
	}


	fflush(NULL);
	free(buf);

	return rs;

} // api_list


/**************************************************************************************//**
 *
 * api_read reads out the device with the pin setting data and writes it to standard
 * out.
 *
 * @param	logfile	Path to the file where the received data should be stored. If NULL,
 * 					only output on standard out.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_read(char *logfile)
{
	/* Local variables */
	struct gpioset_event* databuf;
	int nread, i;
	FILE *logfil;
	int fp;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// Try to open the logfile:
	if (logfile != NULL) {
		if ((logfil = fopen(logfile, "w")) == NULL) {
			syslog(LOG_ERR, "Cannot open log file. Aborting.");
			return EIO;
		}
	}

	// Read data from device file:
	databuf = malloc(SIZEOFEVENTBUF*sizeof(struct gpioset_event));
	if (databuf == NULL) {
		return ENOMEM;
	}
	if ((fp = open("/dev/" DEVICE_PARENT_FOLDER "/" DEVICE_PARENT_GPIO_SETTING "/" DEVICE_NAME_DBBUF, O_RDONLY)) < 0) {
		syslog(LOG_ERR, "Cannot open device file. Aborting.");
		free(databuf);
		return EIO;
	}
	if (logfile) {
		fprintf(logfil, "GPIO, Value, Time planned, Time executed\n");
	} else {
		printf("GPIO, Value, Time planned, Time executed\n");
	}
	while (1) {
		// Collect data in blocks of SIZEOFBUF values:
		i = read(fp, databuf, SIZEOFEVENTBUF);
		if (i == 0) {
			continue;
		} else {
			nread = i / sizeof(struct gpioset_event);
			//DEBUG printf("nread: %i, sizeof struct: %d\n", nread, sizeof(struct gpioset_event));
			for (i=0; i<nread; i++) {
				if (logfile) {
					fprintf(logfil, "%u,%u,%06lu.%06lu,%06lu.%06lu\n", databuf[i].gpio, databuf[i].value, databuf[i].time_planned.tv_sec, databuf[i].time_planned.tv_usec, databuf[i].time_executed.tv_sec, databuf[i].time_executed.tv_usec);
				} else {
					printf("%u %u %06lu.%06lu %06lu.%06lu\n", databuf[i].gpio, databuf[i].value, databuf[i].time_planned.tv_sec, databuf[i].time_planned.tv_usec, databuf[i].time_executed.tv_sec, databuf[i].time_executed.tv_usec);
				}
				/*DEBUG if ((databuf[i].time_executed.tv_sec == 0) && (databuf[i].time_executed.tv_usec == 0)) {
					syslog(LOG_ERR, "Missed event: %u,%u,%06lu.%06lu", databuf[i].gpio, databuf[i].value, databuf[i].time_planned.tv_sec, databuf[i].time_planned.tv_usec);
					continue;
				}*/
			}
			if (logfile)
				fflush(logfil);
		}
	}
	close(fp);
	free(databuf);
	return SUCCESS;
} // api_read



/**************************************************************************************//**
 *
 * api_add processes the API command 'add'. The command adds a pin setting event to the
 * queue.
 *
 * @param job_conf	Pointer to the struct with the configuration data
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/

static int ioctl_add(struct gpiosetting_job_conf *job_conf, int fd) {
	int rs;
	rs = ioctl(fd, GPIO_SETTING_IOCTL_ADD, job_conf);
	switch (rs) {
		case SUCCESS:
			//syslog(LOG_INFO, "Event scheduled successfully");
			return SUCCESS;
			break;
		case ERRMISSEDEVENT:
			syslog(LOG_ERR, "The event could not be scheduled because the execution time is in the past");
			return ETIME;
			break;
		default:
			syslog(LOG_ERR, "Error: job not scheduled because: %d", rs);
			return ENOEXEC;
	}
}

static int api_add(struct gpiosetting_job_conf *job_conf) {
	/* Local variables */
	int fd, rs;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Open device for IOCTL and submit event to kernel module for inserting it into the event queue: */
	// Open the device:
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (fd < 0) {
		syslog(LOG_ERR, "Error: Cannot open device file: %s", strerror(fd));
		return EIO;
	}
	//printf("--> dev file opened.\n");
	
	rs = ioctl_add(job_conf, fd);
	close(fd);
	return rs;
} // api_add



/**************************************************************************************//**
 *
 * api_remove processes the API command 'remove'. The command removes a pin setting event
 * from the queue.
 *
 * @param job_conf	Pointer to the struct with the configuration data
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_remove(struct gpiosetting_job_conf *job_conf) {
	/* Local variables */
	int fd, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Open device for IOCTL and submit event to kernel module for inserting it into the event queue: */
	// Open the device:
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (fd < 0) {
		syslog(LOG_ERR, "Error: Cannot open device file: %s", strerror(fd));
		return EIO;
	}

	rs = ioctl(fd, GPIO_SETTING_IOCTL_REMOVE, job_conf);
	close(fd);
	switch (rs) {
		case ERRNOTFOUND:
			syslog(LOG_ERR, "The event has not been found in the event queue. It has never been in the queue or has already been executed");
			return ENOENT;
			break;
		case ERRLISTEMPTY:
			syslog(LOG_ERR, "The event has not been found in the event queue because the queue is empty. It has never been in the queue or has already been executed");
			return ENOENT;
			break;
		case SUCCESS:
			//syslog(LOG_INFO, "Event removed successfully");
			return SUCCESS;
			break;
		default:
			syslog(LOG_ERR, "Error: job not removed because: %d", rs);
			return ENOEXEC;
	}

	return SUCCESS;

} // api_remove




/**************************************************************************************//**
 *
 * api_removeall processes the API command 'removeall'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_removeall()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_SETTING_IOCTL_REMOVEALL);
	close(file_desc);
	switch (rs) {
		case SUCCESS:
			syslog(LOG_INFO, "All setting jobs have been removed successfully");
			break;
		default:
			rs = ENOEXEC;
			syslog(LOG_ERR, "Unknown error occurred");
			break;
	}

	return rs;

} // api_removeall


/**************************************************************************************//**
 *
 * api_flush processes the API command 'flush'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_flush()
{
	/* Local variables */
	int file_desc, rs;
	char cmd[40];

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_SETTING_IOCTL_FLUSH);
	close(file_desc);
	switch (rs) {
		case SUCCESS:
			syslog(LOG_INFO, "Output buffer for setting results has been flushed.");
			break;
		default:
			rs = ENOEXEC;
			syslog(LOG_ERR, "Unknown error occurred");
			break;
	}

	return rs;

} // api_flush



/**************************************************************************************//**
 *
 * api_errorstats_get processes the API command 'errcnt'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_get()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_SETTING_IOCTL_ERRCNT);
	close(file_desc);
	if (rs >= 0) {
		syslog(LOG_INFO, "Error statistics: kfifo_full_count: %i", rs);
	} else {
		syslog(LOG_ERR, "Unknown error occurred when trying to get error statistics. Ioctl returned %i", rs);
	}

	return SUCCESS;

} // api_errorstats_get



/**************************************************************************************//**
 *
 * api_errorstats_reset processes the API command 'errrst'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_reset()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, GPIO_SETTING_IOCTL_ERRRST);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "Error statistics reset successfully.");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred when trying to reset error statistics.", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_errorstats_reset


/**************************************************************************************//**
 *
 * api_addbatch processes the API command 'addbatch'. The command processes a file with a
 * number of pin settings in it.
 *
 * @param argc_stripped Argument count from main()
 * @param argv_stripped Argument list from main()
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_addbatch(int argc_stripped, char *argv_stripped[])
{
	/* Local variables */
	int i, m, fd, rs = 0;
	int nitems = 6;	// Number of items per line in the batchfile
	char *filename, *tmp[nitems];
	FILE *fp;
	int file_line_size = 100;
	char file_line[file_line_size];
	struct gpiosetting_job_conf job_conf;
	struct arg_detect arg_det;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// There need to be either 1 parameter or 2 parameters (if used with --quiet option)
	if ((argc_stripped != 1) && (argc_stripped != 2))
		return EINVAL;

	// Check the arguments:
	for (i=0; i < argc_stripped; i++){
		if( strncmp( argv_stripped[i], "--file=", 7) == 0) {
			// Extract the file name. If it is empty, abort.
			filename = strtok(argv_stripped[i],"=");
			filename = strtok(NULL,"=");
			if (filename == NULL) {
				syslog(LOG_ERR, "No valid filename provided");
				return ENOENT;
			}
			// Now we have the filename. Lets try to open that file in read-mode:
			if (!(fp = fopen(filename, "r"))) {
				syslog(LOG_ERR, "Error opening the batchfile: %s", strerror(errno));
				return EIO;
			}
			// 
			/* Open device for IOCTL and submit event to kernel module for inserting it into the event queue: */
			// Open the device:
			fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_GPIO_SETTING"/"DEVICE_NAME_LIST, 0);
			if (fd < 0) {
				fclose(fp);
				syslog(LOG_ERR, "Error: Cannot open device file: %s", strerror(fd));
				return EIO;
			}
			//printf("--> dev file opened.\n");
			while((fgets(file_line, file_line_size, fp) != NULL) && (rs==0)) {
				// Parse the line and insert the tokens into the array for validation:
				if ((tmp[0] = strtok(file_line, ";\n")) == NULL) {
					close(fd);
					fclose(fp);
					return EINVAL;
				}
				for (m=1; m < nitems; m++) {
					if ((tmp[m] = strtok(NULL, ";\n")) == NULL) {
						close(fd);
						fclose(fp);
						return EINVAL;
					}
				}
				// Validate the data.
				// Reset the arg detect struct. Changes here should also be done in validate_and_convert_args()
				reset_arg_detect(&arg_det);
				gpiosetting_cpy_job_conf(&job_conf, NULL);

				for (m=0; m < nitems; m++) {
					if (!validate_and_convert_value(tmp[m], m, &job_conf, &arg_det)) {
						//DEBUG printf("tmp[%i]: %s\n", m, tmp[m]);
						close(fd);
						fclose(fp);
						return EINVAL;
					}
				}

				// Check whether all variables have been set. Needed are: level, pin, tv_sec, tv_usec
				if ((arg_det.pin == 0) || (arg_det.level == 0) || (arg_det.time == 0) || (arg_det.us == 0)) {
					close(fd);
					fclose(fp);
					return EINVAL;
				}
				// Fill the time into the struct:
				job_conf.time_planned.tv_sec = arg_det.tv_time.tv_sec;
				job_conf.time_planned.tv_usec = arg_det.us_val;
				// The data is good, thus register it
				rs = ioctl_add(&job_conf, fd);
			}
			close(fd);
			fclose(fp);
		}
	}

	return SUCCESS;
} // api_addbatch


/**************************************************************************************//**
 *
 * Reset all data in the struct to its default values.
 *
 * @param arg_det Pointer to the struct to reset
 *
 /****************************************************************************************/
void reset_arg_detect(struct arg_detect *arg_det) {
	memset(arg_det, 0, sizeof(struct arg_detect));
} // reset_arg_detect


/**************************************************************************************//**
 *
 * Copy the job conf from 'from' to 'to'. If 'from' is NULL, use hardcoded default values.
 *
 * @param to Pointer to the struct to write configuration to
 * @param from Pointer to the struct to copy configuration from. If NULL, use hardcoded defaults.
 *
 * @return no return value
 *
 /****************************************************************************************/
void gpiosetting_cpy_job_conf(struct gpiosetting_job_conf *to, struct gpiosetting_job_conf *from) {
	if (!from) {
		memset(to, 0, sizeof(struct gpiosetting_job_conf));
	} else {
		*to = *from;
	}
} // gpiosetting_cpy_job_conf


/**************************************************************************************//**
 *
 * free_linked_add_list frees all memory that was allocated when building up a linked
 * list of type add_list.
 *
 * @param head head of the linked list
 *
 /****************************************************************************************/
void free_linked_add_list(item *head) {
	/* Local variables */
	item *next, *this;

	this = head;
	// Traverse linked list and delete all elements:
	while (this) {
		next = this->next;
		free(this);
		this = next;
	}
} // free_linked_add_list


