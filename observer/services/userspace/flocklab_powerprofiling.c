/**
 * @file
 * @author Christoph Walser <walserc@tik.ee.ethz.ch>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * @section DESCRIPTION
 *
 * Userspace API implementation for the FlockLab service "Powerprofiling"
 *
 * $Date$
 * $Revision$
 * $Id$
 * $URL$
 */


/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <syslog.h>
#include "flocklab.h"
#include "flocklab_shared.h"
#include "flocklab_powerprof_shared.h"				/* Contains shared definitions of userspace and kernelspace programs for this service */



/* ---- Global variables ---------------------------------------------------*/
#define VERSION 			1.4								// Version number. Has to be incremented manually.


struct arg_detect {
	unsigned duration:	1;
	unsigned time:		1;
	unsigned offset:	1;
	unsigned us:		1;
	unsigned nthsample:1;
	unsigned :			0;
	unsigned long offset_val;
	struct timeval tv_time;
	unsigned long us_val;
};
static int quiet = 0 ;
/**
 * Struct for holding lines of the batch file.
 */
struct add_list {
	struct add_list *next;
	struct powerprof_job_conf job_conf;
};
typedef struct add_list item;


/* ---- Prototypes ----------------------------------------------------------*/
static int api_list();
static int api_start();
static int api_stop();
static int api_add(struct powerprof_job_conf*);
static int api_remove(struct powerprof_job_conf*);
static int api_removeall();
static int api_flush();
static int api_errorstats_get();
static int api_errorstats_reset();
static int api_addbatch(int, char*[]);
int validate_and_convert_args(int, char*[], void*, int);
int validate_and_convert_value(char*, int, void*, struct arg_detect*);
void api_usage();
void free_linked_add_list(item *head);


/************************************************************************************//**
 *
 * Main
 *
 * @param argc Argument count
 * @param argv Argument list
 *
 * @return Returns 0 on success and errno otherwise.
 *
 ***************************************************************************************/
int main(int argc, char *argv[]){
	/* Local variables */
	int i, switch1, rs;
	struct powerprof_job_conf job_conf;
	int nth_sample;

	// Determine whether the quiet option is set and open the syslog accordingly:
	// Set local time to UTC:
	putenv("TZ=UTC");
	tzset();
	quiet = 0;
	for (i = 1; i < argc; i++) {
		if (strcmp( argv[i], "--quiet") == 0) {
			quiet = 1;
			break;
		}
	}
	if (quiet) {
		openlog("flocklab_powerprofiling", LOG_CONS | LOG_PID, LOG_USER);
	} else {
		openlog("flocklab_powerprofiling", LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER);
	}


	// If there are less than 2 arguments, we can abort immediately:
	if ( argc < 2 ) {
		syslog(LOG_ERR, "Wrong API use: specify a command.");
		closelog();
		return EINVAL;
	}

	// Test whether the given API command is in the set of allowed API commands:
	if( strcmp( argv[1], "-list" ) == 0)					switch1 = 1;
	else if( strcmp( argv[1], "-start" ) == 0)				switch1 = 2;
	else if( strcmp( argv[1], "-stop" ) == 0)				switch1 = 3;
	else if( strcmp( argv[1], "-add" ) == 0)				switch1 = 4;
	else if( strcmp( argv[1], "-remove" ) == 0)			switch1 = 5;
	else if( strcmp( argv[1], "-addbatch") == 0)			switch1 = 6;
	else if( strcmp( argv[1], "-help" ) == 0)				switch1 = 7;
	else if( strcmp( argv[1], "-version" ) == 0)			switch1 = 8;
	else if( strcmp( argv[1], "-removeall" ) == 0)			switch1 = 9;
	else if( strcmp( argv[1], "-flush" ) == 0)				switch1 = 10;
	else if( strcmp( argv[1], "-errorstats" ) == 0)		switch1 = 11;
	switch( switch1 ) {
		case 1:	// -list
					// No parameters are to be given to this API command.
					if( argc > 2 ) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
					}
					else
						rs = api_list();
					break;

		case 2:	// -start
					//Check if there are too many parameters:
					if (argc > 4) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
						break;
					}
					rs = api_start();
					break;

		case 3:	// -stop
					// Check if there are enough parameters:
					if( argc > 3 ) {
						syslog(LOG_ERR, "Wrong number of arguments.");
						rs = EINVAL;
						break;
					}
					rs = api_stop();
					break;

		case 4:	// -add
					// Cut the first 2 arguments away as this is the program name and the '-add' command:
					if (validate_and_convert_args(argc-2, &argv[2], &job_conf, 1)) {
						rs = api_add(&job_conf);
					} else {
						rs = EINVAL;
					}
					break;

		case 5:	// -remove
					// Cut the first 2 arguments away as this is the program name and the '-remove' command:
					if (validate_and_convert_args(argc-2, &argv[2], &job_conf, 1)) {
						rs = api_remove(&job_conf);
					} else {
						rs = EINVAL;
					}
					break;

		case 6:	// -addbatch
					rs = api_addbatch(argc-2, &argv[2]);
					break;

		case 7:	// -help
					api_usage();
					rs = SUCCESS;
					break;

		case 8:	// -version
					printf("%.2f\n", VERSION);
					rs = SUCCESS;
					break;

		case 9: // -removeall
					rs = api_removeall();
					break;

		case 10: // -flush
					rs = api_flush();
					break;

		case 11:	// -errorstats
					rs = EINVAL;
					for (i = 2; i < argc; i++) {
						if (strcmp( argv[i], "--get" ) == 0) {
							rs = api_errorstats_get();
							break;
						} else if (strcmp( argv[i], "--reset" ) == 0) {
							rs = api_errorstats_reset();
							break;
						}
					}
					break;

		default:
					syslog(LOG_ERR, "Command not recognized. For help, use flocklab_powerprofiling -help");
					rs = EINVAL;
					break;
	}

	if (rs != SUCCESS) {
		if (rs == EINVAL) {
			syslog(LOG_ERR, "Wrong API use.");
		} else {
			syslog(LOG_ERR, "Error %i occurred!", rs);
		}
	}

	// Close the syslog:
	closelog();

	return rs;
} // main




/**************************************************************************************//**
 *
 * Function to validate user input for API-commands.
 *
 * @param argc_stripped Argument count for the command
 * @param argv_stripped Argument list for the command
 * @param to Pointer to the struct holdig the configuration data
 * @param command Depicting which command wants validation:
 * 					1 -> add or remove
 *
 * @return 	Returns 1 on successful validation, 0 otherwise.
 *
 *****************************************************************************************/
int validate_and_convert_args(int argc_stripped, char *argv_stripped[], void *to, int command)
{
	/* Local variables */
	int i, rs;
	int switch1 = -1;
	char *temp;
	struct arg_detect arg_det;
	struct timeval time_temp;

	// Initialize the structs:
	arg_det.duration = 0;
	arg_det.offset = 0;
	arg_det.time = 0;
	arg_det.offset_val = 0;
	arg_det.tv_time.tv_sec = 0;
	arg_det.tv_time.tv_usec = 0;
	arg_det.us = 0;
	arg_det.us_val = 0;
	arg_det.nthsample = 0;


	// Check the number of parameters: this is dependent on the command:
	if (command == 1) {
		// For the add/remove command, there need to be between 2 and 5 parameters
		if ((argc_stripped < 2) || (argc_stripped > 5)) {
			syslog(LOG_ERR, "Wrong number of arguments");
			return 0;
		}
	}

	// Fill the global variables with their respective arguments:
	for (i=0; i < argc_stripped; i++){
		// Test if the argument is in the list of expected arguments and assign it a number for the switching process:
		if( strncmp( 		argv_stripped[i], "--duration=",	strlen("--duration=")) == 0)	switch1 = 0; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--time=", 		strlen("--time=")) == 0)		switch1 = 1; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--us=", 			strlen("--us=")) == 0)			switch1 = 2; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--divider=", 	strlen("--divider=")) == 0)		switch1 = 3; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strcmp( 	argv_stripped[i], "--quiet"			) == 0)							switch1 = 4; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		else if( strncmp( 	argv_stripped[i], "--offset=", 		strlen("--offset=")) == 0)		switch1 = 5; // Do NOT change the value for switch1 as it is needed as argument for function validate_and_convert_value()
		if (switch1 < 0) {
			syslog(LOG_ERR, "Illegal argument detected.");
			return 0;
		}
		// Extract the argument value. If it is empty, abort.
		temp = strtok(argv_stripped[i],"=");
		temp = strtok(NULL,"=");
		if ((temp == NULL) && (switch1 != 4)) {
			syslog(LOG_ERR, "Argument empty");
			return 0;
		}

		if (validate_and_convert_value(temp, switch1, to, &arg_det) == 0)
			return 0;
	}

	/*
	 * Check whether all variables have been set. This is dependent on the command.
	 */
	if (command == 1) {
		// For add/remove command, needed are: duration and either offset or time.
		if ((arg_det.duration == 0) || ((arg_det.time == 0) && (arg_det.offset == 0)) ) {
			syslog(LOG_ERR, "Not all arguments needed for this command were set.");
			return 0;
		} else {
			// Check which timeval to take: if time was given, take this. Otherwise take offset:
			if (arg_det.time == 1) {
				((struct powerprof_job_conf *)to)->time_start.tv_sec = arg_det.tv_time.tv_sec;
				((struct powerprof_job_conf *)to)->time_start.tv_usec = arg_det.us_val;
			} else {
				// Take offset.
				// If offset and microseconds are 0, pass 0 to kernel space. Otherwise get current time and add the offset.
				if ((arg_det.offset_val == 0) && (arg_det.us_val == 0)) {
					((struct powerprof_job_conf *)to)->time_start.tv_sec = 0;
					((struct powerprof_job_conf *)to)->time_start.tv_usec = 0;
				} else {
					gettimeofday(&time_temp, NULL);
					// Add offset to current time, care for wrap of usec:
					if ( (time_temp.tv_usec + arg_det.us_val) >= 1000000) {
						((struct powerprof_job_conf *)to)->time_start.tv_sec = time_temp.tv_sec + arg_det.offset_val + 1;
						((struct powerprof_job_conf *)to)->time_start.tv_usec = time_temp.tv_usec + arg_det.us_val - 1000000;
					} else {
						((struct powerprof_job_conf *)to)->time_start.tv_sec = time_temp.tv_sec + arg_det.offset_val;
						((struct powerprof_job_conf *)to)->time_start.tv_usec = time_temp.tv_usec + arg_det.us_val;
					}
				}
			}
			// Set nth_sample to 0 if not set:
			if (arg_det.nthsample == 0) {
				((struct powerprof_job_conf *)to)->nth_sample = 0;
			}
		}
	}

	return 1;
} // validate_and_convert_args



/**************************************************************************************//**
 *
 * Function to validate a single value for -add, -remove or -addbatch API commands
 *
 * @param temp Value to validate
 * @param argument Argument to do the switch on
 * @param to Pointer to the struct where the converted values should be written to
 * @param arg_det Pointer to the struct used for detection of already written values.
 *
 * @return Returns 1 on successful validation, 0 otherwise.
 *
 *****************************************************************************************/
int validate_and_convert_value(char *temp, int argument, void *to, struct arg_detect *arg_det)
{
	/* Local variables */
	int itemp = 0;
	long ltemp = 0;

	// Validate the argument and fill the value into the job conf:
	switch( argument ) {
		case 0:		// duration
			// Needs to be a number:
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "duration is not numeric: %s\n", temp);
				return 0;
			}
			ltemp = atol(temp);
			/*
			 * Check the value of itemp: has to be positive.
			 */
			if ((ltemp < 50) || (ltemp > 3600000)) {
				syslog(LOG_ERR, "Illegal value for duration: %d\n", ltemp);
				return 0;
			} else {
				((struct powerprof_job_conf *)to)->sampling_duration_us = ltemp*1000;
			}
			arg_det->duration = 1;
			break;

		case 1:		// time
			// Needs to be a valid time value: This can either be an absolute UTC time or a relative unix timestamp.
			// The time is stored in the special struct arg_det as it is not known at this time whether time or offset are used.
			if (is_numeric(temp)) {
				// The value is a unix timestamp. Thus, add it to the current time:
				arg_det->tv_time.tv_sec = atol(temp);
			} else {
				// Try to convert from a UTC time:
				arg_det->tv_time.tv_sec = str_to_epoch(temp);
				if (arg_det->tv_time.tv_sec < 0) {
					// Conversion from UTC time failed.
					syslog(LOG_ERR, "Time is not UTC: %s\n", temp);
					return 0;
				}
			}
			arg_det->time = 1;
			break;

		case 2:		// us
			// Microseconds. Needs to be an integer between 0 and 999999
			// The time is stored in the special struct arg_det as it is not known at this time whether time or offset are used.
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "us is not numeric: %s\n", temp);
				return 0;
			} else {
				itemp = atoi(temp);
				if ((itemp < 0) || (itemp > 999999)) {
					syslog(LOG_ERR, "us is not in range (0-999999): %s\n", itemp);
					return 0;
				}
				// Everything is ok. Thus, add the microseconds to the struct.
				arg_det->us_val = itemp;
			}
			arg_det->us = 1;
			break;

		case 3:		// divider
			// Sampling divider value for ADS1271 driver. Allowed values: 1,...,32767
			if (!is_numeric(temp)) {
				syslog(LOG_ERR, "divider is not numeric: %s\n", temp);
				return 0;
			}
			itemp = atoi(temp);
			if ((itemp < 1) || (itemp > 2047)) {
				syslog(LOG_ERR, "Illegal value for divider: %d\n", itemp);
				return 0;
			} else {
				((struct powerprof_job_conf *)to)->nth_sample = itemp;
			}
			arg_det->nthsample = 1;
			break;

		case 4:		// quiet
			// Quiet option. Can be ignored as it was already detected in main();
			break;

		case 5:		// offset
			// Offset in seconds to schedule the job. Needs to be a positive integer.
			// The time is stored in the special struct arg_det as it is not known at this time whether time or offset are used.
			// Only taken into consideration if --time is not present.
			if (arg_det->time == 0) {
				if (!is_numeric(temp)) {
					syslog(LOG_ERR, "offset is not numeric: %s\n", temp);
					return 0;
				} else {
					ltemp = atol(temp);
					if ((ltemp < 0) || (ltemp > 3600)) {
						syslog(LOG_ERR, "Illegal value for offset: %s\n", temp);
						return 0;
					} else {
						arg_det->offset_val = ltemp;
						arg_det->offset = 1;
					}
				}
			} else {
				syslog(LOG_ERR, "Offset option ignored because time option has higher priority.");
			}
			break;

		default:
			return 0;
	}

	return 1;
} // validate_and_convert_value


/**************************************************************************************//**
 *
 * Show usage of API to user.
 *
 *****************************************************************************************/
void api_usage()
{
	printf("/---------------------------------------------------------------------------------------\\\n");
	printf("|Usage: flocklab_powerprofiling command [options]\t\t\t\t\t|\n");
	printf("|Commands:\t\t\t\t\t\t\t\t\t\t|\n");
	printf("|  -start\t\t\tStart the service.\t\t\t\t\t|\n");
	printf("|  -stop\t\t\tStop the service.\t\t\t\t\t|\n");
	printf("|  -list\t\t\tList all scheduled power profiles.\t\t\t|\n");
	printf("|  -add\t\t\t\tAdd a new entry to the schedule.\t\t\t|\n");
	printf("|  -addbatch\t\t\tRead a file with multiple schedule entries.\t\t|\n");
	printf("|  -remove\t\t\tRemove a scheduled power profile.\t\t\t|\n");
	printf("|  -removeall\t\t\tRemove all scheduled power profiles.\t\t\t|\n");
	printf("|  -flush\t\t\tFlush output buffer for power profiling results.\t|\n");
	printf("|  -errorstats\t\t\tGet/reset error statistics.\t\t\t\t|\n");
	printf("|  -help\t\t\tPrint this message.\t\t\t\t\t|\n");
	printf("|  -version\t\t\tPrint software version number.\t\t\t\t|\n");
	printf("\\---------------------------------------------------------------------------------------/\n\n");


	printf("\nUsage for start: flocklab_powerprofiling -start [--quiet]\n");
	printf("---------------\n");
	printf("Options for start:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for stop: flocklab_powerprofiling -stop [--quiet]\n");
	printf("--------------\n");
	printf("Options for stop:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for add/remove: flocklab_powerprofiling {-add | -remove} --duration=<int> {--time=<string> | --offset=<long>} [--us=<int>] [--divider=<int>] [--quiet]\n");
	printf("--------------------\n");
	printf("Options for add/remove:\n");
	printf("  --duration=<long>\t\t Duration of sampling in milliseconds. Minimum is 50, maximum is 3600000\n");
	printf("  --time=<string>\t\t Absolute time to schedule the power profiling job. Format: either time in UTC YYYY/MM/DD hh:mm:ss (for example 2009/11/23 12:00:00) or unix timestamp.\n");
	printf("\t\t\t\t Note that microseconds have to be defined separately.\n");
	printf("\t\t\t\t Note that if both --time and --offset are present, --time is used.\n");
	printf("  --offset=<long>\t\t Relative time in seconds to schedule the power profiling job. The job is inserted relative to the time when the command is invoked.\n");
	printf("\t\t\t\t If job is to be started immediately, insert 0 (in this case, also microseconds have to be 0). Maximum is 3600 (1 hour).\n");
	printf("\t\t\t\t Note that microseconds have to be defined separately.\n");
	printf("\t\t\t\t Note that if both --time and --offset are present, --time is used.\n");
	printf("\t\t\t\t Note that removals of jobs always have to be done with absolute times using --time.\n");
	printf("  --us=<int>\t\t\t Optional. Microsecond offset for the power profiling job (relative or absolute). Can take values from 0 - 999999. If absent, 0 will be assumed.\n");
	printf("  --divider=<int>\t\t Optional. Sampling rate divider for ADS1271 driver. SPS=14400/divider. Allowed values are 1 - 2047\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for addbatch: flocklab_powerprofiling -addbatch --file=<string> [--quiet]\n");
	printf("------------------\n");
	printf("  --file=<string>\t\t Path to where the file is stored without whitespaces. Each line of this file has to contain one set of options as for the\n");
	printf("\t\t\t\t add-command in the following form:\n");
	printf("\t\t\t\t\t duration;time;us;divider;\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for removeall: flocklab_powerprofiling -removeall [--quiet]\n");
	printf("--------------------\n");
	printf("Options for removeall:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for flush: flocklab_powerprofiling -flush [--quiet]\n");
	printf("--------------------\n");
	printf("Options for flush:\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");

	printf("\nUsage for errorstats: flocklab_powerprofiling -errorstats {--get | --reset} [--quiet]\n");
	printf("--------------------\n");
	printf("Options for errorstats:\n");
	printf("  --get\t\t\t\t Get error counts.\n");
	printf("  --reset\t\t\t Reset error counts.\n");
	printf("  --quiet\t\t\t Optional. If specified, no output will be written to STDOUT.\n");
} // api_usage



/**************************************************************************************//**
 *
 * api_start processes the API command 'start'. The command starts the service.
 *
 *  @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_start()
{
	syslog(LOG_INFO, "FlockLab service for powerprofiling starting...");

	if ( is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_INFO, POWERPROF_KM_NAME " is already running.");
	} else {
		system("modprobe "OSTIMERFNCT_KM_NAME);
		system("modprobe "POWERPROF_KM_NAME);
		system("modprobe "OSTIMER_KM_NAME);
	}

	syslog(LOG_INFO, "FlockLab service for powerprofiling started.");

	return SUCCESS;

} // api_start



/**************************************************************************************//**
 *
 * api_stop processes the API command 'stop'. The command stops the service.
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_stop()
{
	/* Local variables */
	static int busy_retry;
	static int backoff_time = 1000000;								// Time in us for exponential backoff when trying to remove the kernel module.
	FILE *fp;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not running and can therefore not be stopped");
		return ENOPKG;
	}

	/* Test if the flocklab service GPIO monitor is running which relies on this module. If so, do not stop the service */
	if ( is_module_loaded(GPIO_MONITOR_KM_NAME) ) {
		syslog(LOG_ERR, "The service GPIO monitor which relies on this service is running. Therefore, this service cannot be stopped");
		return EPERM;
	}

	/* If the GPIO setting is running, the module OSTIMER_KN_NAME cannot be removed as it depends on it. */
	if ( is_module_loaded(GPIO_SETTING_KM_NAME) ) {
		syslog(LOG_ERR, "The service GPIO setting which relies on modules used by this service is running. Use 'modprobe -r %s' before trying again.", OSTIMER_KM_NAME);
		return EPERM;
	}

	syslog(LOG_INFO, "FlockLab service for powerprofiling stopping...");

	/* Stop kernel modules */
	busy_retry = 1;
	while ((system("modprobe -r "OSTIMER_KM_NAME) != 0) && (busy_retry<=5)) {
		syslog(LOG_WARNING, OSTIMER_KM_NAME " cannot be removed. Waiting for retry number %i", busy_retry);
		usleep(busy_retry*backoff_time);
		busy_retry++;
	}
	if (busy_retry > 5) {
		syslog(LOG_ERR, OSTIMER_KM_NAME " cannot be removed. Aborting stop command");
		return EBUSY;
	} else {
		syslog(LOG_INFO, OSTIMER_KM_NAME" removed");
	}

	busy_retry = 1;
	while ((system("modprobe -r "POWERPROF_KM_NAME) != 0) && (busy_retry<=5)) {
		syslog(LOG_WARNING, POWERPROF_KM_NAME " cannot be removed. Waiting for retry number %i", busy_retry);
		usleep(busy_retry*backoff_time);
		busy_retry++;
	}
	if (busy_retry > 5) {
		syslog(LOG_ERR, POWERPROF_KM_NAME " cannot be removed. Aborting stop command");
		return EBUSY;
	} else {
		syslog(LOG_INFO, POWERPROF_KM_NAME" removed");
	}

	syslog(LOG_INFO, "FlockLab service for powerprofiling stopped");

	return SUCCESS;
} // api_stop



/**************************************************************************************//**
 *
 * api_list processes the API command 'list'. The command lists all scheduled power
 * profiling jobs.
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_list()
{
	/* Local variables */
	struct powerprof_job_conf* buf;
	int fd, nread;
	unsigned int n=0, nent=0, rs=SUCCESS;
	unsigned int bufsize = 10000;
	char timestr[33];
	time_t time_now;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Allocate memory for the buffer */
	buf = malloc(bufsize*sizeof(struct powerprof_job_conf));
	if (buf == NULL) {
		return ENOMEM;
	}

	/* Open the list device for reading */
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_ERR, "Cannot open list device file because: %s", strerror(fd));
		free(buf);
		return EIO;
	}

	/* Read the file which contains the list. Read line by line and output the entries */
	while (n < bufsize) {
		nread = (read(fd, buf+nent, 100000));
		n += nread;
		if (nread == 0)
			break;
		nent += nread/sizeof(struct powerprof_job_conf);
	}
	close(fd);

	// Set local timezone settings:
	tzset();

	// Output the buffer:
	if (nent > 0) {
		time_now = time(NULL);
		strftime(timestr, 20,"%Y/%m/%d %T", localtime(&time_now));
		printf("Time, Duration[ms], Divider    (Time now is: %s)\n", timestr);
		for (n = 0; n < nent; n++) {
			strftime(timestr, 20,"%Y/%m/%d %T", localtime(&(buf[n].time_start.tv_sec)));
			printf("%s.%06lu, %lu, %d\n", timestr, buf[n].time_start.tv_usec, buf[n].sampling_duration_us/1000, (buf[n].nth_sample));
		}
	} else {
		printf("No jobs scheduled.\n");
		rs = -ENODATA;
	}


	fflush(NULL);
	free(buf);

	return rs;
} // api_list



/**************************************************************************************//**
 *
 * api_add processes the API command 'add'. The command adds a power profiling job to the
 * queue.
 *
 * @param job_conf Pointer to the struct holding the configuration data to send to the kernel module
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_add(struct powerprof_job_conf *job_conf)
{
	/* Local variables */
	int fd, rs;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}
	/* Open device for IOCTL and submit job to kernel module for inserting it into the schedule: */
	// Open the device:
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_ERR, "Error: Cannot open device file: %s", strerror(fd));
		return EIO;
	}

	rs = ioctl(fd, POWERPROF_IOCTL_SCHEDULER_ADD, job_conf);
	close(fd);
	if (rs == SUCCESS) {
		//syslog(LOG_INFO, "Job scheduled successfully");
	} else {
		switch (errno) {
			default:
					syslog(LOG_ERR, "Error: job not scheduled because: %d", errno);
					rs = ENOEXEC;
					break;
		}
	}

	return rs;
} // api_add



/**************************************************************************************//**
 *
 * api_remove processes the API command 'remove'. The command removes a power profiling job
 * from the queue.
 *
 * @param job_conf Pointer to the struct holding the configuration data to send to the kernel module
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_remove(struct powerprof_job_conf* job_conf)
{
	/* Local variables */
	int fd, rs;


	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Open device for IOCTL and submit job to kernel module for inserting it into the schedule: */
	// Open the device:
	fd = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_ERR, "Error: Cannot open device file: %s", strerror(fd));
		return EIO;
	}

	rs = ioctl(fd, POWERPROF_IOCTL_SCHEDULER_REMOVE, job_conf);
	close(fd);
	if (rs == SUCCESS) {
		//syslog(LOG_INFO, "Job removed successfully");
	} else {
		switch (errno) {
			case ERRLISTEMPTY:
			case ERRNOTFOUND:
				syslog(LOG_INFO, "Job was not found in list");
				rs = ENOENT;
				break;
			default:
				syslog(LOG_ERR, "Error: job not removed because: %d", errno);
				rs = ENOEXEC;
				break;
		}
	}
	return rs;

} // api_remove



/**************************************************************************************//**
 *
 * api_removeall processes the API command 'removeall'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_removeall()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, O_RDONLY);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, POWERPROF_IOCTL_SCHEDULER_REMOVEALL);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "All power profiling jobs have been removed successfully");
		rs = SUCCESS;
	} else {
		switch (errno) {
			case ERRLISTEMPTY:
				syslog(LOG_INFO, "There were no power profiling jobs registered.");
				rs = SUCCESS;
				break;
			default:
				syslog(LOG_ERR, "Unknown error %d occurred", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_removeall



/**************************************************************************************//**
 *
 * api_flush processes the API command 'flush'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_flush()
{
	/* Local variables */
	int file_desc, rs;
	char cmd[40];

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, POWERPROF_IOCTL_BUFFER_FLUSH);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "Output buffer for power profiling results has been flushed.");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_flush



/**************************************************************************************//**
 *
 * api_errorstats_get processes the API command 'errcnt'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_get()
{
	/* Local variables */
	int file_desc, rs;
	struct powerprof_errorstats errstats;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, POWERPROF_IOCTL_ERRCNT, &errstats);
	close(file_desc);
	if (rs >= 0) {
		syslog(LOG_INFO, "Error statistics: kfifo_full_occured:\t\t%i", errstats.kfifo_full_occured);
		syslog(LOG_INFO, "Error statistics: rx_fifo_overrun_occured:\t%i", errstats.rx_fifo_overrun_occured);
		syslog(LOG_INFO, "Error statistics: rx_fifo_empty_occured:\t\t%i", errstats.rx_fifo_empty_occured);
	} else {
		syslog(LOG_ERR, "Unknown error occurred when trying to get error statistics. Ioctl returned %i", rs);
	}

	return SUCCESS;

} // api_errorstats_get



/**************************************************************************************//**
 *
 * api_errorstats_reset processes the API command 'errrst'.
 *
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_errorstats_reset()
{
	/* Local variables */
	int file_desc, rs;

	/* Test whether module was loaded at all. If not, show an error message and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	/* Send IOCTL to kernel module. */
	// Open the device:
	file_desc = open("/dev/"DEVICE_PARENT_FOLDER"/"DEVICE_PARENT_POWERPROF"/"DEVICE_NAME_LIST, 0);
	if (file_desc < 0) {
		syslog(LOG_ERR, "Cannot open list device file. Error %i", file_desc);
		return EIO;
	}

	rs = ioctl(file_desc, POWERPROF_IOCTL_ERRRST);
	close(file_desc);
	if (rs == SUCCESS) {
		syslog(LOG_INFO, "Error statistics reset successfully.");
	} else {
		switch (errno) {
			default:
				syslog(LOG_ERR, "Unknown error %d occurred when trying to reset error statistics.", errno);
				rs = ENOEXEC;
				break;
		}
	}

	return rs;

} // api_errorstats_reset



/**************************************************************************************//**
 *
 * api_addbatch processes the API command 'addbatch'. The command processes a file with a
 * number of jobs in it.
 *
 * @param argc_stripped Argument count for the command
 * @param argv_stripped Argument list for the command
 *
 * @return Returns 0 on success, errno otherwise.
 *
 *****************************************************************************************/
static int api_addbatch(int argc_stripped, char *argv_stripped[])
{
	/* Local variables */
	int i, m, rs;
	int nitems = 4;	// Number of items per line in the batchfile
	char *filename, *tmp[nitems];
	FILE *fp;
	int file_line_size = 100;
	char file_line[file_line_size];
	item *curr, *head;
	struct powerprof_job_conf job_conf;
	struct arg_detect arg_det;

	/* Test whether module was loaded at all. If not, give out an error and abort. */
	if ( !is_module_loaded(POWERPROF_KM_NAME) ) {
		syslog(LOG_ERR, "The service is not started.");
		return EPERM;
	}

	// There need to be either 1 or 2 parameters (if used with --quiet option)
	if ((argc_stripped != 1) && (argc_stripped != 2)) {
		return EINVAL;
	}
	// Initialize the linked list:
	head = NULL;

	// Check the arguments:
	for (i=0; i < argc_stripped; i++){
		if( strncmp( argv_stripped[i], "--file=", 7) == 0) {
			// Extract the file name. If it is empty, abort.
			filename = strtok(argv_stripped[i],"=");
			filename = strtok(NULL,"=");
			if (filename == NULL) {
				syslog(LOG_ERR, "No valid filename provided");
				return ENOENT;
			}
			// Now we have the filename. Lets try to open that file in read-mode:
			if (!(fp = fopen(filename, "r"))) {
				syslog(LOG_ERR, "Error opening the batchfile: %s", strerror(errno));
				return EIO;
			}
			while(fgets(file_line, file_line_size, fp) != NULL) {
				// If there are no ';' nor '\n', the current line is invalid:
				if ((tmp[0] = strtok(file_line, ";\n")) == NULL) {
					free_linked_add_list(head);
					fclose(fp);
					return EINVAL;
				}
				// Parse the line and insert the tokens into the array for validation:
				for (m=1; m < nitems; m++) {
					if ((tmp[m] = strtok(NULL, ";\n")) == NULL) {
						free_linked_add_list(head);
						fclose(fp);
						return EINVAL;
					}
				}

				// Reset the arg detect struct. Changes here should also be done in validate_and_convert_args()
				job_conf.sampling_duration_us	= 0;
				job_conf.time_start.tv_sec		= 0;
				job_conf.time_start.tv_usec		= 0;
				job_conf.nth_sample				= 0;

				for (m=0; m < nitems; m++) {
					if (!validate_and_convert_value(tmp[m], m, &job_conf, &arg_det)) {
						free_linked_add_list(head);
						fclose(fp);
						return EINVAL;
					}
				}
				// Check whether all variables have been set. Needed are: duration, tv_sec, tv_usec, divider
				if ((arg_det.duration == 0) || (arg_det.time == 0) || (arg_det.us == 0) || (arg_det.nthsample == 0)) {
					free_linked_add_list(head);
					fclose(fp);
					return EINVAL;
				}
				// Fill the job conf struct:
				job_conf.time_start.tv_sec = arg_det.tv_time.tv_sec;
				job_conf.time_start.tv_usec = arg_det.us_val;
				// The data is good, thus fill it into the linked list:
				curr		= (item *) malloc(sizeof(item));
				curr->next	= head;
				head 		= curr;
				curr->job_conf.sampling_duration_us = job_conf.sampling_duration_us;
				curr->job_conf.time_start.tv_sec  	= job_conf.time_start.tv_sec;
				curr->job_conf.time_start.tv_usec 	= job_conf.time_start.tv_usec;
				curr->job_conf.nth_sample			= job_conf.nth_sample;
			}

			fclose(fp);
			//Insert data with api_add and free memory of linked list:
			curr = head;
			rs = 0;
			while ((curr) && (rs == 0)) {
				rs = api_add(&curr->job_conf);
				curr = curr->next;
			}
			free_linked_add_list(head);
			return rs;
		}
	}

	return SUCCESS;
} // api_addbatch



/**************************************************************************************//**
 *
 * free_linked_add_list frees all memory that was allocated when building up a linked
 * list of type add_list.
 *
 * @param head head of the linked list
 *
 /****************************************************************************************/
void free_linked_add_list(item *head) {
	/* Local variables */
	item *next, *this;

	this = head;
	// Traverse linked list and delete all elements:
	while (this) {
		next = this->next;
		free(this);
		this = next;
	}
} // free_linked_add_list
