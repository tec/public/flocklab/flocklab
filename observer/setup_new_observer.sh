#!/bin/bash
#
# Author: Christoph Walser, (walserc@tik.ee.ethz.ch)
#
#
# Last change: $Date: 2017-08-17 $
#
# $Revision: 3332 $
# $Id: setup_new_observer.sh 3332 2017-08-17 rdaforno $
# $URL: svn://svn.ee.ethz.ch/flocklab/trunk/shared/observer/setup_new_observer.sh $
#
# Purpose: Script to set up and configure a specified observer from scratch.
#

########################################################
#
# Constants (set according to your needs)
#
########################################################

# Constants on local computer:
PACKAGESERVER="http://people.ee.ethz.ch/~perma/gumstix-builds/gumstix-ng/deploy/eglibc/ipk"
TESTMNGMTPATH="testmanagement"
DAQPATH="daq"
VARIOUSPATH="various"
SCRIPTPATH=`dirname $0`

if [[ ! "$SCRIPTPATH" == \/* ]]
then
	SCRIPTPATH="/home/flocklab/observer"	# make the path absolte
fi

# Constants on remote Gumstix:
DBPATH="/home/root/mmc/flocklab/db";
TEMPLATEPATH="/home/root/mmc/flocklab/templates";
USERSPACEMODULEPATH="/usr/bin";


echo "********************************************";
echo "Setting up observer.";
echo "********************************************";

# Check for options:
if [ "$2" ]; then
	if [ "$2" = "-sdsetuponly" -o "$2" = "-notest" -o "$2" = "-bootstrap" ]; then
		echo "found option $2"
	else
		echo "unknown option $2"
		exit
	fi
fi

if [ $# -lt 1 ]; then
	echo "Missing arguments. Try"
	echo ""
	echo "./setup_new_observer.sh <host-name> -bootstrap <serial port>"
	echo " to bootstrap an observer over the serial port, "
	echo ""
	echo "./setup_new_observer.sh <host-name> [-notest]"
	echo " to configure the observer for FlockLab (needs working network connection on the observer), "
	echo ""
	echo "./setup_new_observer.sh <host-name> -sdsetuponly"
	echo " to setup only parts on the SD card."
	exit 1
fi

# If DNS name of Gumstix was not passed as parameter, ask for it:
GUMSTIX=$1;
if [ -z $GUMSTIX ]; then
	echo "Enter full DNS name of Gumstix which you want to set up: ";
	read GUMSTIX;
fi
echo "Gumstix to set up: $GUMSTIX";

if [ "$2" = "-bootstrap" ]
then
	if [ $# -eq 3 -a -c $3 ]
	then
GUMIP=`host $GUMSTIX | awk '/has address/ { print $4 }'`
sed "s/address 129.132.177.XX/address $GUMIP/" network/interfaces > /tmp/interfaces
sed -i 's/'"'"'/'"'"'"'"'"'"'"'"'/g' /tmp/interfaces
/usr/bin/expect -f -<< EOF
log_user 0
set timeout 2
set f [open "/tmp/interfaces"]
set ifconfig [split [read \$f] "\n"]
close \$f
spawn socat $3,b115200,raw,echo=0,crnl,cs8,parenb=0,ignoreeof -
send "\n"
expect {
 timeout {
  exit 1
  }
 "gumstix-ethz-verdex login: "
}
send "root\n"
expect "root@gumstix-ethz-verdex:~# "
send "rm /etc/network/interfaces\n"
expect "root@gumstix-ethz-verdex:~# "
foreach line \$ifconfig {
    send "echo '\$line' >> /etc/network/interfaces\n"
    expect "root@gumstix-ethz-verdex:~# "
}
send "passwd\n"
expect "Enter new password: "
send "PASSWORD\n"
expect "Re-enter new password: "
send "PASSWORD\n"
expect "root@gumstix-ethz-verdex:~# "
send "chmod 0700 ~/.ssh\n"
expect "root@gumstix-ethz-verdex:~# "
send "chmod 0600 ~/.ssh/authorized_keys\n"
expect "root@gumstix-ethz-verdex:~# "
send "chmod g-w /home/root/\n"
expect "root@gumstix-ethz-verdex:~# "
send "exit\n"
expect "exit"
EOF
	if [ $? -eq 0 ];then
		echo 'Bootstraping was successful.'
	else
		echo 'Bootstraping FAILED.'
	fi
	else
		echo "please specify serial port."
	fi
	exit 0
fi

# Find out kernel version of Gumstix:
KERNELVERS=`ssh root@$GUMSTIX "uname -r"`;
if [ $? != 0 ]
then
	echo "Observer $GUMSTIX is not reachable, exiting..."
	exit 1
fi
echo "Found kernel version $KERNELVERS on Gumstix";
KERNELMODULEPATHEXTRA="/lib/modules/$KERNELVERS/extra";
KERNELMODULEPATHMFD="/lib/modules/$KERNELVERS/kernel/drivers/mfd";


echo "/----------------------------------------------------------------------------------------------------------------\\"
echo "| WARNING: Make sure that the target is accessible using SSH and that an SD card is inserted and mounted as EXT3.|";
echo "| WARNING: Make sure that a FlockBoard Target Adaptor TestBoard rev. 1.2 is inserted into each target slot.      |";
echo "\\----------------------------------------------------------------------------------------------------------------/"
if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Press any key to continue...";
	read

	#stop running services
	echo "stop running services"
	ssh root@$GUMSTIX "if [ \$(ps ax | sed '/dbd_gpio_mon/!d;/ sed /d' | wc -l) -gt 0 ]; then flocklab_gpiomonitor -stop;fi"
	ssh root@$GUMSTIX "if [ \$(ps ax | sed '/dbd_gpio_set/!d;/ sed /d' | wc -l) -gt 0 ]; then flocklab_gpiosetting -stop;fi"
	ssh root@$GUMSTIX "if [ \$(ps ax | sed '/dbd_powerpro/!d;/ sed /d' | wc -l) -gt 0 ]; then flocklab_powerprofiling -stop;fi"

	# read only root fs
	ssh root@$GUMSTIX 'sed -i "s/\(rootfs.*defaults\)\(.*\)/\1,ro\2/" /etc/fstab'
	# set ordered mode for ext3 on sd card
	ssh root@$GUMSTIX 'sed -i "/\/dev\/mmcblk0p1/d" /etc/fstab;echo "/dev/mmcblk0p1       /media/card          ext3       defaults,data=ordered,noatime  0  2" >>  /etc/fstab'
fi

# Create directories on mmc:
echo "Create directories and symbolic links on mmc...";
ssh root@$GUMSTIX "rm -rf /media/card/root/flocklab";
ssh root@$GUMSTIX "mkdir -p /media/card/root";
ssh root@$GUMSTIX "mkdir -p /media/card/log";
ssh root@$GUMSTIX "rm -f mmc/root";
ssh root@$GUMSTIX "rm -f mmc";
ssh root@$GUMSTIX "ln -sf /media/card/root /home/root/mmc";

# Create various directories:
echo "Create various directories...";
ssh root@$GUMSTIX "mkdir -p /etc/flocklab/"
ssh root@$GUMSTIX "mkdir -p $DBPATH";

# Copy OPKG config
ssh root@$GUMSTIX "echo 'src/gz base ${PACKAGESERVER}/armv5te' > /etc/opkg/base-feed.conf; echo 'src/gz gumstix-ethz-verdex ${PACKAGESERVER}/gumstix-ethz-verdex' > /etc/opkg/gumstix-ethz-verdex-feed.conf; echo 'src/gz noarch ${PACKAGESERVER}/all' > /etc/opkg/noarch-feed.conf;"
#scp varous/opkg/* root@$GUMSTIX:/etc/opkg/

# Update package repositiories:
echo "Update package repositories...";
ssh root@$GUMSTIX "opkg update"



########################################################
#
# Setup chrony time synchronisation:
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	# Remove and replace existing configurations:
	echo "Set up chrony...";
	ssh root@$GUMSTIX "sed -i 's/^[#]*exit 0/#exit 0/' /etc/init.d/chrony"
	ssh root@$GUMSTIX "/etc/init.d/chrony stop";
	scp chrony/chrony.conf chrony/chrony.keys root@$GUMSTIX:/etc/.;
	scp chrony/chrony.logrotate root@$GUMSTIX:/etc/logrotate.d/.;
	ssh root@$GUMSTIX "rm -f /etc/logrotate.d/chrony";
	#ssh root@$GUMSTIX "mkdir -p /etc/chrony";
	ssh root@$GUMSTIX "if [ -e /usr/sbin/ntpdate ]; then ntpdate swisstime.ethz.ch; fi";
	ssh root@$GUMSTIX "hwclock --systohc --utc";
	ssh root@$GUMSTIX "/etc/init.d/chrony start";
	echo "Adjust system time and RTC..."
	ssh root@$GUMSTIX "/usr/bin/chronyc << EOF
	password chronyPW
	add server 129.132.177.101
	burst 10/100
	EOF
	";
	sleep 5;
	ssh root@$GUMSTIX "/usr/bin/chronyc << EOF
	password chronyPW
	makestep
	EOF
	";
	ssh root@$GUMSTIX "killall -q cron";
	ssh root@$GUMSTIX "/etc/init.d/chrony stop";
	ssh root@$GUMSTIX "hwclock -w";
	ssh root@$GUMSTIX "hwclock -r";
	ssh root@$GUMSTIX "/etc/init.d/chrony start";
	ssh root@$GUMSTIX "date";
fi

########################################################
#
# Upload flocklab services
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	if [ -d "/usr/local/angstrom/arm" ]
	then
		echo "Compile kernel modules and userspace programs"
		cd $SCRIPTPATH/ads1271 && make default
		cd $OLDPWD && cd $SCRIPTPATH/lm3370 && make default
		cd $OLDPWD && cd $SCRIPTPATH/services/kernelspace && make default
		cd $OLDPWD && cd $SCRIPTPATH/services/userspace && make all
	else
		echo "WARNING: Compiler not found! Using precompiled binaries..."
	fi
	cd $OLDPWD && cd $SCRIPTPATH
	echo "Upload kernel module and userspace programs...";
	ssh root@$GUMSTIX "mkdir -p $KERNELMODULEPATHEXTRA"
	ssh root@$GUMSTIX "mkdir -p $KERNELMODULEPATHMFD"
	ssh root@$GUMSTIX "rm -rf $KERNELMODULEPATHEXTRA/ads1271.ko"
	scp services/userspace/flocklab_dbd root@$GUMSTIX:$USERSPACEMODULEPATH/.;
	scp services/kernelspace/flocklab_gpio_setting.ko services/kernelspace/flocklab_gpio_monitor.ko services/kernelspace/flocklab_powerprof.ko root@$GUMSTIX:$KERNELMODULEPATHEXTRA/.;
	scp services/kernelspace/flocklab_ostimer.ko services/kernelspace/flocklab_ostimer_fnct.ko root@$GUMSTIX:$KERNELMODULEPATHEXTRA/.;
	scp ads1271/ads1271.ko root@$GUMSTIX:$KERNELMODULEPATHMFD/.;
	scp lm3370/lm3370.ko root@$GUMSTIX:$KERNELMODULEPATHMFD/.;
	scp services/userspace/flocklab_gpiosetting services/userspace/flocklab_gpiomonitor services/userspace/flocklab_powerprofiling root@$GUMSTIX:$USERSPACEMODULEPATH/.;
	scp services/userspace/flocklab.sh root@$GUMSTIX:/etc/init.d/.;

	echo "Upload python services..."
	scp services/python/flocklab_serial.py root@$GUMSTIX:/usr/bin/.

	echo "Upload and register librabries...";
	scp services/userspace/Libraries/libflocklab.so.1.0.2  root@$GUMSTIX:/usr/lib/.;
	scp services/userspace/Libraries/flocklab.h root@$GUMSTIX:/usr/include/.;
	ssh root@$GUMSTIX "ldconfig";
	ssh root@$GUMSTIX "ln -f -s libflocklab.so.1 /usr/lib/libflocklab.so";

	echo "Register services to start automatically..."
	ssh root@$GUMSTIX "update-rc.d -f flocklab.sh remove"
	ssh root@$GUMSTIX "update-rc.d flocklab.sh defaults 99 01"
fi



########################################################
#
# Upload testmanagement scripts
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload testmanagement scripts...";
	ssh root@$GUMSTIX "mkdir -p /usr/lib/flocklab/python/"
	scp $TESTMNGMTPATH/lib/daemon.py root@$GUMSTIX:/usr/lib/flocklab/python/.
	scp $TESTMNGMTPATH/lib/flocklab.py root@$GUMSTIX:/usr/lib/flocklab/python/.
	scp $TESTMNGMTPATH/logging.conf root@$GUMSTIX:/etc/flocklab/.
	scp $TESTMNGMTPATH/config.ini root@$GUMSTIX:/etc/flocklab/.
	scp $TESTMNGMTPATH/flocklab_starttest.py root@$GUMSTIX:/usr/bin/.
	scp $TESTMNGMTPATH/flocklab_stoptest.py root@$GUMSTIX:/usr/bin/.
	scp $TESTMNGMTPATH/flocklab_scheduler.py root@$GUMSTIX:/usr/bin/.
	scp $SCRIPTPATH/services/python/contiki/contiki-serialdump root@$GUMSTIX:/usr/bin/.
fi

echo "Upload data synchronisation daemons and helper files...";
ssh root@$GUMSTIX "mkdir -p $TEMPLATEPATH;";
scp $TESTMNGMTPATH/default_tg_images/*.ihex root@$GUMSTIX:$TEMPLATEPATH/.
scp $TESTMNGMTPATH/default_tg_images/*.bin root@$GUMSTIX:$TEMPLATEPATH/.
scp $TESTMNGMTPATH/templates.md5 root@$GUMSTIX:$TEMPLATEPATH/../.

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Write observer ID..."
	OBSID=`echo $GUMSTIX | sed 's/flocklab-observer\([0-9]\{3\}\).*/\1/'`
	OBSIDLEN=`echo $OBSID | awk '{print length}'`
	OBSIDPATH="/etc/flocklab/observerid"
	if [ $OBSIDLEN -eq 3 ]; then
		echo "Writing observer id $OBSID to $OBSIDPATH"
		ssh root@$GUMSTIX "echo $OBSID > $OBSIDPATH"
	else
		echo "---> Error: could not determine observer id for $GUMSTIX! Please write the ID on your own into $OBSIDPATH"
	fi
fi


########################################################
#
# Upload flockdaq services
#
########################################################
# install missing packages for FPGA-JTAG
if [ ! "$2" = "-sdsetuponly" ]; then
	ssh root@$GUMSTIX "opkg install xc3sprog;";

	scp $DAQPATH/fpga/DAQ_Board_rev12.bit  root@$GUMSTIX:/lib/firmware/.
	scp $DAQPATH/daq_spi/daq_spi.ko  root@$GUMSTIX:$KERNELMODULEPATHMFD/.
	ssh root@$GUMSTIX "depmod -a"
	scp $DAQPATH/synclog.sh  root@$GUMSTIX:/etc/init.d/.
	ssh root@$GUMSTIX "chmod +x /etc/init.d/synclog.sh"
	ssh root@$GUMSTIX "update-rc.d -f synclog.sh remove"
	ssh root@$GUMSTIX "update-rc.d synclog.sh defaults 99 01"

	scp $DAQPATH/52-flocklab.rules root@$GUMSTIX:/etc/udev/rules.d/;
	ssh root@$GUMSTIX "udevadm control --reload-rules"
	ssh root@$GUMSTIX "udevadm trigger"
	ssh root@$GUMSTIX "/etc/init.d/flocklab.sh start"
        if [ -d "/usr/local/angstrom/arm" ]
        then
		cd $DAQPATH/configdaemon/ && make
		cd $OLDPWD
	else
		echo "WARNING: Compiler not found! Using precompiled binaries..."
	fi
	scp $DAQPATH/configdaemon/flocklab_config_daemon root@$GUMSTIX:/usr/bin
fi


########################################################
#
# Networking...
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload snmp scripts and configuration..."
	ssh root@$GUMSTIX "mkdir -p /etc/snmp/scripts/"
	cd $SCRIPTPATH
	scp snmp/snmpd.conf root@$GUMSTIX:/etc/snmp/.
	scp snmp/scripts/sht21_dewpoint snmp/scripts/uptime.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/cpu_usage_idle.sh snmp/scripts/cpu_usage_system.sh snmp/scripts/cpu_usage_user.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/memoryfragmentation.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/daqsync.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/flocklab_templates_md5sum.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/chrony-stats.sh root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/glossysync_stats.py root@$GUMSTIX:/etc/snmp/scripts/.
	scp snmp/scripts/daq_pps.py root@$GUMSTIX:/etc/snmp/scripts/.

	echo "Upload ssh authorized_keys file..."
	scp network/authorized_keys root@$GUMSTIX:/home/root/.ssh/
	ssh root@$GUMSTIX "sed -i 's/^[#]*UseDNS.*/UseDNS no/' /etc/ssh/sshd_config"

	echo "Set hostname..."
	GUMSTIXHOSTNAME=`echo $GUMSTIX | cut -d'.' -f1`
	ssh root@$GUMSTIX "echo $GUMSTIXHOSTNAME > /etc/hostname; hostname $GUMSTIXHOSTNAME"

	#echo "Rename chrony burst script..."
	#ssh root@$GUMSTIX "mv /etc/network/if-up.d/chrony-burst.sh /etc/network/if-up.d/chrony-burst"

	echo "Upload LED scripts..."
	scp network/gumstix_leds_ifpreup root@$GUMSTIX:/etc/network/if-pre-up.d/;
	scp network/gumstix_leds_ifup root@$GUMSTIX:/etc/network/if-up.d/;
	scp network/gumstix_leds_ifdown root@$GUMSTIX:/etc/network/if-down.d/;
	scp network/gumstix_leds_ifpostdown root@$GUMSTIX:/etc/network/if-post-down.d/;

	echo "Moving execution time of USB script which enables USB ports"
	ssh root@$GUMSTIX "update-rc.d -f enable-usb-ports.sh remove"
	ssh root@$GUMSTIX "update-rc.d enable-usb-ports.sh start 40 S ."
fi


########################################################
#
# Varia...
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload and register watchdogs...";
	scp network/ping_watchdog.sh root@$GUMSTIX:/usr/bin/.;
	ssh root@$GUMSTIX "sed -i '/ping_watchdog\.sh/d' /etc/crontab";
	ssh root@$GUMSTIX "sed -i 's/^1 \* \* \* \* root \/etc\/network\/if-up.d\/chrony-burst.sh/#&/g' /etc/crontab"
	ssh root@$GUMSTIX "echo \"*/2 * * * * root /usr/bin/ping_watchdog.sh whymper ethz.ch 10\" >> /etc/crontab";
fi

if [ ! "$2" = "-sdsetuponly" ]; then
    # adjust the DHCP client
    ssh root@$GUMSTIX "sed -i 's/\/etc\/udhcpc.d/\/var\/tmp/' /etc/udhcpc.d/50default";
fi

echo "Creating directory needed for flocklab tests...";
ssh root@$GUMSTIX "mkdir -p /media/card/root/flocklab/curtest/"

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload target control scripts...";
	scp $TESTMNGMTPATH/tg_interf.py root@$GUMSTIX:/usr/bin/.;
	scp $TESTMNGMTPATH/tg_pwr.py root@$GUMSTIX:/usr/bin/.;
	scp $TESTMNGMTPATH/tg_usbpwr.py root@$GUMSTIX:/usr/bin/.;
	scp $TESTMNGMTPATH/tg_serialid.py root@$GUMSTIX:/usr/bin/.;
	scp $TESTMNGMTPATH/tg_volt.py root@$GUMSTIX:/usr/bin/.;
	scp $TESTMNGMTPATH/tg_reprog.py root@$GUMSTIX:/usr/bin/.
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Remove not needed scripts...";
	ssh root@$GUMSTIX "rm -f /usr/bin/tg_reprogram.sh";
	ssh root@$GUMSTIX "rm -f /usr/bin/tg_on.sh";
	ssh root@$GUMSTIX "rm -f /usr/bin/tg_off.sh";
	ssh root@$GUMSTIX "rm -f /usr/bin/tg_console.sh";
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload and register tools needed for opal targets...";
	echo "Configure cdc_acm kernel-module...";
	ssh root@$GUMSTIX "echo \"cdc_acm\" > /etc/modutils/cdc_acm"
	ssh root@$GUMSTIX "update-modules"
	ssh root@$GUMSTIX "modprobe cdc_acm"
	if [ -d "/usr/local/angstrom/arm" ]
        then
		echo "Compile bossa flash programmer...";
		cd $VARIOUSPATH/opal/bossa/bossa-1.2.1
		make bossac
		cd $OLDPWD
	else
		echo "WARNING: Compiler not found! Using precompiled binary..."
	fi
	echo "Upload bossa flash programmer...";
	scp $VARIOUSPATH/opal/bossa/bossa-1.2.1/bin/bossac root@$GUMSTIX:/usr/bin/.
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Configure lm3370 kernel-module...";
	scp lm3370/udev/reg_lm3370 root@$GUMSTIX:/lib/udev/.;
	scp lm3370/udev/99-flocklab-lm3370.rules root@$GUMSTIX:/etc/udev/rules.d/.;
	ssh root@$GUMSTIX "echo \"lm3370\" > /etc/modutils/lm3370"
	ssh root@$GUMSTIX "update-modules"
	ssh root@$GUMSTIX "modprobe lm3370"
	ssh root@$GUMSTIX "echo \"lm3370 0x21\" > /sys/bus/i2c/devices/i2c-0/new_device"
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Setup logrotate..."
	scp $VARIOUSPATH/logrotate.conf root@$GUMSTIX:/etc/.;
	echo "Setup syslog-ng..."
	scp $VARIOUSPATH/syslog-ng.conf root@$GUMSTIX:/etc/.;
	ssh root@$GUMSTIX "/etc/init.d/syslog restart"
	echo "Set log level for console output..."
	scp $VARIOUSPATH/sysctl.conf root@$GUMSTIX:/etc/.;
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Adjust crontab..."
	ssh root@$GUMSTIX "sed -i 's/^\* \* \* \* \* root \/usr\/bin\/probe-connectivity.sh/#&/g' /etc/crontab"
	#ssh root@$GUMSTIX "sed -i 's/^3 \* \* \* \* root \/usr\/bin\/chrony-trimrtc.sh/#&/g' /etc/crontab"
	#ssh root@$GUMSTIX "sed -i 's/^1 \* \* \* \* root \/etc\/network\/if-up.d\/chrony-burst.sh/#&/g' /etc/crontab"
	ssh root@$GUMSTIX 'echo "*/5 * * * * root /usr/bin/python /etc/snmp/scripts/glossysync_stats.py update" >> /etc/crontab'
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Remove not needed packages..."
	ssh root@$GUMSTIX "opkg remove sqlite3"
	ssh root@$GUMSTIX "opkg remove python-sqlite3"
	ssh root@$GUMSTIX "opkg remove libsqlite3-0"
	ssh root@$GUMSTIX "opkg remove gphoto2 -recursive"
	ssh root@$GUMSTIX "opkg remove libgphoto2-2 -recursive"
	ssh root@$GUMSTIX "opkg remove libgphoto2-port0 -recursive"
	ssh root@$GUMSTIX "opkg remove libexif12 -recursive"
	ssh root@$GUMSTIX "opkg remove backlog"
	ssh root@$GUMSTIX "opkg remove ntpdate"
	ssh root@$GUMSTIX "opkg remove mjpg-streamer"
	ssh root@$GUMSTIX "opkg remove mjpg-streamer-http-plugin"
	ssh root@$GUMSTIX "opkg remove mjpg-streamer-uvc-plugin"
	ssh root@$GUMSTIX "opkg remove libjpeg8"
	ssh root@$GUMSTIX "opkg remove lmsensors-apps"
	ssh root@$GUMSTIX "opkg remove mactelnet"
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Remove not needed stuff from autostart..."
	ssh root@$GUMSTIX "update-rc.d -f ppp remove"
	ssh root@$GUMSTIX "update-rc.d -f backlog remove"
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Add aliases to /etc/profile..."
	ssh root@$GUMSTIX "sed -i '/^alias/d' /etc/profile"
	ssh root@$GUMSTIX "echo \"alias ls=\\\"ls --color\\\"\" >> /etc/profile"
	ssh root@$GUMSTIX "echo \"alias la=\\\"ls -h -al\\\"\" >> /etc/profile"
	ssh root@$GUMSTIX "echo \"alias ps=\\\"ps ax\\\"\" >> /etc/profile"
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload MMC Check script..."
	scp $VARIOUSPATH/mmc_checks/mmc_check.sh root@$GUMSTIX:/usr/bin/.;
	scp $VARIOUSPATH/mmc_checks/mmc_check-screen.sh root@$GUMSTIX:/usr/bin/.;
fi


if [ ! "$2" = "-sdsetuponly" ]; then
	ssh root@$GUMSTIX 'opkg install python-runpy python-msp430-bsl python-msp430-bsl-wismote python-pkgutil python-elementtree python-xml'
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload programming tool for OpenMote..."
	scp $VARIOUSPATH/openmote/cc2538-bsl.py root@$GUMSTIX:/usr/bin
	ssh root@$GUMSTIX "chmod +x /usr/bin/cc2538-bsl.py";
fi

if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Upload stm32loader..."
	scp $VARIOUSPATH/stm32loader/stm32loader.py root@$GUMSTIX:/usr/lib/python2.6/
fi

if [ ! "$2" = "-sdsetuponly" ]; then
    scp $VARIOUSPATH/remount_rootfs_rw.sh root@$GUMSTIX:/usr/bin/
    ssh root@$GUMSTIX "chmod +x /usr/bin/remount_rootfs_rw.sh"
fi

if [ ! "$2" = "-sdsetuponly" ]; then
    scp $VARIOUSPATH/enable-usb-ports.sh root@$GUMSTIX:/etc/init.d/
    #ssh root@$GUMSTIX "chmod +x /usr/bin/remount_rootfs_rw.sh"  -> should already be executable
fi


########################################################
#
# Test target slots
#
########################################################
#if [ ! "$2" = "-sdsetuponly" ]; then
#	if [ ! "$2" = "-notest" ]; then
#		echo "Upload testscript for target slots...";
#		scp ../testing/observer/FlockBoard_rev1.1_testing/Testboard/flockboard_testslots.py root@$GUMSTIX:.;
#		echo "Running testscript for target slots...";
#		ssh root@$GUMSTIX "./flockboard_testslots.py";
#		if [ $? -ne 0 ]; then
#			echo "!!!! Errors occurred when testing the target slots. Correct them and manually run flockboard_testslots.py from the oberserver again. When finished, delete the script from the observer. !!!!";
#		else
#			echo "No errors occurred when testing the target slots. Thus deleting testscript...";
#			ssh root@$GUMSTIX "rm flockboard_testslots.py";
#		fi
#	fi
#fi


########################################################
#
# Configure Gumstix
#
########################################################
if [ ! "$2" = "-sdsetuponly" ]; then
	echo "Update module dependencies using depmod...";
	ssh root@$GUMSTIX "depmod";
fi

echo "Setup finished.";
echo "********************************************";

if [ ! "$2" = "-sdsetuponly" ]; then
    echo "Rebooting observer...";
    ssh root@$GUMSTIX "reboot"
fi

exit;
