#!/bin/sh
# Copyright (C) 2018 Jan Beutel
#
# rdaforno

#210 Number of sources = 3
#Name/IP Address            NP  NR  Span  Frequency  Freq Skew  Offset  Std Dev
#==============================================================================
#PPS0                       49  25   770      0.000      0.001     +0ns   672ns
#time1.ethz.ch              21   9   43m      0.004      0.017    -78us    16us
#time2.ethz.ch              24  15   49m     -0.001      0.010    -87us    12us

sourcename=`chronyc sources | grep "\^\*" | awk '{ print $2 }'`
if [ $1 -eq 5 ]; then
        # write current NTP source to log file
        DATE=`date '+%Y-%m-%d %H:%M:%S'`
        echo "$DATE $sourcename" >> /var/log/chronysource.log
fi

times=`chronyc sourcestats | tail -n +4 | grep $sourcename | sed 's/us/000/g' | sed 's/ms/000000/g' | sed 's/ns//g' | awk '{printf "%s\n", $'$1'}'`

# 6 = skew
# 7 = offset
# 8 = std/dev/dispersion
# for absolute offest: | sed s/^-//
# for dispersion use $8

echo "$times"
