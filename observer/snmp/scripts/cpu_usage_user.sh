#!/bin/sh

/usr/bin/top -b -n 2 -d 0.5 | /bin/sed -n '/Cpu/p' | /usr/bin/tail -n 1 | /bin/sed 's/%/ /g' | /usr/bin/awk '{print $2}'

