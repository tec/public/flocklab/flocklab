#!/usr/bin/env python
#
# checks whether the PPS pulse is present on the FlockDAQ (FPGA)
#
# 2019 rdaforno

import sys
import serial

# config
serialPort = '/dev/flocklab/usb/daq3'
baudRate   = 1000000


def checkPPS():
  global serialPort
  global baudRate
  ser = serial.Serial(port=serialPort, baudrate=baudRate, timeout=2)
  if ser.isOpen():
    # clear the buffers
    ser.flushInput()
    ser.flushOutput()
    # FPGA only outputs data if a PPS pulse is received
    c = ser.read()
    ser.close()
    if len(c) is 0:
      print("NO PPS!")
      sys.exit(1)
    else:
      print("PPS OK")
      sys.exit(0)


if __name__== "__main__":
  checkPPS()
