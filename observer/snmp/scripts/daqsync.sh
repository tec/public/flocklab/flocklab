#!/bin/sh
TESTLOGFILE=/var/log/sync.log
if [ `find $TESTLOGFILE -mmin -1 | wc -l` -eq 1 ]
then
  if [ "`sed '$!d;s/.*GT: [1-9],[1-9],[0-9],1,.*/ok/' $TESTLOGFILE`" = "ok" ]
  then
    echo 1
    exit 0
  fi
fi
echo 0
