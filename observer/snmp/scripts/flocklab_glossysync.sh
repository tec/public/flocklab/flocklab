#!/bin/sh
#
# used to monitor the state of the Glossy Sync protocol used for Flocklab
#
# 2018, rdaforno
#

LOGFILE=/var/log/sync.log
LINECNT=60      # calculate average of x lines

if [ ! -f $LOGFILE ]; then
	#echo "ERROR: file not found!"
	printf "0\n0"
	exit 1;
fi

LASTENTRY=$(date -r $LOGFILE +"%s")
NOW=$(date +"%s")

# older than 10 seconds?
if [ $(($NOW - $LASTENTRY)) -gt 10 ]; then
	#echo "WARNING: timestamp is too old"
	printf "0\n0"
	exit 2;
fi

SYNCINFO=$(tail -n $LINECNT $LOGFILE | awk '{print $NF}')

# check provided argument
if [ $# -gt 0 ]; then
	if [ $1 == "rssi" ]; then
		RSSIVALUES=$(echo $SYNCINFO | awk -F"," '{print $7 " " $8}' | sed 's/\.//')
		echo "$RSSIVALUES"
		exit 0
	elif [ $1 == "snr" ]; then
		SNRVALUE=$(echo $SYNCINFO | awk -F"," '{printf "%d", $7 - $8}')
		echo "$SNRVALUE"
		exit 0
	elif [ $1 == "nrxtx" ]; then
		NRXTX=$(echo $SYNCINFO | awk -F"," '{printf "%u", $1 + $2}')
		echo "$NRXTX"
		exit 0
	else
		exit 1		# unknown parameter
	fi
fi

# without any parameter, provide both SNR and N_RX_TX
#VALUES=$(echo $SYNCINFO | awk -F"," '{printf "%u\n%d", $1+$2, $7-$8}')
VALUES=$(echo $SYNCINFO | awk -F"," '{x+=$1+$2; y+=$7-$8; next} END{printf "%u\n%d", x/NR, y/NR}')
echo "$VALUES"

exit 0
