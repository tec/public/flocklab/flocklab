#!/usr/bin/env python
'''
Script to monitor the state of the Glossy Sync protocol used on FlockLab.
Last n lines are loaded from the sync.log file and analyzed.

2019, rdaforno
'''

import sys
import os
import time
import datetime


logfile   = "/var/log/sync.log"
logfile2  = "/var/log/sync.log.1"
statsfile = "/tmp/glossysync_stats"
timeframe = 300           # seconds
linecnt   = timeframe     # 1 line per second


# collect stats and write to file
def updateStats():
  # check if file exists
  if not os.path.isfile(logfile):
    print("file '%s' not found" % logfile)
    return False
  
  # check the age of the file
  mtime = os.path.getmtime(logfile)
  tdiff = int(time.time() - mtime)
  if tdiff > timeframe:
    print("file '%s' is too old" % logfile)
    return False
  
  # load the whole file
  f = open(logfile, "r")
  lines = f.readlines()
  
  # if not enough lines, also load the previous logfile
  if len(lines) < linecnt:
    if not os.path.isfile(logfile2):
      print("file '%s' not found" % logfile2)
      return False
    f = open(logfile2, "r")
    lines = f.readlines() + lines

  rem = timeframe - tdiff
  lastline = len(lines) - 1
  avgsnr = 0
  nrx = 0
  ntx = 0
  count = 0
  limit = 0
  for i in xrange(lastline, max(lastline - rem, -1), -1):
    pos = lines[i].find(" flocklab")
    if pos:
      try:
        dat = lines[i][:pos]
        t = time.mktime(datetime.datetime.strptime(dat, "%b %d %H:%M:%S").replace(year=2019).timetuple())
        if i == lastline:
          limit = t - rem
          #print("last valid timestamp is %u" % limit)
        elif t < limit:
          break
        gt = lines[i].find("GT: ")
        if gt:
          #print(lines[i].rstrip())
          syncinfo = lines[i].rstrip()[gt+3:-1].split(",")  # remove the dot at the end
          if len(syncinfo) == 8:                            # a valid line must contain 8 elements
            snr = int(syncinfo[6]) - int(syncinfo[7])       # SNR = packet RSSI - noise RSSI
            avgsnr = avgsnr + snr
            count = count + 1
            nrx = nrx + int(syncinfo[0])
            ntx = ntx + int(syncinfo[1])
      except ValueError:
        pass # conversion failed
  
  if count:
    avgsnr = int(avgsnr / count)
  missedpackets = 100 - int(nrx * 100 / (5 * linecnt))  # in % (expect exactly 5x RX per flood)
  missedfloods = int((linecnt - count) * 100 / linecnt) # in %
  
  # each value on a separate line: average SNR (of received packets), % of missed floods, % of missed packets, TX count in %
  result = "\n%d\n%d\n%d\n%d" % (avgsnr, missedfloods, missedpackets, int(ntx * 100 / (5 * linecnt)))
  print(result)
  
  # store results in file
  f = open(statsfile, "w")
  f.write(result)

  # SUCCESS
  return True


# read glossysync stats from file
def getStats():
  # check if file exists
  if not os.path.isfile(statsfile):
    #print("file '%s' not found" % statsfile)
    return False
  
  # check the age of the file
  mtime = os.path.getmtime(statsfile)
  if int(time.time() - mtime) > (timeframe * 2):
    #print("file '%s' is too old" % statsfile)
    return False
  
  f = open(statsfile)
  content = f.read()
  print(content)
  return True


if __name__== "__main__":

  # if any argument is supplied, just read the file content
  if len(sys.argv) < 2:
    if getStats():
      sys.exit(0)

  updateStats()




