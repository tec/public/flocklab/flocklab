/****************************************************************************
*
*   Copyright (c) 2009 Christoph Walser   <walserc@tik.ee.ethz.ch>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License version 2 as
*   published by the Free Software Foundation.
*
*
*
****************************************************************************
*
*   Purpose
*   -------
*   Calculate dew point for sensor readings of SHT75.
*
****************************************************************************/


/* ---- Include Files ---------------------------------------------------- */
#include <stdio.h>
#include <math.h>


/* ---- Global variables ---------------------------------------------------*/



/* ---- Prototypes ----------------------------------------------------------*/



/**************************************************************************************
*
* Main
*
**************************************************************************************/
int main(int argc, char *argv[]){
	/* Local variables */
	char reading[10];
	float rh, temp;
	float tn, m;
	double k, l;
	int dew_point;
	FILE *file_ptr;

	// Get the sensor values.
	// Get the humidity:
	file_ptr = fopen("/sys/devices/platform/sht75/humidity1_input", "r");
	if (file_ptr == NULL) {
		return -1;
	}
	fread(&reading, 10, 1, file_ptr);
	fclose(file_ptr);
	rh = ((float) atoi(reading))/1000;

	// Get the temperature:
	file_ptr = fopen("/sys/devices/platform/sht75/temp1_input", "r");
	if (file_ptr == NULL){
		return -1;
	}
	fread(&reading, 10, 1, file_ptr);
	fclose(file_ptr);
	temp = ((float) atoi(reading))/1000;

	// Determine the right parameters:
	if (temp > 0) {
		tn = 243.12;
		m = 17.62;
	} else {
		tn = 272.62;
		m = 22.46;
	}

	// Calculate the dew point:
	l = log(rh/100);
	k = (m*temp) / (tn+temp);

	dew_point = (int) (1000 * tn * (l+k) / (m-l-k));
	printf("%i\n", dew_point);

	return 0;
} // main
