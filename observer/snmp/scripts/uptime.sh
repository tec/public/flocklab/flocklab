#!/bin/sh

if [ -e /proc/uptime ]
then
  /bin/cat /proc/uptime | /usr/bin/awk '{print $1}'
fi

