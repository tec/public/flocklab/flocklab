#!/bin/sh

if [ -e /sbin/iwconfig ]
then
  /sbin/iwconfig wlan0 | /bin/sed -n '/Access Point/p' | /usr/bin/awk '{ print $6 }'
elif [ -e /usr/sbin/iw ]
then
  /usr/sbin/iw wlan0 link | /bin/sed -n '/Connected to/p' | /usr/bin/awk '{ print $3 }'
fi

