#include "contiki.h"

#define PERIOD (RTIMER_SECOND / 3)

static uint16_t leds = 0;

char toggle_leds(rtimer_t *rt) {
	if (leds & 1) PIN_SET(1, 0); else PIN_CLEAR(1, 0);
	if (leds & 2) PIN_SET(1, 1); else PIN_CLEAR(1, 1);
	if (leds & 4) PIN_SET(1, 2); else PIN_CLEAR(1, 2);
	leds = (leds + 1) % 8;
	return 0;
}

/*---------------------------------------------------------------------------*/
PROCESS(flocklab_blink_process, "FlockLab Blink");
AUTOSTART_PROCESSES(&flocklab_blink_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(flocklab_blink_process, ev, data) {

	PROCESS_BEGIN();

	PORT_SET_AS_OUTPUT(1);

	// execute toggle_leds with period PERIOD, starting from now and using rtimer 0
	rtimer_schedule(0, rtimer_now(), PERIOD, toggle_leds);

	PROCESS_END();

}
