
#include "contiki.h"
#include "contiki-conf.h"
#include "dev/leds.h"
#include "watchdog.h"

#define SET_PIN(a,b)          do { P##a##OUT |=  BV(b); } while (0)
#define UNSET_PIN(a,b)        do { P##a##OUT &= ~BV(b); } while (0)
#define INIT_PIN_IN(a,b)      do { P##a##SEL &= ~BV(b); P##a##DIR &= ~BV(b); } while (0)
#define INIT_PIN_OUT(a,b)     do { P##a##SEL &= ~BV(b); P##a##DIR |=  BV(b); } while (0)
#define PIN_IS_SET(a,b)       (    P##a##IN  &   BV(b))

// LED3 (P6.2) -> LED3
#define SET_PIN_LED3         SET_PIN(8,6)
#define UNSET_PIN_LED3       UNSET_PIN(8,6)
#define INIT_PIN_LED3_IN     INIT_PIN_IN(8,6)
#define INIT_PIN_LED3_OUT    INIT_PIN_OUT(8,6)
#define PIN_LED3_IS_SET      PIN_IS_SET(8,6)

// LED2 (P6.6) -> LED2
#define SET_PIN_LED2         SET_PIN(5,2)
#define UNSET_PIN_LED2       UNSET_PIN(5,2)
#define INIT_PIN_LED2_IN     INIT_PIN_IN(5,2)
#define INIT_PIN_LED2_OUT    INIT_PIN_OUT(5,2)
#define PIN_LED2_IS_SET      PIN_IS_SET(5,2)

// LED1 (P6.7) -> LED1
#define SET_PIN_LED1         SET_PIN(2,4)
#define UNSET_PIN_LED1       UNSET_PIN(2,4)
#define INIT_PIN_LED1_IN     INIT_PIN_IN(2,4)
#define INIT_PIN_LED1_OUT    INIT_PIN_OUT(2,4)
#define PIN_LED1_IS_SET      PIN_IS_SET(2,4)
