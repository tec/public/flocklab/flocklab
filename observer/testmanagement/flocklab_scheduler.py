#! /usr/bin/env python

__author__ 		= "Roman Lim <lim@tik.ee.ethz.ch>"
__copyright__ 	= "Copyright 2016, ETH Zurich, Switzerland"
__license__ 	= "GPL"


import os, sys, getopt, socket, time, subprocess, multiprocessing, Queue, threading, errno, traceback, pickle, tempfile, __main__
from xml.etree.ElementTree import ElementTree
from syslog import *
# Import local libraries:
sys.path.append('/usr/lib/flocklab/python/')
import daemon
from flocklab import SUCCESS
import flocklab


### Global variables ###
###
version = filter(str.isdigit, __version__)
scriptname = os.path.basename(__main__.__file__)
name = "flocklab_scheduler"
###
pidfile					= None
config					= None
debug					= False

def isDaemonRunning():
	try:
		pid = int(open(pidfile,'r').read())
		# Signal the process to stop:
		if (pid > 0):
			try:
				os.kill(pid, 0)
			except OSError:
				os.remove(pidfile)
				return False
		return True
	except ValueError:
		return False # empty pid file ?
	except (IOError, OSError):
		return False # no pid file ?
			
##############################################################################
#
# run_scheduler
#
##############################################################################
def scheduler_thread(msgQueue):
	schedule = []
	syslog(LOG_INFO, "Scheduler thread started.")
	while True:
		if len(schedule) == 0:
			_waittime = None
		else:
			_waittime = min([token["switchtime"] for token in schedule]) - time.time()
			if _waittime < 0:
				_waittime = 0
			syslog(LOG_INFO, "Action scheduled in %f seconds." % _waittime)
		try:
			newtoken = msgQueue.get(True, _waittime)
			syslog(LOG_INFO, "new token or timeout %s." % str(newtoken))
			if newtoken["action"] == "add":
				schedule.append(newtoken)
				syslog(LOG_INFO, "Add token, schedule is %s" % str(schedule))
			elif newtoken["action"] == "remove":
				for r in [test for test in schedule if test["testid"] == newtoken["testid"]]:
					schedule.remove(r)
				syslog(LOG_INFO, "remove token, schedule is %s" % str(schedule))
			elif newtoken["action"] == "stop":
				break
			else:
				syslog(LOG_ERROR,"Unknown action %s" % newtoken["action"])
		except Queue.Empty:
			pass
		now = time.time()
		for s in [test for test in schedule if test["switchtime"] < now]:
			# perform switch
			syslog(LOG_INFO, "Switch to test id %d." % s["testid"])
			# old test:
			#  remove gpio actuation jobs
			#  stop dbd
			try:
				flocklab.stop_services({'flocklab_gpiosetting':['gpio_setting','GPIO setting']}, flocklab.nologger(), 1, debug)
			except:
				syslog(LOG_INFO, traceback.format_exc())
			# new test:
			#  start services (if not already running, e.g., if there was previousely a daq test 
			#  add gpio actuation jobs
			#  start dbd
			# parse xml config
			try:
				flocklab.start_services({'flocklab_powerprofiling':['obsPowerprofConf','powerprofiling','powerprofiling','Powerprofiling'], 'flocklab_gpiosetting':['obsGpioSettingConf','gpio_setting','gpiosetting','GPIO setting'], 'flocklab_gpiomonitor':['obsGpioMonitorConf','gpio_monitor','gpiomonitoring','GPIO monitoring']}, flocklab.nologger(), debug)
				xmlfile = "%s/%d/config.xml" % (config.get("observer","testconfigfolder"), s["testid"])
				tree = ElementTree()
				tree.parse(xmlfile)
				if (tree.find('obsGpioSettingConf') <> None):
					if debug:
						syslog(LOG_INFO,"Found config for GPIO setting.")
					# Cycle trough all configurations and write them to a file which is then fed to the service.
					# Create temporary file:
					(fd, batchfile) = tempfile.mkstemp() 
					f = os.fdopen(fd, 'w+b')
					# Cycle through all configs and insert them into file:
					subtree = tree.find('obsGpioSettingConf')
					pinconfs = list(subtree.getiterator("pinConf"))
					for pinconf in pinconfs:
						if flocklab.timeformat_xml2timestamp(config, pinconf.find('absoluteTime/absoluteDateTime').text) < now:
							continue
						pin = flocklab.pin_abbr2num(pinconf.find('pin').text)
						level = flocklab.level_str2abbr(pinconf.find('level').text)
						interval = pinconf.find('intervalMicrosecs').text
						count = pinconf.find('count').text
						# Get time and bring it into right format:
						starttime = flocklab.timeformat_xml2service(config, pinconf.find('absoluteTime/absoluteDateTime').text)
						microsecs = pinconf.find('absoluteTime/absoluteMicrosecs').text
						f.write("%s;%s;%s;%s;%s;%s;\n" %(pin, level, starttime, microsecs, interval, count))
					f.close()
					# Feed service with batchfile:
					cmd = ['flocklab_gpiosetting', '-addbatch', '--file=%s'%batchfile]
					if not debug:
						cmd.append('--quiet')
					syslog(LOG_INFO, str(cmd))
					p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
					(out, err) = p.communicate()
					if (p.returncode != SUCCESS):
						msg = "Error %d when trying to configure GPIO setting service: %s"%(p.returncode, str(err))
						if debug:
							syslog(LOG_ERROR,msg)
							syslog(LOG_ERROR,"Tried to configure with: %s"%(str(cmd)))
					else:
						# Remove batch file:
						os.remove(batchfile)
						if debug:
							syslog(LOG_INFO,"Configured GPIO setting service.")
					#  start dbd
					cmd = ['flocklab_dbd', '-start', '--testid=%d' % s["testid"], '--service=gpio_setting', '--threshold=%i'%(config.getint("gpiosetting", "dbd_threshold")), '--quiet']
					p = subprocess.Popen(cmd, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT)
					rs = p.wait()
					if (rs not in (SUCCESS,)):
						msg = "Error %d when trying to start database daemon for %s service."%(rs, services[key][3])
						if debug:
							syslog(LOG_ERROR,msg)
							syslog(LOG_ERROR,"Tried to start with: %s"%(str(cmd)))
					else:
						if debug:
							if rs == SUCCESS:
								syslog(LOG_INFO,"Started database daemon for gpio_setting service.")
					#select slot
					slotnr = int(tree.find('obsTargetConf/slotnr').text)
					if slotnr:
						flocklab.tg_interface_set(slotnr)
						syslog(LOG_INFO,"switched to slot %d." % slotnr)
			except IOError:
				msg = "Could not find or open XML file at %s."%(str(xmlfile))
				if debug:
					syslog(LOG_INFO, msg)
				syslog(LOG_INFO, traceback.format_exc())
			schedule.remove(s)
			
	syslog(LOG_INFO, "Scheduler thread stopped.")

##############################################################################
#
# run_scheduler
#
##############################################################################
def run_scheduler():
	try:
		os.remove(socketfile)
	except OSError:
		pass
	daemon.daemonize(pidfile=pidfile, closedesc=True)
	syslog(LOG_INFO, "Daemonized process")
	# start scheduler thread
	msgQueue = multiprocessing.Queue()
	th = threading.Thread(target = scheduler_thread, args=(msgQueue,))
	th.daemon = True
	th.start()
	
	# open socket
	s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	s.bind(socketfile)
	s.listen(1)
	while True:
		conn, addr = s.accept()
		try:
			f = conn.makefile('rb')
			data = pickle.load(f)
			if not data:
				continue
			if isinstance(data, dict) and "action" in data:
				if data["action"] == "add":
					syslog(LOG_INFO, "Received <add> token.")
					# inform scheduler thread
					msgQueue.put(data)
				elif data["action"] == "remove":
					syslog(LOG_INFO, "Received <remove> token.")
					# inform scheduler thread
					msgQueue.put(data)
				elif data["action"] == "stop":
					syslog(LOG_INFO, "Received <stop> token.")
					# inform scheduler thread
					msgQueue.put(data)
					th.join(10)
					break
		except socket.error:
			syslog(LOG_INFO, "Socket error")
		except EOFError:
			pass
		f.close()
		conn.close()
	syslog(LOG_INFO, "Stopped scheduler.")

##############################################################################
#
# Usage
#
##############################################################################
def usage():						 
	print "Usage: %s --(add/remove/stop/start/help/version) [--testid=<int>] [--switchtime=<int>] [--debug]" %sys.argv[0]
	print "Options:"
	print "  --add\t\tAdd a switch job. If there is no scheduler running, a new daemon instance will be started."
	print "  --remove\t\tRemove an existing switch job."
	print "  --stop\t\t\tStop the scheduler daemon."
	print "  --start\t\t\tStart the scheduler daemon."
	print "  --help\t\t\tOptional. Print this help."
	print "  --version\t\t\tOptional. Print version number of software and exit."
	print "  --testid=<int>\t\tID of the test."
	print "  --switchtime=<int>\t\tTime at which the switch-over should take place (Unix timestamp), only needed for the --add option."
	print "  --debug\t\t\tOptional. Print debug messages to log."
	return(0)
### END usage()


##############################################################################
#
# Main
#
##############################################################################
def main(argv):
	
	global pidfile
	global config
	global debug
	global socketfile
	
	action = None
	testid = None
	switchtime = None
	socketfile = "/var/run/flocklab_scheduler.sock"
	
	# Open the syslog:
	openlog('flocklab_scheduler', LOG_CONS | LOG_PID | LOG_PERROR, LOG_USER)
	
	# Get config:
	config = flocklab.get_config()
	if not config:
		logger.warn("Could not read configuration file. Exiting...")
		sys.exit(errno.EAGAIN)
	pidfile = "%s/%s" %(config.get("observer", "pidfolder"), "flocklab_scheduler.pid")
	
	# Get command line parameters.
	try:								
		opts, args = getopt.getopt(argv, "arsthvdi:w:", ["add", "remove", "stop", "start", "help", "version", "debug", "testid=", "switchtime="])
	except getopt.GetoptError, err:
		syslog(LOG_ERR, str(err))
		usage()
		sys.exit(errno.EINVAL)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit(SUCCESS)
		elif opt in ("-v", "--version"):
			print version
			sys.exit(SUCCESS)
		elif opt in ("-d", "--debug"):
			syslog(LOG_INFO, "Debug option detected.")
			debug = True
		elif opt in ("-a", "--add"):
			action = 'add'
		elif opt in ("-r", "--remove"):
			action = 'remove'
		elif opt in ("-s", "--stop"):
			action = 'stop'
		elif opt in ("-t", "--start"):
			action = 'start'
		elif opt in ("-i", "--testid"):
			testid = int(arg)
		elif opt in ("-w", "--switchtime"):
			try:
				switchtime = int(arg)
			except ValueError:
				print "Invalid switch time"
		else:
			print "Wrong API usage"
			syslog(LOG_ERR, "Wrong API usage")
			usage()
			sys.exit(errno.EINVAL)
	
	# check parameters for requested action 
	if action is None:
		m = "Wrong API usage"
		print m
		syslog(LOG_ERR, m)
		usage()
		sys.exit(errno.EINVAL)
	
	if action in ("add", "remove") and testid is None:
		m = "Test id missing for requested action %s" % action
		print m
		syslog(LOG_ERR, m)
		sys.exit(errno.EINVAL)
		
	if action == "start":
		""" Check if daemon option is on. If on, reopen the syslog without the ability to write to the console.
		If the daemon option is on, later on the process will also be daemonized.
		"""
		if not isDaemonRunning():
			closelog()
			openlog('flocklab_scheduler', LOG_CONS | LOG_PID, LOG_USER)
			run_scheduler()
		else:
			syslog(LOG_DEBUG, "Scheduler was already started")
		
	if action in ("add", "remove", "stop"):
		if not isDaemonRunning():
			if action == "stop":
				syslog(LOG_DEBUG, "Scheduler was not running.")
				sys.exit(SUCCESS)
			else:
				# start daemon
				cmd = [sys.argv[0], '--start']
				if debug:
					cmd.append("--debug")
				p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
				p.wait()
		for i in xrange(5):
			try:
				s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
				s.connect(socketfile)
				s.send(pickle.dumps({"action":action, "testid":testid, "switchtime": switchtime}))
				s.close()
				break
			except:
				time.sleep(0.5)
		sys.exit(SUCCESS)

### END main()


if __name__ == "__main__":
	try:
		main(sys.argv[1:])
	except SystemExit:
		pass
	except:
		syslog(LOG_ERR, "Encountered error in line %d: %s: %s: %s\n\n--- traceback ---\n%s--- end traceback ---\n\nCommandline was: %s" % (traceback.tb_lineno(sys.exc_info()[2]), str(sys.exc_info()[0]), str(sys.exc_info()[1]), str(traceback.print_tb(sys.exc_info()[2])), traceback.format_exc(), str(sys.argv)))
		sys.exit(errno.EAGAIN)
