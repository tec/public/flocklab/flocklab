__author__	  = "Christoph Walser <walser@tik.ee.ethz.ch>"
__copyright__   = "Copyright 2010, ETH Zurich, Switzerland, Christoph Walser"
__license__	 = "GPL"
__version__	 = "$Revision$"
__date__		= "$Date$"
__id__		  = "$Id$"
__source__	  = "$URL$"

"""
!!!IMPORTANT!!! This file belongs to /usr/lib/flocklab/python/ on the FlockLab server
"""

# Needed imports:
import sys, os, errno
import time
import ConfigParser
import logging.config
import subprocess
from syslog import *

### Global variables ###
tg_pwr_path 	= """/sys/devices/platform/gpio/TARGET%i_POWER/"""
tg_usbpwr_path 	= """/sys/devices/platform/gpio/TARGET%i_USB_POWER/"""
tg_interf_enable= """/sys/devices/platform/gpio/INTERFACE_ENABLE/"""
tg_interf_addr0 = """/sys/devices/platform/gpio/INTERFACE_ENABLE_ADDR0/"""
tg_interf_addr1 = """/sys/devices/platform/gpio/INTERFACE_ENABLE_ADDR1/"""
tg_rst			= """/sys/devices/platform/gpio/TARGET_RST/"""
tg_prog			= """/sys/devices/platform/gpio/TARGET_PROG/"""
LM3370PATH		= '/proc/lm3370'

# Error code to return if there was no error:
SUCCESS = 0

##############################################################################
#
# get_config - read config.ini and return it to caller.
#
##############################################################################
def get_config():
	"""Arguments: 
			none
	   Return value:
			The configuration object on success
			none otherwise
	"""
	configpath = '/etc/flocklab/config.ini'
	
	try: 
		config = ConfigParser.SafeConfigParser()
		config.read(configpath)
	except:
		syslog(LOG_ERR, "Could not read % because: %s: %s" %(configpath, str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		config = None
	return config
### END get_config()


##############################################################################
#
# get_logger - Open a logger for the caller.
#
##############################################################################
def get_logger(loggername=""):
	"""Arguments: 
			loggername
	   Return value:
			The logger object on success
			none otherwise
	"""
	configpath = '/etc/flocklab/logging.conf'
	
	try:
		logging.config.fileConfig(configpath)
		logger = logging.getLogger(loggername)
	except:
		syslog(LOG_ERR, "%s: Could not open logger because: %s: %s" %(str(loggername), str(sys.exc_info()[0]), str(sys.exc_info()[1])))
		logger = None
	return logger
### END get_logger()


##############################################################################
#
# led_on - Turn the desired LED on
#
##############################################################################
def led_on(ledpath=None):
	"""Arguments: 
			Path to the LED control structure (usually this is somwhere in /sys/class/leds/)
	   Return value:
			0 on success
			errno otherwise
	"""
	if (not ledpath) or (not os.path.isdir(ledpath)):
		return errno.EINVAL
	
	f = open("%s/trigger"%ledpath, 'w')
	f.write("none")
	f.close()
	f = open("%s/brightness"%ledpath, 'w')
	f.write("1")
	f.close()
	
	return 0
### END led_on()


##############################################################################
#
# led_off - Turn the desired LED off
#
##############################################################################
def led_off(ledpath=None):
	"""Arguments: 
			Path to the LED control structure (usually this is somwhere in /sys/class/leds/)
	   Return value:
			0 on success
			errno otherwise
	"""
	if (not ledpath) or (not os.path.isdir(ledpath)):
		return errno.EINVAL
	
	f = open("%s/trigger"%ledpath, 'w')
	f.write("none")
	f.close()
	f = open("%s/brightness"%ledpath, 'w')
	f.write("0")
	f.close()
	
	return 0
### END led_off()


##############################################################################
#
# led_blink - Let the desired LED blink
#
##############################################################################
def led_blink(ledpath=None, ondelay=0, offdelay=0):
	"""Arguments: 
			Path to the LED control structure (usually this is somwhere in /sys/class/leds/)
			Delay in ms before LED is turned on
			Delay in ms before LED is turned off
	   Return value:
			0 on success
			errno otherwise
	"""
	if (not ledpath) or (not os.path.isdir(ledpath)):
		return errno.EINVAL
	
	f = open("%s/trigger"%ledpath, 'w')
	f.write("timer")
	f.close()
	f = open("%s/delay_on"%ledpath, 'w')
	f.write(str(ondelay))
	f.close()
	f = open("%s/delay_off"%ledpath, 'w')
	f.write(str(offdelay))
	f.close()
	
	return 0
### END led_blink()


##############################################################################
#
# tg_pwr_get - get power state of a target slot
#
##############################################################################
def tg_pwr_get(slotnr):
	if slotnr not in (1,2,3,4):
		return errno.EINVAL
	
	f = open(tg_pwr_path%slotnr + 'value', 'r')
	rs = int(f.read(1))
	f.close()
	
	return rs
### END tg_pwr_get()


##############################################################################
#
# tg_pwr_set - set power state of a target slot
#
##############################################################################
def tg_pwr_set(slotnr, value):
	if slotnr not in (1,2,3,4):
		return errno.EINVAL
	if value not in (0,1):
		return errno.EINVAL
	
	f = open(tg_pwr_path%slotnr + 'direction', "w")
	f.write('out')
	f.close()
	f = open(tg_pwr_path%slotnr + 'value', "w")
	f.write(str(value))
	f.close()
	
	return value
### END tg_pwr_get()



##############################################################################
#
# tg_usbpwr_get - get USB power state of a target slot
#
##############################################################################
def tg_usbpwr_get(slotnr):
	if slotnr not in (1,2,3,4):
		return errno.EINVAL
	
	f = open(tg_usbpwr_path%slotnr + 'value', 'r')
	rs = int(f.read(1))
	f.close()
	
	return rs
### END tg_usbpwr_get()


##############################################################################
#
# tg_usbpwr_set - set USB power state of a target slot
#
##############################################################################
def tg_usbpwr_set(slotnr, value):
	if slotnr not in (1,2,3,4):
		return errno.EINVAL
	if value not in (0,1):
		return errno.EINVAL
	
	f = open(tg_usbpwr_path%slotnr + 'direction', "w")
	f.write('out')
	f.close()
	f = open(tg_usbpwr_path%slotnr + 'value', "w")
	f.write(str(value))
	f.close()
	
	return value
### END tg_usbpwr_set()


##############################################################################
#
# tg_interface_get - get currently active slot interface
#
##############################################################################
def tg_interface_get():
	# Read values of relevant GPIOS:
	f = open(tg_interf_enable + "direction", "r")
	interfacenable =f.read()
	f.close()
	f = open(tg_interf_addr0 + "value", "r")
	addr0 = f.read()
	f.close()
	f = open(tg_interf_addr1 + "value", "r")
	addr1 = f.read()
	f.close()
	# If pin direction is set to out, no interface is selected:
	if (interfacenable == "in\n"):
		rs = 0
	elif (addr0 == "0\n") and (addr1 == "0\n"):
		rs = 1
	elif (addr0 == "1\n") and (addr1 == "0\n"):
		rs = 2
	elif (addr0 == "0\n") and (addr1 == "1\n"):
		rs = 3
	elif (addr0 == "1\n") and (addr1 == "1\n"):
		rs = 4
	else:
		rs = None
	
	return rs
### END tg_interface_get()


##############################################################################
#
# tg_interface_set - set specific slot interface to be active
#
##############################################################################
def tg_interface_set(slotnr):
	if slotnr not in (None,1,2,3,4):
		return errno.EINVAL
	
	cmds = [("in",  tg_interf_enable + "direction"), 
	   		("out", tg_interf_addr0  + "direction"), 
	   		("out", tg_interf_addr1  + "direction")]
	if (slotnr == 1):
		cmds.append(("0",   tg_interf_addr0  + "value"))
		cmds.append(("0",   tg_interf_addr1  + "value"))
		cmds.append(("out", tg_interf_enable +  "direction"))
	elif (slotnr == 2):
		cmds.append(("1",   tg_interf_addr0  + "value"))
		cmds.append(("0",   tg_interf_addr1  + "value"))
		cmds.append(("out", tg_interf_enable +  "direction"))
	elif (slotnr == 3):
		cmds.append(("0",   tg_interf_addr0  + "value"))
		cmds.append(("1",   tg_interf_addr1  + "value"))
		cmds.append(("out", tg_interf_enable +  "direction"))
	elif (slotnr == 4):
		cmds.append(("1",   tg_interf_addr0  + "value"))
		cmds.append(("1",   tg_interf_addr1  + "value"))
		cmds.append(("out", tg_interf_enable +  "direction"))
			
	for cmd in cmds:
		f = open(cmd[1], "w")
		f.write(cmd[0])
		f.close()
	
	return SUCCESS
### END tg_interface_set()


##############################################################################
#
# tg_reset - Reset target on active interface
#
##############################################################################
def tg_reset(usleep):
	if (usleep == None):
		usleep = 0.00010
	if ((type(usleep) not in (int, float)) or (usleep <= 0)):
		return errno.EINVAL
	
	for pin in (tg_rst, tg_prog):
		f = open(pin + 'direction', 'w')
		f.write('out')
		f.close()
		f = open(pin + 'value', 'w')
		f.write('0')
		f.close()
	time.sleep(usleep)
	rst = open(tg_rst + 'value', 'w')
	rst.write('1')
	rst.flush()
	time.sleep(usleep)
	rst.write('0')
	rst.close()
	time.sleep(usleep)
	
	return SUCCESS
### END tg_reset()


##############################################################################
#
# tg_reset - Pull reset pin for target on active interface without releasing it.
#
##############################################################################
def tg_reset_keep():
	rst = open(tg_rst + 'direction', 'w')
	rst.write('out')
	rst.close()
	
	rst = open(tg_rst + 'value', 'w')
	rst.write('1')
	rst.close()
	
	return SUCCESS
### END tg_reset_keep()


##############################################################################
#
# pin_abbr2num - Convert a pin abbreviation to its corresponding number
#
##############################################################################
def pin_abbr2num(abbr=""):
	if not abbr:
		return errno.EINVAL
	abbrdict =	{
					'RST' : 60,
					'SIG1': 75,
					'SIG2': 74,
					'INT1': 113,
					'INT2': 87,
					'LED1': 71,
					'LED2': 70,
					'LED3': 69
				}
	try:
		pinnum = abbrdict[abbr.upper()]
	except KeyError:
		return errno.EFAULT
	
	return pinnum	
### END pin_abbr2num()


##############################################################################
#
# level_str2abbr - Convert a pin level string to its abbreviation
#
##############################################################################
def level_str2abbr(levelstr=""):
	if not levelstr:
		return errno.EINVAL
	strdict =	{
					'LOW'   : 'L',
					'HIGH'  : 'H',
					'TOGGLE': 'T'
				}
	try:
		abbr = strdict[levelstr.upper()]
	except KeyError:
		return errno.EFAULT
	
	return abbr	
### END level_str2abbr()



##############################################################################
#
# edge_str2abbr - Convert a pin edge string to its abbreviation
#
##############################################################################
def edge_str2abbr(edgestr=""):
	if not edgestr:
		return errno.EINVAL
	strdict =	{
					'RISING' : 'R',
					'FALLING': 'F',
					'BOTH'   : 'B'
				}
	try:
		abbr = strdict[edgestr.upper()]
	except KeyError:
		return errno.EFAULT
	
	return abbr	
### END edge_str2abbr()


##############################################################################
#
# gpiomon_mode_str2abbr - Convert a GPIO monitoring mode string to its abbreviation
#
##############################################################################
def gpiomon_mode_str2abbr(modestr=""):
	if not modestr:
		return errno.EINVAL
	strdict =	{
					'CONTINUOUS' : 'C',
					'SINGLE'     : 'S'
				}
	try:
		abbr = strdict[modestr.upper()]
	except KeyError:
		return errno.EFAULT
	
	return abbr	
### END gpiomon_mode_str2abbr()


##############################################################################
#
# tg_volt_get - get currently set voltage on the active interface
#
##############################################################################
def tg_volt_get():
	try:
		f = open(LM3370PATH, 'r')
		read_v = f.read(2)
		f.close()
	except IOError, e:
		return -1
		
	return int(read_v)
### END tg_volt_get()


##############################################################################
#
# tg_volt_set - set voltage on the active interface
#
##############################################################################
def tg_volt_set(newvoltage, forcepwm = None):
	try:
		f = open(LM3370PATH, 'w')
		if forcepwm is not None:
			if forcepwm:
				f.write('force')
			else:
				f.write('auto')
			f.flush()
		f.write('%d'%newvoltage)
		f.close()
	except IOError, e:
		return -1
	
	return SUCCESS
### END tg_volt_set()


##############################################################################
#
# is_sdcard_mounted - check if the SD card is mounted
#
##############################################################################
def is_sdcard_mounted():
	try:
		cmd = ["mountpoint", "-q", "/media/card"]
		p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
		p.communicate(None)
		if (p.returncode == 0):
			return True
		else:
			return False
	except:
		return False
### END is_sdcard_mounted()


##############################################################################
#
# timeformat_xml2service -	Convert between different timeformats of 
#							XML config file and FlockLab services
#
##############################################################################
def timeformat_xml2service(config=None, timestring=""):
	if (not config) or (not timestring):
		return errno.EINVAL
	try:
		# First convert time from xml-string to time format:
		xmltime = time.strptime(timestring, config.get("xml", "timeformat_xml"))
		# Now convert to service time-string:
		servicetimestring = time.strftime(config.get("observer", "timeformat_services"), xmltime)
	except:
		return errno.EFAULT
	
	return servicetimestring	
### END timeformat_xml2service()

##############################################################################
#
# timeformat_xml2timestamp -	Convert between different timeformats of 
#							XML config file and FlockLab services
#
##############################################################################
def timeformat_xml2timestamp(config=None, timestring=""):
	if (not config) or (not timestring):
		return errno.EINVAL
	try:
		# First convert time from xml-string to time format:
		xmltime = time.strptime(timestring, config.get("xml", "timeformat_xml"))
	except:
		return errno.EFAULT
	
	return time.mktime(xmltime)
### END timeformat_xml2service()

##############################################################################
#
# start_services -	
#
##############################################################################
def start_services(services, logger, debug):
	errors = []
	for key in services.iterkeys():
		# Start service:
		cmd = [key, '-start']
		if not debug:
			cmd.append('--quiet')
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(out, err) = p.communicate()
		if (p.returncode not in (SUCCESS, errno.EEXIST)):
			msg = "Error %d when trying to start %s service: %s"%(p.returncode, services[key][3], str(err))
			errors.append(msg)
			if debug:
				logger.error(msg)
				logger.error("Tried to start with: %s"%(str(cmd)))
		else:
			if debug:
				if p.returncode == SUCCESS:
					logger.debug("Started %s service."%(services[key][3]))
				elif p.returncode == errno.EEXIST:
					logger.debug("%s service was already running."%(services[key][3]))
	return errors

##############################################################################
#
# stop_services -	
#
##############################################################################
def stop_services(services, logger, testid, debug):
	errors = []
	# Remove all pending jobs:
	for key in services.iterkeys():
		cmd = [key, '-removeall']
		if not debug:
			cmd.append('--quiet')
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(out, err) = p.communicate()
		rs = p.returncode
		if (rs not in (SUCCESS, errno.ENOPKG)):
			msg = "Error %d when trying to run command '-removeall' for %s service."%(rs, services[key][1])
			errors.append(msg)
			logger.error(msg)
			if debug:
				logger.error("Tried to run command '-removeall' for %s service with: %s"%(services[key][1], str(cmd)))
		else:
			if debug:
				logger.debug("Successfully ran command '-removeall' for %s service."%(services[key][1]))
	# Flush all output fifo's:
	for key in services.iterkeys():
		cmd = [key, '-flush']
		if not debug:
			cmd.append('--quiet')
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(out, err) = p.communicate()
		rs = p.returncode
		if (rs not in (SUCCESS, errno.ENOPKG)):
			msg = "Error %d when trying to run command '-flush' for %s service."%(rs, services[key][1])
			errors.append(msg)
			logger.error(msg)
			if debug:
				logger.error("Tried to run command '-flush' for %s service with: %s"%(services[key][1], str(cmd)))
		else:
			if debug:
				logger.debug("Successfully ran command '-flush' for %s service."%(services[key][1]))
	# Stop database daemons:
	for key in services.iterkeys():
		cmd = ['flocklab_dbd', '-stop', '--testid=%d' % testid, '--service=%s'%(services[key][0])]
		p = subprocess.Popen(cmd, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT)
		(out, err) = p.communicate()
		rs = p.returncode
		if (rs not in (SUCCESS,)):
			msg = "Error %d when trying to stop database daemon for %s service."%(rs, services[key][1])
			errors.append(msg)
			logger.error(msg)
			if debug:
				logger.error("Tried to start with: %s"%(str(cmd)))
		else:
			if debug:
				if rs == SUCCESS:
					logger.debug("Stopped database daemon for %s service."%(services[key][1]))
	return errors

### END stop_services()

class nologger():

	def error(self, msg):
		pass
	
	def debug(self, msg):
		pass
		
	def info(self, msg):
		pass
