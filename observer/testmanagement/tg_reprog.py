#! /usr/bin/env python

__author__	  = "Christoph Walser <walser@tik.ee.ethz.ch>"
__copyright__   = "Copyright 2010, ETH Zurich, Switzerland, Christoph Walser"
__license__	 = "GPL"
__version__	 = "$Revision$"
__date__		= "$Date$"
__id__		  = "$Id$"
__source__	  = "$URL$"

"""
This file belongs to /usr/bin/ on the observer
"""

import os, sys, getopt, subprocess, errno, time, serial
# Import local libraries:
sys.path.append('/usr/lib/flocklab/python/')
from flocklab import SUCCESS
import flocklab 


### Global variables ###
###
version = filter(str.isdigit, __version__)
###
imagefile	= None
target	   	= None
targetlist  = ('tmote', 'tinynode', 'opal', 'iris', 'mica2', 'wismote', 'cc430', 'acm2', 'openmote', 'dpp', 'dpp2lora', 'dpp2lorahg')
porttypelist= ('usb', 'serial')
porttype	= None
pin_rst	  	= '/sys/devices/platform/gpio/TARGET_RST/'
pin_prog	= '/sys/devices/platform/gpio/TARGET_PROG/'
pin_sig1	= '/sys/devices/platform/gpio/TARGET_SIG1/'
pin_sig2	= '/sys/devices/platform/gpio/TARGET_SIG2/'
noreset		= False
debug		= False



##############################################################################
#
# Error classes
#
##############################################################################
class Error(Exception):
	"""Base class for exceptions in this module."""
	pass
### END Error classes



##############################################################################
#
# Usage
#
##############################################################################
def usage():
	print "Usage: %s --image=<path> --target=<string> [--port=<string>] [--core=<int>] [--noreset] [--debug] [--help] [--version]" %sys.argv[0]
	print "Reprogram a target node on the FlockBoard. The target will be turned on after the reprogramming."
	print "Options:"
	print "  --image=<path>\t\tAbsolute path to image file which is to be flashed onto target node"
	print "  --target=<string>\t\tType of target. Allowed targets: %s" %(", ".join(targetlist))
	print "  --port=<string>\t\tOptional. Specify port which is used for reprogramming. Allowed values are: %s. Defaults: usb for tmote, opal, iris, mica2 and wismote, serial for tinynode, cc430, acm2, openmote." %(", ".join(porttypelist))
	print "  --core=<int>\t\tOptional. Specify core to program. Defaults to 0."
	print "  --noreset\t\t\tOptional. If set, node will not be reset after reprogramming."
	print "  --debug\t\t\tOptional. Print debug messages to log."
	print "  --help\t\t\tOptional. Print this help."
	print "  --version\t\t\tOptional. Print version number of software and exit."
### END usage()

##############################################################################
#
# Helper functions
#
##############################################################################
def init_pins(pins):
	for pin in pins:
		f = open(pin+'direction', 'w')
		f.write('out')
		f.close()

def set_pin(pin, value):
	f = open(pin+'value', 'w')
	f.write(str(value))
	f.close()

##############################################################################
#
# tinynode_bsl_reset
#
##############################################################################
def tinynode_bsl_reset():
	usleep = 0.00005
	# Set pin directions of needed pins:
	init_pins((pin_rst, pin_prog))
	
	set_pin(pin_rst, 0)
	set_pin(pin_prog, 0)
	time.sleep(usleep)
	
	set_pin(pin_rst, 1)
	time.sleep(usleep)
	
	set_pin(pin_prog, 1)
	time.sleep(usleep)
	
	set_pin(pin_prog, 0)
	time.sleep(usleep)
	
	set_pin(pin_prog, 1)
	time.sleep(usleep)
	
	set_pin(pin_rst, 0)
	time.sleep(usleep)
	
	set_pin(pin_prog, 0)
	time.sleep(usleep)
### END tinynode_bsl_reset()


##############################################################################
#
# reprog_tinynode
#
##############################################################################
def reprog_tinynode(imagefile, slotnr, porttype):
	speed = '38400'
	usleep = 0.00010
	if (porttype == 'serial'):
		port = '/dev/ttyS1'
		cmd_flash	 = ["msp430-bsl-legacy", "-c", port, "-S", speed, "-e", "-p", "-v", "-I", imagefile]
	elif (porttype == 'usb'):
		port = os.path.realpath('/dev/flocklab/usb/target%d'%slotnr)
		cmd_flash	 = ["msp430-bsl-legacy", "--invert-reset", "-c", port, "-S", speed, "-e", "-p", "-v", "-I", imagefile]
		# For some reason if programming over USB, the reset line has to be triggered much slower:
		usleep = 0.15000
	if debug:
		cmd_flash.append("--debug")
		
	flocklab.tg_reset(usleep)
	tinynode_bsl_reset()
		
	# Flash image:
	if subprocess.call(cmd_flash) != 0:
		flocklab.tg_reset(usleep)
		return 3
		
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
		
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_tinynode()


##############################################################################
#
# reprog_tmote_usb
#
##############################################################################
def reprog_tmote_usb(imagefile, slotnr):
	usleep = 0.00005
	port  = os.path.realpath('/dev/flocklab/usb/target%d'%slotnr)
	
	init_pins((pin_rst,pin_prog))
	set_pin(pin_rst,0)
	set_pin(pin_prog,0)
	cmd = ["msp430-bsl-telosb", "-p", port, "-e", "-S", "-V", "-i", "ihex", "-P", imagefile]
	if debug:
		cmd.append("-vvvvvvvvv")
		cmd.append("--debug")
	if subprocess.call(cmd) != 0:
		flocklab.tg_reset(usleep)
		return 3
	
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
	
	# Turn off and on again the power (to prevent bug when reprogramming a contiki image after having a tos image installed:
	flocklab.tg_reset_keep()
	flocklab.tg_pwr_set(slotnr, 0)
	flocklab.tg_usbpwr_set(slotnr, 0)
	time.sleep(0.5)
	flocklab.tg_pwr_set(slotnr, 1)
	flocklab.tg_usbpwr_set(slotnr, 1)
	time.sleep(0.5)
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_tmote_usb()


##############################################################################
#
# reprog_cc430
#
##############################################################################
def reprog_cc430(imagefile, slotnr):
	# deactivate GPS pulse
	gps_port = '/dev/flocklab/usb/ext'
	if (os.path.exists('%s1' % gps_port)):
		gps_port = '%s1' % gps_port
	elif (os.path.exists('%s2' % gps_port)):
		gps_port = '%s2' % gps_port
	else:
		gps_port = '%s3' % gps_port
	
	if (os.path.exists(gps_port)):
		serGPS = serial.Serial(gps_port, 9600, timeout=1)
		deactivateCmd = "\xB5\x62\x06\x31\x20\x00\x00\x01\x00\x00\x32\x00\x00\x00\x40\x42\x0F\x00\x40\x42\x0F\x00\x64\x6D\x0B\x00\x64\x6D\x0B\x00\x00\x00\x00\x00\xF6\x00\x00\x00\x5A\x70"
		serGPS.write(deactivateCmd)
		serGPS.write('\r\n')
	else:
		serGPS = None
	init_pins((pin_rst, pin_prog))
	# Only use a speed of 38400 because it works not flawlessly with 115200
	ret = reprog_msp430bsl5_common(imagefile, slotnr, '/dev/ttyS1', speed = 38400)
	
	# Turn off and on again the power due to a bug in the CC430 (described in CC430F5137 Device Erratasheet SLAZ094H under tag PMM8):
	flocklab.tg_reset_keep()
	flocklab.tg_pwr_set(slotnr, 0)
	flocklab.tg_usbpwr_set(slotnr, 0)
	time.sleep(1.0)
	flocklab.tg_pwr_set(slotnr, 1)
	flocklab.tg_usbpwr_set(slotnr, 1)
	time.sleep(0.5)
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(0.00005)

	# reactivate GPS pulse again
	if (serGPS is not None) and (serGPS.isOpen()):
		activateCmd = "\xB5\x62\x06\x31\x20\x00\x00\x01\x00\x00\x32\x00\x00\x00\x40\x42\x0F\x00\x40\x42\x0F\x00\x64\x6D\x0B\x00\x64\x6D\x0B\x00\x00\x00\x00\x00\xF7\x00\x00\x00\x5B\x74" 
		serGPS.write(activateCmd)
		serGPS.write('\r\n')
		serGPS.close()
	return ret

def reprog_msp430bsl5_common(imagefile, slotnr, port, prog_toggle_num=1, progstate=0, speed=38400):
	usleep = 0.00005
	
	set_pin(pin_rst, 1)
	set_pin(pin_prog, 1)
	time.sleep(usleep)
	
	for i in range(0, prog_toggle_num):
		set_pin(pin_prog, 0)
		time.sleep(usleep)
	
		set_pin(pin_prog, 1)
		time.sleep(usleep)
	
	set_pin(pin_rst, 0)
	time.sleep(usleep)

	set_pin(pin_prog, 0)
	
	if progstate == 1:
		time.sleep(usleep)
		set_pin(pin_prog, 1)

	import msp430.bsl5.uart
	
	cmd = ["-p", port, "-e", "-S", "-V", "--speed=%d" % speed, "-i", "ihex", "-P", imagefile]
	if debug:
		cmd.append("-vvvvvvvvv")
		cmd.append("--debug")
	bsl =  msp430.bsl5.uart.SerialBSL5Target()
	try:
		bsl.main(cmd)
	except Exception:
		flocklab.tg_reset(usleep)
		return 3
	
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
	set_pin(pin_prog, 0)
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_cc430()

##############################################################################
#
# reprog_acm2
#
##############################################################################
def reprog_acm2(imagefile, slotnr):
	port  = '/dev/ttyS1'
	init_pins((pin_rst, pin_prog))
	return reprog_msp430bsl5_common(imagefile, slotnr, port)
### END reprog_acm2()

##############################################################################
#
# reprog_wismote_usb
#
##############################################################################
def reprog_wismote_usb(imagefile, slotnr):
	usleep = 0.1
	port  = os.path.realpath('/dev/flocklab/usb/target%d'%slotnr)
	
	# Bring Wismote into BSL mode:
	init_pins((pin_rst, pin_prog))

	set_pin(pin_rst,0)
	set_pin(pin_prog,1)
	time.sleep(usleep)
	set_pin(pin_prog,0)
	time.sleep(usleep)
	set_pin(pin_rst,1)
	time.sleep(usleep)
	set_pin(pin_rst,0)
	time.sleep(usleep)
	set_pin(pin_prog,1)
	
	cmd = ["python", "-m", "msp430wismote.bsl5.uart", "-p", port, "--erase=0x8000-0x3FFFF", "--input-format=ihex", imagefile]
	if debug:
		cmd.append("-vvvvvvvvv")
		cmd.append("--debug")
	if subprocess.call(cmd) != 0:
		set_pin(pin_rst,1)
		time.sleep(usleep)
		set_pin(pin_rst,0)
		return 3
	
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
	
	# Turn off and on again the power (to prevent bug when reprogramming a contiki image after having a tos image installed:
	set_pin(pin_rst,1)
	flocklab.tg_pwr_set(slotnr, 0)
	flocklab.tg_usbpwr_set(slotnr, 0)
	time.sleep(0.5)
	flocklab.tg_pwr_set(slotnr, 1)
	flocklab.tg_usbpwr_set(slotnr, 1)
	time.sleep(0.5)
	
	# Reset if not forbidden by caller: 
	set_pin(pin_rst,1)
	if noreset:
		pass
	else:
		time.sleep(usleep)
		set_pin(pin_rst,0)

	return 0
### END reprog_wismote_usb()


##############################################################################
#
# reprog_opal_usb
#
##############################################################################
def reprog_opal_usb(imagefile, slotnr):
	port  = os.path.realpath('/dev/flocklab/usb/target%d'%slotnr)
	usleep = 0.00005

	# make sure the cdc_acm kernel module handles the opal USB:
	f = open('/sys/bus/usb/drivers/cdc_acm/new_id', 'w')
	f.write('03eb 6124')
	f.close()
	
	init_pins((pin_rst, pin_prog))
	set_pin(pin_prog,1)
	# Wait at least 220ms according to opal spec:
	time.sleep(0.250)
	set_pin(pin_prog,0)
	flocklab.tg_reset(usleep)
	# Wait for USB driver to start:
	time.sleep(2)
	# Find out to which tty the opal is connected to as bossa cannot handle symlinks:
	realtty = os.path.split(os.path.realpath(port))[1]
	
	cmd = ["bossac", "-e", "-w", "-v", "--port=%s"%realtty, "-b", imagefile]
	if debug:
		cmd.append("--debug")
	if subprocess.call(cmd) != 0:
		flocklab.tg_reset(usleep)
		return 3
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_opal_usb()


##############################################################################
#
# reprog_iris_usb
#
##############################################################################
def reprog_iris_usb(imagefile, slotnr):
	usleep = 0.00005
	
	# Turn off interface #TODO: to be removed
	flocklab.tg_interface_set(None)
	
	# Find out bus and device numbers which are needed by avrdude:
	try:
		f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.4/1-2.4.%d/busnum'%slotnr)
	except IOError:
		f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.1/1-2.1.4/1-2.1.4.%d/busnum'%slotnr) # with fpga board
	busnum = int(f.read())
	f.close()
	try:
		f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.4/1-2.4.%d/devnum'%slotnr)
	except IOError:
		f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.1/1-2.1.4/1-2.1.4.%d/devnum'%slotnr) # with fpga board
	devnum = int(f.read())
	f.close()
	
	cmd = ["avrdude", "-C/etc/avrdude.conf", "-P", "usb:%.3d:%.3d"%(busnum, devnum), "-pm1281", "-cusbtiny", "-B", "1", "-U", "flash:w:%s"%imagefile, "-U", "hfuse:w:0xD8:m", "-U", "lfuse:w:0xFF:m", "-U", "efuse:w:0xFF:m"]
	if debug:
		cmd.append("-vv")
	if subprocess.call(cmd) != 0:
		flocklab.tg_reset(usleep)
		return 3
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)
		
	# Turn interface back on #TODO: to be removed
	flocklab.tg_interface_set(slotnr)
	
	return 0
### END reprog_iris_usb()


##############################################################################
#
# reprog_mica2_usb
#
##############################################################################
def reprog_mica2_usb(imagefile, slotnr):
	usleep = 0.00005
	
	# Turn off interface #TODO: to be removed
	flocklab.tg_interface_set(None)
	
	# Find out bus and device numbers which are needed by avrdude:
	f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.4/1-2.4.%d/busnum'%slotnr)
	busnum = int(f.read())
	f.close()
	f = open('/sys/devices/platform/pxa27x-ohci/usb1/1-2/1-2.4/1-2.4.%d/devnum'%slotnr)
	devnum = int(f.read())
	f.close()
	
	cmd = ["avrdude", "-C/etc/avrdude.conf", "-P", "usb:%.3d:%.3d"%(busnum, devnum), "-pm128", "-cusbtiny", "-B", "1", "-U", "flash:w:%s"%imagefile, "-U", "hfuse:w:0xD8:m", "-U", "lfuse:w:0xFF:m", "-U", "efuse:w:0xFF:m"] 	
	if debug:
		cmd.append("-vv")
	if subprocess.call(cmd) != 0:
		flocklab.tg_reset(usleep)
		return 3
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)
		
	# Turn interface back on #TODO: to be removed
	flocklab.tg_interface_set(slotnr)
	
	return 0
### END reprog_mica2_usb()

##############################################################################
#
# reprog_openmote
#
##############################################################################
def reprog_openmote(imagefile, slotnr):
	port   = '/dev/ttyS1'
	speed  = '230400'
	usleep = 0.00010

	cmd_flash	 = ["cc2538-bsl.py", "-b", speed, "-p", port, "-o", "254", "-e", "-w", "-v", imagefile]

	# Bring OpenMote into BSL mode:
	init_pins((pin_rst, pin_prog))
	set_pin(pin_rst,1) # Pull RESET
	set_pin(pin_prog,1) # Pull PROG
	time.sleep(usleep)
	set_pin(pin_rst,0) # Release RESET
	time.sleep(usleep)
	set_pin(pin_prog,0) # Release PROG (does effect target only after Next reset)
	time.sleep(usleep)

	# Flash image:
	if subprocess.call(cmd_flash) != 0:
		flocklab.tg_reset(usleep)
		return 3

	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])

	# Reset if not forbidden by caller:
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_openmote()

##############################################################################
#
# reprog_dpp
#
##############################################################################
def reprog_dpp(imagefile, slotnr, core):
	port   = '/dev/ttyS1'
	core2sig = ((0,0),(1,0),(0,1),(1,1)) # (sig1,sig2)
	
	init_pins((pin_prog, pin_rst, pin_sig1, pin_sig2))
	# select core
	set_pin(pin_sig1, core2sig[core][0])
	set_pin(pin_sig2, core2sig[core][1])
	
	# program
	ret = 1
	if core == 0: # com
		ret = reprog_msp430bsl5_common(imagefile, slotnr, port, progstate = 1, speed=115200)
	elif core == 1: # bolt
		ret = reprog_msp430bsl5_common(imagefile, slotnr, port, speed=115200)
	elif core == 2: # app
		ret = reprog_msp432(imagefile, slotnr, port, 57600)
	elif core == 3: # sensor
		ret = reprog_msp430bsl5_common(imagefile, slotnr, port, progstate = 1, speed=115200)
		
	set_pin(pin_sig1, 0)
	set_pin(pin_sig2, 0)

	return ret
### END reprog_dpp()

def reprog_msp432(imagefile, slotnr, port, speed):
	usleep = 0.0005
	import msp430.bsl32.uart
	
	set_pin(pin_rst, 1)
	set_pin(pin_prog, 1)
	time.sleep(usleep)

	set_pin(pin_rst, 0)
	
	cmd = ["-p", port, "-e", "-S", "-V","--speed=%d" % speed, "-i", "ihex", "-P", imagefile]
	if debug:
		cmd.append("-vvvvvvvvv")
		cmd.append("--debug")
	bsl =  msp430.bsl32.uart.SerialBSL32Target()
	try:
		bsl.main(cmd)
	except Exception:
		flocklab.tg_reset(usleep)
		return 3
	
	set_pin(pin_prog, 0)
	
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_msp432()

##############################################################################
#
# reprog_stm32l4
#
##############################################################################
def reprog_dpp2lora(imagefile, slotnr):
	port   = '/dev/ttyS1'
	init_pins((pin_prog, pin_rst))
	return reprog_stm32l4(imagefile, slotnr, port)
### END reprog_dpp2lora()

def reprog_stm32l4(imagefile, slotnr, port, speed=115200):
	usleep = 0.0005
	import stm32loader
	
	# BSL entry sequence
	set_pin(pin_rst, 1)
	set_pin(pin_prog, 1)
	time.sleep(usleep)
	set_pin(pin_rst, 0)
	
	# call the bootloader script
	loader = stm32loader.Stm32Loader()
	loader.configuration['data_file'] = imagefile
	loader.configuration['port'] = port
	loader.configuration['baud'] = 115200
	loader.configuration['parity'] = serial.PARITY_EVEN
	loader.configuration['erase'] = True
	loader.configuration['write'] = True
	loader.configuration['verify'] = True
	if debug:
		stm32loader.VERBOSITY = 10
	else:
		stm32loader.VERBOSITY = 0
	try:
		loader.connect()
		if loader.read_device_details() != 0x435:
			print "invalid device ID"
			flocklab.tg_reset(usleep)
			return 2
		loader.perform_commands()
	except Exception:
		flocklab.tg_reset(usleep)
		return 3
	
	set_pin(pin_prog, 0)
	
	# Revert back all config changes:
	subprocess.call(["stty", "-F", port, "-parenb", "iexten", "echoe", "echok", "echoctl", "echoke", "115200"])
	
	# Reset if not forbidden by caller: 
	if noreset:
		flocklab.tg_reset_keep()
	else:
		flocklab.tg_reset(usleep)

	return 0
### END reprog_stm32l4()

##############################################################################
#
# Main
#
##############################################################################
def main(argv):
	
	### Get global variables ###
	global imagefile
	global porttype
	global noreset
	global debug
	
	# Get logger:
	logger = flocklab.get_logger("tg_reprog.py")
	
	core = 0
	
	# Get command line parameters.
	try:								
		opts, args = getopt.getopt(argv, "dhvni:t:p:c:", ["debug", "help", "version", "noreset", "image=", "target=", "port=", "core="])
	except getopt.GetoptError, err:
		logger.error(str(err))
		usage()
		sys.exit(errno.EINVAL)
	for opt, arg in opts:	  
		if opt in ("-h", "--help"):
			usage()
			sys.exit(SUCCESS)
			
		elif opt in ("-d", "--debug"):
			debug = True
			
		elif opt in ("-v", "--version"):
			print version
			sys.exit(SUCCESS)
			
		elif opt in ("-n", "--noreset"):
			noreset = True
			
		elif opt in ("-i", "--image"):
			imagefile = arg
			if not (os.path.exists(imagefile)):
				err = "Error: file %s does not exist" %(str(imagefile))
				logger.error(str(err))
				sys.exit(errno.EINVAL)
				
		elif opt in ("-t", "--target"):
			target = arg
			if not (target in targetlist):
				err = "Error: illegal target %s" %(str(target))
				logger.error(str(err))
				sys.exit(errno.EINVAL)
				
		elif opt in ("-p", "--port"):
			porttype = arg
			if not (porttype in porttypelist):
				err = "Error: illegal port %s" %(str(porttype))
				logger.error(str(err))
				sys.exit(errno.EINVAL)
		
		elif opt in ("-c", "--core"):
			core = int(arg)
				
		else:
			print "Wrong API usage"
			logger.error("Wrong API usage")
			usage()
			sys.exit(errno.EINVAL)
			
	if (imagefile == None) or (target == None):
		print "Wrong API usage"
		logger.error("Wrong API usage")
		usage()
		sys.exit(errno.EINVAL)
	
	# Set default porttypes:
	if not porttype:
		if (target in ('tmote', 'opal', 'iris', 'mica2', 'wismote')):
			porttype = 'usb'
		elif (target in ('tinynode', 'cc430', 'acm2', 'openmote', 'dpp', 'dpp2lora', 'dpp2lorahg')):
			porttype = 'serial' 
	
	# Check port type restrictions for targets:
	if (target in ('tmote', 'opal', 'iris', 'mica2', 'wismote')) and (porttype != 'usb'):
		err = "Error: port type for target %s has to be usb." %target
		logger.error(str(err))
		sys.exit(errno.EINVAL)
	elif (target in ('cc430','acm2', 'openmote', 'dpp', 'dpp2lora')) and (porttype != 'serial'):
		err = "Error: port type for target %s has to be serial." %target
		logger.error(str(err))
		sys.exit(errno.EINVAL)
		
	# Set environment variable needed for programmer: 
	os.environ["PYTHONPATH"] = os.environ.get("PYTHONPATH", "") + ":/media/card/opkg/usr/lib/python2.6/"
	
	# Prepare the target for reprogramming:
	slotnr = flocklab.tg_interface_get()
	if not slotnr:
		err = "No interface active. Please activate one first."
		print err
		logger.error(err)
		sys.exit(errno.EINVAL)
	print "Active interface is target %d"%slotnr
	# Turn on power
	flocklab.tg_pwr_set(slotnr, 1)
	# Find out voltage setting of target:
	tg_volt_state_old = flocklab.tg_volt_get()
	if (tg_volt_state_old < 0):
		tg_volt_state_old = 33
		print "Currently set voltage could not be determined. Defaulting to %1.1f V"%(tg_volt_state_old/10.0)
	else:
		print "Voltage is currently set to: %1.1f V"%(tg_volt_state_old/10.0)
	# set voltage to maximum:
	try:
		flocklab.tg_volt_set(33)
	except IOError:
		logger.error("Could not set voltage to 3.3V")
	# Turn on USB power if needed, otherwise turn it off.
	if porttype == 'usb':
		flocklab.tg_usbpwr_set(slotnr, 1)
		print "Turned on USB power"
		time.sleep(2)
	else:
		flocklab.tg_usbpwr_set(slotnr, 0)
		time.sleep(2)
	
	# Flash the target:
	print "Reprogramming %s..."%target
	if target == 'tmote':
		rs = reprog_tmote_usb(imagefile, slotnr)
	elif target == 'tinynode':
		rs = reprog_tinynode(imagefile, slotnr, porttype)
	elif target == 'opal':
		rs = reprog_opal_usb(imagefile, slotnr)
	elif target == 'iris':
		rs = reprog_iris_usb(imagefile, slotnr)
	elif target == 'mica2':
		rs = reprog_mica2_usb(imagefile, slotnr)
	elif target == 'wismote': 
		rs = reprog_wismote_usb(imagefile, slotnr)
	elif target == 'cc430':
		rs = reprog_cc430(imagefile, slotnr)
	elif target == 'acm2':
		rs = reprog_acm2(imagefile, slotnr)
	elif target == 'openmote':
		rs = reprog_openmote(imagefile, slotnr)
	elif target == 'dpp':
		logger.info("reprog dpp with image %s, slot %d, core %d." % (imagefile, slotnr, core))
		rs = reprog_dpp(imagefile, slotnr, core)
	elif target == 'dpp2lora' or target == 'dpp2lorahg':
		rs = reprog_dpp2lora(imagefile, slotnr)
	
	# Turn off USB power if needed:
	if porttype == 'usb':
		flocklab.tg_usbpwr_set(slotnr, 0)
		
	# Restore old status of target voltage:
	if tg_volt_state_old != 33:
		try:
			flocklab.tg_volt_set(tg_volt_state_old)
		except IOError:
			logger.error("Could not set voltage to %1.1f V"%(tg_volt_state_old/10.0))

	# Return an error if there was one while flashing:
	if (rs != 0):
		logger.error("Image could not be flashed to target. Error %d occurred."%rs)
		sys.exit(errno.EIO)
	else:
		logger.info("Target node flashed successfully.")
		sys.exit(SUCCESS)
### END main()

if __name__ == "__main__":
	main(sys.argv[1:])
