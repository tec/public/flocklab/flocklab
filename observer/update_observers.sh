#!/bin/bash

#obslist="001 002 003 004 006 007 008 010 011 013 014 015 016 017 018 019 020 022 023 024 025 026 027 028 031 032 033"
obslist="029"

for OBS in $obslist
do
        echo "updating observer ${OBS}..."
        #ssh flocklab-observer${OBS} "mount -o remount,rw /"
        #scp testmanagement/tg_reprog.py flocklab-observer${OBS}:/usr/bin/
        #ssh flocklab-observer${OBS} "mount -o remount,ro /"
done
