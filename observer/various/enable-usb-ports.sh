#!/bin/sh

modprobe proc_gpio

USB1_POWER=/proc/gpio/GPIO72
USB2_POWER=/proc/gpio/GPIO66
USB3_POWER=/proc/gpio/GPIO73

ENABLE_USB_POWER='echo GPIO out set'
DISABLE_USB_POWER='echo GPIO in'

$ENABLE_USB_POWER > $USB2_POWER
$ENABLE_USB_POWER > $USB3_POWER

sleep 1

if [ -d '/sys/devices/platform/flockboard' ] || [ -d '/sys/devices/platform/baseboard_old' ]; then
  $ENABLE_USB_POWER > $USB1_POWER
fi
