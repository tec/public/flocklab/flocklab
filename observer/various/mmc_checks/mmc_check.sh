#!/bin/sh

LOGFILENAME="mmcCheck_`date +'%Y%m%d%H%M%S'`.log"
LOGFILE="/tmp/$LOGFILENAME"
LOGFILESTOR="/media/card/log/mmccheck/$LOGFILENAME"
BADBLOCKSOLD="/tmp/badblocks_old"
BADBLOCKSNEW="/tmp/badblocks_new"
DEVICE="/dev/mmcblk0p1"
MOUNT="/media/card"
MAX_RETRIES=5

echo "================== START MMC Check ==============" > $LOGFILE
date >> $LOGFILE
echo "Card to be tested:" >> $LOGFILE
########## SD card info ---v
date=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/date`
name=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/name`
serial=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/serial`
oemid=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/oemid`
manfid=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/manfid`
hwrev=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/hwrev`
fwrev=`cat /sys/devices/platform/pxa2xx-mci.0/mmc_host/mmc0/mmc0:*/fwrev`
echo "$oemid;$manfid;$name;$hwrev;$fwrev;$date;$serial" >> $LOGFILE
########## SD card info ---^
dmesg | grep "mmcblk0" >> $LOGFILE 2>&1

echo "================== Unmount SD card ==============" >> $LOGFILE
RETRY=0
RET=999
if [ `mount | grep $MOUNT | wc -l` -eq 1 ]
then
	while ( [ $RET -ne 0 ] && [ $RETRY -lt $MAX_RETRIES ] ); do
		/etc/init.d/chrony stop >> $LOGFILE 2>&1
		/etc/init.d/syslog stop >> $LOGFILE 2>&1
		umount $MOUNT >> $LOGFILE 2>&1
		RET=$? >> $LOGFILE 2>&1
		RETRY=`expr $RETRY + 1` >> $LOGFILE 2>&1
	done
	if [ $RET -ne 0 ]; then
		echo "Cannot continue because MMC could not be unmounted." >> $LOGFILE
		echo "Cannot continue because MMC could not be unmounted."
		lsof $MOUNT >> $LOGFILE 2>&1
		exit 1
	fi
	echo "Unmounted MMC on try $RETRY." >> $LOGFILE
else
	echo "MMC was not mounted." >> $LOGFILE
	/etc/init.d/chrony stop >> $LOGFILE 2>&1
	/etc/init.d/syslog stop >> $LOGFILE 2>&1
fi
echo "================== Dump badblocks list ==============" >> $LOGFILE
dumpe2fs -b $DEVICE > $BADBLOCKSOLD >> $LOGFILE 2>&1

echo "================== Check for new badblocks ==============" >> $LOGFILE
#badblocks -svw -t 0x00 -b 4096 -c 8192 -i $BADBLOCKSOLD -o $BADBLOCKSNEW $DEVICE >> $LOGFILE  2>&1

echo "================== Recreate filesystem ==============" >> $LOGFILE
mkfs.ext3 -l $BADBLOCKSOLD $DEVICE >> $LOGFILE 2>&1

echo "================== Remove shadow files  ==============" >> $LOGFILE
rm -rf $MOUNT/* >> $LOGFILE 2>&1

echo "================== Mount card  ==============" >> $LOGFILE
mount $DEVICE $MOUNT >> $LOGFILE 2>&1

echo "================== Save log file ==============" >> $LOGFILE
mkdir -p /media/card/log/mmccheck/
cp $LOGFILE $LOGFILESTOR

echo "================== FINISHED ==============" >> $LOGFILE
date >> $LOGFILE

