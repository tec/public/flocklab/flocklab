Installation instructions:
--------------------------

1) cd into this folder

2) Compile using:
	python setup.py build

3) Install using:
	python setup.py install --home=/home/flocklab/testmanagementserver/ext_c_modules/

4) Remove build dir:
	rm -rf build

5) The module is now installed in /home/flocklab/testmanagementserver/ext_c_modules/lib/python/ and can be used in python like this:
	import sys
	sys.path.append('/home/flocklab/testmanagementserver/ext_c_modules/lib/python/')
	import cResultfetcher
