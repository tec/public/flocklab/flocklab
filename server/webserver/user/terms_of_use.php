<?php 
	/*
	 * __author__      = "Christoph Walser <walser@tik.ee.ethz.ch>"
	 * __copyright__   = "Copyright 2010, ETH Zurich, Switzerland, Christoph Walser"
	 * __license__     = "GPL"
	 * __version__     = "$Revision$"
	 * __date__        = "$Date$"
	 * __id__          = "$Id: user_register.php 2066 2012-09-21 13:59:54Z walserc $"
	 * __source__      = "$URL$" 
	 */
?>
<?php require_once('include/libflocklab.php');?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/flocklab.css">
	<link rel="shortcut icon" href="pics/icons/favicon.ico" type="image/x-ico; charset=binary">
	<link rel="icon" href="pics/icons/favicon.ico" type="image/x-ico; charset=binary">

	<title>FlockLab - Register Account</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="AUTHOR" content="ETH Zurich, Christoph Walser, CH-8092 Zurich, Switzerland">
	<meta name="COPYRIGHT" content="ETH Zurich, Switzerland">
	<meta name="LANGUAGE" content="English">
	<meta name="ROBOTS" content="noindex, nofollow">
	<meta name="DATE" content="2011-2012">	
</head>
<body>
	<div id="container" class="container">
		<div id="header" class="header">
			<a href="http://www.flocklab.ethz.ch"><img alt="FlockLab" src="pics/flocklab_eth_logo.png"></a>
		</div> <!-- END header -->
		<div id="content" class="content">
			<h1>Terms of Use - FlockLab</h1>
			By signing up for a FlockLab user account, you accept the following terms of use:
			<ol>
				<li><b>FlockLab is provided as it is:</b> We cannot give any guarantee about the correctness of its functionalities.</li>
				<li><b>Availability:</b> Members of our institute get priority access to FlockLab. We cannot give any guarantee for the availability to external users.</li>
				<li><b>Acknowledgement:</b> The usage of FlockLab must be properly acknowledged in all publications produced with the help of FlockLab.</li>
				<li><b>Cancelation:</b> A FlockLab user account can be deactivated without notice by FlockLab administrators.</li>
			</ol>
			These terms become effective 2012/09/21.
		</div> <!-- END content -->
		<div style="clear:both"></div>
	</div> <!-- END container -->
</body>
</html>
