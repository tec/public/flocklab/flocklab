import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by andreas on 13.10.14.
 */
public class CsvPowerProfilingLoader extends FlocklabCsvLoader {
  private final Map<Integer, NavigableMap<Long, Double>> sampleMaps;
  private final Map<Integer, CurrentTrace> traces;

  public CsvPowerProfilingLoader(Reader reader) {
    super(reader);
    sampleMaps = new HashMap<Integer, NavigableMap<Long, Double>>();
    traces = new HashMap<Integer, CurrentTrace>();
  }

  private NavigableMap<Long, Double> getSampleMap(int nodeId) {
    NavigableMap<Long, Double> map = sampleMaps.get(nodeId);
    if (map == null) {
      sampleMaps.put(nodeId, map = new TreeMap<Long, Double>());
      traces.put(nodeId, new MapCurrentTrace(nodeId, map));
    }
    return map;
  }

  @Override
  protected void processRow(long time, int observerID, int nodeID, String rest) {
    getSampleMap(nodeID).put(time, Double.parseDouble(rest.trim()));
  }

  public Map<Integer, CurrentTrace> getTraces() {
    return traces;
  }
}
