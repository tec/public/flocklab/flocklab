import java.io.*;
import java.math.BigInteger;

public abstract class FlocklabCsvLoader {
  private final BufferedReader fileReader;
  private long startTime;
  private long endTime;
  private int nSkipColumns;

  protected FlocklabCsvLoader(Reader reader) {
    this(reader, 0);
  }

  protected FlocklabCsvLoader(Reader reader, int nSkipColumns) {
    assert nSkipColumns >= 0;
    assert reader != null;

    this.fileReader = new BufferedReader(reader);
    this.nSkipColumns = nSkipColumns;

    this.startTime = Long.MAX_VALUE;
    this.endTime = Long.MIN_VALUE;
  }

  public final void load() throws IOException {
    String line;

    while ((line = fileReader.readLine()) != null) {
      if (line.startsWith("#"))
        continue;

      String [] parts = line.split(",", nSkipColumns + 4);

      // time
      String[] timeParts = parts[nSkipColumns].trim().split("\\.");
      long seconds = Long.parseLong(timeParts[0]);
      int nDecimalDigits = timeParts[1].length();
      assert nDecimalDigits <= 8 : "only a resolution down to 10ns is supported, not below";
      int shift = BigInteger.valueOf(10).pow(8 - nDecimalDigits).intValue();
      int tensOfNanoseconds = Integer.parseInt(timeParts[1]) * shift;
      long time =  seconds * 100000000L + tensOfNanoseconds;

      // observer id, node id
      int observerID = Integer.parseInt(parts[nSkipColumns + 1].trim());
      int nodeID = Integer.parseInt(parts[nSkipColumns + 2].trim());

      startTime = Math.min(startTime, time);
      endTime = Math.max(startTime, time);
      processRow(time, observerID, nodeID, parts[nSkipColumns + 3]);
    }
  }

  public final long getStartTime() {
    return startTime;
  }

  public final long getEndTime() {
    return endTime;
  }

  protected abstract void processRow(long time, int observerID, int nodeID, String rest);
}
