import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andreas on 08.10.14.
 */

/**
 * Class holding references to all the measurement files of one flocklab measurement.
 */
public class FlocklabMeasurements implements IFlockLabMeasurementFiles {
  private File gpioTrace;
  private File serial;
  private File gpioActuation;
  private File powerProfilingCsv;
  private File powerProfilingBinaryDir;

  public FlocklabMeasurements(File folder) {
    if (folder == null)
      throw new IllegalArgumentException("Folder can't be null");
    if (!folder.isDirectory())
      throw new IllegalArgumentException("Folder must be a directory");

    serial = new File(folder, "serial.csv");
    if (!serial.exists() || !serial.isFile())
      serial = null;

    gpioTrace = new File(folder, "gpiotracing.csv");
    if (!gpioTrace.exists() || !gpioTrace.isFile())
      gpioTrace = null;

    gpioActuation = new File(folder, "gpioactuation.csv");
    if (!gpioActuation.exists() || !gpioActuation.isFile())
      gpioActuation = null;

    powerProfilingCsv = new File(folder, "powerprofiling.csv");
    if (!powerProfilingCsv.exists() || !powerProfilingCsv.isFile())
      powerProfilingCsv = null;

    powerProfilingBinaryDir = new File(folder, "powerprofiling");
    if (!powerProfilingBinaryDir.exists() || !powerProfilingBinaryDir.isDirectory())
      powerProfilingBinaryDir = null;
  }

  public boolean hasSerialOut() {
    return serial != null;
  }
  @Override
  public File getSerialOutputFile() {
    return serial;
  }
  public boolean hasGpioTrace() {
    return gpioTrace != null;
  }
  @Override
  public File getGpioTraceFile() {
    return gpioTrace;
  }
  public boolean hasGpioActuation() {
    return gpioActuation != null;
  }
  @Override
  public File getGpioActuationFile() {
    return gpioActuation;
  }
  public boolean hasPowerProfilingCsv() {
    return powerProfilingCsv != null;
  }
  @Override
  public File getPowerProfilingFile() {
    return powerProfilingCsv;
  }
  public boolean hasPowerProfilingBinary() {
    return powerProfilingBinaryDir != null;
  }
  public File getBinaryPowerProfilingDir() {
    return powerProfilingBinaryDir;
  }
  public Set<File> getPreprocessedPowerProfilingFiles() {
    Set<File> files = new HashSet<File>();
    if (powerProfilingBinaryDir != null)
      files.addAll(Arrays.asList(powerProfilingBinaryDir.listFiles(new FileFilter() {
        @Override
        public boolean accept(File file) {
          if (!file.isFile())
            return false;
          try {
            Integer.parseInt(file.getName());
          } catch (NumberFormatException e) {
            return false;
          }
          return true;
        }
      })));
    return files;
  }
}
